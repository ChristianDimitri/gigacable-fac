Imports System.Data.SqlClient

Public Class FrmMotivoCancelacionFactura

    Private Sub FrmMotivoCancelacionFactura_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If eReImprimirF = 0 Then
            Me.Text = "Motivo de Cancelaci�n de la Factura"
            Me.CMBLabel1.Text = "�Por qu� Motivo Cancelar�s la Factura?"
            Me.MUESTRAMOTIVOSTableAdapter.Connection = CON
            Me.MUESTRAMOTIVOSTableAdapter.Fill(Me.DataSetEdgar.MUESTRAMOTIVOS, eReImprimirF)
        Else
            Me.Text = "Motivo de ReImpresi�n de la Factura"
            Me.CMBLabel1.Text = "�Por qu� Motivo ReImprimir�s la Factura?"
            Me.MUESTRAMOTIVOSTableAdapter.Connection = CON
            Me.MUESTRAMOTIVOSTableAdapter.Fill(Me.DataSetEdgar.MUESTRAMOTIVOS, eReImprimirF)
        End If
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If eReImprimirF = 0 Then
            eEntra = True
            eCancelacionRE = True
        Else
            eEntraReImprimir = True
        End If
        Dim Cancelada As Boolean = False

        Cancelada = Valida_Factura_Cancelada()

        If Cancelada = False Then
            Try
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.GuardaMotivosTableAdapter.Connection = CON
                Me.GuardaMotivosTableAdapter.Fill(Me.DataSetEdgar.GuardaMotivos, eCveFactura, Me.Clv_MotivoLabel1.Text, LocLoginUsuario)
                CON.Close()
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else

            MsgBox("La factura ya esta cancelada", MsgBoxStyle.Information)

        End If
        Me.Close()
    End Sub

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Me.GuardaMotivosTableAdapter.Fill(Me.NewsoftvDataSet.GuardaMotivos, New System.Nullable(Of Long)(CType(Clv_FacturaToolStripTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Clv_MotivoToolStripTextBox.Text, Long)))
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub


    Public Function Valida_Factura_Cancelada() As Boolean

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Factura", SqlDbType.Int, factura)
        BaseII.CreateMyParameter("@Cancelada", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("Valida_Factura_Cancelada")
        Return BaseII.dicoPar("@Cancelada")


    End Function
End Class