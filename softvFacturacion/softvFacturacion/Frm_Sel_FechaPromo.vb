﻿Public Class Frm_Sel_FechaPromo

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBox1.Text = "" Then
            MessageBox.Show("Debe seleccionar la promoción", "Listado de Promociones", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Me.DialogResult = Windows.Forms.DialogResult.OK
            locGlo_Fechaini = Me.DateTimePicker1.Text
            locGlo_Fechafin = Me.DateTimePicker2.Text
            If ComboBox1.Text = "6 x 7" Then
                promocion = 1
            End If
            If ComboBox1.Text = "12 x 14" Then
                promocion = 2
            End If
            If ComboBox1.Text = "6 x 7 y 12 x 14" Then
                promocion = 3
            End If
        End If
    End Sub

    Private Sub Frm_Sel_Fecha_1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today

    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub
End Class