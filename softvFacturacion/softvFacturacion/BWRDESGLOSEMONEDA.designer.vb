<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BwrDESGLOSEMONEDA
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim FechaLabel As System.Windows.Forms.Label
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim NomCajeraLabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Button10 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.CMBTextBox3 = New System.Windows.Forms.TextBox
        Me.BUSCADESSGLOCEMONEDABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1
        Me.FechaLabel1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.CMBConsecutivoTextBox = New System.Windows.Forms.TextBox
        Me.CMBNomCajeraTextBox = New System.Windows.Forms.TextBox
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.ImporteTextBox = New System.Windows.Forms.TextBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ConsecutivoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NomCajeraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BUSCADESSGLOCEMONEDATableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCADESSGLOCEMONEDATableAdapter
        Me.CONDESGLOSEMONEDABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDESGLOSEMONEDATableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.CONDESGLOSEMONEDATableAdapter
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar
        Me.DameCveCajeraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameCveCajeraTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DameCveCajeraTableAdapter
        FechaLabel = New System.Windows.Forms.Label
        ConsecutivoLabel = New System.Windows.Forms.Label
        NomCajeraLabel = New System.Windows.Forms.Label
        ImporteLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BUSCADESSGLOCEMONEDABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDESGLOSEMONEDABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameCveCajeraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.ForeColor = System.Drawing.Color.White
        FechaLabel.Location = New System.Drawing.Point(12, 74)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(46, 13)
        FechaLabel.TabIndex = 55
        FechaLabel.Text = "Fecha:"
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.BackColor = System.Drawing.Color.DarkOrange
        ConsecutivoLabel.ForeColor = System.Drawing.Color.White
        ConsecutivoLabel.Location = New System.Drawing.Point(10, 52)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(81, 13)
        ConsecutivoLabel.TabIndex = 49
        ConsecutivoLabel.Text = "Consecutivo:"
        '
        'NomCajeraLabel
        '
        NomCajeraLabel.AutoSize = True
        NomCajeraLabel.BackColor = System.Drawing.Color.DarkOrange
        NomCajeraLabel.ForeColor = System.Drawing.Color.White
        NomCajeraLabel.Location = New System.Drawing.Point(10, 104)
        NomCajeraLabel.Name = "NomCajeraLabel"
        NomCajeraLabel.Size = New System.Drawing.Size(62, 13)
        NomCajeraLabel.TabIndex = 51
        NomCajeraLabel.Text = "Cajero(a):"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.BackColor = System.Drawing.Color.DarkOrange
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(10, 156)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(40, 13)
        ImporteLabel.TabIndex = 53
        ImporteLabel.Text = "Total:"
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.Orange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(839, 656)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(136, 36)
        Me.Button10.TabIndex = 6
        Me.Button10.Text = "&SALIR"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.Orange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(839, 600)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(136, 36)
        Me.Button9.TabIndex = 5
        Me.Button9.Text = "&IMPRIMIR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(868, 120)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 4
        Me.Button8.Text = "&MODIFICAR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.Orange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(868, 69)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 3
        Me.Button7.Text = "&CONSULTAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(868, 21)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 2
        Me.Button6.Text = "&NUEVO"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(806, 711)
        Me.Panel1.TabIndex = 38
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ImporteTextBox)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(806, 711)
        Me.SplitContainer1.SplitterDistance = 313
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button2.Location = New System.Drawing.Point(29, 146)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(152, 120)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 19)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(26, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 15)
        Me.Label6.TabIndex = 53
        Me.Label6.Text = "Fecha:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(29, 120)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(106, 20)
        Me.TextBox2.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button1.Location = New System.Drawing.Point(29, 331)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.TabStop = False
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(26, 289)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 15)
        Me.Label5.TabIndex = 50
        Me.Label5.Text = "Cajero(a):"
        Me.Label5.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(29, 308)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(159, 20)
        Me.TextBox1.TabIndex = 3
        Me.TextBox1.TabStop = False
        Me.TextBox1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Me.CMBTextBox3)
        Me.Panel2.Controls.Add(FechaLabel)
        Me.Panel2.Controls.Add(Me.FechaLabel1)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(ConsecutivoLabel)
        Me.Panel2.Controls.Add(Me.CMBConsecutivoTextBox)
        Me.Panel2.Controls.Add(NomCajeraLabel)
        Me.Panel2.Controls.Add(Me.CMBNomCajeraTextBox)
        Me.Panel2.Controls.Add(ImporteLabel)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(3, 455)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(360, 253)
        Me.Panel2.TabIndex = 0
        '
        'CMBTextBox3
        '
        Me.CMBTextBox3.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox3.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADESSGLOCEMONEDABindingSource, "Total", True))
        Me.CMBTextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTextBox3.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox3.Location = New System.Drawing.Point(56, 157)
        Me.CMBTextBox3.Name = "CMBTextBox3"
        Me.CMBTextBox3.ReadOnly = True
        Me.CMBTextBox3.Size = New System.Drawing.Size(162, 13)
        Me.CMBTextBox3.TabIndex = 58
        Me.CMBTextBox3.TabStop = False
        '
        'BUSCADESSGLOCEMONEDABindingSource
        '
        Me.BUSCADESSGLOCEMONEDABindingSource.DataMember = "BUSCADESSGLOCEMONEDA"
        Me.BUSCADESSGLOCEMONEDABindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FechaLabel1
        '
        Me.FechaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADESSGLOCEMONEDABindingSource, "Fecha", True))
        Me.FechaLabel1.ForeColor = System.Drawing.Color.White
        Me.FechaLabel1.Location = New System.Drawing.Point(70, 74)
        Me.FechaLabel1.Name = "FechaLabel1"
        Me.FechaLabel1.Size = New System.Drawing.Size(142, 13)
        Me.FechaLabel1.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.DarkOrange
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(23, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(150, 16)
        Me.Label7.TabIndex = 56
        Me.Label7.Text = "Datos de la Entrega:"
        '
        'CMBConsecutivoTextBox
        '
        Me.CMBConsecutivoTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBConsecutivoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADESSGLOCEMONEDABindingSource, "Consecutivo", True))
        Me.CMBConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBConsecutivoTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBConsecutivoTextBox.Location = New System.Drawing.Point(96, 52)
        Me.CMBConsecutivoTextBox.Name = "CMBConsecutivoTextBox"
        Me.CMBConsecutivoTextBox.ReadOnly = True
        Me.CMBConsecutivoTextBox.Size = New System.Drawing.Size(162, 13)
        Me.CMBConsecutivoTextBox.TabIndex = 50
        Me.CMBConsecutivoTextBox.TabStop = False
        '
        'CMBNomCajeraTextBox
        '
        Me.CMBNomCajeraTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNomCajeraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNomCajeraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCADESSGLOCEMONEDABindingSource, "NomCajera", True))
        Me.CMBNomCajeraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNomCajeraTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBNomCajeraTextBox.Location = New System.Drawing.Point(73, 104)
        Me.CMBNomCajeraTextBox.Name = "CMBNomCajeraTextBox"
        Me.CMBNomCajeraTextBox.ReadOnly = True
        Me.CMBNomCajeraTextBox.Size = New System.Drawing.Size(162, 13)
        Me.CMBNomCajeraTextBox.TabIndex = 52
        Me.CMBNomCajeraTextBox.TabStop = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(19, 17)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(157, 36)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar El Desglose" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " de Moneda por:"
        '
        'ImporteTextBox
        '
        Me.ImporteTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.ImporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ImporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteTextBox.ForeColor = System.Drawing.Color.White
        Me.ImporteTextBox.Location = New System.Drawing.Point(99, 152)
        Me.ImporteTextBox.Name = "ImporteTextBox"
        Me.ImporteTextBox.Size = New System.Drawing.Size(10, 13)
        Me.ImporteTextBox.TabIndex = 54
        Me.ImporteTextBox.TabStop = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ConsecutivoDataGridViewTextBoxColumn, Me.FechaDataGridViewTextBoxColumn, Me.NomCajeraDataGridViewTextBoxColumn, Me.TotalDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.BUSCADESSGLOCEMONEDABindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(459, 714)
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'ConsecutivoDataGridViewTextBoxColumn
        '
        Me.ConsecutivoDataGridViewTextBoxColumn.DataPropertyName = "Consecutivo"
        Me.ConsecutivoDataGridViewTextBoxColumn.HeaderText = "Consecutivo"
        Me.ConsecutivoDataGridViewTextBoxColumn.Name = "ConsecutivoDataGridViewTextBoxColumn"
        Me.ConsecutivoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        Me.FechaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NomCajeraDataGridViewTextBoxColumn
        '
        Me.NomCajeraDataGridViewTextBoxColumn.DataPropertyName = "NomCajera"
        Me.NomCajeraDataGridViewTextBoxColumn.HeaderText = "NomCajera"
        Me.NomCajeraDataGridViewTextBoxColumn.Name = "NomCajeraDataGridViewTextBoxColumn"
        Me.NomCajeraDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        Me.TotalDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BUSCADESSGLOCEMONEDATableAdapter
        '
        Me.BUSCADESSGLOCEMONEDATableAdapter.ClearBeforeFill = True
        '
        'CONDESGLOSEMONEDABindingSource
        '
        Me.CONDESGLOSEMONEDABindingSource.DataMember = "CONDESGLOSEMONEDA"
        Me.CONDESGLOSEMONEDABindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'CONDESGLOSEMONEDATableAdapter
        '
        Me.CONDESGLOSEMONEDATableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameCveCajeraBindingSource
        '
        Me.DameCveCajeraBindingSource.DataMember = "DameCveCajera"
        Me.DameCveCajeraBindingSource.DataSource = Me.DataSetEdgar
        '
        'DameCveCajeraTableAdapter
        '
        Me.DameCveCajeraTableAdapter.ClearBeforeFill = True
        '
        'BwrDESGLOSEMONEDA
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "BwrDESGLOSEMONEDA"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desglose de Moneda"
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BUSCADESSGLOCEMONEDABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDESGLOSEMONEDABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameCveCajeraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents BUSCADESSGLOCEMONEDABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCADESSGLOCEMONEDATableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCADESSGLOCEMONEDATableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CONDESGLOSEMONEDABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDESGLOSEMONEDATableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.CONDESGLOSEMONEDATableAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ConsecutivoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomCajeraDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents CMBConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBNomCajeraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBTextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents DameCveCajeraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameCveCajeraTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DameCveCajeraTableAdapter
End Class
