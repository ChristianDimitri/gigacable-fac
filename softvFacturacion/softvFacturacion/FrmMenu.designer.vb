<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMenu
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBNombreLabel As System.Windows.Forms.Label
        Dim CMBCiudadLabel As System.Windows.Forms.Label
        Dim CMBLabelciudad1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMenu))
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.FacturacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturacionToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadosYAfectacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobroDeContratosMaestrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotasDeCréditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.CancelaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntregasParcialesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMonedaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArqueoDeCajasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelaciònDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReImpresionDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FACTURAGLOBALToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelaciónDeFacturasRedEfectivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobroEquivocadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesSISTEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PerfilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortesDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasGlobalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeEntragasParcialesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeBonificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeFacturasCanceadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeFacturasReimpresasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeNotasDeCréditoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelaciónDeClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeMensualidadesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DesgloceDeContratacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturasConCargoAutomaticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteGlobalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualidadesAdelantadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NúmeroDeBonificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobroErroneoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagosDiferidosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PromocionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadosDeCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TelefoniaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteDeCobranzaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePagosProporcionalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeBonificacionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeDescuentosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CancelacionDeFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.MUESTRAIMAGENBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DameTipoUsusarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameTipoUsusarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameTipoUsusarioTableAdapter()
        Me.ALTASMENUSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ALTASMENUSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.ALTASMENUSTableAdapter()
        Me.MUESTRAIMAGENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameEspecifTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameEspecifTableAdapter()
        Me.MUESTRAIMAGENTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAIMAGENTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.DameClv_Session_UsuariosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameClv_Session_UsuariosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.DameClv_Session_UsuariosTableAdapter()
        Me.CMBLabelciudad = New System.Windows.Forms.Label()
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.DamePermisosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamePermisosTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DamePermisosTableAdapter()
        Me.CMBLabelNombreSistema = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        CMBNombreLabel = New System.Windows.Forms.Label()
        CMBCiudadLabel = New System.Windows.Forms.Label()
        CMBLabelciudad1 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameClv_Session_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBNombreLabel
        '
        CMBNombreLabel.AutoSize = True
        CMBNombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBNombreLabel.ForeColor = System.Drawing.Color.Gray
        CMBNombreLabel.Location = New System.Drawing.Point(9, 670)
        CMBNombreLabel.Name = "CMBNombreLabel"
        CMBNombreLabel.Size = New System.Drawing.Size(90, 20)
        CMBNombreLabel.TabIndex = 4
        CMBNombreLabel.Text = "Empresa :"
        '
        'CMBCiudadLabel
        '
        CMBCiudadLabel.AutoSize = True
        CMBCiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBCiudadLabel.ForeColor = System.Drawing.Color.Gray
        CMBCiudadLabel.Location = New System.Drawing.Point(36, 709)
        CMBCiudadLabel.Name = "CMBCiudadLabel"
        CMBCiudadLabel.Size = New System.Drawing.Size(63, 20)
        CMBCiudadLabel.TabIndex = 6
        CMBCiudadLabel.Text = "Plaza :"
        '
        'CMBLabelciudad1
        '
        CMBLabelciudad1.AutoSize = True
        CMBLabelciudad1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBLabelciudad1.ForeColor = System.Drawing.Color.Gray
        CMBLabelciudad1.Location = New System.Drawing.Point(724, 709)
        CMBLabelciudad1.Name = "CMBLabelciudad1"
        CMBLabelciudad1.Size = New System.Drawing.Size(75, 20)
        CMBLabelciudad1.TabIndex = 10
        CMBLabelciudad1.Text = "Ciudad :"
        CMBLabelciudad1.Visible = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(137, 26)
        '
        'FacturacionToolStripMenuItem
        '
        Me.FacturacionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajasToolStripMenuItem1, Me.VentasToolStripMenuItem1})
        Me.FacturacionToolStripMenuItem.Name = "FacturacionToolStripMenuItem"
        Me.FacturacionToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.FacturacionToolStripMenuItem.Text = "Facturacion"
        '
        'CajasToolStripMenuItem1
        '
        Me.CajasToolStripMenuItem1.Name = "CajasToolStripMenuItem1"
        Me.CajasToolStripMenuItem1.Size = New System.Drawing.Size(109, 22)
        Me.CajasToolStripMenuItem1.Text = "Cajas"
        '
        'VentasToolStripMenuItem1
        '
        Me.VentasToolStripMenuItem1.Name = "VentasToolStripMenuItem1"
        Me.VentasToolStripMenuItem1.Size = New System.Drawing.Size(109, 22)
        Me.VentasToolStripMenuItem1.Text = "Ventas"
        '
        'FacturacionToolStripMenuItem1
        '
        Me.FacturacionToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajasToolStripMenuItem2, Me.VentasToolStripMenuItem2, Me.CToolStripMenuItem, Me.CobroDeContratosMaestrosToolStripMenuItem, Me.NotasDeCréditoToolStripMenuItem})
        Me.FacturacionToolStripMenuItem1.ForeColor = System.Drawing.Color.Black
        Me.FacturacionToolStripMenuItem1.Name = "FacturacionToolStripMenuItem1"
        Me.FacturacionToolStripMenuItem1.Size = New System.Drawing.Size(108, 22)
        Me.FacturacionToolStripMenuItem1.Text = "&Facturación"
        '
        'CajasToolStripMenuItem2
        '
        Me.CajasToolStripMenuItem2.Name = "CajasToolStripMenuItem2"
        Me.CajasToolStripMenuItem2.Size = New System.Drawing.Size(292, 22)
        Me.CajasToolStripMenuItem2.Text = "Cajas"
        '
        'VentasToolStripMenuItem2
        '
        Me.VentasToolStripMenuItem2.Name = "VentasToolStripMenuItem2"
        Me.VentasToolStripMenuItem2.Size = New System.Drawing.Size(292, 22)
        Me.VentasToolStripMenuItem2.Text = "Ventas"
        '
        'CToolStripMenuItem
        '
        Me.CToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadosYAfectacionesToolStripMenuItem})
        Me.CToolStripMenuItem.Name = "CToolStripMenuItem"
        Me.CToolStripMenuItem.Size = New System.Drawing.Size(292, 22)
        Me.CToolStripMenuItem.Text = "&Cargos Automaticos"
        '
        'ListadosYAfectacionesToolStripMenuItem
        '
        Me.ListadosYAfectacionesToolStripMenuItem.Name = "ListadosYAfectacionesToolStripMenuItem"
        Me.ListadosYAfectacionesToolStripMenuItem.Size = New System.Drawing.Size(252, 22)
        Me.ListadosYAfectacionesToolStripMenuItem.Text = "Listados y Afectaciones"
        '
        'CobroDeContratosMaestrosToolStripMenuItem
        '
        Me.CobroDeContratosMaestrosToolStripMenuItem.Name = "CobroDeContratosMaestrosToolStripMenuItem"
        Me.CobroDeContratosMaestrosToolStripMenuItem.Size = New System.Drawing.Size(292, 22)
        Me.CobroDeContratosMaestrosToolStripMenuItem.Text = "Cobro de Contratos Maestros"
        '
        'NotasDeCréditoToolStripMenuItem
        '
        Me.NotasDeCréditoToolStripMenuItem.Name = "NotasDeCréditoToolStripMenuItem"
        Me.NotasDeCréditoToolStripMenuItem.Size = New System.Drawing.Size(292, 22)
        Me.NotasDeCréditoToolStripMenuItem.Text = "Notas De Crédito"
        Me.NotasDeCréditoToolStripMenuItem.Visible = False
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(56, 22)
        Me.SalirToolStripMenuItem.Text = "&Salir"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Orange
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturacionToolStripMenuItem1, Me.CancelaciónToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1016, 26)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.TabStop = True
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'CancelaciónToolStripMenuItem
        '
        Me.CancelaciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntregasParcialesToolStripMenuItem1, Me.DesgloceDeMonedaToolStripMenuItem, Me.ArqueoDeCajasToolStripMenuItem1, Me.CancelaciònDeFacturasToolStripMenuItem, Me.ReImpresionDeFacturasToolStripMenuItem, Me.FACTURAGLOBALToolStripMenuItem, Me.RecepciónDelArchivoDeOxxoToolStripMenuItem, Me.PolizasToolStripMenuItem, Me.ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem, Me.CancelaciónDeFacturasRedEfectivaToolStripMenuItem, Me.CobroEquivocadoToolStripMenuItem, Me.GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem, Me.PerfilesSISTEToolStripMenuItem, Me.PerfilesToolStripMenuItem})
        Me.CancelaciónToolStripMenuItem.Name = "CancelaciónToolStripMenuItem"
        Me.CancelaciónToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.CancelaciónToolStripMenuItem.Text = "&Procesos"
        '
        'EntregasParcialesToolStripMenuItem1
        '
        Me.EntregasParcialesToolStripMenuItem1.Name = "EntregasParcialesToolStripMenuItem1"
        Me.EntregasParcialesToolStripMenuItem1.Size = New System.Drawing.Size(419, 22)
        Me.EntregasParcialesToolStripMenuItem1.Text = "Entregas Parciales"
        '
        'DesgloceDeMonedaToolStripMenuItem
        '
        Me.DesgloceDeMonedaToolStripMenuItem.Name = "DesgloceDeMonedaToolStripMenuItem"
        Me.DesgloceDeMonedaToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.DesgloceDeMonedaToolStripMenuItem.Text = "Desgloce de Moneda"
        '
        'ArqueoDeCajasToolStripMenuItem1
        '
        Me.ArqueoDeCajasToolStripMenuItem1.Name = "ArqueoDeCajasToolStripMenuItem1"
        Me.ArqueoDeCajasToolStripMenuItem1.Size = New System.Drawing.Size(419, 22)
        Me.ArqueoDeCajasToolStripMenuItem1.Text = "Arqueo de Cajas"
        '
        'CancelaciònDeFacturasToolStripMenuItem
        '
        Me.CancelaciònDeFacturasToolStripMenuItem.Name = "CancelaciònDeFacturasToolStripMenuItem"
        Me.CancelaciònDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.CancelaciònDeFacturasToolStripMenuItem.Text = "Cancelación de Facturas"
        '
        'ReImpresionDeFacturasToolStripMenuItem
        '
        Me.ReImpresionDeFacturasToolStripMenuItem.Name = "ReImpresionDeFacturasToolStripMenuItem"
        Me.ReImpresionDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.ReImpresionDeFacturasToolStripMenuItem.Text = "ReImpresion de Facturas"
        '
        'FACTURAGLOBALToolStripMenuItem
        '
        Me.FACTURAGLOBALToolStripMenuItem.Name = "FACTURAGLOBALToolStripMenuItem"
        Me.FACTURAGLOBALToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.FACTURAGLOBALToolStripMenuItem.Text = "Factura Global"
        '
        'RecepciónDelArchivoDeOxxoToolStripMenuItem
        '
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Name = "RecepciónDelArchivoDeOxxoToolStripMenuItem"
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.RecepciónDelArchivoDeOxxoToolStripMenuItem.Text = "Recepción del Archivo de Oxxo"
        '
        'PolizasToolStripMenuItem
        '
        Me.PolizasToolStripMenuItem.Name = "PolizasToolStripMenuItem"
        Me.PolizasToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.PolizasToolStripMenuItem.Text = "Polizas"
        Me.PolizasToolStripMenuItem.Visible = False
        '
        'ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem
        '
        Me.ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem.Name = "ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem"
        Me.ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem.Text = "Conciliación Bancaria De Bancomer"
        '
        'CancelaciónDeFacturasRedEfectivaToolStripMenuItem
        '
        Me.CancelaciónDeFacturasRedEfectivaToolStripMenuItem.Name = "CancelaciónDeFacturasRedEfectivaToolStripMenuItem"
        Me.CancelaciónDeFacturasRedEfectivaToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.CancelaciónDeFacturasRedEfectivaToolStripMenuItem.Text = "Cancelación de Facturas (Red Efectiva)"
        '
        'CobroEquivocadoToolStripMenuItem
        '
        Me.CobroEquivocadoToolStripMenuItem.Name = "CobroEquivocadoToolStripMenuItem"
        Me.CobroEquivocadoToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.CobroEquivocadoToolStripMenuItem.Text = "Corrección de Pagos Equivocados"
        '
        'GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem
        '
        Me.GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem.Name = "GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem"
        Me.GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem.Text = "Generación de Facturas Fiscales (FacturaNet)"
        '
        'PerfilesSISTEToolStripMenuItem
        '
        Me.PerfilesSISTEToolStripMenuItem.Name = "PerfilesSISTEToolStripMenuItem"
        Me.PerfilesSISTEToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.PerfilesSISTEToolStripMenuItem.Text = "Perfiles (SISTE)"
        '
        'PerfilesToolStripMenuItem
        '
        Me.PerfilesToolStripMenuItem.Name = "PerfilesToolStripMenuItem"
        Me.PerfilesToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.PerfilesToolStripMenuItem.Text = "Perfiles"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CortesDeFacturasToolStripMenuItem, Me.FacturasGlobalesToolStripMenuItem, Me.ResumenGeneralFacturaGlobalToolStripMenuItem, Me.ListadoDeEntragasParcialesToolStripMenuItem, Me.RelaciónDeIngresosPorConceptosToolStripMenuItem, Me.ListadoDeBonificacionesToolStripMenuItem, Me.ListadoDeFacturasCanceadasToolStripMenuItem, Me.ListadoDeFacturasReimpresasToolStripMenuItem, Me.ListadoDeNotasDeCréditoToolStripMenuItem, Me.RelaciónDeClientesToolStripMenuItem, Me.DesgloceDeMensualidadesToolStripMenuItem, Me.DesgloceDeContratacionesToolStripMenuItem, Me.FacturasConCargoAutomaticoToolStripMenuItem, Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem, Me.ReporteGlobalToolStripMenuItem, Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem, Me.MensualidadesAdelantadasToolStripMenuItem, Me.IngresosDeClientesPorUnMontoToolStripMenuItem, Me.NúmeroDeBonificacionesToolStripMenuItem, Me.CobroErroneoToolStripMenuItem, Me.PagosDiferidosToolStripMenuItem, Me.PromocionesToolStripMenuItem, Me.EstadosDeCuentaToolStripMenuItem, Me.TelefoniaToolStripMenuItem, Me.ListadoDePagosProporcionalesToolStripMenuItem, Me.ListadoDeBonificacionesToolStripMenuItem1, Me.ListadoDeDescuentosToolStripMenuItem, Me.ToolStripMenuItem1})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(88, 22)
        Me.ReportesToolStripMenuItem.Text = "&Reportes"
        '
        'CortesDeFacturasToolStripMenuItem
        '
        Me.CortesDeFacturasToolStripMenuItem.Name = "CortesDeFacturasToolStripMenuItem"
        Me.CortesDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.CortesDeFacturasToolStripMenuItem.Text = "Cortes de Facturas"
        '
        'FacturasGlobalesToolStripMenuItem
        '
        Me.FacturasGlobalesToolStripMenuItem.Name = "FacturasGlobalesToolStripMenuItem"
        Me.FacturasGlobalesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.FacturasGlobalesToolStripMenuItem.Text = "Facturas Globales"
        '
        'ResumenGeneralFacturaGlobalToolStripMenuItem
        '
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Name = "ResumenGeneralFacturaGlobalToolStripMenuItem"
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ResumenGeneralFacturaGlobalToolStripMenuItem.Text = "Resumen General  Factura Global"
        '
        'ListadoDeEntragasParcialesToolStripMenuItem
        '
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Name = "ListadoDeEntragasParcialesToolStripMenuItem"
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeEntragasParcialesToolStripMenuItem.Text = "Listado de Entregas Parciales"
        '
        'RelaciónDeIngresosPorConceptosToolStripMenuItem
        '
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Name = "RelaciónDeIngresosPorConceptosToolStripMenuItem"
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.RelaciónDeIngresosPorConceptosToolStripMenuItem.Text = "Relación de Ingresos por Conceptos"
        '
        'ListadoDeBonificacionesToolStripMenuItem
        '
        Me.ListadoDeBonificacionesToolStripMenuItem.Name = "ListadoDeBonificacionesToolStripMenuItem"
        Me.ListadoDeBonificacionesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeBonificacionesToolStripMenuItem.Text = "Listado de Bonificaciones"
        '
        'ListadoDeFacturasCanceadasToolStripMenuItem
        '
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Name = "ListadoDeFacturasCanceadasToolStripMenuItem"
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeFacturasCanceadasToolStripMenuItem.Text = "Listado de Facturas Canceladas"
        '
        'ListadoDeFacturasReimpresasToolStripMenuItem
        '
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Name = "ListadoDeFacturasReimpresasToolStripMenuItem"
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeFacturasReimpresasToolStripMenuItem.Text = "Listado de Facturas Reimpresas"
        '
        'ListadoDeNotasDeCréditoToolStripMenuItem
        '
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Name = "ListadoDeNotasDeCréditoToolStripMenuItem"
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Text = "Listado de Notas de Crédito"
        Me.ListadoDeNotasDeCréditoToolStripMenuItem.Visible = False
        '
        'RelaciónDeClientesToolStripMenuItem
        '
        Me.RelaciónDeClientesToolStripMenuItem.Name = "RelaciónDeClientesToolStripMenuItem"
        Me.RelaciónDeClientesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.RelaciónDeClientesToolStripMenuItem.Text = "Relación de Pagos Adelantados"
        Me.RelaciónDeClientesToolStripMenuItem.Visible = False
        '
        'DesgloceDeMensualidadesToolStripMenuItem
        '
        Me.DesgloceDeMensualidadesToolStripMenuItem.Name = "DesgloceDeMensualidadesToolStripMenuItem"
        Me.DesgloceDeMensualidadesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.DesgloceDeMensualidadesToolStripMenuItem.Text = "Desgloce de Mensualidades"
        Me.DesgloceDeMensualidadesToolStripMenuItem.Visible = False
        '
        'DesgloceDeContratacionesToolStripMenuItem
        '
        Me.DesgloceDeContratacionesToolStripMenuItem.Name = "DesgloceDeContratacionesToolStripMenuItem"
        Me.DesgloceDeContratacionesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.DesgloceDeContratacionesToolStripMenuItem.Text = "Desgloce de Contrataciones"
        Me.DesgloceDeContratacionesToolStripMenuItem.Visible = False
        '
        'FacturasConCargoAutomaticoToolStripMenuItem
        '
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Name = "FacturasConCargoAutomaticoToolStripMenuItem"
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.FacturasConCargoAutomaticoToolStripMenuItem.Text = "Facturas Con Cargo Automatico"
        '
        'ListadoDePagosEfectuadosPorElClienteToolStripMenuItem
        '
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Name = "ListadoDePagosEfectuadosPorElClienteToolStripMenuItem"
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Text = "Listado De Pagos Efectuados Por El Cliente"
        Me.ListadoDePagosEfectuadosPorElClienteToolStripMenuItem.Visible = False
        '
        'ReporteGlobalToolStripMenuItem
        '
        Me.ReporteGlobalToolStripMenuItem.Name = "ReporteGlobalToolStripMenuItem"
        Me.ReporteGlobalToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ReporteGlobalToolStripMenuItem.Text = "Reporte Global"
        Me.ReporteGlobalToolStripMenuItem.Visible = False
        '
        'ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem
        '
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Name = "ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem"
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem.Text = "Reporte Ingresos Tarjetas Crédito/Débito"
        '
        'MensualidadesAdelantadasToolStripMenuItem
        '
        Me.MensualidadesAdelantadasToolStripMenuItem.Name = "MensualidadesAdelantadasToolStripMenuItem"
        Me.MensualidadesAdelantadasToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.MensualidadesAdelantadasToolStripMenuItem.Text = "Mensualidades Adelantadas"
        '
        'IngresosDeClientesPorUnMontoToolStripMenuItem
        '
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Name = "IngresosDeClientesPorUnMontoToolStripMenuItem"
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.IngresosDeClientesPorUnMontoToolStripMenuItem.Text = "Ingresos de Clientes por un Monto"
        '
        'NúmeroDeBonificacionesToolStripMenuItem
        '
        Me.NúmeroDeBonificacionesToolStripMenuItem.Name = "NúmeroDeBonificacionesToolStripMenuItem"
        Me.NúmeroDeBonificacionesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.NúmeroDeBonificacionesToolStripMenuItem.Text = "Número de Bonificaciones"
        '
        'CobroErroneoToolStripMenuItem
        '
        Me.CobroErroneoToolStripMenuItem.Name = "CobroErroneoToolStripMenuItem"
        Me.CobroErroneoToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.CobroErroneoToolStripMenuItem.Text = "Cobro Corrección de Pagos Equivocados"
        '
        'PagosDiferidosToolStripMenuItem
        '
        Me.PagosDiferidosToolStripMenuItem.Name = "PagosDiferidosToolStripMenuItem"
        Me.PagosDiferidosToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.PagosDiferidosToolStripMenuItem.Text = "Pagos Diferidos"
        '
        'PromocionesToolStripMenuItem
        '
        Me.PromocionesToolStripMenuItem.Name = "PromocionesToolStripMenuItem"
        Me.PromocionesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.PromocionesToolStripMenuItem.Text = "Promociones"
        '
        'EstadosDeCuentaToolStripMenuItem
        '
        Me.EstadosDeCuentaToolStripMenuItem.Name = "EstadosDeCuentaToolStripMenuItem"
        Me.EstadosDeCuentaToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.EstadosDeCuentaToolStripMenuItem.Text = "Estados de Cuenta"
        '
        'TelefoniaToolStripMenuItem
        '
        Me.TelefoniaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReporteDeCobranzaToolStripMenuItem})
        Me.TelefoniaToolStripMenuItem.Name = "TelefoniaToolStripMenuItem"
        Me.TelefoniaToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.TelefoniaToolStripMenuItem.Text = "Telefonía"
        '
        'ReporteDeCobranzaToolStripMenuItem
        '
        Me.ReporteDeCobranzaToolStripMenuItem.Name = "ReporteDeCobranzaToolStripMenuItem"
        Me.ReporteDeCobranzaToolStripMenuItem.Size = New System.Drawing.Size(232, 22)
        Me.ReporteDeCobranzaToolStripMenuItem.Text = "Reporte de Cobranza"
        '
        'ListadoDePagosProporcionalesToolStripMenuItem
        '
        Me.ListadoDePagosProporcionalesToolStripMenuItem.Name = "ListadoDePagosProporcionalesToolStripMenuItem"
        Me.ListadoDePagosProporcionalesToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDePagosProporcionalesToolStripMenuItem.Text = "Listado de Pagos Proporcionales"
        '
        'ListadoDeBonificacionesToolStripMenuItem1
        '
        Me.ListadoDeBonificacionesToolStripMenuItem1.Name = "ListadoDeBonificacionesToolStripMenuItem1"
        Me.ListadoDeBonificacionesToolStripMenuItem1.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeBonificacionesToolStripMenuItem1.Text = "Listado de Bonificaciones"
        '
        'ListadoDeDescuentosToolStripMenuItem
        '
        Me.ListadoDeDescuentosToolStripMenuItem.Name = "ListadoDeDescuentosToolStripMenuItem"
        Me.ListadoDeDescuentosToolStripMenuItem.Size = New System.Drawing.Size(395, 22)
        Me.ListadoDeDescuentosToolStripMenuItem.Text = "Listado de Descuentos"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(395, 22)
        Me.ToolStripMenuItem1.Text = "Promoción 6x7 y 12x14"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'CancelacionDeFacturasToolStripMenuItem
        '
        Me.CancelacionDeFacturasToolStripMenuItem.Name = "CancelacionDeFacturasToolStripMenuItem"
        Me.CancelacionDeFacturasToolStripMenuItem.Size = New System.Drawing.Size(272, 22)
        Me.CancelacionDeFacturasToolStripMenuItem.Text = "Cancelacion de Facturas"
        '
        'PictureBox2
        '
        Me.PictureBox2.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MUESTRAIMAGENBindingSource1, "IMAGEN", True))
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(65, 205)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(881, 294)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'MUESTRAIMAGENBindingSource1
        '
        Me.MUESTRAIMAGENBindingSource1.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource1.DataSource = Me.NewsoftvDataSet2
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel1.Location = New System.Drawing.Point(116, 670)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(63, 20)
        Me.CMBLabel1.TabIndex = 7
        Me.CMBLabel1.Text = "Label1"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabel2.Location = New System.Drawing.Point(116, 709)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(63, 20)
        Me.CMBLabel2.TabIndex = 8
        Me.CMBLabel2.Text = "Label2"
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DameTipoUsusarioBindingSource
        '
        Me.DameTipoUsusarioBindingSource.DataMember = "DameTipoUsusario"
        Me.DameTipoUsusarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameTipoUsusarioTableAdapter
        '
        Me.DameTipoUsusarioTableAdapter.ClearBeforeFill = True
        '
        'ALTASMENUSBindingSource
        '
        Me.ALTASMENUSBindingSource.DataMember = "ALTASMENUS"
        Me.ALTASMENUSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'ALTASMENUSTableAdapter
        '
        Me.ALTASMENUSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENBindingSource
        '
        Me.MUESTRAIMAGENBindingSource.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameEspecifBindingSource
        '
        Me.DameEspecifBindingSource.DataMember = "DameEspecif"
        Me.DameEspecifBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameEspecifTableAdapter
        '
        Me.DameEspecifTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAIMAGENTableAdapter
        '
        Me.MUESTRAIMAGENTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(693, 628)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(54, 32)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Prueba"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameClv_Session_UsuariosBindingSource
        '
        Me.DameClv_Session_UsuariosBindingSource.DataMember = "DameClv_Session_Usuarios"
        Me.DameClv_Session_UsuariosBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'DameClv_Session_UsuariosTableAdapter
        '
        Me.DameClv_Session_UsuariosTableAdapter.ClearBeforeFill = True
        '
        'CMBLabelciudad
        '
        Me.CMBLabelciudad.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CMBLabelciudad.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelciudad.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelciudad.Location = New System.Drawing.Point(27, 50)
        Me.CMBLabelciudad.Name = "CMBLabelciudad"
        Me.CMBLabelciudad.Size = New System.Drawing.Size(964, 29)
        Me.CMBLabelciudad.TabIndex = 11
        Me.CMBLabelciudad.Text = "Label2"
        Me.CMBLabelciudad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DamePermisosBindingSource
        '
        Me.DamePermisosBindingSource.DataMember = "DamePermisos"
        Me.DamePermisosBindingSource.DataSource = Me.DataSetLydia
        '
        'DamePermisosTableAdapter
        '
        Me.DamePermisosTableAdapter.ClearBeforeFill = True
        '
        'CMBLabelNombreSistema
        '
        Me.CMBLabelNombreSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelNombreSistema.ForeColor = System.Drawing.Color.Gray
        Me.CMBLabelNombreSistema.Location = New System.Drawing.Point(27, 89)
        Me.CMBLabelNombreSistema.Name = "CMBLabelNombreSistema"
        Me.CMBLabelNombreSistema.Size = New System.Drawing.Size(964, 29)
        Me.CMBLabelNombreSistema.TabIndex = 12
        Me.CMBLabelNombreSistema.Text = "Facturación"
        Me.CMBLabelNombreSistema.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(439, 739)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(161, 45)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = "Generar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(62, 509)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(257, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Versión : 41.0.0.53  Fecha Publicación : 25/04/2019"
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(245, 525)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(149, 44)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "Generar FF cliente"
        Me.Button3.UseVisualStyleBackColor = True
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(409, 525)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(149, 44)
        Me.Button4.TabIndex = 17
        Me.Button4.Text = "Generar FF Global"
        Me.Button4.UseVisualStyleBackColor = True
        Me.Button4.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(781, 555)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Label2"
        Me.Label2.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(564, 527)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(149, 42)
        Me.Button5.TabIndex = 19
        Me.Button5.Text = "Generar FF NC"
        Me.Button5.UseVisualStyleBackColor = True
        Me.Button5.Visible = False
        '
        'FrmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1016, 695)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CMBLabelNombreSistema)
        Me.Controls.Add(CMBCiudadLabel)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(CMBLabelciudad1)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(CMBNombreLabel)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CMBLabelciudad)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.PictureBox2)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FrmMenu"
        Me.RightToLeftLayout = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menú Principal del Sistema de Facturación"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameTipoUsusarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ALTASMENUSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameEspecifBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameClv_Session_UsuariosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamePermisosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents FacturacionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturacionToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelacionDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CortesDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadosYAfectacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntregasParcialesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMonedaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArqueoDeCajasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciònDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReImpresionDeFacturasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FACTURAGLOBALToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FacturasGlobalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ResumenGeneralFacturaGlobalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeEntragasParcialesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents DameClv_Session_UsuariosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameClv_Session_UsuariosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.DameClv_Session_UsuariosTableAdapter
    Friend WithEvents DameTipoUsusarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameTipoUsusarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameTipoUsusarioTableAdapter
    Friend WithEvents ALTASMENUSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ALTASMENUSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.ALTASMENUSTableAdapter
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents DameEspecifBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameEspecifTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameEspecifTableAdapter
    Friend WithEvents MUESTRAIMAGENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIMAGENTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAIMAGENTableAdapter
    Friend WithEvents RelaciónDeIngresosPorConceptosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MUESTRAIMAGENBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents CobroDeContratosMaestrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListadoDeBonificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeFacturasCanceadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeFacturasReimpresasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotasDeCréditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeNotasDeCréditoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecepciónDelArchivoDeOxxoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelaciónDeClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelciudad As System.Windows.Forms.Label
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents DamePermisosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamePermisosTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DamePermisosTableAdapter
    Friend WithEvents PolizasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeMensualidadesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DesgloceDeContratacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConciliaciónBancariaDeSantanderSerfínToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CMBLabelNombreSistema As System.Windows.Forms.Label
    Friend WithEvents FacturasConCargoAutomaticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDePagosEfectuadosPorElClienteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteGlobalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteIngresosTarjetasCréditoDébitoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MensualidadesAdelantadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosDeClientesPorUnMontoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NúmeroDeBonificacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CancelaciónDeFacturasRedEfectivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobroEquivocadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobroErroneoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagosDiferidosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PromocionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadosDeCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneraciónDeFacturasFiscalesFacturaNetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TelefoniaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteDeCobranzaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDePagosProporcionalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeBonificacionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDeDescuentosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerfilesSISTEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PerfilesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
End Class
