﻿Public Class FrmFechas

    Private Sub FrmFechas_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dtpFechaIni.Value = Today
        dtpFechaFin.Value = Today
    End Sub

    Private Sub dtpFechaIni_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        dtpFechaIni.MaxDate = dtpFechaFin.Value
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        FrmImprimir.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        LiTipo = 0
        Me.Close()
    End Sub
End Class