Imports System.Data.SqlClient

Public Class FrmBonificacion
    Dim LIM_DIAS As Int32 = 0
    Dim PRECIO_POR_DIA As Double = 0

    Private Sub Damemiultimodia()
        Try
            Dim DIA As Int32 = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DAMEUTLIMODIATableAdapter.Connection = CON
            Me.DAMEUTLIMODIATableAdapter.Fill(Me.DataSetEdgar.DAMEUTLIMODIA, DIA)
            CON.Close()
            Me.NumericUpDown1.Minimum = 1
            Me.NumericUpDown1.Maximum = DIA
            LIM_DIAS = DIA
            Me.NumericUpDown1.Value = 1
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub FrmBonificacion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmBonificacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.REDLabel4.Text = GloDes_Ser
        Damemiultimodia()
        DameFila()
        Me.TextBox2.Text = Format(System.Math.Round((Me.NumericUpDown1.Value * PRECIO_POR_DIA), 1), "##,##0.00")
    End Sub

    Private Sub DameFila()
        Try
            If IsNumeric(gloClv_Session) = True And IsNumeric(gloClv_Detalle) = True Then
                If gloClv_Session > 0 And gloClv_Detalle > 0 Then
                    Dim Total As Double
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DameFilaPreDetFacturas_2TableAdapter.Connection = CON
                    Me.DameFilaPreDetFacturas_2TableAdapter.Fill(Me.DataSetEdgar.DameFilaPreDetFacturas_2, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Detalle, Long)), Total)
                    CON.Close()
                    Me.TextBox1.Text = Format(Total, "##,##0.00")
                    PRECIO_POR_DIA = Total / LIM_DIAS
                End If

            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        GloBonif = 0
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.TextBox1.Text) = False Then Me.TextBox1.Text = 0
        If IsNumeric(Me.TextBox2.Text) = False Then Me.TextBox2.Text = 0
        If CLng(Me.TextBox2.Text) <= CLng(Me.TextBox1.Text) Then
            GloBonif = 1
            guarda()
            If Me.TextBox3.Text.Length > 0 Then
                Me.Close()
            End If
        Else
            MsgBox("La cantida capturada no es valida ya que sobrepasa el importe del concepto ", MsgBoxStyle.Information)
        End If
    End Sub


   

    Private Sub NumericUpDown1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NumericUpDown1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.NumericUpDown1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDown1.ValueChanged

        Me.TextBox2.Text = Format(System.Math.Round((Me.NumericUpDown1.Value * PRECIO_POR_DIA), 1), "##,##0.00")

    End Sub

    Private Sub guarda()

        'FORZAR AL USUARIO QUE LLENE EL TXTBOX DEL MOTIVO DE LA BONIFICAción
        If Me.TextBox3.Text.Length > 0 Then
            'eMotivoBonificacion = Me.TextBox3.Text
            Try
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ModFilaPreDetFacturasBonificacionTableAdapter.Connection = CON
                Me.ModFilaPreDetFacturasBonificacionTableAdapter.Fill(Me.DataSetEdgar.ModFilaPreDetFacturasBonificacion, New System.Nullable(Of Long)(CType(gloClv_Session, Long)), New System.Nullable(Of Long)(CType(gloClv_Detalle, Long)), New System.Nullable(Of Integer)(CType(Me.NumericUpDown1.Value, Integer)), New System.Nullable(Of Decimal)(CType(Me.TextBox2.Text, Decimal)))
                Me.InsPreMotivoBonificacionTableAdapter.Connection = CON
                Me.InsPreMotivoBonificacionTableAdapter.Fill(Me.DataSetEdgar.InsPreMotivoBonificacion, gloClv_Session, gloClv_Detalle, CType(Me.TextBox2.Text, Decimal), Me.TextBox3.Text)
                CON.Close()
                bitsist(LocSupBon, LiContrato, GloSistema, Me.Name, "", "Se Realizo la Bonificacion del Detalle" + CStr(gloClv_Detalle) + " Por la Cantidad de: " + Me.TextBox2.Text, "Con Motivo de: " + Me.TextBox3.Text, LocClv_Ciudad)
            Catch ex As System.Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
            End Try
        Else
            MsgBox("Debes Introducir el Motivo de la Bonificación", , "Atención")
        End If

    End Sub



    
End Class