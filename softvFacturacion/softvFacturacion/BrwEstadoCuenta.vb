﻿
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class BrwEstadoCuenta

    Dim Op As Integer = 0
    Dim rutaUsuario As String
    Dim cont As Integer = 0
    Dim arregloImagen() As Byte


    Private Sub ConEstadoCuentaStatus()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConEstadoCuentaStatus")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgEstadoCuentaStatus.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConEstadoCuenta(ByVal Fecha As Date)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConEstadoCuenta '" + Fecha + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgEstadoCuenta.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function DameBanner() As Byte()
        Try
            Dim fs As New FileStream(rutaBanner + "\Banner.jpg", FileMode.Open, FileAccess.Read)
            Dim arreglo(fs.Length) As Byte

            fs.Read(arreglo, 0, arreglo.Length)

            Return arreglo
        Catch ex As Exception

        End Try

    End Function

    Private Sub ImprimirEstadoCuenta(ByVal IdEstadoCuenta As Integer, ByVal Contrato As Long, ByVal Fecha As Date)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ImprimirEstadoCuenta " + IdEstadoCuenta.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet
        Dim rDocument As New ReportDocument
        Dim dTable As New DataTable
        Dim dRow As DataRow = dTable.NewRow()


        Try


            dAdapter.Fill(dSet)

            dTable.Columns.Add("Imagen", arregloImagen.GetType())
            dRow("Imagen") = arregloImagen
            dTable.Rows.Add(dRow)

            dSet.Tables.Add(dTable)
            dSet.Tables(0).TableName = "EstadoCuenta"
            dSet.Tables(1).TableName = "DetEstadoCuenta"
            dSet.Tables(2).TableName = "Banner"

            rDocument.Load(RutaReportes + "\ReportEstadoCuentaCorreoSoftv.rpt")
            rDocument.SetDataSource(dSet)
            rDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaUsuario & "\" & Contrato & ".pdf")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            dAdapter.Dispose()
            dSet.Dispose()
            rDocument.Dispose()
            dTable.Dispose()
        End Try

    End Sub

    Private Sub ConEstadoCuentaAImprimir(ByVal Op As Integer, ByVal Fecha As Date, ByVal IdEstadoCuenta As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConEstadoCuentaAImprimir", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Fecha", SqlDbType.DateTime)
        par2.Direction = ParameterDirection.Input
        par2.Value = Fecha
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@IdEstadoCuenta", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = IdEstadoCuenta
        comando.Parameters.Add(par3)

        Try
            
            rutaUsuario = ""
            FolderBrowserDialog1.RootFolder = Environment.SpecialFolder.Desktop
            FolderBrowserDialog1.ShowDialog()
            rutaUsuario = FolderBrowserDialog1.SelectedPath

            If rutaUsuario.Length = 0 Then
                Exit Sub
            End If


            cont = 0

            conexion.Open()
            reader = comando.ExecuteReader

            CMBProcesando.Visible = True
            pbProcesando.Visible = True

            arregloImagen = DameBanner()


            While (reader.Read())
                cont = cont + 1
                CMBProcesando.Text = "Exportando " & cont.ToString() & " de " & reader(3).ToString() & " estados de cuenta."
                CMBProcesando.Refresh()
                ImprimirEstadoCuenta(reader(0).ToString(), reader(1).ToString(), reader(2).ToString())
            End While

            CMBProcesando.Visible = False
            pbProcesando.Visible = False

            MsgBox("Finalizó con éxito.", MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BrwEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        ConEstadoCuentaStatus()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub dgEstadoCuentaStatus_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgEstadoCuentaStatus.SelectionChanged
        Try
            If dgEstadoCuentaStatus.Rows.Count = 0 Then Exit Sub
            ConEstadoCuenta(dgEstadoCuentaStatus.SelectedCells(1).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bnVerTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVerTodos.Click

        If dgEstadoCuentaStatus.Rows.Count = 0 Then
            MsgBox("Selecciona un periodo.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        ConEstadoCuentaAImprimir(0, dgEstadoCuentaStatus.SelectedCells(1).Value, 0)

    End Sub

    Private Sub bnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVer.Click

        If dgEstadoCuenta.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If

        ConEstadoCuentaAImprimir(1, Today, dgEstadoCuenta.SelectedCells(0).Value)

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class