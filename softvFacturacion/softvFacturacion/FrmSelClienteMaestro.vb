Imports System.Data.SqlClient

Public Class FrmSelClienteMaestro



    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If OP = 0 Then
                If IsNumeric(Me.bcONTRATO.Text) = True Then
                    Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Connection = CON
                    Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Fill(Me.DataSetEdgar.BUSCLIPORCONTRATO2MAESTRO, New System.Nullable(Of Long)(CType(bcONTRATO.Text, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
                Else
                    Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Connection = CON
                    Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Fill(Me.DataSetEdgar.BUSCLIPORCONTRATO2MAESTRO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
                End If
                Me.bcONTRATO.Clear()
            ElseIf OP = 1 Then
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Fill(Me.DataSetEdgar.BUSCLIPORCONTRATO2MAESTRO, New System.Nullable(Of Long)(CType(0, Long)), (CType(Me.BNOMBRE.Text, String)), (CType("", String)), (CType("", String)), (CType("", String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
                'Me.BNOMBRE.Clear()
            ElseIf OP = 2 Then
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Fill(Me.DataSetEdgar.BUSCLIPORCONTRATO2MAESTRO, New System.Nullable(Of Long)(CType(0, Long)), (CType("", String)), (CType(Me.BCALLE.Text, String)), (CType(Me.BNUMERO.Text, String)), (CType(Me.BCIUDAD.Text, String)), New System.Nullable(Of Integer)(CType(OP, Integer)))
                'Me.BCALLE.Clear()
                'Me.BNUMERO.Clear()
                'Me.BCIUDAD.Clear()
            Else
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO2MAESTROTableAdapter.Fill(Me.DataSetEdgar.BUSCLIPORCONTRATO2MAESTRO, New System.Nullable(Of Long)(CType(0, Long)), "", "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                'Me.BCALLE.Clear()
                'Me.BNUMERO.Clear()
                'Me.BCIUDAD.Clear()
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Glocontratosel = Me.CONTRATOLabel1.Text
            Me.Close()
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.BUSCACLIENTES(0)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Me.BUSCACLIENTES(1)
    End Sub




    Private Sub BrwClientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRACALLES)
        CON.Close()
        Me.BUSCACLIENTES(3)
    End Sub

    Private Sub bcONTRATO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles bcONTRATO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(0)
        End If
    End Sub



    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Me.BUSCACLIENTES(2)
    End Sub

    Private Sub BNOMBRE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNOMBRE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(1)
            'Me.BNOMBRE.Text = ""
        End If
    End Sub



    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub



    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub


    Private Sub BCIUDAD_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCIUDAD.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Me.BUSCACLIENTES(2)
        End If
    End Sub



    Private Sub CONTRATOLabel1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CONTRATOLabel1.TextChanged
        Try
            CREAARBOL()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SOLOINTERNETCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SOLOINTERNETCheckBox.CheckedChanged
        If Me.SOLOINTERNETCheckBox.Checked = False Then
            Me.SOLOINTERNETCheckBox.Enabled = False
        Else
            Me.SOLOINTERNETCheckBox.Enabled = True
        End If
    End Sub

    Private Sub ESHOTELCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ESHOTELCheckBox.CheckedChanged
        If Me.ESHOTELCheckBox.Checked = False Then
            Me.ESHOTELCheckBox.Enabled = False
        Else
            Me.ESHOTELCheckBox.Enabled = True
        End If
    End Sub


    Private Sub ServicioListBox_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.bcONTRATO.Focus()
    End Sub




    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(Me.CONTRATOLabel1.Text, Long)))
                CON.Close()
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 16) = "Servicio De Tele" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                            pasa = False
                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            Me.TreeView1.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub



    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If IsNumeric(Me.CONTRATOLabel1.Text) = True Then
            Glocontratosel = Me.CONTRATOLabel1.Text
            Me.Close()
        End If
    End Sub


    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class