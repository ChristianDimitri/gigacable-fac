
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmImprimirRepGral

    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Sub ConfigureCrystalReportsNotasCredito1()

        Dim ba As Boolean
        Dim opc1, opc2 As String
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable

        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet

        dSet = REPORTENotaDeCredito(gloClvNota, 0, 0, DateTime.Today, DateTime.Today, 0)
        rDocument.Load(RutaReportes + "\REPORTENotasCredito.rpt")
        rDocument.SetDataSource(dSet)

            opc1 = "Nota de Cr�dito"
            opc2 = "Nota de Cr�dito :"

            If ba = False Then
                rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & opc1 & "'"
                rDocument.DataDefinition.FormulaFields("Clave").Text = "'" & opc2 & "'"
                rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
                rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
                rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
                rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
                rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
                rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
                If locoprepnotas = 0 Then
                    rDocument.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
                ElseIf locoprepnotas = 1 Then
                    rDocument.DataDefinition.FormulaFields("Copia").Text = "'Original'"
                End If
            End If

            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.ShowPrintButton = True
            rDocument = Nothing
    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        Try


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\ReporteListadoPreliminar.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_SessionBancos
            customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))
            '@Op
            customersByCityReport.SetParameterValue(1, "0")

            mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"
            CrystalReportViewer1.ReportSource = customersByCityReport
            'Me.CrystalReportViewer1.RefreshReport()
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsOxxo(ByVal Clave As Long, ByVal Titulo As String, ByVal SubTitulo As String)
        Try


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\RepListadoOxxo_1.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            '@Clv_SessionBancos
            customersByCityReport.SetParameterValue(0, CStr(GloClv_SessionBancos))

            mySelectFormula = "Listado de Clientes (Proceso de Oxxo)"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & GloEmpresa & "'"
            mySelectFormula = " "
            customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            'Me.CrystalReportViewer1.RefreshReport()
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReportsParciales(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDataBaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_EntregaParcial.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        customersByCityReport.SetParameterValue(1, CStr(Consecutivo))
        '@Supervisor
        customersByCityReport.SetParameterValue(2, locnomsupervisor)

        'mySelectFormula = "Listado de Clientes con Cargo Autom�tico"
        'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        'mySelectFormula = " "
        'customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & GloNomSucursal & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        'Me.CrystalReportViewer1.RefreshReport()
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalDesglose2(ByVal Consecutivo As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDataBaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim mySelectFormula As String = Nothing

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Reporte_DesgloseMoneda.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '@Op
        customersByCityReport.SetParameterValue(0, 0)
        '@Consecutivo
        customersByCityReport.SetParameterValue(1, CStr(Consecutivo))

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport

        customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalArqueo(ByVal Fecha As Date, ByVal Cajera As String)
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim basededatos As String = Nothing
            Dim Total As Double = 0
            Dim Efectivo_Entergas As Double = 0
            Dim Tarjeta As Double = 0
            Dim Cheques As Double = 0
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword
            Dim mySelectFormula As String = Nothing
            'Dim Parametro As String, Parametro1 As String = nothing
            Dim sumaefectivo As Long = 0
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.SumaArqueoTableAdapter.Connection = CON
            Me.SumaArqueoTableAdapter.Fill(Me.Procedimientos_arnoldo.SumaArqueo, Fecha_ini, GloCajera, LocDesglose, LocParciales, LocAuto, LocTarjeta, LocCheque, LocEfectivo, Efectivo_Entergas, Tarjeta, Cheques)
            Me.Dame_base_datosTableAdapter.Connection = CON
            Me.Dame_base_datosTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_base_datos, basededatos)
            CON.Close()
            Dim reportPath As String = Nothing

            Select Case basededatos
                Case "SA"
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"
                Case "JI"
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_2_Ji.rpt"
                Case "SP"
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_San.rpt"
                Case "PA"
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_PA.rpt"
                Case "CU"
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3_CU.rpt"
                Case Else
                    reportPath = RutaReportes + "\Reporte_ArqueoPrincipal_3.rpt"
            End Select




            'Dim report As New ReportDocument()
            Dim connection As IConnectionInfo
            Dim serverName1 As String = GloServerName
            Dim userID As String = GloUserID
            Dim password As String = GloPassword

            customersByCityReport.Load(reportPath)

            ' Establecer conexi�n con base de datos al informe principal
            For Each connection In customersByCityReport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Establecer conexi�n al subinforme
            Dim subreport As ReportDocument
            For Each subreport In customersByCityReport.Subreports
                For Each connection In subreport.DataSourceConnections
                    connection.SetLogon(userID, password)
                Next
            Next



            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport2(connectionInfo, customersByCityReport)


            '@Fecha
            customersByCityReport.SetParameterValue(0, Fecha_ini)
            '@Cajera
            customersByCityReport.SetParameterValue(1, GloCajera)






            '' SetDBLogonForSubReport(connectionInfo, customersByCityReport)







            mySelectFormula = "Arqueo de Caja"
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            mySelectFormula = "Sucursal : " & GloNomSucursal
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("TotalEfectivoParciales").Text = Efectivo_Entergas.ToString
            customersByCityReport.DataDefinition.FormulaFields("Efectivo").Text = LocEfectivo.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalTarjeta").Text = Tarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TarjetaCredito").Text = LocTarjeta.ToString
            customersByCityReport.DataDefinition.FormulaFields("TotalCheques").Text = Cheques.ToString
            customersByCityReport.DataDefinition.FormulaFields("Cheques").Text = LocCheque.ToString
            customersByCityReport.DataDefinition.FormulaFields("CargoAutomatico").Text = LocAuto.ToString
            customersByCityReport.DataDefinition.FormulaFields("FondoF").Text = "0"

            Total = Efectivo_Entergas + Tarjeta + Cheques + LocAuto
            'MsgBox(Total)
            customersByCityReport.DataDefinition.FormulaFields("Total").Text = Total.ToString
            'Total = LocDesglose + LocParciales
            ' customersByCityReport.DataDefinition.FormulaFields("Total2").Text = Total.ToString
            CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub SetDBLogonForSubReport2(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(1).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(2).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        customersByCityReport.Subreports(3).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub


    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub
    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal importe2 As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim cliente2 As String = "P�blico en General"
        Dim concepto2 As String = "Ingreso por Pago de Servicios"
        Dim txtsubtotal As String = Nothing
        Dim subtotal2 As Double
        Dim iva2 As Double
        Dim myString As String = iva2.ToString("00.00")
        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalticket.rpt"
        'MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
        subtotal2 = CDec(importe2) / 1.15
        txtsubtotal = subtotal2
        txtsubtotal = subtotal2.ToString("##0.00")
        iva2 = CDec(importe2) / 1.15 * 0.15
        myString = iva2.ToString("##0.00")
        customersByCityReport.DataDefinition.FormulaFields("Subtotal").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & myString & "'"
        customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & txtsubtotal & "'"
        customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
        customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & importe2 & "'"

        'SetDBLogonForReport(connectionInfo, customersByCityReport)
        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing


    End Sub

    Private Sub ConfigureCrystalReportefacturaGlobal2(ByVal Fecha As String, ByVal Tipo As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        If Tipo = "V" Then

            ' @fecha DateTime,
            customersByCityReport.SetParameterValue(0, Fecha)
            '  @Selsucursal  int
            customersByCityReport.SetParameterValue(1, CStr(0))
            '  @Op int
            customersByCityReport.SetParameterValue(2, CStr(0))

            mySelectFormula = "Comprobaci�n de Facturas Globales de Ventas por Sucursal"


        Else
            If Tipo = "C" Then
                '        'Fec_Ini
                customersByCityReport.SetParameterValue(0, Fecha)
                '        @Selsucursal  int
                customersByCityReport.SetParameterValue(1, GloSucursal)
                '        '@Op
                customersByCityReport.SetParameterValue(2, CStr(2))

                '        'Titulo del Reporte
                mySelectFormula = "Comprobaci�n de Facturas Globales de Cajas por Sucursal"

            End If
        End If
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub
    Private Sub ConfigureCrystalReportefacturaGlobal3(ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\ReporteFacturaGlobalW.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)

        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'Fecha
        customersByCityReport.SetParameterValue(0, Fecha)
        '    '@Sucursal
        customersByCityReport.SetParameterValue(1, CStr(0))
        '    '@Op
        customersByCityReport.SetParameterValue(2, CStr(3))

        mySelectFormula = " Cortes Generales de Cajas, Ventas y Facturas Globales "
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & mySelectFormula & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub

    Private Sub ConfigureCrystalReporteListEntregaParcial(ByVal clv_session As Integer, ByVal Fecha1 As String, ByVal Fecha As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim mySelectFormula As String = Nothing
        Dim Fecha2 As String = " "
        Dim Extra As String = " "

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\Listadp_Entregas_Parciales.rpt"
        ' MsgBox(reportPath)
        customersByCityReport.Load(reportPath)
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    'clave session
        customersByCityReport.SetParameterValue(0, clv_session)
        '    '@Fecha_ini
        customersByCityReport.SetParameterValue(1, Fecha1)
        '    '@Fecha_fin
        customersByCityReport.SetParameterValue(2, Fecha)
        ''Encabezados Reporte
        mySelectFormula = "Listado de Entregas Parciales por Cajera"
        Fecha2 = "Desde Fecha: " & Fecha1 & "  Hasta Fecha: " & Fecha
        Extra = "Sucursal:" & NomSucursal
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        mySelectFormula = " Aguascalientes "
        customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Fecha2 & "'"
        mySelectFormula = " Fecha: " & bec_fecha
        customersByCityReport.DataDefinition.FormulaFields("Encabezado").Text = "'" & Extra & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing

    End Sub
    Private Sub ConfigureCrystalBonificaciones()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing
            If LocResumenBon = True Then
                reportPath = RutaReportes + "\ResumenBonificaciones.rpt"
                mySelectFormula = "Resumen de Facturas Bonificadas"
            Else
                reportPath = RutaReportes + "\ListadodeBonificaciones.rpt"
                mySelectFormula = "Listado de Facturas Bonificadas"
            End If



            '' MsgBox(reportPath)
            'customersByCityReport.Load(reportPath)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            ''    '@Fecha_ini
            'customersByCityReport.SetParameterValue(0, LocFecha1)
            ''    '@Fecha_fin
            'customersByCityReport.SetParameterValue(1, LocFecha2)
            '' @clv_txt varchar(5)
            'customersByCityReport.SetParameterValue(2, Locclv_usuario)
            ' ''Encabezados Reporte

            Dim DS As New DataSet

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@fecha1", SqlDbType.DateTime, CObj(LocFecha1))
            BaseII.CreateMyParameter("@fecha2", SqlDbType.DateTime, CObj(LocFecha2))
            BaseII.CreateMyParameter("@clv_txt", SqlDbType.Int, CLng(LocClv_session))

            Dim listatablas As New List(Of String)

            listatablas.Add("Listado_Bonificaciones")
            'listatablas.Add("CALLES")
            'listatablas.Add("CLIENTES")
            'listatablas.Add("COLONIAS")
            'listatablas.Add("DetOrdSer")
            'listatablas.Add("Rel_Contrato_NoInt")
            DS = BaseII.ConsultaDS("Listado_Bonificaciones", listatablas)


            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(DS)

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            Dim Supervisor As String = Nothing
            Supervisor = "Supervisor: " + LocNombreusuario



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Supervisor").Text = "'" & Supervisor & "'"

            If LocResumenBon = True Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            End If

            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndBon = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalFacturasCanceladas()

        Try
            Dim dSet As New DataSet
            customersByCityReport = New ReportDocument
            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "
            Dim reportPath As String = Nothing
            Dim Nomsucursal As String = Nothing
            Dim RangoFechas As String = Nothing
            Dim Cajero As String = Nothing

            dSet = Listado_Facturas_Canceladas(LocFecha1, LocFecha2, Locclv_usuario, LocBanderaRep1 + 1)

            reportPath = RutaReportes + "\ListadoFacturasCanceladas.rpt"

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(dSet)

            Nomsucursal = "Sucursal:" + GloNomSucursal
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2
            Cajero = "Usuario: " + LocNombreusuario

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
            customersByCityReport.DataDefinition.FormulaFields("Cajero").Text = "'" & Cajero & "'"
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        'Try
        '    customersByCityReport = New ReportDocument
        '    Dim connectionInfo As New ConnectionInfo
        '    '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    '    "=True;User ID=DeSistema;Password=1975huli")
        '    connectionInfo.ServerName = GloServerName
        '    connectionInfo.DatabaseName = GloDatabaseName
        '    connectionInfo.UserID = GloUserID
        '    connectionInfo.Password = GloPassword

        '    Dim mySelectFormula As String = Nothing
        '    Dim Fecha2 As String = " "
        '    Dim Extra As String = " "

        '    Dim reportPath As String = Nothing
        '    Select Case LocBanderaRep1
        '        Case 0
        '            mySelectFormula = "Listado de Facturas Canceladas"
        '        Case 1
        '            mySelectFormula = "Listado de Facturas Reimpresas"
        '    End Select

        '    reportPath = RutaReportes + "\ListadoFacturasCanceladas.rpt"





        '    ' MsgBox(reportPath)
        '    customersByCityReport.Load(reportPath)
        '    SetDBLogonForReport(connectionInfo, customersByCityReport)
        '    'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '    '    '@Fecha_ini
        '    customersByCityReport.SetParameterValue(0, LocFecha1)
        '    '    '@Fecha_fin
        '    customersByCityReport.SetParameterValue(1, LocFecha2)
        '    ' @clv_txt varchar(5)
        '    customersByCityReport.SetParameterValue(2, Locclv_usuario)
        '    '@clv_reporte
        '    customersByCityReport.SetParameterValue(3, LocBanderaRep1 + 1)

        '    ''Encabezados Reporte

        '    Dim Nomsucursal As String = Nothing
        '    Nomsucursal = "Sucursal:" + GloNomSucursal

        '    Dim RangoFechas As String = Nothing
        '    RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

        '    Dim Cajero As String = Nothing
        '    Cajero = "Usuario: " + LocNombreusuario


        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Nomsucursal & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Cajero").Text = "'" & Cajero & "'"

        '    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait


        '    CrystalReportViewer1.ReportSource = customersByCityReport
        '    customersByCityReport = Nothing
        '    LocBndrepfac1 = False
        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try

    End Sub
    Private Sub ConfigureCrystalNotasCredito()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Nothing
            Dim Fecha2 As String = " "
            Dim Extra As String = " "

            Dim reportPath As String = Nothing

            Select Case Locbndrepnotas
                Case 0
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(cajero).rpt"
                Case 1
                    reportPath = RutaReportes + "\ReporteNotasdeCredito(sucursal).rpt"
            End Select

            'reportPath = RutaReportes + "\Reporte_Notas_Credito01.rpt"




            '(@op int,@clv_Usuario varchar(6),@clv_sucursal bigint,@cancelada bit,@saldada bit,@activa bit,@fecha1 datetime,@fecha2 datetime)


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@op int
            customersByCityReport.SetParameterValue(0, Locbndrepnotas)
            '    '@clv_Usuario int
            customersByCityReport.SetParameterValue(1, LocUsuariosNotas)
            '@clv_sucursal bigint
            customersByCityReport.SetParameterValue(2, Locclv_sucursalnotas)
            '    '@cancelada bit
            customersByCityReport.SetParameterValue(3, LocCancelada)
            ' @saldada bit
            customersByCityReport.SetParameterValue(4, LocSaldada)
            '@activa bit
            customersByCityReport.SetParameterValue(5, LocActiva)
            '@fecha1 datetime
            customersByCityReport.SetParameterValue(6, LocFecha1)
            '@fecha2 datetime
            customersByCityReport.SetParameterValue(7, LocFecha2)


            ''Encabezados Reporte

            Dim Nomsucursal As String = Nothing
            Nomsucursal = "Sucursal:" + GloNomSucursal

            Dim RangoFechas As String = Nothing
            RangoFechas = "De la Fecha: " + LocFecha1 + " A la Fecha: " + LocFecha2

            mySelectFormula = "Listado de Notas de Cr�dito"




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & RangoFechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Nomsucursal & "'"


            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape


            CrystalReportViewer1.ReportSource = customersByCityReport
            customersByCityReport = Nothing
            LocBndrepfac1 = False
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalDesglosePagos()
        Try

            Dim rDocument As New ReportDocument
            Dim dSet As New DataSet
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing

            Ciudades = " Ciudad(es): " + LocCiudades
            Titulo = "Relaci�n de Ingresos por Conceptos"
            Sucursal = " Sucursal: " + GloNomSucursal
            eFechaTitulo = "de la Fecha " & eFechaInicial & " a la Fecha " & eFechaFinal

            dSet = DESGLOSEPAGOSRANGOFECHAS(eFechaInicial, eFechaFinal, "", 0, 0, "", 0, gloClv_Session)

            rDocument.Load(RutaReportes + "\ReportePagosRangoFechas.rpt")
            rDocument.SetDataSource(dSet)

            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            rDocument.DataDefinition.FormulaFields("Fecha").Text = "'" & eFechaTitulo & "'"
            rDocument.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"
            rDocument.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"

            CrystalReportViewer1.ReportSource = rDocument
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ConfigureCrystalReportsDetalleConciliacion()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades

            reportPath = RutaReportes + "\Detalle_Prefacturas_Pagolinea.rpt"
            Titulo = "Detalle De Movimientos Por Fecha"

            Sucursal = " Sucursal: " + GloNomSucursal
            customersByCityReport.Load(reportPath)

            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForReport(connectionInfo, customersByCityReport)
            'SetDBLogonForSubReport(connectionInfo, customersByCityReport)

            '@FECHA_INI
            customersByCityReport.SetParameterValue(0, locGlo_Fechaini)
            '@FECHA_FIN
            customersByCityReport.SetParameterValue(1, locGlo_Fechafin)





            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & locGlo_Fechaini & " a la Fecha: " & locGlo_Fechafin
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsCargosAutomaticos()
        Try

            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            If losresumencargos = True Then
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoResumen.rpt"
                Titulo = "Resumen De Facturas Con Cargo Automatico"
            Else
                reportPath = RutaReportes + "\ReporteFacturasCargoAutoDetallado.rpt"
                Titulo = "Listado De Facturas Con Cargo Automatico"
            End If


            Sucursal = " Sucursal: " + GloNomSucursal
            customersByCityReport.Load(reportPath)



            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)

            '@clv_session
            customersByCityReport.SetParameterValue(0, locclv_sessioncargosauto)
            '@op
            customersByCityReport.SetParameterValue(1, 0)
            '@fechaIni
            customersByCityReport.SetParameterValue(2, Fecha_IniCargo)
            '@fechaFin
            customersByCityReport.SetParameterValue(3, Fecha_FinCargo)
            '@SelCajera
            customersByCityReport.SetParameterValue(4, Cajero_Cargo)






            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            eFechaTitulo = "De la Fecha: " & Fecha_IniCargo & " a la Fecha: " & Fecha_FinCargo
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & eFechaTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & Sucursal & "'"
            'ustomersByCityReport.DataDefinition.FormulaFields("Ciudades").Text = "'" & Ciudades & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)

            customersByCityReport = Nothing
            bndcancelareportcargos = True
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ConfigureCrystalReportsPagosEfectuadosCliente()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword
            Dim reportPath As String = Nothing
            Dim Titulo As String = Nothing
            Dim Sucursal As String = Nothing
            Dim Ciudades As String = Nothing
            ' Ciudades = " Ciudad(es): " + LocCiudades
            reportPath = RutaReportes + "\Reporte_Clientes_facturas_detallado_Jiq.rpt"
            Titulo = "Listado De Pagos Efectuados Por El Cliente"
            Sucursal = " Sucursal: " + GloNomSucursal

            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '(@clv_Session bigint,@op bigint,@FechaIni datetime,@FechaFin datetime,@SelCajera varchar(250)


            '@op
            customersByCityReport.SetParameterValue(0, 0)
            '@contratoini
            customersByCityReport.SetParameterValue(1, contratoini)
            '@contratofin
            customersByCityReport.SetParameterValue(2, contratofin)



            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Subtitulo").Text = "'" & Sucursal & "'"


            CrystalReportViewer1.ReportSource = customersByCityReport
            CrystalReportViewer1.Zoom(75)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ConfigureCrystalReportsCorteGlobal()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportCortesGlobal.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_Session
            customersByCityReport.SetParameterValue(0, CStr(eClv_Session))
            '@FechaInicial
            customersByCityReport.SetParameterValue(1, CStr(eFechaInicial))
            '@FechaFinal
            customersByCityReport.SetParameterValue(2, CStr(eFechaFinal))

            titulo = "Reporte de Cortes Global"
            fechas = "Del " & CStr(eFechaInicial) & " al " & CStr(eFechaFinal)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub rptCobroErroneo()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\rptCobroErroneo.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Op
            customersByCityReport.SetParameterValue(0, 0)
            '@FechaIni
            customersByCityReport.SetParameterValue(1, CStr(eFechaIni))
            '@FechaFin
            customersByCityReport.SetParameterValue(2, CStr(eFechaFin))


            fechas = "Del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ReportePagosDifFac()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportPagosDifFac.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Periodo1
            customersByCityReport.SetParameterValue(0, ePeriodo1)
            '@Periodo2
            customersByCityReport.SetParameterValue(1, ePeriodo2)
            '@Mes
            customersByCityReport.SetParameterValue(2, eMes)
            '@Anio
            customersByCityReport.SetParameterValue(3, eAnio)
            '@Pago1
            customersByCityReport.SetParameterValue(4, ePago1)
            '@Pago2
            customersByCityReport.SetParameterValue(5, ePago2)




            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ReportePromocion()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim fechas As String = Nothing
            Dim reportPath As String = Nothing
            Dim titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            reportPath = RutaReportes + "\ReportPromocion.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Periodo1
            customersByCityReport.SetParameterValue(0, eFechaIni)
            '@Periodo2
            customersByCityReport.SetParameterValue(1, eFechaFin)
            '@C
            customersByCityReport.SetParameterValue(2, eC)
            '@I
            customersByCityReport.SetParameterValue(3, eI)
            '@D
            customersByCityReport.SetParameterValue(4, eD)
            '@S
            customersByCityReport.SetParameterValue(5, eS)
            '@B
            customersByCityReport.SetParameterValue(6, eB)
            '@F
            customersByCityReport.SetParameterValue(7, eF)


            fechas = "Contratados del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & eTitulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub FrmImprimirRepGral_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub
    Private Sub FrmImprimirRepGral_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If GloReporte = 1 Then
            ConfigureCrystalReports(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 2 Then
            ConfigureCrystalReportsParciales(GloConsecutivo)
        ElseIf GloReporte = 3 Then
            'Me.ConfigureCrystalDesglose2(GloConsecutivo)
            Reporte_DesgloseMoneda(0, GloConsecutivo)
        ElseIf GloReporte = 4 Then
            Me.ConfigureCrystalArqueo(Fecha_ini, GloCajera)
        ElseIf GloReporte = 5 Then
            ' ConfigureCrystalReortefacturaGlobal("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal(bec_letra, bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
        ElseIf GloReporte = 6 Then
            ' ConfigureCrystalReortefacturaGlobal2("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal2(bec_fecha, bec_tipo)
        ElseIf GloReporte = 7 Then
            ' ConfigureCrystalReortefacturaGlobal3("", bec_importe, bec_serie, bec_fecha, GloUsuario, bec_factura)
            Me.ConfigureCrystalReportefacturaGlobal3(bec_fecha)
        ElseIf GloReporte = 8 Then
            Me.ConfigureCrystalReporteListEntregaParcial(LocClv_session, Fecha_ini, Fecha_Fin)
        ElseIf GloReporte = 9 Then
            ConfigureCrystalReportsOxxo(GloClv_SessionBancos, GloTitulo, GloSubTitulo)
        ElseIf GloReporte = 10 Then
            GloReporte = 0
            ConfigureCrystalReportsCorteGlobal()
        ElseIf GloReporte = 11 Then
            GloReporte = 0
            rptCobroErroneo()
        ElseIf GloReporte = 12 Then
            GloReporte = 0
            ReportePagosDifFac()
        ElseIf GloReporte = 13 Then
            GloReporte = 0
            ReportePromocion()
        End If
        If LocBndBon = True Then
            LocBndBon = False
            ConfigureCrystalBonificaciones()
        End If
        If LocBndrepfac1 = True Then
            LocBndrepfac1 = False
            ConfigureCrystalFacturasCanceladas()
        End If
        If LocbndNotas = True Then
            LocbndNotas = False
            ConfigureCrystalNotasCredito()
        End If
        If LocbndDesPagos = True Then
            LocbndDesPagos = False
            ConfigureCrystalDesglosePagos()
            Me.CrystalReportViewer1.ShowPrintButton = True
        End If
        If LocBndNotasReporteTick = True Then
            LocBndNotasReporteTick = False
            ConfigureCrystalReportsNotasCredito1()
        End If
        If bndreporteconciliacion = True Then
            bndreporteconciliacion = False
            ConfigureCrystalReportsDetalleConciliacion()
        End If
        If bndreportcargos = True Then
            bndreportcargos = False
            ConfigureCrystalReportsCargosAutomaticos()
        End If
        If BndRepImporteClietnes = True Then
            BndRepImporteClietnes = False
            Me.Text = "Listado De Pagos Efectuados Por El Cliente"
            ConfigureCrystalReportsPagosEfectuadosCliente()
        End If
    End Sub

    Public Sub ReporteMontos()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Fechas As String = Nothing
            Dim ReportPath As String = Nothing
            Dim Titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            ReportPath = RutaReportes + "\ReportMontos.rpt"
            customersByCityReport.Load(ReportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer
            customersByCityReport.SetParameterValue(0, eClvTipSer)
            '@FechaIni
            customersByCityReport.SetParameterValue(1, eFechaIni)
            '@FechaFin
            customersByCityReport.SetParameterValue(2, eFechaFin)
            '@Op
            customersByCityReport.SetParameterValue(3, eOp)
            '@Monto
            customersByCityReport.SetParameterValue(4, eMonto)

            Titulo = "Ingresos de Clientes del Servicio de" & eConcepto & " por un Monto " & eOperador & " a $" & CStr(eMonto)
            Fechas = "En el Periodo del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub


    Public Sub ReporteBonificacion()
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Fechas As String = Nothing
            Dim ReportPath As String = Nothing
            Dim Titulo As String = Nothing

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword


            ReportPath = RutaReportes + "\ReportBonificacion.rpt"
            customersByCityReport.Load(ReportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@FechaIni
            customersByCityReport.SetParameterValue(0, eFechaIni)
            '@FechaFin
            customersByCityReport.SetParameterValue(1, eFechaFin)
            '@Numero
            customersByCityReport.SetParameterValue(2, eNumero)

            Titulo = "N�mero de Bonificaciones: " & CStr(eNumero)
            Fechas = "En el Periodo del " & CStr(eFechaIni) & " al " & CStr(eFechaFin)


            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Titulo & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloNomSucursal & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
            SetDBLogonForReport2(connectionInfo)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Function Listado_Facturas_Canceladas(ByVal Fecha1 As DateTime, ByVal Fecha2 As DateTime, ByVal Clv_Usuario As String, ByVal Clv_Reporte As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Listado_Facturas_Canceladas")
        tableNameList.Add("SUCURSALES")
        tableNameList.Add("Parametros")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha1", SqlDbType.DateTime, Fecha1)
        BaseII.CreateMyParameter("@Fecha2", SqlDbType.DateTime, Fecha2)
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 5)
        BaseII.CreateMyParameter("@Clv_Reporte", SqlDbType.Int, Clv_Reporte)
        Return BaseII.ConsultaDS("Listado_Facturas_Canceladas", tableNameList)
    End Function

    Private Sub Reporte_DesgloseMoneda(ByVal Op As Integer, ByVal Consecutivo As Integer)
        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        Dim rDocument As New ReportDocument
        tableNameList.Add("Rel_Desglose_dolares")
        tableNameList.Add("DesgloseMoneda")
        tableNameList.Add("DesgloseMonedaOtros")
        tableNameList.Add("General")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Consecutivo", SqlDbType.Int, Consecutivo)
        dSet = BaseII.ConsultaDS("Reporte_DesgloseMoneda", tableNameList)
        rDocument.Load(RutaReportes + "\Reporte_DesgloseMoneda.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument
        rDocument = Nothing
    End Sub

    Private Function DESGLOSEPAGOSRANGOFECHAS(ByVal FECHA_INI As DateTime, ByVal FECHA_FIN As DateTime, ByVal TIPO As String, ByVal SUCURSAL As Integer, ByVal CAJA As Integer, ByVal CAJERA As String, ByVal OP As Integer, ByVal CLV_SESSION As Integer)
        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("DESGLOSEPAGOSRANGOFECHAS")
        tableNameList.Add("SUCURSALES")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, FECHA_INI)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, FECHA_FIN)
        BaseII.CreateMyParameter("@TIPO", SqlDbType.VarChar, TIPO, 1)
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.Int, SUCURSAL)
        BaseII.CreateMyParameter("@CAJA", SqlDbType.Int, CAJA)
        BaseII.CreateMyParameter("@CAJERA", SqlDbType.VarChar, CAJERA, 11)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        Return BaseII.ConsultaDS("DESGLOSEPAGOSRANGOFECHAS", tableNameList)
    End Function

    Private Function REPORTENotaDeCredito(ByVal CLAVE As Integer, ByVal CLV_NOTA_INI As Integer, ByVal CLV_NOTA_FIN As Integer, ByVal FECHA_INI As DateTime, ByVal FECHA_FIN As DateTime, ByVal OP As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("DETFACTURAS_NOTADECREDITO")
        tableNameList.Add("NOTAS_DE_CREDITO")
        tableNameList.Add("REL_NOTACREDITO_CONCEPTOSSERV")
        tableNameList.Add("REPORTENOTACREDITO")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLAVE", SqlDbType.Int, CLAVE)
        BaseII.CreateMyParameter("@CLV_NOTA_INI", SqlDbType.Int, CLV_NOTA_INI)
        BaseII.CreateMyParameter("@CLV_NOTA_FIN", SqlDbType.Int, CLV_NOTA_FIN)
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, FECHA_INI)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, FECHA_FIN)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        Return BaseII.ConsultaDS("REPORTENotaDeCredito", tableNameList)
    End Function

End Class