﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGeneracionFF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbTelefonoR = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tbCodigoPostalR = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbPaisR = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbEstadoR = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.tbMunicipioR = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbReferenciaR = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tbrfcR = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tbNombreR = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tbCalleR = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tbNoExtR = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tbNoIntR = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tbColoniaR = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.tbLocalidadR = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tbEmailR = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.gbReceptor = New System.Windows.Forms.GroupBox()
        Me.ComboBoxUsoCFDI = New System.Windows.Forms.ComboBox()
        Me.LabelUsoCFDI = New System.Windows.Forms.Label()
        Me.ComboBoxEdo = New System.Windows.Forms.ComboBox()
        Me.ComboBoxMun = New System.Windows.Forms.ComboBox()
        Me.ComboBoxLoc = New System.Windows.Forms.ComboBox()
        Me.gbComprobante = New System.Windows.Forms.GroupBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ComboBoxMetodoDePago = New System.Windows.Forms.ComboBox()
        Me.MetodoDePago = New System.Windows.Forms.Label()
        Me.TextBoxIEPS = New System.Windows.Forms.TextBox()
        Me.ComboBoxFormaDePago = New System.Windows.Forms.ComboBox()
        Me.FormaDePago = New System.Windows.Forms.Label()
        Me.lblNumCtaPago = New System.Windows.Forms.Label()
        Me.txtNumCtaPago = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.tbTotal = New System.Windows.Forms.TextBox()
        Me.cbSerie = New System.Windows.Forms.ComboBox()
        Me.tbSubTotal = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.tbIVA = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.lblMetodoPago = New System.Windows.Forms.Label()
        Me.txtMetodoPag = New System.Windows.Forms.TextBox()
        Me.lblMetodoPag = New System.Windows.Forms.Label()
        Me.tbDescripcion = New System.Windows.Forms.TextBox()
        Me.tbImporte = New System.Windows.Forms.TextBox()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.dgConcepto = New System.Windows.Forms.DataGridView()
        Me.IdProdServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdImpuestoIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoFactor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteIVA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdImpuestoIEPS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Impuesto2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoFactor2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteIEPS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.gbConcepto = New System.Windows.Forms.GroupBox()
        Me.ComboBoxServ = New System.Windows.Forms.ComboBox()
        Me.LabelServ = New System.Windows.Forms.Label()
        Me.ComboBoxTipFacIEPS = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.ComboBoxImpIEPS = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.TextBoxImpIVA = New System.Windows.Forms.TextBox()
        Me.TextBoxImpIEPS = New System.Windows.Forms.TextBox()
        Me.LblImpIEPS = New System.Windows.Forms.Label()
        Me.LblImporteIVA = New System.Windows.Forms.Label()
        Me.ComboBoxTipofac = New System.Windows.Forms.ComboBox()
        Me.LabelTipoFactor = New System.Windows.Forms.Label()
        Me.ComboBoxImpuestos = New System.Windows.Forms.ComboBox()
        Me.LabelImpuesto = New System.Windows.Forms.Label()
        Me.ComboBoxClvProdSer = New System.Windows.Forms.ComboBox()
        Me.ComboBoxRegFis = New System.Windows.Forms.ComboBox()
        Me.LabelRegimenFis = New System.Windows.Forms.Label()
        Me.bnGenerar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.gbReceptor.SuspendLayout()
        Me.gbComprobante.SuspendLayout()
        CType(Me.dgConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbConcepto.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbTelefonoR
        '
        Me.tbTelefonoR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbTelefonoR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTelefonoR.Location = New System.Drawing.Point(558, 119)
        Me.tbTelefonoR.MaxLength = 50
        Me.tbTelefonoR.Name = "tbTelefonoR"
        Me.tbTelefonoR.Size = New System.Drawing.Size(209, 21)
        Me.tbTelefonoR.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(55, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 15)
        Me.Label1.TabIndex = 120
        Me.Label1.Text = "Nombre :"
        '
        'tbCodigoPostalR
        '
        Me.tbCodigoPostalR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCodigoPostalR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCodigoPostalR.Location = New System.Drawing.Point(128, 176)
        Me.tbCodigoPostalR.MaxLength = 50
        Me.tbCodigoPostalR.Name = "tbCodigoPostalR"
        Me.tbCodigoPostalR.Size = New System.Drawing.Size(209, 21)
        Me.tbCodigoPostalR.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(73, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 15)
        Me.Label2.TabIndex = 119
        Me.Label2.Text = "Calle :"
        '
        'tbPaisR
        '
        Me.tbPaisR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbPaisR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbPaisR.Location = New System.Drawing.Point(558, 92)
        Me.tbPaisR.MaxLength = 150
        Me.tbPaisR.Name = "tbPaisR"
        Me.tbPaisR.Size = New System.Drawing.Size(308, 21)
        Me.tbPaisR.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(30, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 15)
        Me.Label3.TabIndex = 118
        Me.Label3.Text = "No. Exterior :"
        '
        'tbEstadoR
        '
        Me.tbEstadoR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEstadoR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEstadoR.Location = New System.Drawing.Point(308, 69)
        Me.tbEstadoR.MaxLength = 150
        Me.tbEstadoR.Name = "tbEstadoR"
        Me.tbEstadoR.Size = New System.Drawing.Size(308, 21)
        Me.tbEstadoR.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(35, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 15)
        Me.Label4.TabIndex = 117
        Me.Label4.Text = "No. Interior :"
        '
        'tbMunicipioR
        '
        Me.tbMunicipioR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbMunicipioR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbMunicipioR.Location = New System.Drawing.Point(308, 90)
        Me.tbMunicipioR.MaxLength = 150
        Me.tbMunicipioR.Name = "tbMunicipioR"
        Me.tbMunicipioR.Size = New System.Drawing.Size(308, 21)
        Me.tbMunicipioR.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(58, 155)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 15)
        Me.Label5.TabIndex = 116
        Me.Label5.Text = "Colonia :"
        '
        'tbReferenciaR
        '
        Me.tbReferenciaR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbReferenciaR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbReferenciaR.Location = New System.Drawing.Point(556, 11)
        Me.tbReferenciaR.MaxLength = 150
        Me.tbReferenciaR.Name = "tbReferenciaR"
        Me.tbReferenciaR.Size = New System.Drawing.Size(308, 21)
        Me.tbReferenciaR.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(43, 211)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 15)
        Me.Label6.TabIndex = 115
        Me.Label6.Text = "Localidad :"
        '
        'tbrfcR
        '
        Me.tbrfcR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbrfcR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbrfcR.Location = New System.Drawing.Point(128, 11)
        Me.tbrfcR.MaxLength = 50
        Me.tbrfcR.Name = "tbrfcR"
        Me.tbrfcR.Size = New System.Drawing.Size(308, 21)
        Me.tbrfcR.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(464, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 15)
        Me.Label7.TabIndex = 114
        Me.Label7.Text = "Referencia :"
        '
        'tbNombreR
        '
        Me.tbNombreR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNombreR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNombreR.Location = New System.Drawing.Point(128, 38)
        Me.tbNombreR.MaxLength = 150
        Me.tbNombreR.Name = "tbNombreR"
        Me.tbNombreR.Size = New System.Drawing.Size(308, 21)
        Me.tbNombreR.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(472, 44)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 15)
        Me.Label8.TabIndex = 113
        Me.Label8.Text = "Municipio :"
        '
        'tbCalleR
        '
        Me.tbCalleR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCalleR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbCalleR.Location = New System.Drawing.Point(128, 65)
        Me.tbCalleR.MaxLength = 250
        Me.tbCalleR.Name = "tbCalleR"
        Me.tbCalleR.Size = New System.Drawing.Size(308, 21)
        Me.tbCalleR.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(492, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 15)
        Me.Label9.TabIndex = 112
        Me.Label9.Text = "Estado :"
        '
        'tbNoExtR
        '
        Me.tbNoExtR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNoExtR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoExtR.Location = New System.Drawing.Point(128, 92)
        Me.tbNoExtR.MaxLength = 50
        Me.tbNoExtR.Name = "tbNoExtR"
        Me.tbNoExtR.Size = New System.Drawing.Size(209, 21)
        Me.tbNoExtR.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(19, 182)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 15)
        Me.Label10.TabIndex = 111
        Me.Label10.Text = "Código Postal :"
        '
        'tbNoIntR
        '
        Me.tbNoIntR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbNoIntR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNoIntR.Location = New System.Drawing.Point(128, 119)
        Me.tbNoIntR.MaxLength = 50
        Me.tbNoIntR.Name = "tbNoIntR"
        Me.tbNoIntR.Size = New System.Drawing.Size(209, 21)
        Me.tbNoIntR.TabIndex = 4
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(509, 98)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 15)
        Me.Label11.TabIndex = 110
        Me.Label11.Text = "País :"
        '
        'tbColoniaR
        '
        Me.tbColoniaR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbColoniaR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbColoniaR.Location = New System.Drawing.Point(128, 149)
        Me.tbColoniaR.MaxLength = 150
        Me.tbColoniaR.Name = "tbColoniaR"
        Me.tbColoniaR.Size = New System.Drawing.Size(308, 21)
        Me.tbColoniaR.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(479, 125)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(71, 15)
        Me.Label12.TabIndex = 109
        Me.Label12.Text = "Teléfono :"
        '
        'tbLocalidadR
        '
        Me.tbLocalidadR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbLocalidadR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbLocalidadR.Location = New System.Drawing.Point(308, 108)
        Me.tbLocalidadR.MaxLength = 150
        Me.tbLocalidadR.Name = "tbLocalidadR"
        Me.tbLocalidadR.Size = New System.Drawing.Size(308, 21)
        Me.tbLocalidadR.TabIndex = 6
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(498, 152)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 15)
        Me.Label13.TabIndex = 108
        Me.Label13.Text = "Email :"
        '
        'tbEmailR
        '
        Me.tbEmailR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbEmailR.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEmailR.Location = New System.Drawing.Point(558, 146)
        Me.tbEmailR.MaxLength = 50
        Me.tbEmailR.Name = "tbEmailR"
        Me.tbEmailR.Size = New System.Drawing.Size(209, 21)
        Me.tbEmailR.TabIndex = 13
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(79, 17)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(42, 15)
        Me.Label14.TabIndex = 94
        Me.Label14.Text = "RFC :"
        '
        'gbReceptor
        '
        Me.gbReceptor.Controls.Add(Me.ComboBoxUsoCFDI)
        Me.gbReceptor.Controls.Add(Me.LabelUsoCFDI)
        Me.gbReceptor.Controls.Add(Me.ComboBoxEdo)
        Me.gbReceptor.Controls.Add(Me.ComboBoxMun)
        Me.gbReceptor.Controls.Add(Me.ComboBoxLoc)
        Me.gbReceptor.Controls.Add(Me.Label3)
        Me.gbReceptor.Controls.Add(Me.tbTelefonoR)
        Me.gbReceptor.Controls.Add(Me.Label14)
        Me.gbReceptor.Controls.Add(Me.Label1)
        Me.gbReceptor.Controls.Add(Me.tbEmailR)
        Me.gbReceptor.Controls.Add(Me.tbCodigoPostalR)
        Me.gbReceptor.Controls.Add(Me.Label13)
        Me.gbReceptor.Controls.Add(Me.Label2)
        Me.gbReceptor.Controls.Add(Me.Label10)
        Me.gbReceptor.Controls.Add(Me.tbPaisR)
        Me.gbReceptor.Controls.Add(Me.Label12)
        Me.gbReceptor.Controls.Add(Me.tbColoniaR)
        Me.gbReceptor.Controls.Add(Me.Label11)
        Me.gbReceptor.Controls.Add(Me.Label4)
        Me.gbReceptor.Controls.Add(Me.tbNoIntR)
        Me.gbReceptor.Controls.Add(Me.Label5)
        Me.gbReceptor.Controls.Add(Me.tbNoExtR)
        Me.gbReceptor.Controls.Add(Me.tbReferenciaR)
        Me.gbReceptor.Controls.Add(Me.Label9)
        Me.gbReceptor.Controls.Add(Me.Label6)
        Me.gbReceptor.Controls.Add(Me.tbCalleR)
        Me.gbReceptor.Controls.Add(Me.tbrfcR)
        Me.gbReceptor.Controls.Add(Me.Label8)
        Me.gbReceptor.Controls.Add(Me.Label7)
        Me.gbReceptor.Controls.Add(Me.tbNombreR)
        Me.gbReceptor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbReceptor.Location = New System.Drawing.Point(60, 215)
        Me.gbReceptor.Name = "gbReceptor"
        Me.gbReceptor.Size = New System.Drawing.Size(878, 236)
        Me.gbReceptor.TabIndex = 1
        Me.gbReceptor.TabStop = False
        Me.gbReceptor.Text = "Receptor"
        '
        'ComboBoxUsoCFDI
        '
        Me.ComboBoxUsoCFDI.FormattingEnabled = True
        Me.ComboBoxUsoCFDI.Location = New System.Drawing.Point(557, 173)
        Me.ComboBoxUsoCFDI.Name = "ComboBoxUsoCFDI"
        Me.ComboBoxUsoCFDI.Size = New System.Drawing.Size(308, 23)
        Me.ComboBoxUsoCFDI.TabIndex = 125
        '
        'LabelUsoCFDI
        '
        Me.LabelUsoCFDI.AutoSize = True
        Me.LabelUsoCFDI.Location = New System.Drawing.Point(478, 182)
        Me.LabelUsoCFDI.Name = "LabelUsoCFDI"
        Me.LabelUsoCFDI.Size = New System.Drawing.Size(71, 15)
        Me.LabelUsoCFDI.TabIndex = 124
        Me.LabelUsoCFDI.Text = "Uso CFDI:"
        '
        'ComboBoxEdo
        '
        Me.ComboBoxEdo.FormattingEnabled = True
        Me.ComboBoxEdo.Location = New System.Drawing.Point(557, 63)
        Me.ComboBoxEdo.Name = "ComboBoxEdo"
        Me.ComboBoxEdo.Size = New System.Drawing.Size(309, 23)
        Me.ComboBoxEdo.TabIndex = 123
        '
        'ComboBoxMun
        '
        Me.ComboBoxMun.FormattingEnabled = True
        Me.ComboBoxMun.Location = New System.Drawing.Point(556, 36)
        Me.ComboBoxMun.Name = "ComboBoxMun"
        Me.ComboBoxMun.Size = New System.Drawing.Size(309, 23)
        Me.ComboBoxMun.TabIndex = 122
        '
        'ComboBoxLoc
        '
        Me.ComboBoxLoc.FormattingEnabled = True
        Me.ComboBoxLoc.Location = New System.Drawing.Point(128, 203)
        Me.ComboBoxLoc.Name = "ComboBoxLoc"
        Me.ComboBoxLoc.Size = New System.Drawing.Size(308, 23)
        Me.ComboBoxLoc.TabIndex = 121
        '
        'gbComprobante
        '
        Me.gbComprobante.Controls.Add(Me.Label23)
        Me.gbComprobante.Controls.Add(Me.ComboBoxMetodoDePago)
        Me.gbComprobante.Controls.Add(Me.MetodoDePago)
        Me.gbComprobante.Controls.Add(Me.TextBoxIEPS)
        Me.gbComprobante.Controls.Add(Me.ComboBoxFormaDePago)
        Me.gbComprobante.Controls.Add(Me.FormaDePago)
        Me.gbComprobante.Controls.Add(Me.lblNumCtaPago)
        Me.gbComprobante.Controls.Add(Me.txtNumCtaPago)
        Me.gbComprobante.Controls.Add(Me.Label18)
        Me.gbComprobante.Controls.Add(Me.Label17)
        Me.gbComprobante.Controls.Add(Me.tbTotal)
        Me.gbComprobante.Controls.Add(Me.cbSerie)
        Me.gbComprobante.Controls.Add(Me.tbSubTotal)
        Me.gbComprobante.Controls.Add(Me.Label15)
        Me.gbComprobante.Controls.Add(Me.tbIVA)
        Me.gbComprobante.Controls.Add(Me.Label16)
        Me.gbComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbComprobante.Location = New System.Drawing.Point(60, 12)
        Me.gbComprobante.Name = "gbComprobante"
        Me.gbComprobante.Size = New System.Drawing.Size(691, 134)
        Me.gbComprobante.TabIndex = 0
        Me.gbComprobante.TabStop = False
        Me.gbComprobante.Text = "Comprobante"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(470, 80)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(42, 15)
        Me.Label23.TabIndex = 139
        Me.Label23.Text = "IEPS:"
        '
        'ComboBoxMetodoDePago
        '
        Me.ComboBoxMetodoDePago.FormattingEnabled = True
        Me.ComboBoxMetodoDePago.Location = New System.Drawing.Point(145, 78)
        Me.ComboBoxMetodoDePago.Name = "ComboBoxMetodoDePago"
        Me.ComboBoxMetodoDePago.Size = New System.Drawing.Size(256, 23)
        Me.ComboBoxMetodoDePago.TabIndex = 137
        '
        'MetodoDePago
        '
        Me.MetodoDePago.AutoSize = True
        Me.MetodoDePago.Location = New System.Drawing.Point(21, 84)
        Me.MetodoDePago.Name = "MetodoDePago"
        Me.MetodoDePago.Size = New System.Drawing.Size(118, 15)
        Me.MetodoDePago.TabIndex = 136
        Me.MetodoDePago.Text = "Metodo De Pago:"
        '
        'TextBoxIEPS
        '
        Me.TextBoxIEPS.Enabled = False
        Me.TextBoxIEPS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxIEPS.Location = New System.Drawing.Point(523, 74)
        Me.TextBoxIEPS.MaxLength = 50
        Me.TextBoxIEPS.Name = "TextBoxIEPS"
        Me.TextBoxIEPS.Size = New System.Drawing.Size(128, 21)
        Me.TextBoxIEPS.TabIndex = 138
        '
        'ComboBoxFormaDePago
        '
        Me.ComboBoxFormaDePago.FormattingEnabled = True
        Me.ComboBoxFormaDePago.Location = New System.Drawing.Point(145, 51)
        Me.ComboBoxFormaDePago.Name = "ComboBoxFormaDePago"
        Me.ComboBoxFormaDePago.Size = New System.Drawing.Size(256, 23)
        Me.ComboBoxFormaDePago.TabIndex = 135
        '
        'FormaDePago
        '
        Me.FormaDePago.AutoSize = True
        Me.FormaDePago.Location = New System.Drawing.Point(30, 59)
        Me.FormaDePago.Name = "FormaDePago"
        Me.FormaDePago.Size = New System.Drawing.Size(109, 15)
        Me.FormaDePago.TabIndex = 134
        Me.FormaDePago.Text = "Forma de Pago:"
        '
        'lblNumCtaPago
        '
        Me.lblNumCtaPago.AutoSize = True
        Me.lblNumCtaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumCtaPago.Location = New System.Drawing.Point(14, 108)
        Me.lblNumCtaPago.Name = "lblNumCtaPago"
        Me.lblNumCtaPago.Size = New System.Drawing.Size(125, 15)
        Me.lblNumCtaPago.TabIndex = 133
        Me.lblNumCtaPago.Text = "# Cuenta de Pago:"
        '
        'txtNumCtaPago
        '
        Me.txtNumCtaPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumCtaPago.Location = New System.Drawing.Point(145, 105)
        Me.txtNumCtaPago.MaxLength = 20
        Me.txtNumCtaPago.Name = "txtNumCtaPago"
        Me.txtNumCtaPago.Size = New System.Drawing.Size(173, 21)
        Me.txtNumCtaPago.TabIndex = 132
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(90, 31)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(49, 15)
        Me.Label18.TabIndex = 131
        Me.Label18.Text = "Serie :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(470, 107)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(47, 15)
        Me.Label17.TabIndex = 129
        Me.Label17.Text = "Total :"
        '
        'tbTotal
        '
        Me.tbTotal.Enabled = False
        Me.tbTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbTotal.Location = New System.Drawing.Point(523, 101)
        Me.tbTotal.MaxLength = 50
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Size = New System.Drawing.Size(128, 21)
        Me.tbTotal.TabIndex = 3
        '
        'cbSerie
        '
        Me.cbSerie.DisplayMember = "Serie"
        Me.cbSerie.FormattingEnabled = True
        Me.cbSerie.Location = New System.Drawing.Point(145, 23)
        Me.cbSerie.Name = "cbSerie"
        Me.cbSerie.Size = New System.Drawing.Size(128, 23)
        Me.cbSerie.TabIndex = 0
        Me.cbSerie.ValueMember = "Id"
        '
        'tbSubTotal
        '
        Me.tbSubTotal.Enabled = False
        Me.tbSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbSubTotal.Location = New System.Drawing.Point(523, 20)
        Me.tbSubTotal.MaxLength = 50
        Me.tbSubTotal.Name = "tbSubTotal"
        Me.tbSubTotal.Size = New System.Drawing.Size(128, 21)
        Me.tbSubTotal.TabIndex = 1
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(441, 26)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(76, 15)
        Me.Label15.TabIndex = 128
        Me.Label15.Text = "Sub Total :"
        '
        'tbIVA
        '
        Me.tbIVA.Enabled = False
        Me.tbIVA.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbIVA.Location = New System.Drawing.Point(523, 47)
        Me.tbIVA.MaxLength = 50
        Me.tbIVA.Name = "tbIVA"
        Me.tbIVA.Size = New System.Drawing.Size(128, 21)
        Me.tbIVA.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(470, 53)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 15)
        Me.Label16.TabIndex = 130
        Me.Label16.Text = "I.V.A. :"
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(404, 40)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(173, 21)
        Me.TextBox1.TabIndex = 129
        Me.TextBox1.Visible = False
        '
        'lblMetodoPago
        '
        Me.lblMetodoPago.AutoSize = True
        Me.lblMetodoPago.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMetodoPago.Location = New System.Drawing.Point(282, 43)
        Me.lblMetodoPago.Name = "lblMetodoPago"
        Me.lblMetodoPago.Size = New System.Drawing.Size(116, 15)
        Me.lblMetodoPago.TabIndex = 130
        Me.lblMetodoPago.Text = "Metodo de Pago:"
        Me.lblMetodoPago.Visible = False
        '
        'txtMetodoPag
        '
        Me.txtMetodoPag.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMetodoPag.Location = New System.Drawing.Point(404, 40)
        Me.txtMetodoPag.MaxLength = 50
        Me.txtMetodoPag.Name = "txtMetodoPag"
        Me.txtMetodoPag.Size = New System.Drawing.Size(128, 21)
        Me.txtMetodoPag.TabIndex = 129
        Me.txtMetodoPag.Visible = False
        '
        'lblMetodoPag
        '
        Me.lblMetodoPag.AutoSize = True
        Me.lblMetodoPag.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMetodoPag.Location = New System.Drawing.Point(282, 43)
        Me.lblMetodoPag.Name = "lblMetodoPag"
        Me.lblMetodoPag.Size = New System.Drawing.Size(116, 15)
        Me.lblMetodoPag.TabIndex = 130
        Me.lblMetodoPag.Text = "Metodo de Pago:"
        Me.lblMetodoPag.Visible = False
        '
        'tbDescripcion
        '
        Me.tbDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDescripcion.Location = New System.Drawing.Point(395, 517)
        Me.tbDescripcion.MaxLength = 150
        Me.tbDescripcion.Name = "tbDescripcion"
        Me.tbDescripcion.Size = New System.Drawing.Size(475, 21)
        Me.tbDescripcion.TabIndex = 0
        '
        'tbImporte
        '
        Me.tbImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbImporte.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbImporte.Location = New System.Drawing.Point(456, 20)
        Me.tbImporte.MaxLength = 50
        Me.tbImporte.Name = "tbImporte"
        Me.tbImporte.Size = New System.Drawing.Size(133, 21)
        Me.tbImporte.TabIndex = 1
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(789, 79)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregar.TabIndex = 2
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(789, 109)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminar.TabIndex = 3
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'dgConcepto
        '
        Me.dgConcepto.AllowUserToAddRows = False
        Me.dgConcepto.AllowUserToDeleteRows = False
        Me.dgConcepto.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgConcepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgConcepto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdProdServ, Me.Descripcion, Me.Importe, Me.IdServicio, Me.Servicio, Me.IdImpuestoIVA, Me.Impuesto, Me.TipoFactor, Me.ImporteIVA, Me.IdImpuestoIEPS, Me.Impuesto2, Me.TipoFactor2, Me.ImporteIEPS})
        Me.dgConcepto.Location = New System.Drawing.Point(11, 138)
        Me.dgConcepto.Name = "dgConcepto"
        Me.dgConcepto.ReadOnly = True
        Me.dgConcepto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgConcepto.Size = New System.Drawing.Size(861, 167)
        Me.dgConcepto.TabIndex = 127
        Me.dgConcepto.TabStop = False
        '
        'IdProdServ
        '
        Me.IdProdServ.DataPropertyName = "IdProdServ"
        Me.IdProdServ.HeaderText = "IdProdServ"
        Me.IdProdServ.Name = "IdProdServ"
        Me.IdProdServ.ReadOnly = True
        Me.IdProdServ.Visible = False
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Producto SAT"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 200
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        DataGridViewCellStyle1.Format = "C2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle1
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        Me.Importe.Width = 65
        '
        'IdServicio
        '
        Me.IdServicio.DataPropertyName = "IdServicio"
        Me.IdServicio.HeaderText = "IdServicio"
        Me.IdServicio.Name = "IdServicio"
        Me.IdServicio.ReadOnly = True
        Me.IdServicio.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.ReadOnly = True
        '
        'IdImpuestoIVA
        '
        Me.IdImpuestoIVA.DataPropertyName = "IdImpuestoIVA"
        Me.IdImpuestoIVA.HeaderText = "IdImpuestoIVA"
        Me.IdImpuestoIVA.Name = "IdImpuestoIVA"
        Me.IdImpuestoIVA.ReadOnly = True
        Me.IdImpuestoIVA.Visible = False
        '
        'Impuesto
        '
        Me.Impuesto.DataPropertyName = "Impuesto"
        Me.Impuesto.HeaderText = "ImpuestoIVA"
        Me.Impuesto.Name = "Impuesto"
        Me.Impuesto.ReadOnly = True
        '
        'TipoFactor
        '
        Me.TipoFactor.DataPropertyName = "TipoFactor"
        Me.TipoFactor.HeaderText = "Tipo Factor IVA"
        Me.TipoFactor.Name = "TipoFactor"
        Me.TipoFactor.ReadOnly = True
        '
        'ImporteIVA
        '
        Me.ImporteIVA.DataPropertyName = "ImporteIVA"
        Me.ImporteIVA.HeaderText = "Importe IVA"
        Me.ImporteIVA.Name = "ImporteIVA"
        Me.ImporteIVA.ReadOnly = True
        '
        'IdImpuestoIEPS
        '
        Me.IdImpuestoIEPS.DataPropertyName = "IdImpuestoIEPS"
        Me.IdImpuestoIEPS.HeaderText = "IdImpuestoIEPS"
        Me.IdImpuestoIEPS.Name = "IdImpuestoIEPS"
        Me.IdImpuestoIEPS.ReadOnly = True
        Me.IdImpuestoIEPS.Visible = False
        '
        'Impuesto2
        '
        Me.Impuesto2.DataPropertyName = "Impuesto2"
        Me.Impuesto2.HeaderText = "Impuesto IEPS"
        Me.Impuesto2.Name = "Impuesto2"
        Me.Impuesto2.ReadOnly = True
        '
        'TipoFactor2
        '
        Me.TipoFactor2.DataPropertyName = "TipoFactor2"
        Me.TipoFactor2.HeaderText = "Tipo Factor IEPS"
        Me.TipoFactor2.Name = "TipoFactor2"
        Me.TipoFactor2.ReadOnly = True
        '
        'ImporteIEPS
        '
        Me.ImporteIEPS.DataPropertyName = "ImporteIEPS"
        Me.ImporteIEPS.HeaderText = "Importe IEPS"
        Me.ImporteIEPS.Name = "ImporteIEPS"
        Me.ImporteIEPS.ReadOnly = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(14, 26)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(101, 15)
        Me.Label19.TabIndex = 121
        Me.Label19.Text = "Producto SAT :"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(383, 24)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(64, 15)
        Me.Label21.TabIndex = 129
        Me.Label21.Text = "Importe :"
        '
        'gbConcepto
        '
        Me.gbConcepto.Controls.Add(Me.ComboBoxServ)
        Me.gbConcepto.Controls.Add(Me.LabelServ)
        Me.gbConcepto.Controls.Add(Me.ComboBoxTipFacIEPS)
        Me.gbConcepto.Controls.Add(Me.Label20)
        Me.gbConcepto.Controls.Add(Me.ComboBoxImpIEPS)
        Me.gbConcepto.Controls.Add(Me.Label22)
        Me.gbConcepto.Controls.Add(Me.TextBoxImpIVA)
        Me.gbConcepto.Controls.Add(Me.TextBoxImpIEPS)
        Me.gbConcepto.Controls.Add(Me.LblImpIEPS)
        Me.gbConcepto.Controls.Add(Me.LblImporteIVA)
        Me.gbConcepto.Controls.Add(Me.ComboBoxTipofac)
        Me.gbConcepto.Controls.Add(Me.LabelTipoFactor)
        Me.gbConcepto.Controls.Add(Me.bnAgregar)
        Me.gbConcepto.Controls.Add(Me.bnEliminar)
        Me.gbConcepto.Controls.Add(Me.ComboBoxImpuestos)
        Me.gbConcepto.Controls.Add(Me.LabelImpuesto)
        Me.gbConcepto.Controls.Add(Me.ComboBoxClvProdSer)
        Me.gbConcepto.Controls.Add(Me.dgConcepto)
        Me.gbConcepto.Controls.Add(Me.Label21)
        Me.gbConcepto.Controls.Add(Me.Label19)
        Me.gbConcepto.Controls.Add(Me.tbImporte)
        Me.gbConcepto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbConcepto.Location = New System.Drawing.Point(60, 457)
        Me.gbConcepto.Name = "gbConcepto"
        Me.gbConcepto.Size = New System.Drawing.Size(878, 313)
        Me.gbConcepto.TabIndex = 2
        Me.gbConcepto.TabStop = False
        Me.gbConcepto.Text = "Concepto"
        '
        'ComboBoxServ
        '
        Me.ComboBoxServ.FormattingEnabled = True
        Me.ComboBoxServ.Location = New System.Drawing.Point(684, 16)
        Me.ComboBoxServ.Name = "ComboBoxServ"
        Me.ComboBoxServ.Size = New System.Drawing.Size(188, 23)
        Me.ComboBoxServ.TabIndex = 146
        '
        'LabelServ
        '
        Me.LabelServ.AutoSize = True
        Me.LabelServ.Location = New System.Drawing.Point(616, 22)
        Me.LabelServ.Name = "LabelServ"
        Me.LabelServ.Size = New System.Drawing.Size(62, 15)
        Me.LabelServ.TabIndex = 145
        Me.LabelServ.Text = "Servicio:"
        '
        'ComboBoxTipFacIEPS
        '
        Me.ComboBoxTipFacIEPS.FormattingEnabled = True
        Me.ComboBoxTipFacIEPS.Location = New System.Drawing.Point(479, 76)
        Me.ComboBoxTipFacIEPS.Name = "ComboBoxTipFacIEPS"
        Me.ComboBoxTipFacIEPS.Size = New System.Drawing.Size(164, 23)
        Me.ComboBoxTipFacIEPS.TabIndex = 144
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(355, 83)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(118, 15)
        Me.Label20.TabIndex = 143
        Me.Label20.Text = "Tipo Factor IEPS:"
        '
        'ComboBoxImpIEPS
        '
        Me.ComboBoxImpIEPS.FormattingEnabled = True
        Me.ComboBoxImpIEPS.Location = New System.Drawing.Point(479, 47)
        Me.ComboBoxImpIEPS.Name = "ComboBoxImpIEPS"
        Me.ComboBoxImpIEPS.Size = New System.Drawing.Size(164, 23)
        Me.ComboBoxImpIEPS.TabIndex = 142
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(367, 55)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(105, 15)
        Me.Label22.TabIndex = 141
        Me.Label22.Text = "Impuesto IEPS:"
        '
        'TextBoxImpIVA
        '
        Me.TextBoxImpIVA.Location = New System.Drawing.Point(121, 104)
        Me.TextBoxImpIVA.Name = "TextBoxImpIVA"
        Me.TextBoxImpIVA.Size = New System.Drawing.Size(133, 21)
        Me.TextBoxImpIVA.TabIndex = 140
        '
        'TextBoxImpIEPS
        '
        Me.TextBoxImpIEPS.Location = New System.Drawing.Point(479, 104)
        Me.TextBoxImpIEPS.Name = "TextBoxImpIEPS"
        Me.TextBoxImpIEPS.Size = New System.Drawing.Size(135, 21)
        Me.TextBoxImpIEPS.TabIndex = 139
        '
        'LblImpIEPS
        '
        Me.LblImpIEPS.AutoSize = True
        Me.LblImpIEPS.Location = New System.Drawing.Point(378, 110)
        Me.LblImpIEPS.Name = "LblImpIEPS"
        Me.LblImpIEPS.Size = New System.Drawing.Size(95, 15)
        Me.LblImpIEPS.TabIndex = 138
        Me.LblImpIEPS.Text = "Importe IEPS:"
        '
        'LblImporteIVA
        '
        Me.LblImporteIVA.AutoSize = True
        Me.LblImporteIVA.Location = New System.Drawing.Point(31, 110)
        Me.LblImporteIVA.Name = "LblImporteIVA"
        Me.LblImporteIVA.Size = New System.Drawing.Size(84, 15)
        Me.LblImporteIVA.TabIndex = 137
        Me.LblImporteIVA.Text = "Importe IVA:"
        '
        'ComboBoxTipofac
        '
        Me.ComboBoxTipofac.FormattingEnabled = True
        Me.ComboBoxTipofac.Location = New System.Drawing.Point(121, 76)
        Me.ComboBoxTipofac.Name = "ComboBoxTipofac"
        Me.ComboBoxTipofac.Size = New System.Drawing.Size(164, 23)
        Me.ComboBoxTipofac.TabIndex = 136
        '
        'LabelTipoFactor
        '
        Me.LabelTipoFactor.AutoSize = True
        Me.LabelTipoFactor.Location = New System.Drawing.Point(8, 83)
        Me.LabelTipoFactor.Name = "LabelTipoFactor"
        Me.LabelTipoFactor.Size = New System.Drawing.Size(107, 15)
        Me.LabelTipoFactor.TabIndex = 134
        Me.LabelTipoFactor.Text = "Tipo Factor IVA:"
        '
        'ComboBoxImpuestos
        '
        Me.ComboBoxImpuestos.FormattingEnabled = True
        Me.ComboBoxImpuestos.Location = New System.Drawing.Point(121, 47)
        Me.ComboBoxImpuestos.Name = "ComboBoxImpuestos"
        Me.ComboBoxImpuestos.Size = New System.Drawing.Size(164, 23)
        Me.ComboBoxImpuestos.TabIndex = 132
        '
        'LabelImpuesto
        '
        Me.LabelImpuesto.AutoSize = True
        Me.LabelImpuesto.Location = New System.Drawing.Point(21, 55)
        Me.LabelImpuesto.Name = "LabelImpuesto"
        Me.LabelImpuesto.Size = New System.Drawing.Size(94, 15)
        Me.LabelImpuesto.TabIndex = 131
        Me.LabelImpuesto.Text = "Impuesto IVA:"
        '
        'ComboBoxClvProdSer
        '
        Me.ComboBoxClvProdSer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.ComboBoxClvProdSer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.ComboBoxClvProdSer.FormattingEnabled = True
        Me.ComboBoxClvProdSer.Location = New System.Drawing.Point(121, 18)
        Me.ComboBoxClvProdSer.Name = "ComboBoxClvProdSer"
        Me.ComboBoxClvProdSer.Size = New System.Drawing.Size(242, 23)
        Me.ComboBoxClvProdSer.TabIndex = 130
        '
        'ComboBoxRegFis
        '
        Me.ComboBoxRegFis.FormattingEnabled = True
        Me.ComboBoxRegFis.Location = New System.Drawing.Point(145, 20)
        Me.ComboBoxRegFis.Name = "ComboBoxRegFis"
        Me.ComboBoxRegFis.Size = New System.Drawing.Size(521, 23)
        Me.ComboBoxRegFis.TabIndex = 138
        '
        'LabelRegimenFis
        '
        Me.LabelRegimenFis.AutoSize = True
        Me.LabelRegimenFis.Location = New System.Drawing.Point(29, 23)
        Me.LabelRegimenFis.Name = "LabelRegimenFis"
        Me.LabelRegimenFis.Size = New System.Drawing.Size(111, 15)
        Me.LabelRegimenFis.TabIndex = 137
        Me.LabelRegimenFis.Text = "Regimen Fiscal:"
        '
        'bnGenerar
        '
        Me.bnGenerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGenerar.Location = New System.Drawing.Point(860, 12)
        Me.bnGenerar.Name = "bnGenerar"
        Me.bnGenerar.Size = New System.Drawing.Size(136, 36)
        Me.bnGenerar.TabIndex = 3
        Me.bnGenerar.Text = "&GENERAR"
        Me.bnGenerar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 54)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 4
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBoxRegFis)
        Me.GroupBox1.Controls.Add(Me.LabelRegimenFis)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(60, 152)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(878, 57)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Emisor"
        '
        'FrmGeneracionFF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 774)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnGenerar)
        Me.Controls.Add(Me.gbConcepto)
        Me.Controls.Add(Me.gbComprobante)
        Me.Controls.Add(Me.lblMetodoPago)
        Me.Controls.Add(Me.txtMetodoPag)
        Me.Controls.Add(Me.gbReceptor)
        Me.Controls.Add(Me.lblMetodoPag)
        Me.Controls.Add(Me.tbEstadoR)
        Me.Controls.Add(Me.tbMunicipioR)
        Me.Controls.Add(Me.tbLocalidadR)
        Me.Controls.Add(Me.tbDescripcion)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "FrmGeneracionFF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Generación de Facturas Fiscales (FacturaNet)"
        Me.gbReceptor.ResumeLayout(False)
        Me.gbReceptor.PerformLayout()
        Me.gbComprobante.ResumeLayout(False)
        Me.gbComprobante.PerformLayout()
        CType(Me.dgConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbConcepto.ResumeLayout(False)
        Me.gbConcepto.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbTelefonoR As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbCodigoPostalR As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbPaisR As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbEstadoR As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbMunicipioR As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbReferenciaR As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbrfcR As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbNombreR As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbCalleR As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbNoExtR As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbNoIntR As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tbColoniaR As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbLocalidadR As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbEmailR As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents gbReceptor As System.Windows.Forms.GroupBox
    Friend WithEvents gbComprobante As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tbTotal As System.Windows.Forms.TextBox
    Friend WithEvents cbSerie As System.Windows.Forms.ComboBox
    Friend WithEvents tbIVA As System.Windows.Forms.TextBox
    Friend WithEvents tbSubTotal As System.Windows.Forms.TextBox
    Friend WithEvents tbDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents tbImporte As System.Windows.Forms.TextBox
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents dgConcepto As System.Windows.Forms.DataGridView
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents gbConcepto As System.Windows.Forms.GroupBox
    Friend WithEvents bnGenerar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents lblNumCtaPago As System.Windows.Forms.Label
    Friend WithEvents txtNumCtaPago As System.Windows.Forms.TextBox
    Friend WithEvents lblMetodoPago As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents txtMetodoPag As System.Windows.Forms.TextBox
    Friend WithEvents lblMetodoPag As System.Windows.Forms.Label
    Friend WithEvents ComboBoxMetodoDePago As System.Windows.Forms.ComboBox
    Friend WithEvents MetodoDePago As System.Windows.Forms.Label
    Friend WithEvents ComboBoxFormaDePago As System.Windows.Forms.ComboBox
    Friend WithEvents FormaDePago As System.Windows.Forms.Label
    Friend WithEvents ComboBoxUsoCFDI As System.Windows.Forms.ComboBox
    Friend WithEvents LabelUsoCFDI As System.Windows.Forms.Label
    Friend WithEvents ComboBoxEdo As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxMun As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxLoc As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxTipofac As System.Windows.Forms.ComboBox
    Friend WithEvents LabelTipoFactor As System.Windows.Forms.Label
    Friend WithEvents ComboBoxImpuestos As System.Windows.Forms.ComboBox
    Friend WithEvents LabelImpuesto As System.Windows.Forms.Label
    Friend WithEvents ComboBoxClvProdSer As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBoxRegFis As System.Windows.Forms.ComboBox
    Friend WithEvents LabelRegimenFis As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxImpIVA As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxImpIEPS As System.Windows.Forms.TextBox
    Friend WithEvents LblImpIEPS As System.Windows.Forms.Label
    Friend WithEvents LblImporteIVA As System.Windows.Forms.Label
    Friend WithEvents ComboBoxTipFacIEPS As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxImpIEPS As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TextBoxIEPS As System.Windows.Forms.TextBox
    Friend WithEvents ComboBoxServ As System.Windows.Forms.ComboBox
    Friend WithEvents LabelServ As System.Windows.Forms.Label
    Friend WithEvents IdProdServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdImpuestoIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoFactor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteIVA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdImpuestoIEPS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Impuesto2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoFactor2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteIEPS As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
