﻿Public Class FrmAgregaPaqAdic

    Private Sub CMBLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel1.Click

    End Sub

    Private Sub ButtonCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelar.Click
        Me.Close()
    End Sub

    Private Sub Llena_Paquetes_Adicionales(ByVal oContrato As Long)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
        Dim DS As DataTable = BaseII.ConsultaDT("DameServiciosFacturacion_Telefonia")
        CmbPaqAdic.DisplayMember = "Descripcion"
        CmbPaqAdic.ValueMember = "Clv_Txt"
        CmbPaqAdic.DataSource = DS

    End Sub

    Private Sub Llena_Telefono(ByVal oContrato As Long)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
        Dim DS As DataTable = BaseII.ConsultaDT("DameTelefonos_Facturacion")
        CmbTelefono.DisplayMember = "numero_tel"
        CmbTelefono.ValueMember = "clave"
        CmbTelefono.DataSource = DS
    End Sub

    Private Sub AgregaPaqueteAdicional(ByVal oClv_Session As Long, ByVal oClv_Txt As String, ByVal oOp As Integer, ByVal oClv_Telefono As Integer, ByVal oContrato As String)
        '@Clv_Session bigint,@Clv_Txt varchar(10),@op int,@Clv_Telefono int,@Contrato bigint
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.BigInt, oClv_Session)
        BaseII.CreateMyParameter("@Clv_Txt", SqlDbType.VarChar, oClv_Txt)
        BaseII.CreateMyParameter("@op", SqlDbType.Int, oOp)
        BaseII.CreateMyParameter("@Clv_Telefono", SqlDbType.Int, oClv_Telefono)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.BigInt, oContrato)
        BaseII.ConsultaDT("AgregarServicioAdicionales_Telefonia")
    End Sub


    Private Sub FrmAgregaPaqAdic_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Llena_Paquetes_Adicionales(GloContrato)
        Llena_Telefono(GloContrato)
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        AgregaPaqueteAdicional(gloClv_Session, CmbPaqAdic.SelectedValue, 0, CmbTelefono.SelectedValue, GloContrato)
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class