﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class FrmGeneracionFF

    Dim lista As New List(Of Concepto)
    Dim concepto As Concepto
    Dim subTotal As Decimal
    Dim Serie As String
    Dim Folio As Integer
    Dim id_Estado As String
    Dim id_Municipio As Integer
    Dim id_Localidad As Integer


    Private Sub MuestraDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Serie As String, ByVal Calle As String, ByVal Colonia As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraDatosFiscalesCFDSucursales " + Op.ToString() + ", '" + Serie + "', '" + Calle + "', '" + Colonia + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbSerie.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub MuestraServicios()
        BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim dt As DataTable = BaseII.ConsultaDT("MuestraServicios_CFD")

        If dt.Rows.Count > 0 Then
            ComboBoxServ.DataSource = dt
            ComboBoxServ.DisplayMember = "Descripcion"
            ComboBoxServ.ValueMember = "Clv_Servicio"
        End If
    End Sub


    Private Sub MuestraFormaDePago()
        BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_FormaPago")


        If dt.Rows.Count > 0 Then
            ComboBoxFormaDePago.DataSource = dt
            ComboBoxFormaDePago.DisplayMember = "Descripcion"
            ComboBoxFormaDePago.ValueMember = "id_FormaPago"
        End If
    End Sub

    Private Sub MuestraMetodoDePago()
        BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_Metodos_pago")


        If dt.Rows.Count > 0 Then
            ComboBoxMetodoDePago.DataSource = dt
            ComboBoxMetodoDePago.DisplayMember = "Descripcion"
            ComboBoxMetodoDePago.ValueMember = "c_MetodoPago"
        End If
    End Sub

    Private Sub MuestraUsoCFDI()
        BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@ClvUsuario", SqlDbType.Int, GloClvUsuario)
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_UsoCFDI")


        If dt.Rows.Count > 0 Then
            ComboBoxUsoCFDI.DataSource = dt
            ComboBoxUsoCFDI.DisplayMember = "Descripcion"
            ComboBoxUsoCFDI.ValueMember = "id_UsoCFDI"
        End If
    End Sub


    Private Sub ConsultaCP()
        BaseII.limpiaParametros()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("RegresaDatosByCP", conexion)
        comando.CommandType = CommandType.StoredProcedure

        comando.Parameters.AddWithValue("@CP", tbCodigoPostalR.Text)
        comando.Parameters("@CP").Direction = ParameterDirection.Input

        comando.Parameters.Add("@id_Estado", SqlDbType.VarChar, 10)
        comando.Parameters("@id_Estado").Direction = ParameterDirection.Output

        comando.Parameters.Add("@id_Municipio", SqlDbType.Int)
        comando.Parameters("@id_Municipio").Direction = ParameterDirection.Output

        comando.Parameters.Add("@id_Localidad", SqlDbType.Int)
        comando.Parameters("@id_Localidad").Direction = ParameterDirection.Output

        conexion.Open()
        comando.ExecuteNonQuery()
        conexion.Close()

        id_Estado = comando.Parameters("@id_Estado").Value.ToString()
        id_Municipio = comando.Parameters("@id_Municipio").Value
        id_Localidad = comando.Parameters("@id_Localidad").Value

    End Sub

    Private Sub RegresaFolioCFDX(ByVal Serie As String)
        BaseII.limpiaParametros()
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("RegresaFolio_CFDX", conexion)
        comando.CommandType = CommandType.StoredProcedure

        comando.Parameters.Add("@Folio", SqlDbType.Int)
        comando.Parameters("@Folio").Direction = ParameterDirection.Output

        comando.Parameters.AddWithValue("@Serie", Serie)
        comando.Parameters("@Serie").Direction = ParameterDirection.Input

        conexion.Open()
        comando.ExecuteNonQuery()
        conexion.Close()

        Folio = comando.Parameters("@Folio").Value.ToString()

    End Sub


    Private Sub MuestraEstados()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id_Estado", SqlDbType.VarChar, id_Estado)
        Dim dt As DataTable = BaseII.ConsultaDT("MuestraEstados_byCP")


        If dt.Rows.Count > 0 Then
            ComboBoxEdo.DataSource = dt
            ComboBoxEdo.DisplayMember = "Descripcion"
            ComboBoxEdo.ValueMember = "id_Estado"
        End If
    End Sub

    Private Sub MuestraMunicpios()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id_Estado", SqlDbType.VarChar, id_Estado)
        BaseII.CreateMyParameter("@id_Municipio", SqlDbType.Int, id_Municipio)
        Dim dt As DataTable = BaseII.ConsultaDT("MuestraMunicipios_byCP")


        If dt.Rows.Count > 0 Then
            ComboBoxMun.DataSource = dt
            ComboBoxMun.DisplayMember = "Descripcion"
            ComboBoxMun.ValueMember = "id_Municipio"
        End If
    End Sub

    Private Sub MuestraLocalidad()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@id_Estado", SqlDbType.VarChar, id_Estado)
        BaseII.CreateMyParameter("@id_Localidad", SqlDbType.Int, id_Localidad)
        Dim dt As DataTable = BaseII.ConsultaDT("MuestraLocalidad_byCP")


        If dt.Rows.Count > 0 Then
            ComboBoxLoc.DataSource = dt
            ComboBoxLoc.DisplayMember = "Descripcion"
            ComboBoxLoc.ValueMember = "id_Localidad"
        End If
    End Sub

    Private Sub MuestraClvProdSer()
        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_ClaveProdServ")

        If dt.Rows.Count > 0 Then
            ComboBoxClvProdSer.DataSource = dt
            ComboBoxClvProdSer.DisplayMember = "Descripcion"
            ComboBoxClvProdSer.ValueMember = "id_ClaveProdServ"
        End If
    End Sub

    Private Sub MuestraImpuestos()
        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_Impuesto ")
        Dim dt2 As DataTable = BaseII.ConsultaDT("Get_c_Impuesto ")

        If dt.Rows.Count > 0 Then
            ComboBoxImpuestos.DataSource = dt
            ComboBoxImpuestos.DisplayMember = "Descripcion"
            ComboBoxImpuestos.ValueMember = "id_Impuesto"
        End If
        If dt2.Rows.Count > 0 Then
            ComboBoxImpIEPS.DataSource = dt2
            ComboBoxImpIEPS.DisplayMember = "Descripcion"
            ComboBoxImpIEPS.ValueMember = "id_Impuesto"
        End If

    End Sub

    Private Sub MuestraTipoFactor()
        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_TipoFactor")
        Dim dt2 As DataTable = BaseII.ConsultaDT("Get_c_TipoFactor")

        If dt.Rows.Count > 0 Then
            ComboBoxTipofac.DataSource = dt
            ComboBoxTipofac.DisplayMember = "Descripcion"
            ComboBoxTipofac.ValueMember = "id_TipoFactor"
        End If
        If dt2.Rows.Count > 0 Then
            ComboBoxTipFacIEPS.DataSource = dt2
            ComboBoxTipFacIEPS.DisplayMember = "Descripcion"
            ComboBoxTipFacIEPS.ValueMember = "id_TipoFactor"
        End If
    End Sub

    Private Sub MuestraRegimenFiscal()
        BaseII.limpiaParametros()
        Dim dt As DataTable = BaseII.ConsultaDT("Get_c_RegimenFiscal")

        If dt.Rows.Count > 0 Then
            ComboBoxRegFis.DataSource = dt
            ComboBoxRegFis.DisplayMember = "Descripcion"
            ComboBoxRegFis.ValueMember = "id_RegimenFiscal"
        End If
    End Sub

    Public Sub GeneraFacturaCFD(ByVal Clv_Sucursal As Integer, ByVal Serie As String, ByVal SubTotal As Decimal, ByVal Iva As Decimal, ByVal Total As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        Dim dataSet As New DataSet
        Dim generador As New GeneraCFDFacturaNet.Generador
        Dim cadena As StringBuilder
        Dim arreglo() As Byte
        Dim encoding As New System.Text.UTF8Encoding

        RegresaFolioCFDX(Serie)

        'strSQL.Append("EXEC GeneraFacturaCFDX " + Clv_Sucursal.ToString + ", '" + Serie + "', " + SubTotal.ToString + ", " + Iva.ToString + ", " + Total.ToString + ", '" + txtMetodoPag.Text + "', '" + txtNumCtaPago.Text + "'")
        'strSQL.Append("EXEC GeneraFacturaCFDX " + Clv_Sucursal.ToString + ", '" + Serie + "', " + Folio.ToString + ", " + SubTotal.ToString + ", " + Iva.ToString + ", " + TextBoxIEPS.Text + ", " + Total.ToString + ", '" + ComboBoxMetodoDePago.SelectedValue + "', '" + txtNumCtaPago.Text + "', '" + ComboBoxFormaDePago.SelectedValue + "', '" + ComboBoxRegFis.SelectedValue + "', '" + tbrfcR.Text + "', '" + tbNombreR.Text + "', '" + ComboBoxUsoCFDI.SelectedValue + "', '" + tbCalleR.Text + "', '" + tbNoExtR.Text + "', '" + tbNoIntR.Text + "', '" + tbColoniaR.Text + "', '" + ComboBoxLoc.Text + "', '" + tbReferenciaR.Text + "', '" + ComboBoxMun.Text + "', '" + ComboBoxEdo.Text + "', '" + tbPaisR.Text + "', '" + tbCodigoPostalR.Text + "', '" + tbTelefonoR.Text + "', '" + tbEmailR.Text + "'")
        strSQL.Append("EXEC GeneraFacturaCFDX " + Clv_Sucursal.ToString + ", '" + Serie + "', " + SubTotal.ToString + ", " + Iva.ToString + ", " + TextBoxIEPS.Text + ", " + Total.ToString + ", '" + ComboBoxMetodoDePago.SelectedValue + "', '" + txtNumCtaPago.Text + "', '" + ComboBoxFormaDePago.SelectedValue + "', '" + ComboBoxRegFis.SelectedValue + "', '" + tbrfcR.Text + "', '" + tbNombreR.Text + "', '" + ComboBoxUsoCFDI.SelectedValue + "', '" + tbCalleR.Text + "', '" + tbNoExtR.Text + "', '" + tbNoIntR.Text + "', '" + tbColoniaR.Text + "', '" + ComboBoxLoc.Text + "', '" + tbReferenciaR.Text + "', '" + ComboBoxMun.Text + "', '" + ComboBoxEdo.Text + "', '" + tbPaisR.Text + "', '" + tbCodigoPostalR.Text + "', '" + tbTelefonoR.Text + "', '" + tbEmailR.Text + "'")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try

            dataAdapter.Fill(dataSet)
            'dataSet.Tables(0).TableName = "Comprobante"
            'dataSet.Tables(1).TableName = "Emisor"
            'dataSet.Tables(2).TableName = "DomicilioFiscal"
            'dataSet.Tables(3).TableName = "ExpedidoEn"
            'dataSet.Tables(4).TableName = "Otros"
            'dataSet.Tables(5).TableName = "addenda"
            dataAdapter.Fill(dataSet)
            dataSet.Tables(0).TableName = "Comprobante"
            dataSet.Tables(1).TableName = "Emisor"
            dataSet.Tables(2).TableName = "DomicilioFiscal"
            dataSet.Tables(3).TableName = "ExpedidoEn"
            dataSet.Tables(4).TableName = "Receptor"
            dataSet.Tables(5).TableName = "Cliente"
            dataSet.Tables(6).TableName = "Concepto"
            dataSet.Tables(7).TableName = "Impuestos"
            dataSet.Tables(8).TableName = "TrasladosIVA"
            dataSet.Tables(9).TableName = "TrasladosIEPS"
            dataSet.Tables(10).TableName = "Otros"
            dataSet.Tables(11).TableName = "addenda"

            cadena = GeneraCFD(dataSet)
            arreglo = encoding.GetBytes(cadena.ToString())

            Dim fS As New FileStream(eRutaCFD + Serie + Folio.ToString + ".txt", FileMode.Create)
            fS.Write(arreglo, 0, arreglo.Length)
            fS.Flush()
            fS.Close()
            fS.Dispose()

            Rename(eRutaCFD + Serie + Folio.ToString + ".txt", eRutaCFD + Serie + Folio.ToString + ".ff")

            NueComprobantesCFDX(Serie, Folio, GloUsuario, arreglo)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueComprobantesCFDX(ByVal Serie As String, ByVal Folio As Integer, ByVal Usuario As String, ByVal Archivo As Byte())
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueComprobantesCFDX", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
        par1.Direction = ParameterDirection.Input
        par1.Value = Serie
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Folio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Folio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Usuario", SqlDbType.VarChar, 5)
        par3.Direction = ParameterDirection.Input
        par3.Value = Usuario
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Archivo", SqlDbType.VarBinary, 8000)
        par4.Direction = ParameterDirection.Input
        par4.Value = Archivo
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click
        If ComboBoxClvProdSer.Items.Count <= 0 Then
            MsgBox("Selecciona una descripción.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If tbImporte.Text.Length = 0 Then
            MsgBox("Capura un importe.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If TextBoxImpIVA.Text.Length = 0 Then
            MsgBox("Capura el importe IVA.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxTipofac.Items.Count = 0 Then
            MsgBox("Selecciona un tipo factor para el IVA.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxImpuestos.Items.Count = 0 Then
            MsgBox("Selecciona los impuestos para el IVA.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If TextBoxImpIEPS.Text.Length = 0 Then
            MsgBox("Capura el importe IEPS.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxTipFacIEPS.Items.Count = 0 Then
            MsgBox("Selecciona un tipo factor para el IEPS.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxImpIEPS.Items.Count = 0 Then
            MsgBox("Selecciona los impuestos para el IEPS.", MsgBoxStyle.Information)
            Exit Sub
        End If

        concepto = New Concepto
        concepto.IdProdServ = ComboBoxClvProdSer.SelectedValue
        concepto.descripcion = ComboBoxClvProdSer.Text
        concepto.importe = tbImporte.Text
        concepto.IdServicio = ComboBoxServ.SelectedValue
        concepto.Servicio = ComboBoxServ.Text
        concepto.IdImpuestoIVA = ComboBoxImpuestos.SelectedValue
        concepto.impuesto = ComboBoxImpuestos.Text
        concepto.tipoFactor = ComboBoxTipofac.Text
        concepto.importeIVA = TextBoxImpIVA.Text
        concepto.IdImpuestoIEPS = ComboBoxImpIEPS.SelectedValue
        concepto.impuesto2 = ComboBoxImpIEPS.Text
        concepto.tipoFactor2 = ComboBoxTipFacIEPS.Text
        concepto.importeIEPS = TextBoxImpIEPS.Text

        lista.Add(concepto)
        dgConcepto.DataSource = New List(Of Concepto)
        dgConcepto.DataSource = lista


        Dim x As Integer
        subTotal = 0
        For x = 0 To dgConcepto.Rows.Count - 1
            subTotal = subTotal + dgConcepto.Item("Importe", x).Value
        Next

        Dim y As Integer
        Dim subTotalIVA As Decimal
        subTotalIVA = 0
        For y = 0 To dgConcepto.Rows.Count - 1
            subTotalIVA = subTotalIVA + dgConcepto.Item("ImporteIVA", y).Value
        Next

        Dim z As Integer
        Dim subTotalIEPS As Decimal
        subTotalIEPS = 0
        For z = 0 To dgConcepto.Rows.Count - 1
            subTotalIEPS = subTotalIEPS + dgConcepto.Item("ImporteIEPS", z).Value
        Next

        Dim zT, Zts As Decimal
        zT = 0
        zT = Convert.ToDecimal(subTotal) + Convert.ToDecimal(subTotalIVA) + Convert.ToDecimal(subTotalIEPS)
        Zts = Math.Round(Convert.ToDecimal(zT), 2)


        tbSubTotal.Text = subTotal
        tbIVA.Text = subTotalIVA
        TextBoxIEPS.Text = subTotalIEPS
        tbTotal.Text = Zts


        'tbDescripcion.Clear()
        tbImporte.Clear()
        TextBoxImpIVA.Clear()
        TextBoxImpIEPS.Clear()

    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click
        If dgConcepto.Rows.Count = 0 Then
            MsgBox("Selecciona un concepto.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim pos As Integer
        pos = dgConcepto.CurrentRow.Index
        lista.RemoveAt(pos)

        dgConcepto.DataSource = New List(Of Concepto)
        dgConcepto.DataSource = lista
    End Sub

    Private Sub bnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGenerar.Click

        'If txtMetodoPag.Text.Length = 0 Then
        '    MessageBox.Show("Captura el método de pago.")
        '    Exit Sub
        'End If

        If cbSerie.Items.Count <= 0 Then
            MsgBox("Selecciona una serie.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxFormaDePago.Items.Count <= 0 Then
            MsgBox("Selecciona una forma de pago.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxMetodoDePago.Items.Count <= 0 Then
            MsgBox("Selecciona una método de pago.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxRegFis.Items.Count <= 0 Then
            MsgBox("Selecciona un regimen fiscal.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If ComboBoxUsoCFDI.Items.Count <= 0 Then
            MsgBox("Selecciona un uso de CFDI.", MsgBoxStyle.Information)
            Exit Sub
        End If


        'If txtNumCtaPago.Text.Length > 0 Then
        '    If txtNumCtaPago.Text.Length <> 4 Then
        '        MessageBox.Show("Captura los 4 últimos dígitos de la cuenta de pago.")
        '        Exit Sub
        '    End If
        'End If

        If dgConcepto.Rows.Count = 0 Then
            MsgBox("No se ha agregado al menos un concepto.", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim x As Integer
        subTotal = 0
        For x = 0 To dgConcepto.Rows.Count - 1
            subTotal = subTotal + dgConcepto.Item("Importe", x).Value
        Next

        Dim y As Integer
        Dim subTotalIVA As Decimal
        For y = 0 To dgConcepto.Rows.Count - 1
            subTotalIVA = subTotalIVA + dgConcepto.Item("ImporteIVA", y).Value
        Next

        Dim z As Integer
        Dim subTotalIEPS As Decimal
        For z = 0 To dgConcepto.Rows.Count - 1
            subTotalIEPS = subTotalIEPS + dgConcepto.Item("ImporteIEPS", z).Value
        Next


        For Each row As DataGridViewRow In Me.dgConcepto.Rows
            Dim conexion As New SqlConnection(MiConexion)
            Dim cmd As New System.Data.SqlClient.SqlCommand
            cmd.CommandType = System.Data.CommandType.Text
            cmd.CommandText = "INSERT INTO Tmp_GeneraFacturaCFD VALUES('" & row.Cells(0).Value & "','" & row.Cells(2).Value & "'," & row.Cells(3).Value & ",'" & row.Cells(4).Value & "','" & row.Cells(5).Value & "','" & row.Cells(7).Value & "','" & row.Cells(8).Value & "','" & row.Cells(9).Value & "','" & row.Cells(11).Value & "','" & row.Cells(12).Value & "')"
            cmd.Connection = conexion

            conexion.Open()
            cmd.ExecuteNonQuery()
            conexion.Close()
        Next

        If Decimal.Parse(tbSubTotal.Text) <> subTotal Then
            MsgBox("El subtotal es diferente a la suma de los importes de los conceptos.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Decimal.Parse(tbIVA.Text) <> subTotalIVA Then
            MsgBox("El IVA es diferente a la suma de los importes de los conceptos.", MsgBoxStyle.Information)
            Exit Sub
        End If

        If Decimal.Parse(TextBoxIEPS.Text) <> subTotalIEPS Then
            MsgBox("El IEPS es diferente a la suma de los importes de los conceptos.", MsgBoxStyle.Information)
            Exit Sub
        End If

        GeneraFacturaCFD(Locclv_sucursalglo, cbSerie.Text, tbSubTotal.Text, tbIVA.Text, tbTotal.Text)

        MsgBox("Se generó una Factura Fiscal Electrónica con el nombre de " + cbSerie.Text + Folio.ToString() + ".ff")

        Dim conexion2 As New SqlConnection(MiConexion)
        Dim cmd2 As New System.Data.SqlClient.SqlCommand
        cmd2.CommandType = System.Data.CommandType.Text
        cmd2.CommandText = "delete from Tmp_GeneraFacturaCFD "
        cmd2.Connection = conexion2

        conexion2.Open()
        cmd2.ExecuteNonQuery()
        conexion2.Close()

        Limpirar()

    End Sub



    Function GeneraCFD(ByVal ds As DataSet) As StringBuilder


        Dim i As Integer = 0
        Dim x As Integer = 0
        Dim Caracter As String = ""
        Dim Variable As Decimal = 0
        'Serie = ""
        'Folio = 0
        Dim stringBuilder As New StringBuilder


        'Serie = ds.Tables("Comprobante").Rows(0)("serie").ToString()
        'Folio = CInt(ds.Tables("Comprobante").Rows(0)("folio").ToString())

        'stringBuilder.AppendLine("Outputmode=")
        stringBuilder.AppendLine("<Documento>")
        'stringBuilder.AppendLine("<Factura>")

        'stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Comprobante>")
        stringBuilder.AppendLine("serie=" + ds.Tables("Comprobante").Rows(0)("serie").ToString())
        stringBuilder.AppendLine("folio=" + ds.Tables("Comprobante").Rows(0)("folio").ToString())
        stringBuilder.AppendLine("fecha=" + ds.Tables("Comprobante").Rows(0)("fecha").ToString())
        stringBuilder.AppendLine("tipoDeComprobante=" + ds.Tables("Comprobante").Rows(0)("tipoDeComprobante").ToString())
        stringBuilder.AppendLine("TipoDeComprobanteLeyenda=" + ds.Tables("Comprobante").Rows(0)("TipoDeComprobanteLeyenda").ToString())
        stringBuilder.AppendLine("tituloDocumento=" + ds.Tables("Comprobante").Rows(0)("tituloDocumento").ToString())
        stringBuilder.AppendLine("FormaPago=" + ds.Tables("Comprobante").Rows(0)("formaDePago").ToString())
        stringBuilder.AppendLine("FormaPagoLeyenda=" + ds.Tables("Comprobante").Rows(0)("FormaPagoLeyenda").ToString())
        stringBuilder.AppendLine("condicionesDePago=" + ds.Tables("Comprobante").Rows(0)("condicionesDePago").ToString())
        stringBuilder.AppendLine("MetodoPago=" + ds.Tables("Comprobante").Rows(0)("MetodoPago").ToString())
        stringBuilder.AppendLine("MetodoPagoLeyenda=" + ds.Tables("Comprobante").Rows(0)("MetodoPagoLeyenda").ToString())
        stringBuilder.AppendLine("numCtaPago=" + ds.Tables("Comprobante").Rows(0)("numCtaPago").ToString())
        stringBuilder.AppendLine("subtotal=" + ds.Tables("Comprobante").Rows(0)("subtotal").ToString())
        stringBuilder.AppendLine("descuento=" + ds.Tables("Comprobante").Rows(0)("descuento").ToString())
        stringBuilder.AppendLine("iva=" + ds.Tables("Comprobante").Rows(0)("iva").ToString())
        'stringBuilder.AppendLine("ieps=" + ds.Tables("Comprobante").Rows(0)("ieps").ToString())
        stringBuilder.AppendLine("total=" + ds.Tables("Comprobante").Rows(0)("total").ToString())
        stringBuilder.AppendLine("Moneda=" + ds.Tables("Comprobante").Rows(0)("Moneda").ToString())
        stringBuilder.AppendLine("LugarExpedicion=" + ds.Tables("Comprobante").Rows(0)("LugarExpedicion").ToString())
        stringBuilder.AppendLine("</Comprobante>")

        stringBuilder.AppendLine("<Emisor>")
        stringBuilder.AppendLine("erfc=" + ds.Tables("Emisor").Rows(0)("erfc").ToString())
        stringBuilder.AppendLine("enombre=" + ds.Tables("Emisor").Rows(0)("enombre").ToString())
        stringBuilder.AppendLine("regimenFiscal=" + ds.Tables("Emisor").Rows(0)("regimenFiscal").ToString())
        stringBuilder.AppendLine("RegimenFiscalLeyenda=" + ds.Tables("Emisor").Rows(0)("RegimenFiscalLeyenda").ToString())
        stringBuilder.AppendLine("ecalle=" + ds.Tables("DomicilioFiscal").Rows(0)("ecalle").ToString())
        stringBuilder.AppendLine("enoExterior=" + ds.Tables("DomicilioFiscal").Rows(0)("enoExterior").ToString())
        stringBuilder.AppendLine("enoInterior=" + ds.Tables("DomicilioFiscal").Rows(0)("enoInterior").ToString())
        stringBuilder.AppendLine("ecolonia=" + ds.Tables("DomicilioFiscal").Rows(0)("ecolonia").ToString())
        stringBuilder.AppendLine("elocalidad=" + ds.Tables("DomicilioFiscal").Rows(0)("elocalidad").ToString())
        stringBuilder.AppendLine("ereferencia=" + ds.Tables("DomicilioFiscal").Rows(0)("ereferencia").ToString())
        stringBuilder.AppendLine("emunicipio=" + ds.Tables("DomicilioFiscal").Rows(0)("emunicipio").ToString())
        stringBuilder.AppendLine("eestado=" + ds.Tables("DomicilioFiscal").Rows(0)("eestado").ToString())
        stringBuilder.AppendLine("epais=" + ds.Tables("DomicilioFiscal").Rows(0)("epais").ToString())
        stringBuilder.AppendLine("ecodigoPostal=" + ds.Tables("DomicilioFiscal").Rows(0)("ecodigoPostal").ToString())
        stringBuilder.AppendLine("etel=" + ds.Tables("DomicilioFiscal").Rows(0)("etel").ToString())
        stringBuilder.AppendLine("eemail=" + ds.Tables("DomicilioFiscal").Rows(0)("eemail").ToString())
        stringBuilder.AppendLine("</Emisor>")

        stringBuilder.AppendLine("<Receptor>")
        stringBuilder.AppendLine("rfc=" + ds.Tables("Receptor").Rows(0)("rfc").ToString())
        stringBuilder.AppendLine("nombre=" + ds.Tables("Receptor").Rows(0)("nombre").ToString())
        stringBuilder.AppendLine("noCliente=" + ds.Tables("Receptor").Rows(0)("noCliente").ToString())
        stringBuilder.AppendLine("UsoCFDI=" + ds.Tables("Receptor").Rows(0)("UsoCFDI").ToString())
        stringBuilder.AppendLine("UsoCFDILeyenda=" + ds.Tables("Receptor").Rows(0)("UsoCFDILeyenda").ToString())
        stringBuilder.AppendLine("calle=" + ds.Tables("Cliente").Rows(0)("calle").ToString())
        stringBuilder.AppendLine("noExterior=" + ds.Tables("Cliente").Rows(0)("noExterior").ToString())
        stringBuilder.AppendLine("noInterior=" + ds.Tables("Cliente").Rows(0)("noInterior").ToString())
        stringBuilder.AppendLine("colonia=" + ds.Tables("Cliente").Rows(0)("colonia").ToString())
        stringBuilder.AppendLine("localidad=" + ds.Tables("Cliente").Rows(0)("localidad").ToString())
        stringBuilder.AppendLine("referencia=" + ds.Tables("Cliente").Rows(0)("referencia").ToString())
        stringBuilder.AppendLine("municipio=" + ds.Tables("Cliente").Rows(0)("municipio").ToString())
        stringBuilder.AppendLine("estado=" + ds.Tables("Cliente").Rows(0)("estado").ToString())
        stringBuilder.AppendLine("pais=" + ds.Tables("Cliente").Rows(0)("pais").ToString())
        stringBuilder.AppendLine("codigoPostal=" + ds.Tables("Cliente").Rows(0)("codigoPostal").ToString())
        stringBuilder.AppendLine("tel=" + ds.Tables("Cliente").Rows(0)("tel").ToString())
        stringBuilder.AppendLine("email=" + ds.Tables("Cliente").Rows(0)("email").ToString())
        stringBuilder.AppendLine("</Receptor>")

        If (ds.Tables("ExpedidoEn").Rows(0)("ex_calle").ToString().Length > 0) Then

            'stringBuilder.AppendLine("")
            stringBuilder.AppendLine("<ExpedidoEn>")
            stringBuilder.AppendLine("ex_calle=" + ds.Tables("ExpedidoEn").Rows(0)("ex_calle").ToString())
            stringBuilder.AppendLine("ex_noExterior=" + ds.Tables("ExpedidoEn").Rows(0)("ex_noExterior").ToString())
            stringBuilder.AppendLine("ex_noInterior=" + ds.Tables("ExpedidoEn").Rows(0)("ex_noInterior").ToString())
            stringBuilder.AppendLine("ex_colonia=" + ds.Tables("ExpedidoEn").Rows(0)("ex_colonia").ToString())
            stringBuilder.AppendLine("ex_localidad=" + ds.Tables("ExpedidoEn").Rows(0)("ex_localidad").ToString())
            'stringBuilder.AppendLine("ex_referencia=" + ds.Tables("ExpedidoEn").Rows(0)("ex_referencia").ToString())
            stringBuilder.AppendLine("ex_municipio=" + ds.Tables("ExpedidoEn").Rows(0)("ex_municipio").ToString())
            stringBuilder.AppendLine("ex_estado=" + ds.Tables("ExpedidoEn").Rows(0)("ex_estado").ToString())
            stringBuilder.AppendLine("ex_pais=" + ds.Tables("ExpedidoEn").Rows(0)("ex_pais").ToString())
            stringBuilder.AppendLine("ex_codigoPostal=" + ds.Tables("ExpedidoEn").Rows(0)("ex_codigoPostal").ToString())
            stringBuilder.AppendLine("</ExpedidoEn>")
        End If

        stringBuilder.AppendLine("<Concepto>")

        For x = 0 To dgConcepto.Rows.Count - 1

            If (i < 25) Then
                Caracter = "0"
            Else
                Caracter = ""
            End If

            'Variable = 0
            'Variable = dgConcepto.Item("Importe", x).Value

            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ClaveProdServ=" + ds.Tables("Concepto").Rows(i)("ClvProdServ").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_NoIdentificacion=" + ds.Tables("Concepto").Rows(i)("NoIdentificacion").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_cantidad=" + ds.Tables("Concepto").Rows(i)("cantidad").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ClaveUnidad=" + ds.Tables("Concepto").Rows(i)("ClaveUnidad").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_unidad=" + ds.Tables("Concepto").Rows(i)("unidad").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_descripcion=" + ds.Tables("Concepto").Rows(i)("descripcion").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_valorUnitario=" + ds.Tables("Concepto").Rows(i)("valorUnitario").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_importe=" + ds.Tables("Concepto").Rows(i)("importe").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_Descuento=" + ds.Tables("Concepto").Rows(i)("Descuento").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_factorIVA=" + ds.Tables("Concepto").Rows(i)("factorIVA").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_cuentaPredial=" + ds.Tables("Concepto").Rows(i)("cuentaPredial").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_Pedimento=" + ds.Tables("Concepto").Rows(i)("Pedimento").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_fechaPedimento=" + ds.Tables("Concepto").Rows(i)("fechaPedimento").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_aduana=" + ds.Tables("Concepto").Rows(i)("aduana").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITBase=" + ds.Tables("Concepto").Rows(i)("ITBaseIVA").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITImpuesto=" + ds.Tables("Concepto").Rows(i)("ITImpuestoIVA").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITTipoFactor=" + ds.Tables("Concepto").Rows(i)("ITTipoFactorIVA").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITTasaoCuota=" + ds.Tables("Concepto").Rows(i)("ITTasaoCuotaIVA").ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITImporte=" + ds.Tables("Concepto").Rows(i)("ITImporteIVA").ToString())

            Dim xa As String
            xa = (ds.Tables("Concepto").Rows(i)("ITImporteIEPS").ToString())
            'If (xa <> "0.00" & xa <> "0.0000") Then
            If (xa <> "0.00") Then
                stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITBase2=" + ds.Tables("Concepto").Rows(i)("ITBaseIEPS").ToString())
                stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITImpuesto2=" + ds.Tables("Concepto").Rows(i)("ITImpuestoIEPS").ToString())
                stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITTipoFactor2=" + ds.Tables("Concepto").Rows(i)("ITTipoFactorIEPS").ToString())
                stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITTasaoCuota2=" + ds.Tables("Concepto").Rows(i)("ITTasaoCuotaIEPS").ToString())
                stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_ITImporte2=" + ds.Tables("Concepto").Rows(i)("ITImporteIEPS").ToString())
            End If


            i += 1

        Next

        stringBuilder.AppendLine("</Concepto>")


        stringBuilder.AppendLine("<Impuestos>")
        stringBuilder.AppendLine("TotalImpuestosRetenidos=" + ds.Tables("Impuestos").Rows(0)("TotalImpuestosRetenidos").ToString())
        stringBuilder.AppendLine("TotalImpuestosTrasladados=" + ds.Tables("Impuestos").Rows(0)("TotalImpuestosTrasladados").ToString())

        stringBuilder.AppendLine("<trasladados>")

        Dim a As String = ""
        a = (ds.Tables("TrasladosIVA").Rows(0)("ImpTrasladado1_ImporteIVA").ToString())
        If (a <> "0.00") Then
            stringBuilder.AppendLine("ImpTrasladado1_Impuesto=" + ds.Tables("TrasladosIVA").Rows(0)("ImpTrasladado1_ImpuestoIVA").ToString())
            stringBuilder.AppendLine("ImpTrasladado1_TipoFactor=" + ds.Tables("TrasladosIVA").Rows(0)("ImpTrasladado1_TipoFactorIVA").ToString())
            stringBuilder.AppendLine("ImpTrasladado1_TasaOCuota=" + ds.Tables("TrasladosIVA").Rows(0)("ImpTrasladado1_TasaOCuotaIVA").ToString())
            stringBuilder.AppendLine("ImpTrasladado1_Importe=" + ds.Tables("TrasladosIVA").Rows(0)("ImpTrasladado1_ImporteIVA").ToString())
        End If

        Dim b As String = ""
        b = (ds.Tables("TrasladosIEPS").Rows(0)("ImpTrasladado1_ImporteIEPS").ToString())
        If (b <> "0.00") Then
            stringBuilder.AppendLine("ImpTrasladado2_Impuesto=" + ds.Tables("TrasladosIEPS").Rows(0)("ImpTrasladado1_ImpuestoIEPS").ToString())
            stringBuilder.AppendLine("ImpTrasladado2_TipoFactor=" + ds.Tables("TrasladosIEPS").Rows(0)("ImpTrasladado1_TipoFactorIEPS").ToString())
            stringBuilder.AppendLine("ImpTrasladado2_TasaOCuota=" + ds.Tables("TrasladosIEPS").Rows(0)("ImpTrasladado1_TasaOCuotaIEPS").ToString())
            stringBuilder.AppendLine("ImpTrasladado2_Importe=" + ds.Tables("TrasladosIEPS").Rows(0)("ImpTrasladado1_ImporteIEPS").ToString())
        End If
        stringBuilder.AppendLine("</trasladados>")
        stringBuilder.AppendLine("</Impuestos>")


        stringBuilder.AppendLine("<Otros>")
        stringBuilder.AppendLine("cant_letra=" + ds.Tables("Otros").Rows(0)("cant_letra").ToString())
        stringBuilder.AppendLine("factoriva=" + ds.Tables("Otros").Rows(0)("factoriva").ToString())
        stringBuilder.AppendLine("tipoCambio=" + ds.Tables("Otros").Rows(0)("tipoCambio").ToString())
        stringBuilder.AppendLine("observaciones=" + ds.Tables("Otros").Rows(0)("observaciones").ToString())
        stringBuilder.AppendLine("tipoimpresion=" + ds.Tables("Otros").Rows(0)("tipoimpresion").ToString())
        stringBuilder.AppendLine("formato=" + ds.Tables("Otros").Rows(0)("formato").ToString())
        stringBuilder.AppendLine("expedicion=" + ds.Tables("Otros").Rows(0)("expedicion").ToString())
        stringBuilder.AppendLine("</Otros>")

        'stringBuilder.AppendLine("") 'eb
        'stringBuilder.AppendLine("<addenda>")
        'stringBuilder.AppendLine("</addenda>")

        'stringBuilder.AppendLine("") 'eb
        stringBuilder.AppendLine("</Documento>")
        'stringBuilder.AppendLine("</Factura>")
        'stringBuilder.AppendLine("</Documento>")

        Return stringBuilder

    End Function

    Private Sub Limpirar()

        tbSubTotal.Clear()
        tbIVA.Clear()
        tbTotal.Clear()
        tbrfcR.Clear()
        tbNombreR.Clear()
        tbCalleR.Clear()
        tbNoExtR.Clear()
        tbNoIntR.Clear()
        tbColoniaR.Clear()
        tbLocalidadR.Clear()
        tbReferenciaR.Clear()
        tbMunicipioR.Clear()
        tbEstadoR.Clear()
        tbPaisR.Clear()
        tbCodigoPostalR.Clear()
        tbTelefonoR.Clear()
        tbEmailR.Clear()
        txtMetodoPag.Text = ""
        txtNumCtaPago.Text = ""
        TextBoxIEPS.Clear()
        ComboBoxLoc.SelectedIndex = -1
        ComboBoxMun.SelectedIndex = -1
        ComboBoxEdo.SelectedIndex = -1
        ComboBoxClvProdSer.SelectedIndex = -1
        ComboBoxServ.SelectedIndex = -1

        lista = New List(Of Concepto)
        dgConcepto.DataSource = lista

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmGeneracionFF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'colorea(Me)
        MuestraDatosFiscalesCFDSucursales(0, "", "", "")
        MuestraFormaDePago()
        MuestraMetodoDePago()
        MuestraClvProdSer()
        MuestraImpuestos()
        MuestraTipoFactor()
        MuestraRegimenFiscal()
        MuestraServicios()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    'Private Sub txtNumCtaPago_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumCtaPago.KeyPress
    '    e.KeyChar = ChrW(ValidaKey(txtNumCtaPago, Asc(e.KeyChar), "N"))
    'End Sub
#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

    Private Sub tbCodigoPostalR_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbCodigoPostalR.KeyUp

        If e.KeyCode = Keys.Enter Then
            If tbCodigoPostalR.TextLength = 5 Then
                ConsultaCP()
                MuestraEstados()
                MuestraMunicpios()
                MuestraLocalidad()
                MuestraUsoCFDI()
            Else
                MessageBox.Show("Código Postal incorrecto, ingresar uno válido")
            End If
        End If

    End Sub

    Private Sub findButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim index As Integer
        index = ComboBoxClvProdSer.FindString(ComboBoxClvProdSer.Text)
        ComboBoxClvProdSer.SelectedIndex = index
    End Sub
End Class



Public Class Concepto

    Dim _descripcion As String
    Dim _importe As Decimal
    Dim _importeIVA As Decimal
    Dim _importeIEPS As Decimal
    Dim _tipoFactor As String
    Dim _impuesto As String
    Dim _tipoFactor2 As String
    Dim _impuesto2 As String
    Dim _IdProdServ As String
    Dim _IdServicio As Integer
    Dim _Servicio As String
    Dim _IdImpuestoIVA As String
    Dim _IdImpuestoIEPS As String


    Public Property IdProdServ() As String
        Get
            Return _IdProdServ
        End Get
        Set(ByVal Value As String)
            _IdProdServ = Value
        End Set
    End Property
    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property
    Public Property importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal Value As Decimal)
            _importe = Value
        End Set
    End Property
    Public Property IdServicio() As Integer
        Get
            Return _IdServicio
        End Get
        Set(ByVal Value As Integer)
            _IdServicio = Value
        End Set
    End Property
    Public Property Servicio() As String
        Get
            Return _Servicio
        End Get
        Set(ByVal Value As String)
            _Servicio = Value
        End Set
    End Property
    Public Property IdImpuestoIVA() As String
        Get
            Return _IdImpuestoIVA
        End Get
        Set(ByVal Value As String)
            _IdImpuestoIVA = Value
        End Set
    End Property
    Public Property impuesto() As String
        Get
            Return _impuesto
        End Get
        Set(ByVal Value As String)
            _impuesto = Value
        End Set
    End Property
    Public Property tipoFactor() As String
        Get
            Return _tipoFactor
        End Get
        Set(ByVal Value As String)
            _tipoFactor = Value
        End Set
    End Property
    Public Property importeIVA() As Decimal
        Get
            Return _importeIVA
        End Get
        Set(ByVal Value As Decimal)
            _importeIVA = Value
        End Set
    End Property
    Public Property IdImpuestoIEPS() As String
        Get
            Return _IdImpuestoIEPS
        End Get
        Set(ByVal Value As String)
            _IdImpuestoIEPS = Value
        End Set
    End Property
    Public Property impuesto2() As String
        Get
            Return _impuesto2
        End Get
        Set(ByVal Value As String)
            _impuesto2 = Value
        End Set
    End Property
    Public Property tipoFactor2() As String
        Get
            Return _tipoFactor2
        End Get
        Set(ByVal Value As String)
            _tipoFactor2 = Value
        End Set
    End Property
    Public Property importeIEPS() As Decimal
        Get
            Return _importeIEPS
        End Get
        Set(ByVal Value As Decimal)
            _importeIEPS = Value
        End Set
    End Property

End Class