Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class FrmCortesdeFacturas
    Private customersByCityReport As ReportDocument
    'Private Const PARAMETER_FIELD_NAME As String = "Op"
    Private Titulo As String
    Private bndfiscal As Boolean = False


    Private Sub ConfigureCrystalReportsResumen(ByVal op As Integer)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim dSet As New DataSet
        Dim mySelectFormula As String = " "
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"



        If Me.ORSTATUS.Checked = True Then
            OpOrdenar = "2"
        Else
            OpOrdenar = "1"
        End If

        If GloOpRepGral = "R" Then GloOpRepGral = "T"

        Dim reportPath As String = Nothing
        If GloOpRepGral = "V" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt" 'ok
                    op = 11
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt" 'ok
                    op = 12
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt" 'ok
                    ElseIf bndfiscal = True Then
                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt" 'ok
                    End If
                    op = 13
                Case 4
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt" 'ok
                    op = 14
                Case 7
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt" 'ok
                    op = 101
                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros y Facturas con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas de Ventas por Cajero y Facturas con Puntos."
                    End If
                Case Else
                    reportPath = RutaReportes + "\ReporteResumenFacturas.rpt" 'ok
                    op = 10
            End Select
        ElseIf GloOpRepGral = "C" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt" 'ok
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCaja.rpt" 'ok
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteCorteFacturas_Sucursal(Resumen).rpt" 'ok
                    ElseIf bndfiscal = True Then
                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt" 'ok
                    End If
                Case 7
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorCajera.rpt" 'ok
                    op = 100

                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
                    End If
                Case Else
                    reportPath = RutaReportes + "\ReporteResumenFacturas.rpt" 'ok
            End Select
        ElseIf GloOpRepGral = "T" Then
            Select Case op
                Case 1
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt"
                Case 2
                    reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt" 'ok
                    op = 16
                Case 3
                    If bndfiscal = False Then
                        reportPath = RutaReportes + "\ReporteResumenFacturas_PorSucursal.rpt" 'ok
                    ElseIf bndfiscal = True Then
                        reportPath = RutaReportes + "\CorteFacturasFiscalSucursal(Resumen).rpt" 'ok
                    End If
                    op = 16
                Case 7
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Resumen.rpt" 'ok
                    op = 102
                    If (Resumen2 = 1 And NomCajera = "TUSU") Then
                        mySelectFormula = "Resumen de Facturas por Todos los Cajeros y Facturas Con Puntos."
                    Else
                        mySelectFormula = "Resumen de Facturas por Cajero y Facturas Con Puntos."
                    End If
            End Select
        End If


        If op = 1 Then

            If (Resumen2 = 1 And NomCajera = "TUSU") Then
                mySelectFormula = "Resumen de Facturas por Todos los Cajeros."
            Else
                mySelectFormula = "Resumen de Facturas por Cajero."
            End If
            If GloOpRepGral = "T" Then
                op = 15
            End If

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0)

        ElseIf op = 2 Then

            If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
                mySelectFormula = "Resumen de Facturas por Todos los Puntos de Ventas de Cajas."
            Else
                mySelectFormula = "Resumen de Facturas Punto de Venta de Cajas."
            End If

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", SelCajaResumen, NomSucursal, OpOrdenar, Resumen, Resumen2, 0)

        ElseIf op = 3 Then
            If bndfiscal = False Then

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Cajas."
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal de Cajas."
                End If

                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0)

            ElseIf bndfiscal = True Then
                bndfiscal = False
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Cajas."
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Cajas."
                End If

                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)

            End If
        End If



        If op = 11 Then
            If (Resumen2 = 1 And NomCajera = "10000") Then
                mySelectFormula = "Resumen de Facturas por Todos los Vendedores."
            Else
                mySelectFormula = "Resumen de Facturas por Vendedor."
            End If

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, NomCajera)

        ElseIf op = 12 Then
            If (Resumen1 = 1 And CStr(SelCajaResumen) = "888") Then
                mySelectFormula = "Resumen de Facturas por Todos los Puntos de Venta de Ventas."
            Else
                mySelectFormula = "Resumen de Facturas por Punto de Venta de Ventas."
            End If

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", SelCajaResumen, NomSucursal, OpOrdenar, Resumen, Resumen2, 0)

        ElseIf op = 13 Then

            If bndfiscal = False Then

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales de Ventas."
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal de Ventas."
                End If

                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0)

            
            ElseIf bndfiscal = True Then
                bndfiscal = False

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales de Ventas."
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal de Ventas."
                End If

                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)
                
            End If
        ElseIf op = 14 Then

            If (Resumen2 = 1 And NomCajera = "TUSU") Then
                mySelectFormula = "Resumen de Facturas de Ventas por Todos los Cajeros."
            Else
                mySelectFormula = "Resumen de Facturas de Ventas por Cajero."
            End If

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0)

        ElseIf op = 16 Then
            If bndfiscal = False Then

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas por Todas las Sucursales"
                Else
                    mySelectFormula = "Resumen de Facturas por Sucursal"
                End If

                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, Resumen, Resumen2, 0)

            ElseIf bndfiscal = True Then
                bndfiscal = False
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                If (Resumen1 = 1 And NomSucursal = "999") Then
                    mySelectFormula = "Resumen de Facturas Fiscales por Todas las Sucursales"
                Else
                    mySelectFormula = "Resumen de Facturas Fiscales por Sucursal"
                End If

                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)

            End If
        ElseIf op >= 100 Then

            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, Resumen, Resumen2, 0)

        End If

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(dSet)

        Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin

        Dim sucursal As String = Nothing
        sucursal = "Sucursal: " + GloNomSucursal

        If GloOpRepGral = "R" Or GloOpRepGral = "T" Then
            If op = 3 Or op = 2 Or op = 16 Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        ElseIf GloOpRepGral <> "R" Or GloOpRepGral <> "T" Then
            If op = 3 Or op = 13 Or op = 16 Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        End If

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        customersByCityReport.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"

        CrystalReportViewer1.ReportSource = customersByCityReport
        'SetDBLogonForReport2(connectionInfo)
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        customersByCityReport = Nothing
    End Sub


    Private Sub ConfigureCrystalReportsFac(ByVal op As Integer, ByVal Titulo As String)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        Dim mySelectFormula As String = Nothing
        Dim Fecha1 As String = " "
        Dim Extra As String = " "
        Dim OpOrdenar As String = "0"
        Dim dSet As New DataSet

        If GloOpRepGral = "R" Then GloOpRepGral = "T"

        If GloOpRepGral = "V" Then
            Select Case op
                Case 0 : op = 10
                    Extra = ""
                Case 1 : op = 11
                    Extra = "Vendedor: " & ExtraT
                Case 2 : op = 12
                    Extra = "Caja: " & ExtraT
                Case 3 : op = 13
                    Extra = "Sucursal: " & ExtraT
                Case 4 : op = 14
                    Extra = "Cajera(o): " & ExtraT
                Case 7 : op = 101
                    Extra = "Cajera(o):" & ExtraT
            End Select
        ElseIf GloOpRepGral = "C" Then
            Select Case op
                Case 0
                    Extra = " "
                Case 1
                    Extra = "Cajera(o): " & ExtraT
                Case 2
                    Extra = "Caja: " & ExtraT
                Case 3
                    Extra = "Sucursal: " & ExtraT
                Case 7
                    Extra = "Cajera(o) :" & ExtraT
                    op = 100
            End Select
        ElseIf GloOpRepGral = "T" Then
            Select Case op
                Case 1
                    Extra = "Cajera(o): " & ExtraT
                    op = 15
                Case 2
                    Extra = "Sucursal: " & ExtraT
                    op = 16
                Case 3
                    Extra = "Sucursal: " & ExtraT
                    op = 16
                Case 7
                    Extra = "Cajera(o): " & ExtraT
                    op = 102
            End Select

        End If

        If Me.ORSTATUS.Checked = True Then
            OpOrdenar = "2"
        Else
            OpOrdenar = "1"
        End If

        Dim reportPath As String = Nothing
        If op = 0 Or op = 10 Then
            If bndfiscal = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(general).rpt" 'ok
            ElseIf bndfiscal = True Then
                reportPath = RutaReportes + "\CorteFacturasFiscal.rpt" 'ok
            End If
        ElseIf op = 12 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajas).rpt" 'ok
        ElseIf op = 3 Or op = 13 Or op = 16 Then
            If bndfiscal = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(sucursales).rpt" 'ok
            ElseIf bndfiscal = True Then
                reportPath = RutaReportes + "\CorteFacturasFiscalSucursal.rpt" 'ok
            End If

        ElseIf op = 14 Or op = 15 Then
            If GloTipoUsuario = 21 Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1Cajero.rpt" 'ok
            Else
                reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)ventas1.rpt" 'ok
            End If

        ElseIf op = 1 Then
            If Locbndcortedet = False Then
                If GloTipoUsuario = 21 Then
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)Cajero.rpt" 'ok
                Else
                    reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt" 'ok
                End If

                'reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)LO.rpt"
            Else
                'reporte nuevo
                reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt" 'ok, checar bien
            End If
        ElseIf op = 11 Then
            If Locbndcortedet = False Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(vendedores).rpt" 'ok
            ElseIf Locbndcortedet = True Then
                reportPath = RutaReportes + "\Reportecortesdetallado(cajero).rpt" 'ok, checar bien
            End If
        ElseIf op = 2 Then
            reportPath = RutaReportes + "\ReporteCorteFacturas(cajas)1.rpt" 'ok
        ElseIf op >= 100 Then
            If GloTipoUsuario = 21 Then
                reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros)Cajero.rpt" 'ok
            Else
                reportPath = RutaReportes + "\ReporteCorteFacturas(cajeros).rpt" 'ok
            End If
        End If

        If op = 0 Then
            If bndfiscal = False Then

                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, 0, OpOrdenar, False, Resumen2, 0)

            ElseIf bndfiscal = True Then

                mySelectFormula = "Corte General de Cajas de Facturas Fiscales"
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If
                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, 0, OpOrdenar)

            End If
        ElseIf op = 1 Then
            If Locbndcortedet = False Then

                mySelectFormula = "Corte por Cajero(a)"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, False, Resumen2, 0)

            ElseIf Locbndcortedet = True Then

                mySelectFormula = "Corte Detallado por Cajero(a)"
                dSet = corte_facturas_detallado(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, 0)

            End If

        ElseIf op = 2 Then

            mySelectFormula = "Corte por Punto de Venta de Cajas"
            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", NomCaja, 0, OpOrdenar, False, 0, 0)

        ElseIf op = 3 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Cajas"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, False, 0, 0)

            ElseIf bndfiscal = True Then
                bndfiscal = False

                mySelectFormula = "Corte Fiscal por Sucursal de Cajas"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)

            End If
        End If

        If op = 10 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte General de Ventas"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, 0, OpOrdenar, False, Resumen2, 0)

            ElseIf bndfiscal = True Then
                mySelectFormula = "Corte General de Ventas de Facturas Fiscales"

                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If

                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, 0, OpOrdenar)

            End If

        ElseIf op = 11 Then
            If Locbndcortedet = False Then

                mySelectFormula = "Corte por Vendedor(a)"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, False, Resumen2, NomCajera)

            ElseIf Locbndcortedet = True Then

                mySelectFormula = "Corte Detallado por Vendedor(a)"
                dSet = corte_facturas_detallado(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, NomCajera)

            End If
        ElseIf op = 12 Then

            mySelectFormula = "Corte por Punto de Venta de Ventas"
            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", NomCaja, 0, OpOrdenar, False, 0, 0)

        ElseIf op = 13 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Ventas"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, False, 0, 0)

            ElseIf bndfiscal = True Then

                mySelectFormula = "Corte Fiscal por Sucursal de Ventas"
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If
                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)

            End If

        ElseIf op = 14 Then

            mySelectFormula = "Corte por Cajero(a) de Ventas"
            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, False, Resumen2, 0)

        ElseIf op = 15 Then

            mySelectFormula = "Corte por Cajero(a)"
            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, False, Resumen2, 0)

        ElseIf op = 16 Then
            If bndfiscal = False Then

                mySelectFormula = "Corte por Sucursal de Ventas y Cajas"
                dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, "0", 0, NomSucursal, OpOrdenar, False, 0, 0)

            ElseIf bndfiscal = True Then

                mySelectFormula = "Corte Fiscal Por Sucursal de Ventas y Cajas"
                If GloOpRepGral = "T" Then
                    GloOpRepGral = "R"
                End If
                dSet = Corte_Facturas_Fiscal(0, Fecha_ini, Fecha_Fin, GloOpRepGral, NomSucursal, OpOrdenar)

            End If
        End If

        If op >= 100 Then
            mySelectFormula = "Corte por Cajero(a) y Facturas Con Puntos"
            dSet = Corte_de_facturas(Fecha_ini, Fecha_Fin, op, NomCajera, 0, 0, OpOrdenar, False, Resumen2, 0)

        End If

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(dSet)

        Fecha1 = "Fecha Inicial: " & Fecha_ini & " Fecha Final: " & Fecha_Fin

        Dim sucursal As String = Nothing
        sucursal = "Sucursal: " + GloNomSucursal

        If GloOpRepGral = "T" Or GloOpRepGral = "R" Then
            If op = 3 Or op = 2 Or op = 16 Then
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
        ElseIf GloOpRepGral <> "T" Or GloOpRepGral <> "R" Then
            If op = 3 Or op = 13 Or op = 16 Or op = 12 Or op = 10 Then
                If op = 3 Or op = 13 Or op = 16 And bndfiscal = True Then
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                Else
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
                If op = 10 And bndfiscal = False Then
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                ElseIf op = 10 And bndfiscal = True Then
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                End If
            Else
                customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
            End If
            If op = 11 Then
                If Locbndcortedet = True Then
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                Else
                    customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                End If
            End If
        End If

        If op = 12 Then
            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        End If

        If bndfiscal = True Then
            bndfiscal = False
        End If

        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocNomEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
        customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & Fecha1 & "'"
        customersByCityReport.DataDefinition.FormulaFields("NomCaja").Text = "'" & Extra & "'"
        customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & sucursal & "'"
        Locbndcortedet = False


        CrystalReportViewer1.ReportSource = customersByCityReport
        SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub





    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Reporte_cortesTableAdapter.Connection = CON
        Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
        CON.Close()
        If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            'ConfigureCrystalReportsFac(op, Titulo)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 4) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 5) Then
            op = 0
            bndfiscal = True
            My.Forms.FrmOpRep.Show()
            Titulo = "General de Facturas Fiscales"
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 6) Then
            op = 3
            bndfiscal = True
            My.Forms.FrmOpRep.Show()
            Titulo = "General De Facturas Fiscales por Sucursal"
        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 7) Then
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            'bndreportecajeropuntos = True
            My.Forms.FrmOpRep.Show()
            Titulo = "Reporte Por Cajero y Facturas Con Puntos"

            '            MsgBox("En Proceso De Desarrollo", MsgBoxStyle.Information)

            '           Exit Sub
        Else
            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
            My.Forms.FrmOpRep.Show()
            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
        End If
    End Sub

    Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
                'ConfigureCrystalReportsFac(op, Titulo)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 4) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 5) Then
                op = 0
                bndfiscal = True
                My.Forms.FrmOpRep.Show()
                Titulo = "General de Facturas Fiscales"
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 6) Then
                op = 3
                bndfiscal = True
                My.Forms.FrmOpRep.Show()
                Titulo = "General De Facturas Fiscales por Sucursal"
            ElseIf (Me.DataGridView1.SelectedCells(0).Value = 7) Then
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                'bndreportecajeropuntos = True
                My.Forms.FrmOpRep.Show()
                Titulo = "Reporte Por Cajero y Facturas Con Puntos"
            Else
                op = CStr(Me.DataGridView1.SelectedCells(0).Value)
                My.Forms.FrmOpRep.Show()
                Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
            End If

        End If
    End Sub



    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        myReportDocument.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmCortesdeFacturas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If BanderaReporte = True And Resumen = False Then
            BanderaReporte = False
            Me.ConfigureCrystalReportsFac(op, Titulo)
        ElseIf Resumen3 = True Then
            Resumen3 = False
            If Resumen = True And (Resumen1 = 1 Or Resumen2 = 1) Then
                Me.ConfigureCrystalReportsResumen(op)

            End If
        End If
    End Sub

    Private Sub CatalogodeReportes_Factura(ByVal PRMOP As Integer, ByVal PRMTIPOUSUARIO As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@op", SqlDbType.Int, PRMOP)
            BaseII.CreateMyParameter("@TipoUsuario", SqlDbType.Int, PRMTIPOUSUARIO)
            DataGridView1.DataSource = (BaseII.ConsultaDT("CatalogodeReportes_Factura"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FrmCortesdeFacturas_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.HandleDestroyed

    End Sub

    Private Sub FrmCortesdeFacturas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientos_arnoldo.Borra_Reporte_cortes' Puede moverla o quitarla seg�n sea necesario.
        Me.Borra_Reporte_cortesTableAdapter.Connection = CON
        Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter.Connection = CON
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter.Fill(Me.Procedimientos_arnoldo.MUESTRAOP_REPORTES_FACTURA, 0)
        GloOpRepGral = "C"
        CatalogodeReportes_Factura(0, GloTipoUsuario)
        'Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
        'Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 0)
        CON.Close()
    End Sub

    'Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
    '    Me.Borra_Reporte_cortesTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Reporte_cortes)
    '    If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        'ConfigureCrystalReportsFac(op, Titulo)
    '    ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    Else
    '        op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '        My.Forms.FrmOpRep.Show()
    '        Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '    End If
    'End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub FrmCortesdeFacturas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp

    End Sub

    Private Sub CrystalReportViewer1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CrystalReportViewer1.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SplitContainer1_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitContainer1.Panel1.Paint

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub


    Private Sub ComboBox1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If (Me.ComboBox2.Text = "Cajas") Then
            GloOpRepGral = "C"
            CatalogodeReportes_Factura(0, GloTipoUsuario)
            'Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            'Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 0)
        ElseIf (Me.ComboBox2.Text = "Ventas") Then
            GloOpRepGral = "V"
            CatalogodeReportes_Factura(1, GloTipoUsuario)
            'Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            'Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 1)
        ElseIf (Me.ComboBox2.Text = "Resumen") Then
            GloOpRepGral = "T"
            CatalogodeReportes_Factura(2, GloTipoUsuario)
            'Me.CatalogodeReportes_FacturaTableAdapter.Connection = CON
            'Me.CatalogodeReportes_FacturaTableAdapter.Fill(Me.NewsoftvDataSet.CatalogodeReportes_Factura, 2)
        End If
        CON.Close()
    End Sub


    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub DataGridView1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DataGridView1.KeyPress
    '    If Asc(e.KeyChar) = 13 Then
    '        If (Me.DataGridView1.SelectedCells(0).Value = 0) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 1) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '            'ConfigureCrystalReportsFac(op, Titulo)
    '        ElseIf (Me.DataGridView1.SelectedCells(0).Value = 2) Then
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        Else
    '            op = CStr(Me.DataGridView1.SelectedCells(0).Value)
    '            My.Forms.FrmOpRep.Show()
    '            Titulo = CStr(Me.DataGridView1.SelectedCells(1).Value)
    '        End If

    '    End If
    'End Sub

    Private Function Corte_de_facturas(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Op As Integer, ByVal SelCajera As String, ByVal SelCaja As Integer, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer, ByVal Resumen As Boolean, ByVal SelCaja1 As Integer, ByVal clv_vendedor As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Corte_de_facturas;1")
        tableNameList.Add("General")
        tableNameList.Add("Parametros")
        tableNameList.Add("Reporte_Cortes_Liga")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@SelCajera", SqlDbType.VarChar, SelCajera, 250)
        BaseII.CreateMyParameter("@SelCaja", SqlDbType.Int, SelCaja)
        BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, SelSucursal)
        BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, Op_Ordena)
        BaseII.CreateMyParameter("@Resumen", SqlDbType.Bit, Resumen)
        BaseII.CreateMyParameter("@SelCaja1", SqlDbType.Int, SelCaja1)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, clv_vendedor)
        Return BaseII.ConsultaDS("Corte_de_facturas", tableNameList)
    End Function

    Private Function Corte_Facturas_Fiscal(ByVal op As Integer, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Tipo As String, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Corte_Facturas_Fiscal;1")
        tableNameList.Add("General")
        tableNameList.Add("Parametros")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@op", SqlDbType.Int, op)
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        BaseII.CreateMyParameter("@Tipo", SqlDbType.VarChar, Tipo, 1)
        BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, SelSucursal)
        BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, Op_Ordena)
        Return BaseII.ConsultaDS("Corte_Facturas_Fiscal", tableNameList)
    End Function

    Private Function corte_facturas_detallado(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Op As Integer, ByVal SelCajera As String, ByVal SelCaja As Integer, ByVal SelSucursal As Integer, ByVal Op_Ordena As Integer, ByVal clv_vendedor As Integer)
        Dim tableNameList As New List(Of String)
        tableNameList.Add("corte_facturas_detallado;1")
        tableNameList.Add("General")
        tableNameList.Add("Parametros")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FechaIni", SqlDbType.DateTime, FechaIni)
        BaseII.CreateMyParameter("@FechaFin", SqlDbType.DateTime, FechaFin)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@SelCajera", SqlDbType.VarChar, SelCajera, 250)
        BaseII.CreateMyParameter("@SelCaja", SqlDbType.Int, SelCaja)
        BaseII.CreateMyParameter("@SelSucursal", SqlDbType.Int, SelSucursal)
        BaseII.CreateMyParameter("@Op_Ordena", SqlDbType.Int, Op_Ordena)
        BaseII.CreateMyParameter("@clv_vendedor", SqlDbType.Int, clv_vendedor)
        Return BaseII.ConsultaDS("corte_facturas_detallado", tableNameList)
    End Function
End Class