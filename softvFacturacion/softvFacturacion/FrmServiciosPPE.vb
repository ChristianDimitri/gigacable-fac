Imports System.Data.SqlClient

Public Class FrmServiciosPPE


    Private Sub FrmServiciosPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Today, 0)
        eFechaServidor = Today
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.EricDataSet.DAMEFECHADELSERVIDOR, eFechaServidor)
        CON.Close()
        Me.DateTimePicker1.Value = eFechaServidor
        Me.DateTimePicker1.MinDate = eFechaServidor
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, Me.TextBox1.Text, "", "", Today, 1)
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, Me.TextBox1.Text, "", "", Today, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", Me.TextBox2.Text, "", Today, 2)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", Me.TextBox2.Text, "", Today, 2)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", Me.TextBox3.Text, Today, 3)
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", Me.TextBox3.Text, Today, 3)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Res As Integer = 0
        If Me.MuestraProgramacionPPEDataGridView.RowCount > 0 Then
            Res = MsgBox("Haz Seleccionado " & Me.DescripcionLabel1.Text & " a transmitirse el d�a " & Me.FechaLabel1.Text & ". ��sto es correcto?", MsgBoxStyle.YesNo, "Atenci�n")
            If Res = 6 Then
                eClv_Progra = Me.Clv_PrograTextBox.Text
                eClv_Txt = Me.Clv_TxtLabel1.Text
                GloBndExt = True
                eBndPPE = True
                GloClv_Txt = Me.Clv_TxtTextBox.Text
                Me.Close()
            End If
        Else
            MsgBox("No hay una Programaci�n para �ste PPE.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Clv_TxtTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TxtTextBox.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraProgramacionPPETableAdapter.Connection = CON
        Me.MuestraProgramacionPPETableAdapter.Fill(Me.EricDataSet.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 1)
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Me.DateTimePicker1.Value, 4)
            CON.Close()
        Catch
            MsgBox("Teclea una Fecha V�lida.", , "Error")
        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Me.DateTimePicker1.Value, 4)
        CON.Close()
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class