Public Class FrmLoginCajera

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Me.Clv_UsuarioTextBox.Text = eLoginCajera And Me.PasaporteTextBox.Text = ePassCajera Then
            eConModDesglose = True
            Me.Close()

        Else
            eConModDesglose = False
            MsgBox("El Login y/o Pasaporte No corresponden a la Sesi�n de la Cajera Actual", , "Advertencia")
            Me.Close()
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        eConModDesglose = False
        Me.Close()
    End Sub

    Private Sub PasaporteTextBox_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles PasaporteTextBox.KeyDown
        If e.KeyData = Keys.Enter Then
            If Me.Clv_UsuarioTextBox.Text = eLoginCajera And Me.PasaporteTextBox.Text = ePassCajera Then
                eConModDesglose = True
                Me.Close()

            Else
                eConModDesglose = False
                MsgBox("El Login y/o Pasaporte No corresponden a la Sesi�n de la Cajera Actual", , "Advertencia")
                Me.Close()
            End If
        End If
    End Sub

    Private Sub FrmLoginCajera_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eConModDesglose = True
        Me.Close()

        colorea(Me)
    End Sub
End Class