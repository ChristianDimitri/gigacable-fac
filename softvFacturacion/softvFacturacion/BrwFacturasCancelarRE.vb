Imports System.Data.SqlClient
Imports System.Text
Public Class BrwFacturasCancelarRE

    Private Sub MuestraFacturaRE(ByVal Serie As String, ByVal Folio As Long, ByVal Fecha As Date, ByVal Contrato As Long, ByVal Nombre As String, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC MuestraFacturaRE ")
        strSQL.Append("'" & Serie & "' , ")
        strSQL.Append(CStr(Folio) & ", ")
        strSQL.Append("'" & Fecha.ToShortDateString() & "' , ")
        strSQL.Append(CStr(Contrato) & ", ")
        strSQL.Append("'" & Nombre & "' , ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridView1.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub CANCELACIONFACTURAS(ByVal Clv_Factura As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CANCELACIONFACTURAS", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Op", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 250)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@BndError", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

            If CInt(parametro4.Value) <> 0 Then
                MsgBox(parametro3.Value.ToString, MsgBoxStyle.Exclamation)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub CancelaCNRPPE(ByVal Clv_Factura As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CancelaCNRPPE", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub CancelaCambioServCliente(ByVal Clv_Factura)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("CancelaCambioServCliente", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub Cancela_NotasDeFactura(ByVal Clv_Factura)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("Cancela_NotasDeFactura", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Factura
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub ConFacturas()
        TextBoxSerie.Text = DataGridView1.SelectedCells.Item(1).Value
        TextBoxFolio.Text = DataGridView1.SelectedCells.Item(2).Value
        TextBoxFecha.Text = DataGridView1.SelectedCells.Item(3).Value
        TextBoxContrato.Text = DataGridView1.SelectedCells.Item(4).Value
        TextBoxNombre.Text = DataGridView1.SelectedCells.Item(5).Value
        TextBoxImporte.Text = DataGridView1.SelectedCells.Item(6).Value
    End Sub


    Private Sub TextBoxBusSerie_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusSerie.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBoxBusSerie.Text.Length > 0 And TextBoxBusFolio.Text.Length > 0 Then
                MuestraFacturaRE(TextBoxBusSerie.Text, TextBoxBusFolio.Text, Today, 0, String.Empty, 1)
            End If
        End If
    End Sub

    Private Sub TextBoxBusFolio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusFolio.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBoxBusSerie.Text.Length > 0 And TextBoxBusFolio.Text.Length > 0 Then
                MuestraFacturaRE(TextBoxBusSerie.Text, TextBoxBusFolio.Text, Today, 0, String.Empty, 1)
            End If
        End If
    End Sub

    Private Sub ButtonBusSerieYFolio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusSerieYFolio.Click
        If TextBoxBusSerie.Text.Length > 0 And TextBoxBusFolio.Text.Length > 0 Then
            MuestraFacturaRE(TextBoxBusSerie.Text, TextBoxBusFolio.Text, Today, 0, String.Empty, 1)
        End If
    End Sub

    Private Sub ButtonBusFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusFecha.Click
        MuestraFacturaRE(String.Empty, 0, Me.DateTimePickerFecha.Value, 0, String.Empty, 2)
    End Sub

    Private Sub TextBoxBusContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusContrato.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBoxBusContrato.Text.Length > 0 Then
                MuestraFacturaRE(String.Empty, 0, Today, TextBoxBusContrato.Text, String.Empty, 3)
            End If
        End If
    End Sub

    Private Sub ButtonBusContrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusContrato.Click
        If TextBoxBusContrato.Text.Length > 0 Then
            MuestraFacturaRE(String.Empty, 0, Today, TextBoxBusContrato.Text, String.Empty, 3)
        End If
    End Sub


    Private Sub ButtonBusNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusNombre.Click
        If TextBoxBusNombre.Text.Length > 0 Then
            MuestraFacturaRE(String.Empty, 0, Today, 0, TextBoxBusNombre.Text, 4)
        End If
    End Sub

    Private Sub BrwFacturasCancelarRE_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eCancelacionRE = True Then
            Try

                eCancelacionRE = False
                CANCELACIONFACTURAS(eCveFactura)
                CancelaCNRPPE(eCveFactura)
                CancelaCambioServCliente(eCveFactura)
                Cancela_NotasDeFactura(eCveFactura)
                bitsist(LocLoginUsuario, eContrato, "Facturaci�n", Me.Text, "Clv_Factura: " & CStr(eCveFactura), "Activa", "Cancelada", "AG")

                'FacturaFiscalCFD---------------------------------------------------------------------
                'Generaci�n de FF
                facturaFiscalCFD = False
                facturaFiscalCFD = ChecaSiEsFacturaFiscal("N", eCveFactura)
                If facturaFiscalCFD = True Then
                    DameSerieFolio(0, eCveFactura)
                    CancelaFacturaCFD("N", eCveFactura, eSerie, eFolio, 0, "")
                End If
                '--------------------------------------------------------------------------------------

                MsgBox("Se Cancel� con �xito.", MsgBoxStyle.Information)
                MuestraFacturaRE(String.Empty, 0, Today, 0, String.Empty, 0)
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
    End Sub

    Private Sub BrwFacturasCancelarRE_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        eCancelacionRE = False
        eCveFactura = 0
        eContrato = 0
        MuestraFacturaRE(String.Empty, 0, Today, 0, String.Empty, 0)
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DataGridView1.RowCount = 0 Then
            MsgBox("Seleccione una Factura a cancelar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eReImprimirF = 0
        eCveFactura = DataGridView1.SelectedCells.Item(0).Value
        eContrato = DataGridView1.SelectedCells.Item(4).Value
        FrmMotivoCancelacionFactura.Show()
    End Sub

    Private Sub DatGridView1_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            ConFacturas()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub TextBoxBusNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBoxBusNombre.Text.Length > 0 Then
                MuestraFacturaRE(String.Empty, 0, Today, 0, TextBoxBusNombre.Text, 4)
            End If
        End If
    End Sub


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class