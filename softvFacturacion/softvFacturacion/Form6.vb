﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class Form6

    Public eSerie As String = Nothing
    Public eFolio As Long = 0

    Public Sub DameSerieFolio(ByVal Op As Integer, ByVal Clv_Factura As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameSerieFolio", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                eSerie = reader(0).ToString()
                eFolio = Integer.Parse(reader(1).ToString())
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim Op As Integer
        Op = FrmMenu.Label2.Text

        If Op = 1 Then
            DameSerieFolio(0, CInt(Textclv_factura.Text))
            GeneraFacturaCFD("N", Textclv_factura.Text, TextSerie.Text, TextFolio.Text, Textcontrato.Text, "")
            MsgBox("Se generó una Factura Fiscal Electrónica con el nombre de " + eSerie + eFolio.ToString() + ".ff")
        End If

        If Op = 2 Then
            DameSerieFolio(0, CInt(Textclv_factura.Text))
            GeneraFacturaCFD("G", Textclv_factura.Text, TextSerie.Text, TextFolio.Text, Textcontrato.Text, "")
            MsgBox("Se generó una Factura Fiscal Electrónica con el nombre de " + eSerie + eFolio.ToString() + ".ff")

        End If

        If Op = 3 Then
            DameSerieFolio(0, CInt(Textclv_factura.Text))
            GeneraFacturaCFD("C", Textclv_factura.Text, TextSerie.Text, TextFolio.Text, Textcontrato.Text, "")
            MsgBox("Se generó una Factura Fiscal Electrónica con el nombre de " + eSerie + eFolio.ToString() + ".ff")

        End If

    End Sub

    Private Sub Form6_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class