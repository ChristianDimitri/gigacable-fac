<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFAC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SumaDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.BUSCLIPORCONTRATO_FACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.DameDetalleTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDetalleTableAdapter()
        Me.BorraClv_SessionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BorraClv_SessionTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter()
        Me.CobraTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CobraTableAdapter()
        Me.DameSerDELCliFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliFACTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter()
        Me.CobraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DameDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgregarServicioAdicionalesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.AgregarServicioAdicionalesTableAdapter()
        Me.GrabaFacturasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GrabaFacturasTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.GrabaFacturasTableAdapter()
        Me.AgregarServicioAdicionalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SumaDetalleTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.SumaDetalleTableAdapter()
        Me.PagosAdelantadosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PagosAdelantadosTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.PagosAdelantadosTableAdapter()
        Me.BUSCLIPORCONTRATOFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DamedatosUsuarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter()
        Me.DAMENOMBRESUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LblVersion = New System.Windows.Forms.Label()
        Me.LblFecha = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblUsuario = New System.Windows.Forms.Label()
        Me.DamedatosUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblSucursal = New System.Windows.Forms.Label()
        Me.DAMENOMBRESUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblNomCaja = New System.Windows.Forms.Label()
        Me.LblSistema = New System.Windows.Forms.Label()
        Me.LblNomEmpresa = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLVSERVICIODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvllavedelservicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvUnicaNetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAVEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MACCABLEMODEM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCORTADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pagos_Adelantados = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.TvAdicDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesCortesiaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesApagarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescuentoNet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Otr_Ser_Misma_Categoria = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BonificacionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteAdicionalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColumnaDetalleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiasBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MesesBonificarDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteBonificaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoMesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UltimoanioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AdelantadoDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DESCRIPCIONDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLV_DETALLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Fecha_Venta = New System.Windows.Forms.DateTimePicker()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.UltimoSERIEYFOLIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.FolioTextBox = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAVENDEDORES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label15 = New System.Windows.Forms.Label()
        Me.REDLabel26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.REDLabel25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.CLV_TIPOCLIENTELabel1 = New System.Windows.Forms.Label()
        Me.DAMETIPOSCLIENTESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DESCRIPCIONLabel1 = New System.Windows.Forms.Label()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Clv_Session = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.MUESTRAVENDEDORES_2TableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVENDEDORES_2TableAdapter()
        Me.Ultimo_SERIEYFOLIOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Ultimo_SERIEYFOLIOTableAdapter()
        Me.DAMEUltimo_FOLIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEUltimo_FOLIOTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMEUltimo_FOLIOTableAdapter()
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.DAMETOTALSumaDetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMETOTALSumaDetalleTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.DAMETOTALSumaDetalleTableAdapter()
        Me.GUARDATIPOPAGOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDATIPOPAGOTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.GUARDATIPOPAGOTableAdapter()
        Me.QUITARDELDETALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.QUITARDELDETALLETableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.QUITARDELDETALLETableAdapter()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.LABEL19 = New System.Windows.Forms.TextBox()
        Me.TextBoxClienteObs = New System.Windows.Forms.TextBox()
        Me.DAMETIPOSCLIENTESTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GuardaMotivosBonificacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GuardaMotivosBonificacionTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosBonificacionTableAdapter()
        Me.SumaDetalleDataGridView = New System.Windows.Forms.DataGridView()
        Me.ClvSessionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PosicionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NivelDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.BORCAMDOCFAC_QUITABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BORCAMDOCFAC_QUITATableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BORCAMDOCFAC_QUITATableAdapter()
        Me.DamelasOrdenesque_GeneroFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DamelasOrdenesque_GeneroFacturaTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DamelasOrdenesque_GeneroFacturaTableAdapter()
        Me.Dame_Impresora_OrdenesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_Impresora_OrdenesTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Dame_Impresora_OrdenesTableAdapter()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.DimesiahiConexBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DimesiahiConexTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DimesiahiConexTableAdapter()
        Me.DameServicioAsignadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuscaBloqueadoTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BuscaBloqueadoTableAdapter()
        Me.BusFacFiscalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BusFacFiscalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter()
        Me.Inserta_Comentario2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Comentario2TableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Inserta_Comentario2TableAdapter()
        Me.Hora_insBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Hora_insTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Hora_insTableAdapter()
        Me.Selecciona_Impresora_SucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_Impresora_SucursalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Selecciona_Impresora_SucursalTableAdapter()
        Me.Dime_ContratacionBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_ContratacionTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_ContratacionTableAdapter()
        Me.CMBPanel6 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Dime_Si_ProcedePagoParcialBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dime_Si_ProcedePagoParcialTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Dime_Si_ProcedePagoParcialTableAdapter()
        Me.Cobra_PagosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cobra_PagosTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Cobra_PagosTableAdapter()
        Me.AgregarServicioAdicionales_PPEBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AgregarServicioAdicionales_PPETableAdapter1 = New softvFacturacion.DataSetEdgarTableAdapters.AgregarServicioAdicionales_PPETableAdapter()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.CLV_DETALLETextBox = New System.Windows.Forms.TextBox()
        Me.CobraAdeudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CobraAdeudoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CobraAdeudoTableAdapter()
        Me.Pregunta_Si_Puedo_AdelantarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Pregunta_Si_Puedo_AdelantarTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Pregunta_Si_Puedo_AdelantarTableAdapter()
        Me.Cobra_VentasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Cobra_VentasTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Cobra_VentasTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Inserta_Bonificacion_SupervisorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bonificacion_SupervisorTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter()
        Me.EricDataSet2 = New softvFacturacion.EricDataSet2()
        Me.DameGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralMsjTicketsTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter()
        Me.EntregaAparatoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.EntregaAparatoTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.EntregaAparatoTableAdapter()
        Me.EricDataSet = New softvFacturacion.EricDataSet()
        Me.DameServicioAsignadoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameServicioAsignadoTableAdapter = New softvFacturacion.EricDataSetTableAdapters.DameServicioAsignadoTableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.ButtonPaqAdic = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter6 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter7 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter8 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter9 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter10 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter11 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter12 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrabaFacturasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgregarServicioAdicionalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PagosAdelantadosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.UltimoSERIEYFOLIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAVENDEDORES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DAMEUltimo_FOLIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMETOTALSumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDATIPOPAGOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.QUITARDELDETALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.GuardaMotivosBonificacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BORCAMDOCFAC_QUITABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamelasOrdenesque_GeneroFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DimesiahiConexBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServicioAsignadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dime_ContratacionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel6.SuspendLayout()
        CType(Me.Dime_Si_ProcedePagoParcialBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cobra_PagosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AgregarServicioAdicionales_PPEBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CobraAdeudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pregunta_Si_Puedo_AdelantarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Cobra_VentasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EntregaAparatoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameServicioAsignadoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(224, 24)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(95, 16)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet"
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        ESHOTELLabel1.Location = New System.Drawing.Point(333, 24)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(67, 16)
        ESHOTELLabel1.TabIndex = 18
        ESHOTELLabel1.Text = "Es Hotel"
        ESHOTELLabel1.Visible = False
        '
        'SumaDetalleBindingSource
        '
        Me.SumaDetalleBindingSource.DataMember = "SumaDetalle"
        Me.SumaDetalleBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BUSCLIPORCONTRATO_FACTableAdapter
        '
        Me.BUSCLIPORCONTRATO_FACTableAdapter.ClearBeforeFill = True
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkRed
        Me.Button7.Enabled = False
        Me.Button7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.Location = New System.Drawing.Point(202, 544)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(181, 31)
        Me.Button7.TabIndex = 501
        Me.Button7.Text = "&QUITAR DE LA LISTA"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'DameDetalleTableAdapter
        '
        Me.DameDetalleTableAdapter.ClearBeforeFill = True
        '
        'BorraClv_SessionBindingSource
        '
        Me.BorraClv_SessionBindingSource.DataMember = "BorraClv_Session"
        Me.BorraClv_SessionBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'BorraClv_SessionTableAdapter
        '
        Me.BorraClv_SessionTableAdapter.ClearBeforeFill = True
        '
        'CobraTableAdapter
        '
        Me.CobraTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliFACBindingSource
        '
        Me.DameSerDELCliFACBindingSource.DataMember = "DameSerDELCliFAC"
        Me.DameSerDELCliFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameSerDELCliFACTableAdapter
        '
        Me.DameSerDELCliFACTableAdapter.ClearBeforeFill = True
        '
        'CobraBindingSource
        '
        Me.CobraBindingSource.DataMember = "Cobra"
        Me.CobraBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkRed
        Me.Button5.Enabled = False
        Me.Button5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Location = New System.Drawing.Point(6, 544)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(189, 31)
        Me.Button5.TabIndex = 500
        Me.Button5.Text = "&AGREGAR A LA LISTA"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(575, 676)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(139, 42)
        Me.Button4.TabIndex = 504
        Me.Button4.Text = "&LIMPIAR PANTALLA"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(865, 676)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(139, 42)
        Me.Button3.TabIndex = 506
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkRed
        Me.Button2.Enabled = False
        Me.Button2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(720, 676)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(139, 42)
        Me.Button2.TabIndex = 505
        Me.Button2.Text = "&PAGAR"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'DameDetalleBindingSource
        '
        Me.DameDetalleBindingSource.DataMember = "DameDetalle"
        Me.DameDetalleBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'AgregarServicioAdicionalesTableAdapter
        '
        Me.AgregarServicioAdicionalesTableAdapter.ClearBeforeFill = True
        '
        'GrabaFacturasBindingSource
        '
        Me.GrabaFacturasBindingSource.DataMember = "GrabaFacturas"
        Me.GrabaFacturasBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'GrabaFacturasTableAdapter
        '
        Me.GrabaFacturasTableAdapter.ClearBeforeFill = True
        '
        'AgregarServicioAdicionalesBindingSource
        '
        Me.AgregarServicioAdicionalesBindingSource.DataMember = "AgregarServicioAdicionales"
        Me.AgregarServicioAdicionalesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'SumaDetalleTableAdapter
        '
        Me.SumaDetalleTableAdapter.ClearBeforeFill = True
        '
        'PagosAdelantadosBindingSource
        '
        Me.PagosAdelantadosBindingSource.DataMember = "PagosAdelantados"
        Me.PagosAdelantadosBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'PagosAdelantadosTableAdapter
        '
        Me.PagosAdelantadosTableAdapter.ClearBeforeFill = True
        '
        'BUSCLIPORCONTRATOFACBindingSource
        '
        Me.BUSCLIPORCONTRATOFACBindingSource.DataMember = "BUSCLIPORCONTRATO_FAC"
        Me.BUSCLIPORCONTRATOFACBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.TreeView1)
        Me.Panel2.Location = New System.Drawing.Point(630, 64)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(374, 216)
        Me.Panel2.TabIndex = 195
        '
        'TreeView1
        '
        Me.TreeView1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TreeView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.ForeColor = System.Drawing.Color.Black
        Me.TreeView1.Location = New System.Drawing.Point(0, 0)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(374, 213)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DamedatosUsuarioTableAdapter
        '
        Me.DamedatosUsuarioTableAdapter.ClearBeforeFill = True
        '
        'DAMENOMBRESUCURSALTableAdapter
        '
        Me.DAMENOMBRESUCURSALTableAdapter.ClearBeforeFill = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(4, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 15)
        Me.Label3.TabIndex = 174
        Me.Label3.Text = "Empresa : "
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Me.LblVersion)
        Me.Panel1.Controls.Add(Me.LblFecha)
        Me.Panel1.Controls.Add(Me.LblUsuario)
        Me.Panel1.Controls.Add(Me.LblSucursal)
        Me.Panel1.Controls.Add(Me.LblNomCaja)
        Me.Panel1.Controls.Add(Me.LblSistema)
        Me.Panel1.Controls.Add(Me.LblNomEmpresa)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(6, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(998, 46)
        Me.Panel1.TabIndex = 186
        '
        'LblVersion
        '
        Me.LblVersion.AutoSize = True
        Me.LblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVersion.Location = New System.Drawing.Point(879, 28)
        Me.LblVersion.Name = "LblVersion"
        Me.LblVersion.Size = New System.Drawing.Size(52, 13)
        Me.LblVersion.TabIndex = 187
        Me.LblVersion.Text = "Label14"
        '
        'LblFecha
        '
        Me.LblFecha.AutoSize = True
        Me.LblFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Fecha", True))
        Me.LblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.Location = New System.Drawing.Point(879, 7)
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(52, 13)
        Me.LblFecha.TabIndex = 186
        Me.LblFecha.Text = "Label14"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblUsuario
        '
        Me.LblUsuario.AutoSize = True
        Me.LblUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DamedatosUsuarioBindingSource, "Nombre", True))
        Me.LblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUsuario.Location = New System.Drawing.Point(579, 7)
        Me.LblUsuario.Name = "LblUsuario"
        Me.LblUsuario.Size = New System.Drawing.Size(52, 13)
        Me.LblUsuario.TabIndex = 185
        Me.LblUsuario.Text = "Label14"
        '
        'DamedatosUsuarioBindingSource
        '
        Me.DamedatosUsuarioBindingSource.DataMember = "DamedatosUsuario"
        Me.DamedatosUsuarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblSucursal
        '
        Me.LblSucursal.AutoSize = True
        Me.LblSucursal.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMENOMBRESUCURSALBindingSource, "Nombre", True))
        Me.LblSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSucursal.Location = New System.Drawing.Point(458, 28)
        Me.LblSucursal.Name = "LblSucursal"
        Me.LblSucursal.Size = New System.Drawing.Size(52, 13)
        Me.LblSucursal.TabIndex = 184
        Me.LblSucursal.Text = "Label14"
        '
        'DAMENOMBRESUCURSALBindingSource
        '
        Me.DAMENOMBRESUCURSALBindingSource.DataMember = "DAMENOMBRESUCURSAL"
        Me.DAMENOMBRESUCURSALBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblNomCaja
        '
        Me.LblNomCaja.AutoSize = True
        Me.LblNomCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomCaja.Location = New System.Drawing.Point(383, 7)
        Me.LblNomCaja.Name = "LblNomCaja"
        Me.LblNomCaja.Size = New System.Drawing.Size(52, 13)
        Me.LblNomCaja.TabIndex = 183
        Me.LblNomCaja.Text = "Label14"
        '
        'LblSistema
        '
        Me.LblSistema.AutoSize = True
        Me.LblSistema.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.LblSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSistema.Location = New System.Drawing.Point(78, 28)
        Me.LblSistema.Name = "LblSistema"
        Me.LblSistema.Size = New System.Drawing.Size(52, 13)
        Me.LblSistema.TabIndex = 182
        Me.LblSistema.Text = "Label14"
        '
        'LblNomEmpresa
        '
        Me.LblNomEmpresa.AutoSize = True
        Me.LblNomEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.LblNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomEmpresa.Location = New System.Drawing.Point(78, 7)
        Me.LblNomEmpresa.Name = "LblNomEmpresa"
        Me.LblNomEmpresa.Size = New System.Drawing.Size(52, 13)
        Me.LblNomEmpresa.TabIndex = 181
        Me.LblNomEmpresa.Text = "Label14"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(498, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 15)
        Me.Label13.TabIndex = 180
        Me.Label13.Text = "Cajero(a) :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(806, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 15)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Versión : "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(815, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 15)
        Me.Label7.TabIndex = 178
        Me.Label7.Text = "Fecha : "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(383, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 15)
        Me.Label6.TabIndex = 177
        Me.Label6.Text = "Sucursal : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(336, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 15)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Caja : "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 15)
        Me.Label4.TabIndex = 175
        Me.Label4.Text = "Sistema : "
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn, Me.CLVSERVICIODataGridViewTextBoxColumn, Me.ClvllavedelservicioDataGridViewTextBoxColumn, Me.ClvUnicaNetDataGridViewTextBoxColumn, Me.CLAVEDataGridViewTextBoxColumn, Me.MACCABLEMODEM, Me.DESCORTADataGridViewTextBoxColumn, Me.Pagos_Adelantados, Me.TvAdicDataGridViewTextBoxColumn, Me.MesesCortesiaDataGridViewTextBoxColumn, Me.MesesApagarDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.PeriodoPagadoIniDataGridViewTextBoxColumn, Me.PeriodoPagadoFinDataGridViewTextBoxColumn, Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn, Me.PuntosAplicadosAntDataGridViewTextBoxColumn, Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn, Me.DescuentoNet, Me.Des_Otr_Ser_Misma_Categoria, Me.BonificacionDataGridViewTextBoxColumn, Me.ImporteAdicionalDataGridViewTextBoxColumn, Me.ColumnaDetalleDataGridViewTextBoxColumn, Me.DiasBonificaDataGridViewTextBoxColumn, Me.MesesBonificarDataGridViewTextBoxColumn, Me.ImporteBonificaDataGridViewTextBoxColumn, Me.UltimoMesDataGridViewTextBoxColumn, Me.UltimoanioDataGridViewTextBoxColumn, Me.AdelantadoDataGridViewCheckBoxColumn, Me.DESCRIPCIONDataGridViewTextBoxColumn, Me.CLV_DETALLE})
        Me.DataGridView1.DataSource = Me.DameDetalleBindingSource
        Me.DataGridView1.GridColor = System.Drawing.Color.DimGray
        Me.DataGridView1.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.DataGridView1.Location = New System.Drawing.Point(6, 286)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(998, 255)
        Me.DataGridView1.TabIndex = 187
        Me.DataGridView1.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn
        '
        Me.ClvSessionDataGridViewTextBoxColumn.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn.Name = "ClvSessionDataGridViewTextBoxColumn"
        Me.ClvSessionDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvSessionDataGridViewTextBoxColumn.Visible = False
        '
        'CLVSERVICIODataGridViewTextBoxColumn
        '
        Me.CLVSERVICIODataGridViewTextBoxColumn.DataPropertyName = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.HeaderText = "CLV_SERVICIO"
        Me.CLVSERVICIODataGridViewTextBoxColumn.Name = "CLVSERVICIODataGridViewTextBoxColumn"
        Me.CLVSERVICIODataGridViewTextBoxColumn.ReadOnly = True
        Me.CLVSERVICIODataGridViewTextBoxColumn.Visible = False
        '
        'ClvllavedelservicioDataGridViewTextBoxColumn
        '
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.DataPropertyName = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.HeaderText = "Clv_llavedelservicio"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Name = "ClvllavedelservicioDataGridViewTextBoxColumn"
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvllavedelservicioDataGridViewTextBoxColumn.Visible = False
        '
        'ClvUnicaNetDataGridViewTextBoxColumn
        '
        Me.ClvUnicaNetDataGridViewTextBoxColumn.DataPropertyName = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.HeaderText = "Clv_UnicaNet"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Name = "ClvUnicaNetDataGridViewTextBoxColumn"
        Me.ClvUnicaNetDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvUnicaNetDataGridViewTextBoxColumn.Visible = False
        '
        'CLAVEDataGridViewTextBoxColumn
        '
        Me.CLAVEDataGridViewTextBoxColumn.DataPropertyName = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.HeaderText = "CLAVE"
        Me.CLAVEDataGridViewTextBoxColumn.Name = "CLAVEDataGridViewTextBoxColumn"
        Me.CLAVEDataGridViewTextBoxColumn.ReadOnly = True
        Me.CLAVEDataGridViewTextBoxColumn.Visible = False
        '
        'MACCABLEMODEM
        '
        Me.MACCABLEMODEM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.MACCABLEMODEM.DataPropertyName = "MACCABLEMODEM"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MACCABLEMODEM.DefaultCellStyle = DataGridViewCellStyle2
        Me.MACCABLEMODEM.HeaderText = "Aparato"
        Me.MACCABLEMODEM.Name = "MACCABLEMODEM"
        Me.MACCABLEMODEM.ReadOnly = True
        Me.MACCABLEMODEM.Width = 81
        '
        'DESCORTADataGridViewTextBoxColumn
        '
        Me.DESCORTADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DESCORTADataGridViewTextBoxColumn.DataPropertyName = "DESCORTA"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCORTADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.DESCORTADataGridViewTextBoxColumn.HeaderText = "Concepto"
        Me.DESCORTADataGridViewTextBoxColumn.Name = "DESCORTADataGridViewTextBoxColumn"
        Me.DESCORTADataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCORTADataGridViewTextBoxColumn.Width = 92
        '
        'Pagos_Adelantados
        '
        Me.Pagos_Adelantados.DataPropertyName = "Pagos_Adelantados"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Pagos_Adelantados.DefaultCellStyle = DataGridViewCellStyle4
        Me.Pagos_Adelantados.HeaderText = "Pagos Adelantados"
        Me.Pagos_Adelantados.Name = "Pagos_Adelantados"
        Me.Pagos_Adelantados.ReadOnly = True
        Me.Pagos_Adelantados.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Pagos_Adelantados.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'TvAdicDataGridViewTextBoxColumn
        '
        Me.TvAdicDataGridViewTextBoxColumn.DataPropertyName = "tvAdic"
        Me.TvAdicDataGridViewTextBoxColumn.HeaderText = "Tv's Extra"
        Me.TvAdicDataGridViewTextBoxColumn.Name = "TvAdicDataGridViewTextBoxColumn"
        Me.TvAdicDataGridViewTextBoxColumn.ReadOnly = True
        Me.TvAdicDataGridViewTextBoxColumn.Width = 50
        '
        'MesesCortesiaDataGridViewTextBoxColumn
        '
        Me.MesesCortesiaDataGridViewTextBoxColumn.DataPropertyName = "Meses_Cortesia"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesCortesiaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.MesesCortesiaDataGridViewTextBoxColumn.HeaderText = "Meses Cortesía"
        Me.MesesCortesiaDataGridViewTextBoxColumn.Name = "MesesCortesiaDataGridViewTextBoxColumn"
        Me.MesesCortesiaDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesCortesiaDataGridViewTextBoxColumn.Width = 70
        '
        'MesesApagarDataGridViewTextBoxColumn
        '
        Me.MesesApagarDataGridViewTextBoxColumn.DataPropertyName = "mesesApagar"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "N0"
        Me.MesesApagarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.MesesApagarDataGridViewTextBoxColumn.HeaderText = "Meses a Pagar"
        Me.MesesApagarDataGridViewTextBoxColumn.Name = "MesesApagarDataGridViewTextBoxColumn"
        Me.MesesApagarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesApagarDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MesesApagarDataGridViewTextBoxColumn.Width = 70
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "importe"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.Format = "C2"
        DataGridViewCellStyle7.NullValue = Nothing
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ImporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "Importe"
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteDataGridViewTextBoxColumn.Width = 80
        '
        'PeriodoPagadoIniDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoIni"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.HeaderText = "Periodo Pagado Inicial"
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Name = "PeriodoPagadoIniDataGridViewTextBoxColumn"
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoIniDataGridViewTextBoxColumn.Width = 80
        '
        'PeriodoPagadoFinDataGridViewTextBoxColumn
        '
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DataPropertyName = "periodoPagadoFin"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.HeaderText = "Periodo Pagado Final"
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Name = "PeriodoPagadoFinDataGridViewTextBoxColumn"
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.ReadOnly = True
        Me.PeriodoPagadoFinDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosOtrosDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosOtros"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.Format = "N0"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por  Pago Oportuno"
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Name = "PuntosAplicadosOtrosDataGridViewTextBoxColumn"
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosOtrosDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosAntDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DataPropertyName = "puntosAplicadosAnt"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N0"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por Antigüedad"
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Name = "PuntosAplicadosAntDataGridViewTextBoxColumn"
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosAntDataGridViewTextBoxColumn.Width = 80
        '
        'PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn
        '
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DataPropertyName = "PuntosAplicadosPagoAdelantado"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle12
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.HeaderText = "Puntos Aplicados por Pagos Adelantados"
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Name = "PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn"
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.ReadOnly = True
        Me.PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn.Width = 80
        '
        'DescuentoNet
        '
        Me.DescuentoNet.DataPropertyName = "DescuentoNet"
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.Format = "N0"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.DescuentoNet.DefaultCellStyle = DataGridViewCellStyle13
        Me.DescuentoNet.HeaderText = "Puntos Combo"
        Me.DescuentoNet.Name = "DescuentoNet"
        Me.DescuentoNet.ReadOnly = True
        Me.DescuentoNet.Width = 60
        '
        'Des_Otr_Ser_Misma_Categoria
        '
        Me.Des_Otr_Ser_Misma_Categoria.DataPropertyName = "Des_Otr_Ser_Misma_Categoria"
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.Format = "N0"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Des_Otr_Ser_Misma_Categoria.DefaultCellStyle = DataGridViewCellStyle14
        Me.Des_Otr_Ser_Misma_Categoria.HeaderText = "Puntos Ppe"
        Me.Des_Otr_Ser_Misma_Categoria.Name = "Des_Otr_Ser_Misma_Categoria"
        Me.Des_Otr_Ser_Misma_Categoria.ReadOnly = True
        Me.Des_Otr_Ser_Misma_Categoria.Width = 60
        '
        'BonificacionDataGridViewTextBoxColumn
        '
        Me.BonificacionDataGridViewTextBoxColumn.DataPropertyName = "bonificacion"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BonificacionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle15
        Me.BonificacionDataGridViewTextBoxColumn.HeaderText = "bonificacion"
        Me.BonificacionDataGridViewTextBoxColumn.Name = "BonificacionDataGridViewTextBoxColumn"
        Me.BonificacionDataGridViewTextBoxColumn.ReadOnly = True
        Me.BonificacionDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteAdicionalDataGridViewTextBoxColumn
        '
        Me.ImporteAdicionalDataGridViewTextBoxColumn.DataPropertyName = "importeAdicional"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.HeaderText = "importeAdicional"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Name = "ImporteAdicionalDataGridViewTextBoxColumn"
        Me.ImporteAdicionalDataGridViewTextBoxColumn.ReadOnly = True
        Me.ImporteAdicionalDataGridViewTextBoxColumn.Visible = False
        '
        'ColumnaDetalleDataGridViewTextBoxColumn
        '
        Me.ColumnaDetalleDataGridViewTextBoxColumn.DataPropertyName = "columnaDetalle"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.HeaderText = "columnaDetalle"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Name = "ColumnaDetalleDataGridViewTextBoxColumn"
        Me.ColumnaDetalleDataGridViewTextBoxColumn.ReadOnly = True
        Me.ColumnaDetalleDataGridViewTextBoxColumn.Visible = False
        '
        'DiasBonificaDataGridViewTextBoxColumn
        '
        Me.DiasBonificaDataGridViewTextBoxColumn.DataPropertyName = "DiasBonifica"
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiasBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle16
        Me.DiasBonificaDataGridViewTextBoxColumn.HeaderText = "DiasBonifica"
        Me.DiasBonificaDataGridViewTextBoxColumn.Name = "DiasBonificaDataGridViewTextBoxColumn"
        Me.DiasBonificaDataGridViewTextBoxColumn.ReadOnly = True
        Me.DiasBonificaDataGridViewTextBoxColumn.Visible = False
        '
        'MesesBonificarDataGridViewTextBoxColumn
        '
        Me.MesesBonificarDataGridViewTextBoxColumn.DataPropertyName = "mesesBonificar"
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MesesBonificarDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle17
        Me.MesesBonificarDataGridViewTextBoxColumn.HeaderText = "mesesBonificar"
        Me.MesesBonificarDataGridViewTextBoxColumn.Name = "MesesBonificarDataGridViewTextBoxColumn"
        Me.MesesBonificarDataGridViewTextBoxColumn.ReadOnly = True
        Me.MesesBonificarDataGridViewTextBoxColumn.Visible = False
        '
        'ImporteBonificaDataGridViewTextBoxColumn
        '
        Me.ImporteBonificaDataGridViewTextBoxColumn.DataPropertyName = "importeBonifica"
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteBonificaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle18
        Me.ImporteBonificaDataGridViewTextBoxColumn.HeaderText = "Importe a Bonificar"
        Me.ImporteBonificaDataGridViewTextBoxColumn.Name = "ImporteBonificaDataGridViewTextBoxColumn"
        Me.ImporteBonificaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UltimoMesDataGridViewTextBoxColumn
        '
        Me.UltimoMesDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_Mes"
        Me.UltimoMesDataGridViewTextBoxColumn.HeaderText = "Ultimo_Mes"
        Me.UltimoMesDataGridViewTextBoxColumn.Name = "UltimoMesDataGridViewTextBoxColumn"
        Me.UltimoMesDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoMesDataGridViewTextBoxColumn.Visible = False
        '
        'UltimoanioDataGridViewTextBoxColumn
        '
        Me.UltimoanioDataGridViewTextBoxColumn.DataPropertyName = "Ultimo_anio"
        Me.UltimoanioDataGridViewTextBoxColumn.HeaderText = "Ultimo_anio"
        Me.UltimoanioDataGridViewTextBoxColumn.Name = "UltimoanioDataGridViewTextBoxColumn"
        Me.UltimoanioDataGridViewTextBoxColumn.ReadOnly = True
        Me.UltimoanioDataGridViewTextBoxColumn.Visible = False
        '
        'AdelantadoDataGridViewCheckBoxColumn
        '
        Me.AdelantadoDataGridViewCheckBoxColumn.DataPropertyName = "Adelantado"
        Me.AdelantadoDataGridViewCheckBoxColumn.HeaderText = "Adelantado"
        Me.AdelantadoDataGridViewCheckBoxColumn.Name = "AdelantadoDataGridViewCheckBoxColumn"
        Me.AdelantadoDataGridViewCheckBoxColumn.ReadOnly = True
        Me.AdelantadoDataGridViewCheckBoxColumn.Visible = False
        '
        'DESCRIPCIONDataGridViewTextBoxColumn
        '
        Me.DESCRIPCIONDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.HeaderText = ""
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Name = "DESCRIPCIONDataGridViewTextBoxColumn"
        Me.DESCRIPCIONDataGridViewTextBoxColumn.ReadOnly = True
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Visible = False
        Me.DESCRIPCIONDataGridViewTextBoxColumn.Width = 5
        '
        'CLV_DETALLE
        '
        Me.CLV_DETALLE.DataPropertyName = "Clv_Detalle"
        Me.CLV_DETALLE.HeaderText = "CLV_DETALLE"
        Me.CLV_DETALLE.Name = "CLV_DETALLE"
        Me.CLV_DETALLE.ReadOnly = True
        Me.CLV_DETALLE.Width = 5
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label15)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel2.Controls.Add(Me.REDLabel26)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label25)
        Me.SplitContainer1.Panel2.Controls.Add(Me.REDLabel25)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label24)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label23)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ContratoTextBox)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label20)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CLV_TIPOCLIENTELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DESCRIPCIONLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button8)
        Me.SplitContainer1.Panel2.Controls.Add(ESHOTELLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ESHOTELCheckBox)
        Me.SplitContainer1.Panel2.Controls.Add(SOLOINTERNETLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.NOMBRELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CIUDADLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.NUMEROLabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label11)
        Me.SplitContainer1.Panel2.Controls.Add(Me.COLONIALabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label10)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CALLELabel1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Clv_Session)
        Me.SplitContainer1.Size = New System.Drawing.Size(618, 216)
        Me.SplitContainer1.SplitterDistance = 206
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.Fecha_Venta)
        Me.Panel4.Controls.Add(Me.Label22)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Controls.Add(Me.Label16)
        Me.Panel4.Controls.Add(Me.FolioTextBox)
        Me.Panel4.Controls.Add(Me.Label14)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.Label17)
        Me.Panel4.Controls.Add(Me.ComboBox1)
        Me.Panel4.Location = New System.Drawing.Point(3, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 210)
        Me.Panel4.TabIndex = 8
        '
        'Fecha_Venta
        '
        Me.Fecha_Venta.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_Venta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.Fecha_Venta.Location = New System.Drawing.Point(6, 174)
        Me.Fecha_Venta.Name = "Fecha_Venta"
        Me.Fecha_Venta.Size = New System.Drawing.Size(118, 24)
        Me.Fecha_Venta.TabIndex = 503
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label22.Location = New System.Drawing.Point(6, 158)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(50, 13)
        Me.Label22.TabIndex = 500
        Me.Label22.Text = "Fecha :"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.UltimoSERIEYFOLIOBindingSource
        Me.ComboBox2.DisplayMember = "SERIE"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(6, 81)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(103, 23)
        Me.ComboBox2.TabIndex = 499
        Me.ComboBox2.ValueMember = "SERIE"
        '
        'UltimoSERIEYFOLIOBindingSource
        '
        Me.UltimoSERIEYFOLIOBindingSource.DataMember = "Ultimo_SERIEYFOLIO"
        Me.UltimoSERIEYFOLIOBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label16.Location = New System.Drawing.Point(3, 5)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(69, 18)
        Me.Label16.TabIndex = 4
        Me.Label16.Text = "Ventas :"
        '
        'FolioTextBox
        '
        Me.FolioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FolioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FolioTextBox.Location = New System.Drawing.Point(6, 129)
        Me.FolioTextBox.Name = "FolioTextBox"
        Me.FolioTextBox.Size = New System.Drawing.Size(103, 24)
        Me.FolioTextBox.TabIndex = 7
        Me.FolioTextBox.TabStop = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label14.Location = New System.Drawing.Point(8, 23)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(69, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Vendedor :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label18.Location = New System.Drawing.Point(8, 113)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(42, 13)
        Me.Label18.TabIndex = 6
        Me.Label18.Text = "Folio :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label17.Location = New System.Drawing.Point(8, 65)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(44, 13)
        Me.Label17.TabIndex = 5
        Me.Label17.Text = "Serie :"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRAVENDEDORES2BindingSource
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(6, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(191, 23)
        Me.ComboBox1.TabIndex = 498
        Me.ComboBox1.ValueMember = "Clv_Vendedor"
        '
        'MUESTRAVENDEDORES2BindingSource
        '
        Me.MUESTRAVENDEDORES2BindingSource.DataMember = "MUESTRAVENDEDORES_2"
        Me.MUESTRAVENDEDORES2BindingSource.DataSource = Me.DataSetEdgar
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(4, 53)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(0, 13)
        Me.Label15.TabIndex = 3
        '
        'REDLabel26
        '
        Me.REDLabel26.AutoSize = True
        Me.REDLabel26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel26.Location = New System.Drawing.Point(7, 197)
        Me.REDLabel26.Name = "REDLabel26"
        Me.REDLabel26.Size = New System.Drawing.Size(73, 13)
        Me.REDLabel26.TabIndex = 510
        Me.REDLabel26.Text = "Al Corriente"
        Me.REDLabel26.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(7, 181)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 13)
        Me.Label25.TabIndex = 189
        Me.Label25.Text = "Periodo"
        '
        'REDLabel25
        '
        Me.REDLabel25.AutoSize = True
        Me.REDLabel25.BackColor = System.Drawing.Color.Yellow
        Me.REDLabel25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel25.ForeColor = System.Drawing.Color.Red
        Me.REDLabel25.Location = New System.Drawing.Point(300, 4)
        Me.REDLabel25.Name = "REDLabel25"
        Me.REDLabel25.Size = New System.Drawing.Size(90, 13)
        Me.REDLabel25.TabIndex = 188
        Me.REDLabel25.Text = "Datos Fiscales"
        Me.REDLabel25.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoEllipsis = True
        Me.Label24.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label24.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "TELEFONO", True))
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(291, 100)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(114, 23)
        Me.Label24.TabIndex = 187
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label23.Location = New System.Drawing.Point(248, 96)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(46, 18)
        Me.Label23.TabIndex = 186
        Me.Label23.Text = "Tel. :"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(76, 14)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(91, 24)
        Me.ContratoTextBox.TabIndex = 0
        Me.ContratoTextBox.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label20.Location = New System.Drawing.Point(10, 164)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 15)
        Me.Label20.TabIndex = 185
        Me.Label20.Text = "Tipo Cobro :"
        '
        'CLV_TIPOCLIENTELabel1
        '
        Me.CLV_TIPOCLIENTELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "CLV_TIPOCLIENTE", True))
        Me.CLV_TIPOCLIENTELabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.CLV_TIPOCLIENTELabel1.Location = New System.Drawing.Point(24, 162)
        Me.CLV_TIPOCLIENTELabel1.Name = "CLV_TIPOCLIENTELabel1"
        Me.CLV_TIPOCLIENTELabel1.Size = New System.Drawing.Size(100, 23)
        Me.CLV_TIPOCLIENTELabel1.TabIndex = 184
        '
        'DAMETIPOSCLIENTESBindingSource
        '
        Me.DAMETIPOSCLIENTESBindingSource.DataMember = "DAMETIPOSCLIENTES"
        Me.DAMETIPOSCLIENTESBindingSource.DataSource = Me.DataSetEdgar
        '
        'DESCRIPCIONLabel1
        '
        Me.DESCRIPCIONLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMETIPOSCLIENTESBindingSource, "DESCRIPCION", True))
        Me.DESCRIPCIONLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DESCRIPCIONLabel1.ForeColor = System.Drawing.Color.Black
        Me.DESCRIPCIONLabel1.Location = New System.Drawing.Point(101, 164)
        Me.DESCRIPCIONLabel1.Name = "DESCRIPCIONLabel1"
        Me.DESCRIPCIONLabel1.Size = New System.Drawing.Size(299, 19)
        Me.DESCRIPCIONLabel1.TabIndex = 183
        Me.DESCRIPCIONLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkRed
        Me.Button8.Enabled = False
        Me.Button8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.Location = New System.Drawing.Point(189, 185)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(211, 25)
        Me.Button8.TabIndex = 2
        Me.Button8.Text = "&VER HISTORIAL "
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label2.Location = New System.Drawing.Point(14, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 15)
        Me.Label2.TabIndex = 173
        Me.Label2.Text = "Ciudad :"
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOFACBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.ForeColor = System.Drawing.Color.Black
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(320, 20)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 19
        Me.ESHOTELCheckBox.TabStop = False
        Me.ESHOTELCheckBox.Visible = False
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOFACBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.Enabled = False
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.ForeColor = System.Drawing.Color.Black
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(213, 20)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label1.Location = New System.Drawing.Point(4, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Contrato :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkRed
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(172, 14)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(30, 25)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = False
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.ForeColor = System.Drawing.Color.Black
        Me.NOMBRELabel1.Location = New System.Drawing.Point(76, 44)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(329, 25)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(10, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 15)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CIUDADLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CIUDAD", True))
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.ForeColor = System.Drawing.Color.Black
        Me.CIUDADLabel1.Location = New System.Drawing.Point(76, 146)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(232, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.Black
        Me.NUMEROLabel1.Location = New System.Drawing.Point(76, 96)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(172, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label11.Location = New System.Drawing.Point(10, 119)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 15)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Colonia :"
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.ForeColor = System.Drawing.Color.Black
        Me.COLONIALabel1.Location = New System.Drawing.Point(76, 121)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(314, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label10.Location = New System.Drawing.Point(49, 92)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(27, 18)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOFACBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.Black
        Me.CALLELabel1.Location = New System.Drawing.Point(76, 71)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(329, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label9.Location = New System.Drawing.Point(0, 69)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 15)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Clv_Session
        '
        Me.Clv_Session.BackColor = System.Drawing.Color.DarkOrange
        Me.Clv_Session.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_Session.Location = New System.Drawing.Point(89, 18)
        Me.Clv_Session.Name = "Clv_Session"
        Me.Clv_Session.Size = New System.Drawing.Size(78, 13)
        Me.Clv_Session.TabIndex = 182
        Me.Clv_Session.TabStop = False
        Me.Clv_Session.Text = "0"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.SplitContainer1)
        Me.Panel3.Location = New System.Drawing.Point(6, 64)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(618, 216)
        Me.Panel3.TabIndex = 196
        '
        'MUESTRAVENDEDORES_2TableAdapter
        '
        Me.MUESTRAVENDEDORES_2TableAdapter.ClearBeforeFill = True
        '
        'Ultimo_SERIEYFOLIOTableAdapter
        '
        Me.Ultimo_SERIEYFOLIOTableAdapter.ClearBeforeFill = True
        '
        'DAMEUltimo_FOLIOBindingSource
        '
        Me.DAMEUltimo_FOLIOBindingSource.DataMember = "DAMEUltimo_FOLIO"
        Me.DAMEUltimo_FOLIOBindingSource.DataSource = Me.DataSetEdgar
        '
        'DAMEUltimo_FOLIOTableAdapter
        '
        Me.DAMEUltimo_FOLIOTableAdapter.ClearBeforeFill = True
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMETOTALSumaDetalleBindingSource
        '
        Me.DAMETOTALSumaDetalleBindingSource.DataMember = "DAMETOTALSumaDetalle"
        Me.DAMETOTALSumaDetalleBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'DAMETOTALSumaDetalleTableAdapter
        '
        Me.DAMETOTALSumaDetalleTableAdapter.ClearBeforeFill = True
        '
        'GUARDATIPOPAGOBindingSource
        '
        Me.GUARDATIPOPAGOBindingSource.DataMember = "GUARDATIPOPAGO"
        Me.GUARDATIPOPAGOBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'GUARDATIPOPAGOTableAdapter
        '
        Me.GUARDATIPOPAGOTableAdapter.ClearBeforeFill = True
        '
        'QUITARDELDETALLEBindingSource
        '
        Me.QUITARDELDETALLEBindingSource.DataMember = "QUITARDELDETALLE"
        Me.QUITARDELDETALLEBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'QUITARDELDETALLETableAdapter
        '
        Me.QUITARDELDETALLETableAdapter.ClearBeforeFill = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.LABEL19)
        Me.Panel5.Location = New System.Drawing.Point(6, 614)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(324, 136)
        Me.Panel5.TabIndex = 197
        Me.Panel5.Visible = False
        '
        'LABEL19
        '
        Me.LABEL19.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.LABEL19.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LABEL19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LABEL19.ForeColor = System.Drawing.Color.Red
        Me.LABEL19.Location = New System.Drawing.Point(2, 3)
        Me.LABEL19.Multiline = True
        Me.LABEL19.Name = "LABEL19"
        Me.LABEL19.ReadOnly = True
        Me.LABEL19.Size = New System.Drawing.Size(317, 123)
        Me.LABEL19.TabIndex = 500
        Me.LABEL19.TabStop = False
        '
        'TextBoxClienteObs
        '
        Me.TextBoxClienteObs.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TextBoxClienteObs.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxClienteObs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxClienteObs.ForeColor = System.Drawing.Color.Red
        Me.TextBoxClienteObs.Location = New System.Drawing.Point(336, 618)
        Me.TextBoxClienteObs.Multiline = True
        Me.TextBoxClienteObs.Name = "TextBoxClienteObs"
        Me.TextBoxClienteObs.Size = New System.Drawing.Size(221, 121)
        Me.TextBoxClienteObs.TabIndex = 501
        Me.TextBoxClienteObs.TabStop = False
        '
        'DAMETIPOSCLIENTESTableAdapter
        '
        Me.DAMETIPOSCLIENTESTableAdapter.ClearBeforeFill = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkRed
        Me.Button6.Enabled = False
        Me.Button6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.Location = New System.Drawing.Point(388, 544)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 31)
        Me.Button6.TabIndex = 502
        Me.Button6.Text = "&BONIFICAR"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'GuardaMotivosBonificacionBindingSource
        '
        Me.GuardaMotivosBonificacionBindingSource.DataMember = "GuardaMotivosBonificacion"
        Me.GuardaMotivosBonificacionBindingSource.DataSource = Me.DataSetEdgar
        '
        'GuardaMotivosBonificacionTableAdapter
        '
        Me.GuardaMotivosBonificacionTableAdapter.ClearBeforeFill = True
        '
        'SumaDetalleDataGridView
        '
        Me.SumaDetalleDataGridView.AllowUserToAddRows = False
        Me.SumaDetalleDataGridView.AllowUserToDeleteRows = False
        DataGridViewCellStyle19.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle19
        Me.SumaDetalleDataGridView.AutoGenerateColumns = False
        Me.SumaDetalleDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SumaDetalleDataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.SumaDetalleDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SumaDetalleDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvSessionDataGridViewTextBoxColumn1, Me.PosicionDataGridViewTextBoxColumn, Me.NivelDataGridViewTextBoxColumn, Me.DescripcionDataGridViewTextBoxColumn1, Me.TotalDataGridViewTextBoxColumn})
        Me.SumaDetalleDataGridView.DataSource = Me.SumaDetalleBindingSource
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.SumaDetalleDataGridView.DefaultCellStyle = DataGridViewCellStyle23
        Me.SumaDetalleDataGridView.GridColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Location = New System.Drawing.Point(514, 524)
        Me.SumaDetalleDataGridView.MultiSelect = False
        Me.SumaDetalleDataGridView.Name = "SumaDetalleDataGridView"
        Me.SumaDetalleDataGridView.RightToLeft = System.Windows.Forms.RightToLeft.No
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SumaDetalleDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle24
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.RowsDefaultCellStyle = DataGridViewCellStyle25
        Me.SumaDetalleDataGridView.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SumaDetalleDataGridView.Size = New System.Drawing.Size(490, 132)
        Me.SumaDetalleDataGridView.TabIndex = 199
        Me.SumaDetalleDataGridView.TabStop = False
        '
        'ClvSessionDataGridViewTextBoxColumn1
        '
        Me.ClvSessionDataGridViewTextBoxColumn1.DataPropertyName = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.HeaderText = "Clv_Session"
        Me.ClvSessionDataGridViewTextBoxColumn1.Name = "ClvSessionDataGridViewTextBoxColumn1"
        Me.ClvSessionDataGridViewTextBoxColumn1.Visible = False
        '
        'PosicionDataGridViewTextBoxColumn
        '
        Me.PosicionDataGridViewTextBoxColumn.DataPropertyName = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.HeaderText = "Posicion"
        Me.PosicionDataGridViewTextBoxColumn.Name = "PosicionDataGridViewTextBoxColumn"
        Me.PosicionDataGridViewTextBoxColumn.Visible = False
        '
        'NivelDataGridViewTextBoxColumn
        '
        Me.NivelDataGridViewTextBoxColumn.DataPropertyName = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.HeaderText = "Nivel"
        Me.NivelDataGridViewTextBoxColumn.Name = "NivelDataGridViewTextBoxColumn"
        Me.NivelDataGridViewTextBoxColumn.Visible = False
        '
        'DescripcionDataGridViewTextBoxColumn1
        '
        Me.DescripcionDataGridViewTextBoxColumn1.DataPropertyName = "Descripcion"
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionDataGridViewTextBoxColumn1.DefaultCellStyle = DataGridViewCellStyle21
        Me.DescripcionDataGridViewTextBoxColumn1.HeaderText = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn1.Name = "DescripcionDataGridViewTextBoxColumn1"
        Me.DescripcionDataGridViewTextBoxColumn1.Width = 200
        '
        'TotalDataGridViewTextBoxColumn
        '
        Me.TotalDataGridViewTextBoxColumn.DataPropertyName = "Total"
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomRight
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle22
        Me.TotalDataGridViewTextBoxColumn.HeaderText = "Total"
        Me.TotalDataGridViewTextBoxColumn.Name = "TotalDataGridViewTextBoxColumn"
        Me.TotalDataGridViewTextBoxColumn.Width = 245
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BORCAMDOCFAC_QUITABindingSource
        '
        Me.BORCAMDOCFAC_QUITABindingSource.DataMember = "BORCAMDOCFAC_QUITA"
        Me.BORCAMDOCFAC_QUITABindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BORCAMDOCFAC_QUITATableAdapter
        '
        Me.BORCAMDOCFAC_QUITATableAdapter.ClearBeforeFill = True
        '
        'DamelasOrdenesque_GeneroFacturaBindingSource
        '
        Me.DamelasOrdenesque_GeneroFacturaBindingSource.DataMember = "DamelasOrdenesque_GeneroFactura"
        Me.DamelasOrdenesque_GeneroFacturaBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DamelasOrdenesque_GeneroFacturaTableAdapter
        '
        Me.DamelasOrdenesque_GeneroFacturaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Impresora_OrdenesBindingSource
        '
        Me.Dame_Impresora_OrdenesBindingSource.DataMember = "Dame_Impresora_Ordenes"
        Me.Dame_Impresora_OrdenesBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Dame_Impresora_OrdenesTableAdapter
        '
        Me.Dame_Impresora_OrdenesTableAdapter.ClearBeforeFill = True
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkRed
        Me.Button9.Enabled = False
        Me.Button9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.Location = New System.Drawing.Point(201, 577)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(181, 31)
        Me.Button9.TabIndex = 503
        Me.Button9.Text = "&SERVICIOS PPE"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'DimesiahiConexBindingSource
        '
        Me.DimesiahiConexBindingSource.DataMember = "DimesiahiConex"
        Me.DimesiahiConexBindingSource.DataSource = Me.DataSetEdgar
        '
        'DimesiahiConexTableAdapter
        '
        Me.DimesiahiConexTableAdapter.ClearBeforeFill = True
        '
        'BuscaBloqueadoBindingSource
        '
        Me.BuscaBloqueadoBindingSource.DataMember = "BuscaBloqueado"
        Me.BuscaBloqueadoBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BuscaBloqueadoTableAdapter
        '
        Me.BuscaBloqueadoTableAdapter.ClearBeforeFill = True
        '
        'BusFacFiscalBindingSource
        '
        Me.BusFacFiscalBindingSource.DataMember = "BusFacFiscal"
        Me.BusFacFiscalBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'BusFacFiscalTableAdapter
        '
        Me.BusFacFiscalTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Comentario2BindingSource
        '
        Me.Inserta_Comentario2BindingSource.DataMember = "Inserta_Comentario2"
        Me.Inserta_Comentario2BindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Inserta_Comentario2TableAdapter
        '
        Me.Inserta_Comentario2TableAdapter.ClearBeforeFill = True
        '
        'Hora_insBindingSource
        '
        Me.Hora_insBindingSource.DataMember = "Hora_ins"
        Me.Hora_insBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'Hora_insTableAdapter
        '
        Me.Hora_insTableAdapter.ClearBeforeFill = True
        '
        'Selecciona_Impresora_SucursalBindingSource
        '
        Me.Selecciona_Impresora_SucursalBindingSource.DataSource = Me.NewsoftvDataSet2
        Me.Selecciona_Impresora_SucursalBindingSource.Position = 0
        '
        'Selecciona_Impresora_SucursalTableAdapter
        '
        Me.Selecciona_Impresora_SucursalTableAdapter.ClearBeforeFill = True
        '
        'Dime_ContratacionBindingSource
        '
        Me.Dime_ContratacionBindingSource.DataSource = Me.NewsoftvDataSet2
        Me.Dime_ContratacionBindingSource.Position = 0
        '
        'Dime_ContratacionTableAdapter
        '
        Me.Dime_ContratacionTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel6
        '
        Me.CMBPanel6.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBPanel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CMBPanel6.Controls.Add(Me.Label21)
        Me.CMBPanel6.Controls.Add(Me.Button11)
        Me.CMBPanel6.Controls.Add(Me.Button10)
        Me.CMBPanel6.Location = New System.Drawing.Point(273, 275)
        Me.CMBPanel6.Name = "CMBPanel6"
        Me.CMBPanel6.Size = New System.Drawing.Size(448, 192)
        Me.CMBPanel6.TabIndex = 507
        Me.CMBPanel6.Visible = False
        '
        'Label21
        '
        Me.Label21.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(34, 18)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(381, 87)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "¿ Deseas Hacer el Pago de la Contratación en Pagos Parciales ?"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button11
        '
        Me.Button11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button11.Location = New System.Drawing.Point(227, 121)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(95, 40)
        Me.Button11.TabIndex = 1
        Me.Button11.Text = "No"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(108, 121)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(95, 40)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "Sí"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Dime_Si_ProcedePagoParcialBindingSource
        '
        Me.Dime_Si_ProcedePagoParcialBindingSource.DataMember = "Dime_Si_ProcedePagoParcial"
        Me.Dime_Si_ProcedePagoParcialBindingSource.DataSource = Me.DataSetEdgar
        '
        'Dime_Si_ProcedePagoParcialTableAdapter
        '
        Me.Dime_Si_ProcedePagoParcialTableAdapter.ClearBeforeFill = True
        '
        'Cobra_PagosBindingSource
        '
        Me.Cobra_PagosBindingSource.DataMember = "Cobra_Pagos"
        Me.Cobra_PagosBindingSource.DataSource = Me.DataSetEdgar
        '
        'Cobra_PagosTableAdapter
        '
        Me.Cobra_PagosTableAdapter.ClearBeforeFill = True
        '
        'AgregarServicioAdicionales_PPEBindingSource1
        '
        Me.AgregarServicioAdicionales_PPEBindingSource1.DataMember = "AgregarServicioAdicionales_PPE"
        Me.AgregarServicioAdicionales_PPEBindingSource1.DataSource = Me.DataSetEdgar
        '
        'AgregarServicioAdicionales_PPETableAdapter1
        '
        Me.AgregarServicioAdicionales_PPETableAdapter1.ClearBeforeFill = True
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.DarkRed
        Me.Button12.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button12.ForeColor = System.Drawing.Color.White
        Me.Button12.Location = New System.Drawing.Point(388, 577)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(120, 31)
        Me.Button12.TabIndex = 508
        Me.Button12.Text = "&COBRA ADEUDO"
        Me.Button12.UseVisualStyleBackColor = False
        '
        'CLV_DETALLETextBox
        '
        Me.CLV_DETALLETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDetalleBindingSource, "CLV_DETALLE", True))
        Me.CLV_DETALLETextBox.Enabled = False
        Me.CLV_DETALLETextBox.Location = New System.Drawing.Point(268, 550)
        Me.CLV_DETALLETextBox.Name = "CLV_DETALLETextBox"
        Me.CLV_DETALLETextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLV_DETALLETextBox.TabIndex = 509
        '
        'CobraAdeudoBindingSource
        '
        Me.CobraAdeudoBindingSource.DataMember = "CobraAdeudo"
        Me.CobraAdeudoBindingSource.DataSource = Me.DataSetEdgar
        '
        'CobraAdeudoTableAdapter
        '
        Me.CobraAdeudoTableAdapter.ClearBeforeFill = True
        '
        'Pregunta_Si_Puedo_AdelantarBindingSource
        '
        Me.Pregunta_Si_Puedo_AdelantarBindingSource.DataMember = "Pregunta_Si_Puedo_Adelantar"
        Me.Pregunta_Si_Puedo_AdelantarBindingSource.DataSource = Me.DataSetEdgar
        '
        'Pregunta_Si_Puedo_AdelantarTableAdapter
        '
        Me.Pregunta_Si_Puedo_AdelantarTableAdapter.ClearBeforeFill = True
        '
        'Cobra_VentasBindingSource
        '
        Me.Cobra_VentasBindingSource.DataMember = "Cobra_Ventas"
        Me.Cobra_VentasBindingSource.DataSource = Me.DataSetEdgar
        '
        'Cobra_VentasTableAdapter
        '
        Me.Cobra_VentasTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Bonificacion_SupervisorBindingSource
        '
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataMember = "Inserta_Bonificacion_Supervisor"
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_Bonificacion_SupervisorTableAdapter
        '
        Me.Inserta_Bonificacion_SupervisorTableAdapter.ClearBeforeFill = True
        '
        'EricDataSet2
        '
        Me.EricDataSet2.DataSetName = "EricDataSet2"
        Me.EricDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameGeneralMsjTicketsBindingSource
        '
        Me.DameGeneralMsjTicketsBindingSource.DataMember = "DameGeneralMsjTickets"
        Me.DameGeneralMsjTicketsBindingSource.DataSource = Me.EricDataSet2
        '
        'DameGeneralMsjTicketsTableAdapter
        '
        Me.DameGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'EntregaAparatoBindingSource
        '
        Me.EntregaAparatoBindingSource.DataMember = "EntregaAparato"
        Me.EntregaAparatoBindingSource.DataSource = Me.EricDataSet2
        '
        'EntregaAparatoTableAdapter
        '
        Me.EntregaAparatoTableAdapter.ClearBeforeFill = True
        '
        'EricDataSet
        '
        Me.EricDataSet.DataSetName = "EricDataSet"
        Me.EricDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameServicioAsignadoBindingSource1
        '
        Me.DameServicioAsignadoBindingSource1.DataMember = "DameServicioAsignado"
        Me.DameServicioAsignadoBindingSource1.DataSource = Me.EricDataSet
        '
        'DameServicioAsignadoTableAdapter
        '
        Me.DameServicioAsignadoTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'ButtonPaqAdic
        '
        Me.ButtonPaqAdic.BackColor = System.Drawing.Color.DarkRed
        Me.ButtonPaqAdic.Enabled = False
        Me.ButtonPaqAdic.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonPaqAdic.ForeColor = System.Drawing.Color.White
        Me.ButtonPaqAdic.Location = New System.Drawing.Point(6, 577)
        Me.ButtonPaqAdic.Name = "ButtonPaqAdic"
        Me.ButtonPaqAdic.Size = New System.Drawing.Size(189, 31)
        Me.ButtonPaqAdic.TabIndex = 510
        Me.ButtonPaqAdic.Text = "&AGREGAR PAQ. ADIC. TEL."
        Me.ButtonPaqAdic.UseVisualStyleBackColor = False
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter6
        '
        Me.VerAcceso2TableAdapter6.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter7
        '
        Me.VerAcceso2TableAdapter7.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter8
        '
        Me.VerAcceso2TableAdapter8.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter9
        '
        Me.VerAcceso2TableAdapter9.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter10
        '
        Me.VerAcceso2TableAdapter10.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter11
        '
        Me.VerAcceso2TableAdapter11.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter12
        '
        Me.VerAcceso2TableAdapter12.ClearBeforeFill = True
        '
        'FrmFAC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 750)
        Me.Controls.Add(Me.ButtonPaqAdic)
        Me.Controls.Add(Me.TextBoxClienteObs)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.CLV_DETALLETextBox)
        Me.Controls.Add(Me.CMBPanel6)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.SumaDetalleDataGridView)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "FrmFAC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Facturacion"
        CType(Me.SumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BorraClv_SessionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CobraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrabaFacturasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgregarServicioAdicionalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PagosAdelantadosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCLIPORCONTRATOFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.UltimoSERIEYFOLIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAVENDEDORES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMETIPOSCLIENTESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.DAMEUltimo_FOLIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMETOTALSumaDetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDATIPOPAGOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.QUITARDELDETALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.GuardaMotivosBonificacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SumaDetalleDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BORCAMDOCFAC_QUITABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamelasOrdenesque_GeneroFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_Impresora_OrdenesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DimesiahiConexBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServicioAsignadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BuscaBloqueadoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BusFacFiscalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Comentario2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hora_insBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_Impresora_SucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dime_ContratacionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel6.ResumeLayout(False)
        CType(Me.Dime_Si_ProcedePagoParcialBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cobra_PagosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AgregarServicioAdicionales_PPEBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CobraAdeudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pregunta_Si_Puedo_AdelantarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Cobra_VentasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EntregaAparatoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameServicioAsignadoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SumaDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents BUSCLIPORCONTRATO_FACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCLIPORCONTRATO_FACTableAdapter
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents DameDetalleTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDetalleTableAdapter
    Friend WithEvents BorraClv_SessionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BorraClv_SessionTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BorraClv_SessionTableAdapter
    Friend WithEvents CobraTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CobraTableAdapter
    Friend WithEvents DameSerDELCliFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliFACTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameSerDELCliFACTableAdapter
    Friend WithEvents CobraBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DameDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AgregarServicioAdicionalesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.AgregarServicioAdicionalesTableAdapter
    Friend WithEvents GrabaFacturasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GrabaFacturasTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.GrabaFacturasTableAdapter
    Friend WithEvents AgregarServicioAdicionalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SumaDetalleTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.SumaDetalleTableAdapter
    Friend WithEvents PagosAdelantadosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PagosAdelantadosTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.PagosAdelantadosTableAdapter
    Friend WithEvents BUSCLIPORCONTRATOFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents DamedatosUsuarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter
    Friend WithEvents DAMENOMBRESUCURSALTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LblVersion As System.Windows.Forms.Label
    Friend WithEvents LblFecha As System.Windows.Forms.Label
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblUsuario As System.Windows.Forms.Label
    Friend WithEvents DamedatosUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblSucursal As System.Windows.Forms.Label
    Friend WithEvents DAMENOMBRESUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LblNomCaja As System.Windows.Forms.Label
    Friend WithEvents LblSistema As System.Windows.Forms.Label
    Friend WithEvents LblNomEmpresa As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents FolioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Clv_Session As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents MUESTRAVENDEDORES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAVENDEDORES_2TableAdapter As softvFacturacion.DataSetEdgarTableAdapters.MUESTRAVENDEDORES_2TableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents UltimoSERIEYFOLIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Ultimo_SERIEYFOLIOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Ultimo_SERIEYFOLIOTableAdapter
    Friend WithEvents DAMEUltimo_FOLIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEUltimo_FOLIOTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMEUltimo_FOLIOTableAdapter
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents DAMETOTALSumaDetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETOTALSumaDetalleTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.DAMETOTALSumaDetalleTableAdapter
    Friend WithEvents GUARDATIPOPAGOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDATIPOPAGOTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.GUARDATIPOPAGOTableAdapter
    Friend WithEvents QUITARDELDETALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents QUITARDELDETALLETableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.QUITARDELDETALLETableAdapter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DAMETIPOSCLIENTESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMETIPOSCLIENTESTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DAMETIPOSCLIENTESTableAdapter
    Friend WithEvents DESCRIPCIONLabel1 As System.Windows.Forms.Label
    Friend WithEvents CLV_TIPOCLIENTELabel1 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents LABEL19 As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLVSERVICIODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvllavedelservicioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvUnicaNetDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAVEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MACCABLEMODEM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCORTADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pagos_Adelantados As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TvAdicDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesCortesiaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesApagarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoIniDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PeriodoPagadoFinDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosOtrosDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosAntDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PuntosAplicadosPagoAdelantadoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescuentoNet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Otr_Ser_Misma_Categoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BonificacionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteAdicionalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColumnaDetalleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiasBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MesesBonificarDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteBonificaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoMesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UltimoanioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AdelantadoDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DESCRIPCIONDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLV_DETALLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents GuardaMotivosBonificacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GuardaMotivosBonificacionTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.GuardaMotivosBonificacionTableAdapter
    Friend WithEvents SumaDetalleDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents BORCAMDOCFAC_QUITABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BORCAMDOCFAC_QUITATableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BORCAMDOCFAC_QUITATableAdapter
    Friend WithEvents DamelasOrdenesque_GeneroFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DamelasOrdenesque_GeneroFacturaTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DamelasOrdenesque_GeneroFacturaTableAdapter
    Friend WithEvents Dame_Impresora_OrdenesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Impresora_OrdenesTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dame_Impresora_OrdenesTableAdapter
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents DimesiahiConexBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DimesiahiConexTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DimesiahiConexTableAdapter
    Friend WithEvents DameServicioAsignadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BuscaBloqueadoTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BuscaBloqueadoTableAdapter
    Friend WithEvents BusFacFiscalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BusFacFiscalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Friend WithEvents Inserta_Comentario2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Comentario2TableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Inserta_Comentario2TableAdapter
    Friend WithEvents Hora_insBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Hora_insTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Hora_insTableAdapter
    Friend WithEvents Selecciona_Impresora_SucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_Impresora_SucursalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Selecciona_Impresora_SucursalTableAdapter
    Friend WithEvents Dime_ContratacionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_ContratacionTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_ContratacionTableAdapter
    Friend WithEvents CMBPanel6 As System.Windows.Forms.Panel
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Dime_Si_ProcedePagoParcialBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dime_Si_ProcedePagoParcialTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Dime_Si_ProcedePagoParcialTableAdapter
    Friend WithEvents Cobra_PagosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cobra_PagosTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Cobra_PagosTableAdapter
    Friend WithEvents AgregarServicioAdicionales_PPEBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents AgregarServicioAdicionales_PPETableAdapter1 As softvFacturacion.DataSetEdgarTableAdapters.AgregarServicioAdicionales_PPETableAdapter
    Friend WithEvents ClvSessionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PosicionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NivelDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents CLV_DETALLETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CobraAdeudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CobraAdeudoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CobraAdeudoTableAdapter
    Friend WithEvents Pregunta_Si_Puedo_AdelantarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Pregunta_Si_Puedo_AdelantarTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Pregunta_Si_Puedo_AdelantarTableAdapter
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Cobra_VentasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Cobra_VentasTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Cobra_VentasTableAdapter
    Friend WithEvents Fecha_Venta As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Inserta_Bonificacion_SupervisorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bonificacion_SupervisorTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter
    Friend WithEvents REDLabel25 As System.Windows.Forms.Label
    Friend WithEvents Dime_Si_DatosFiscalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EricDataSet2 As softvFacturacion.EricDataSet2
    Friend WithEvents DameGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralMsjTicketsTableAdapter As softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents EntregaAparatoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EntregaAparatoTableAdapter As softvFacturacion.EricDataSet2TableAdapters.EntregaAparatoTableAdapter
    Friend WithEvents REDLabel26 As System.Windows.Forms.Label
    Friend WithEvents EricDataSet As softvFacturacion.EricDataSet
    Friend WithEvents DameServicioAsignadoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DameServicioAsignadoTableAdapter As softvFacturacion.EricDataSetTableAdapters.DameServicioAsignadoTableAdapter
    Friend WithEvents TextBoxClienteObs As System.Windows.Forms.TextBox
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents ButtonPaqAdic As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter6 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter7 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter8 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter9 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter10 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter11 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter12 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    'Friend WithEvents Dime_Si_DatosFiscalesTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.Dime_Si_DatosFiscalesTableAdapter
End Class
