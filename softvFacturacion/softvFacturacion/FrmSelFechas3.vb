Imports System.Data.SqlClient
Imports System.Text
Public Class FrmSelFechas3

    Private Function MuestraTipServEric() As BindingSource
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC MuestraTipServEric 0,0", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        dataAdapter.Fill(dataTable)
        bindingSource.DataSource = dataTable
        Return bindingSource
    End Function

    Private Function MuestraOperadores() As BindingSource
        Dim conexion As New SqlConnection(MiConexion)
        Dim dataAdapter As New SqlDataAdapter("EXEC MuestraOperadores", conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        dataAdapter.Fill(dataTable)
        bindingSource.DataSource = dataTable
        Return bindingSource
    End Function

    Private Sub FrmSelFechas3_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.DateTimePicker1.Value = Today
        Me.DateTimePicker2.Value = Today

        If eOpRep = 1 Then
            Me.Text = "Ingresos de Clientes por un Monto"
            Me.GroupBox1.Text = "Tipo de Servicio"
            Me.ComboBox1.Visible = True
            Me.GroupBox3.Visible = True
            Me.ComboBox1.DataSource = MuestraTipServEric()
            Me.ComboBox2.DataSource = MuestraOperadores()
        ElseIf eOpRep = 2 Then
            Me.Text = "N�mero de Bonificaciones"
            Me.GroupBox1.Text = "N�mero de Bonificaciones"
            Me.TextBox1.Visible = True
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(TextBox2, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        eFechaIni = Me.DateTimePicker1.Value
        eFechaFin = Me.DateTimePicker2.Value

        If eOpRep = 1 Then
            If Me.ComboBox1.Text.Length = 0 Then
                MsgBox("Selecciona un Tipo de Servicio.", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.ComboBox2.Text.Length = 0 Then
                MsgBox("Selecciona un Operador.", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.TextBox2.Text.Length = 0 Then
                MsgBox("Establece un Monto.", MsgBoxStyle.Information)
                Exit Sub
            End If
            eClvTipSer = Me.ComboBox1.SelectedValue
            eConcepto = Me.ComboBox1.Text
            eOp = Me.ComboBox2.SelectedValue
            eOperador = Me.ComboBox2.Text
            eMonto = Me.TextBox2.Text

            Dim reporte As New FrmImprimirRepGral
            reporte.ReporteMontos()
            reporte.Show()

        ElseIf eOpRep = 2 Then
            If Me.TextBox1.Text.Length = 0 Then
                MsgBox("Establece un N�mero de Bonificaciones.", MsgBoxStyle.Information)
                Exit Sub
            End If

            eNumero = Me.TextBox1.Text

            Dim reporte As New FrmImprimirRepGral
            reporte.ReporteBonificacion()
            reporte.Show()

        End If

        Me.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class