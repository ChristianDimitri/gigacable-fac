<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.EfectivoTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CMBLabel2 = New System.Windows.Forms.Label()
        Me.CMBTotalaPagarLabel = New System.Windows.Forms.Label()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.CMBCambioLabel = New System.Windows.Forms.Label()
        Me.ChequeTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TarjetaTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CMBAbonoLabel = New System.Windows.Forms.Label()
        Me.CMBLabel7 = New System.Windows.Forms.Label()
        Me.CMBSaldoLabel = New System.Windows.Forms.Label()
        Me.CMBLabel8 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.cbBancoTransferencia = New System.Windows.Forms.ComboBox()
        Me.tbAutorizacionTransferencia = New System.Windows.Forms.TextBox()
        Me.tbNumeroTransferencia = New System.Windows.Forms.TextBox()
        Me.tbTransferencia = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.pnFONACOT = New System.Windows.Forms.Panel()
        Me.tbNumeroFONACOTEmpresa = New System.Windows.Forms.TextBox()
        Me.tbNumeroCreditoFONACOT = New System.Windows.Forms.TextBox()
        Me.tbNumeroFONACOTCliente = New System.Windows.Forms.TextBox()
        Me.tbFONACOT = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.REDLabel7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.CmbTipCuenta = New System.Windows.Forms.ComboBox()
        Me.LblTipCuenta = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.MUESTRABANCOS1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.tbNumeroDeCuentaCheque = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.MUESTRABANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.MUESTRABANCOSTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRABANCOSTableAdapter()
        Me.MUESTRABANCOS1TableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRABANCOS1TableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Inserta_Bonificacion_SupervisorBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Bonificacion_SupervisorTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.pnFONACOT.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.MUESTRABANCOS1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'EfectivoTextBox
        '
        Me.EfectivoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EfectivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EfectivoTextBox.Location = New System.Drawing.Point(303, 5)
        Me.EfectivoTextBox.MaxLength = 18
        Me.EfectivoTextBox.Name = "EfectivoTextBox"
        Me.EfectivoTextBox.Size = New System.Drawing.Size(105, 22)
        Me.EfectivoTextBox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(127, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Pago en Efectivo :"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(138, 11)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(280, 29)
        Me.CMBLabel2.TabIndex = 2
        Me.CMBLabel2.Text = "Importe Total a Pagar :"
        '
        'CMBTotalaPagarLabel
        '
        Me.CMBTotalaPagarLabel.AutoSize = True
        Me.CMBTotalaPagarLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBTotalaPagarLabel.Location = New System.Drawing.Point(422, 11)
        Me.CMBTotalaPagarLabel.Name = "CMBTotalaPagarLabel"
        Me.CMBTotalaPagarLabel.Size = New System.Drawing.Size(62, 29)
        Me.CMBTotalaPagarLabel.TabIndex = 3
        Me.CMBTotalaPagarLabel.Text = "0.00"
        '
        'CMBLabel5
        '
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel5.Location = New System.Drawing.Point(33, 664)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(174, 66)
        Me.CMBLabel5.TabIndex = 9
        Me.CMBLabel5.Text = "Cambio : "
        '
        'CMBCambioLabel
        '
        Me.CMBCambioLabel.AutoSize = True
        Me.CMBCambioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBCambioLabel.ForeColor = System.Drawing.Color.Black
        Me.CMBCambioLabel.Location = New System.Drawing.Point(213, 664)
        Me.CMBCambioLabel.Name = "CMBCambioLabel"
        Me.CMBCambioLabel.Size = New System.Drawing.Size(40, 42)
        Me.CMBCambioLabel.TabIndex = 10
        Me.CMBCambioLabel.Text = "0"
        '
        'ChequeTextBox
        '
        Me.ChequeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ChequeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChequeTextBox.Location = New System.Drawing.Point(303, 6)
        Me.ChequeTextBox.MaxLength = 18
        Me.ChequeTextBox.Name = "ChequeTextBox"
        Me.ChequeTextBox.Size = New System.Drawing.Size(105, 22)
        Me.ChequeTextBox.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(130, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(131, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Pago en Cheque :"
        '
        'TarjetaTextBox
        '
        Me.TarjetaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TarjetaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TarjetaTextBox.Location = New System.Drawing.Point(303, 6)
        Me.TarjetaTextBox.MaxLength = 18
        Me.TarjetaTextBox.Name = "TarjetaTextBox"
        Me.TarjetaTextBox.Size = New System.Drawing.Size(105, 22)
        Me.TarjetaTextBox.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(133, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(128, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Pago en Tarjeta :"
        '
        'CMBAbonoLabel
        '
        Me.CMBAbonoLabel.AutoSize = True
        Me.CMBAbonoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBAbonoLabel.Location = New System.Drawing.Point(779, 663)
        Me.CMBAbonoLabel.Name = "CMBAbonoLabel"
        Me.CMBAbonoLabel.Size = New System.Drawing.Size(45, 24)
        Me.CMBAbonoLabel.TabIndex = 12
        Me.CMBAbonoLabel.Text = "0.00"
        '
        'CMBLabel7
        '
        Me.CMBLabel7.AutoSize = True
        Me.CMBLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel7.Location = New System.Drawing.Point(614, 663)
        Me.CMBLabel7.Name = "CMBLabel7"
        Me.CMBLabel7.Size = New System.Drawing.Size(159, 24)
        Me.CMBLabel7.TabIndex = 11
        Me.CMBLabel7.Text = "Total a Abonado :"
        '
        'CMBSaldoLabel
        '
        Me.CMBSaldoLabel.AutoSize = True
        Me.CMBSaldoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBSaldoLabel.Location = New System.Drawing.Point(779, 694)
        Me.CMBSaldoLabel.Name = "CMBSaldoLabel"
        Me.CMBSaldoLabel.Size = New System.Drawing.Size(49, 24)
        Me.CMBSaldoLabel.TabIndex = 14
        Me.CMBSaldoLabel.Text = "0.00"
        '
        'CMBLabel8
        '
        Me.CMBLabel8.AutoSize = True
        Me.CMBLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel8.Location = New System.Drawing.Point(698, 694)
        Me.CMBLabel8.Name = "CMBLabel8"
        Me.CMBLabel8.Size = New System.Drawing.Size(75, 24)
        Me.CMBLabel8.TabIndex = 13
        Me.CMBLabel8.Text = "Saldo :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.pnFONACOT)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(214, 43)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(575, 618)
        Me.Panel1.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel6.Controls.Add(Me.cbBancoTransferencia)
        Me.Panel6.Controls.Add(Me.tbAutorizacionTransferencia)
        Me.Panel6.Controls.Add(Me.tbNumeroTransferencia)
        Me.Panel6.Controls.Add(Me.tbTransferencia)
        Me.Panel6.Controls.Add(Me.Label15)
        Me.Panel6.Controls.Add(Me.Label16)
        Me.Panel6.Controls.Add(Me.Label17)
        Me.Panel6.Controls.Add(Me.Label18)
        Me.Panel6.Location = New System.Drawing.Point(10, 376)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(554, 118)
        Me.Panel6.TabIndex = 4
        '
        'cbBancoTransferencia
        '
        Me.cbBancoTransferencia.DisplayMember = "nombre"
        Me.cbBancoTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbBancoTransferencia.FormattingEnabled = True
        Me.cbBancoTransferencia.Location = New System.Drawing.Point(303, 34)
        Me.cbBancoTransferencia.Name = "cbBancoTransferencia"
        Me.cbBancoTransferencia.Size = New System.Drawing.Size(239, 24)
        Me.cbBancoTransferencia.TabIndex = 13
        Me.cbBancoTransferencia.ValueMember = "Clave"
        '
        'tbAutorizacionTransferencia
        '
        Me.tbAutorizacionTransferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbAutorizacionTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbAutorizacionTransferencia.Location = New System.Drawing.Point(303, 88)
        Me.tbAutorizacionTransferencia.MaxLength = 10
        Me.tbAutorizacionTransferencia.Name = "tbAutorizacionTransferencia"
        Me.tbAutorizacionTransferencia.Size = New System.Drawing.Size(212, 22)
        Me.tbAutorizacionTransferencia.TabIndex = 3
        '
        'tbNumeroTransferencia
        '
        Me.tbNumeroTransferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumeroTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNumeroTransferencia.Location = New System.Drawing.Point(303, 60)
        Me.tbNumeroTransferencia.MaxLength = 18
        Me.tbNumeroTransferencia.Name = "tbNumeroTransferencia"
        Me.tbNumeroTransferencia.Size = New System.Drawing.Size(212, 22)
        Me.tbNumeroTransferencia.TabIndex = 2
        '
        'tbTransferencia
        '
        Me.tbTransferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTransferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbTransferencia.Location = New System.Drawing.Point(303, 6)
        Me.tbTransferencia.Name = "tbTransferencia"
        Me.tbTransferencia.Size = New System.Drawing.Size(100, 22)
        Me.tbTransferencia.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(160, 94)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(101, 16)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "Autorización :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(115, 62)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(144, 16)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "Número de Cuenta :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(201, 42)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(60, 16)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Banco :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(13, 10)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(248, 16)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "Pago con Transferencia Bancaria :"
        '
        'pnFONACOT
        '
        Me.pnFONACOT.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnFONACOT.Controls.Add(Me.tbNumeroFONACOTEmpresa)
        Me.pnFONACOT.Controls.Add(Me.tbNumeroCreditoFONACOT)
        Me.pnFONACOT.Controls.Add(Me.tbNumeroFONACOTCliente)
        Me.pnFONACOT.Controls.Add(Me.tbFONACOT)
        Me.pnFONACOT.Controls.Add(Me.Label14)
        Me.pnFONACOT.Controls.Add(Me.Label13)
        Me.pnFONACOT.Controls.Add(Me.Label8)
        Me.pnFONACOT.Controls.Add(Me.Label7)
        Me.pnFONACOT.Location = New System.Drawing.Point(10, 497)
        Me.pnFONACOT.Name = "pnFONACOT"
        Me.pnFONACOT.Size = New System.Drawing.Size(554, 114)
        Me.pnFONACOT.TabIndex = 5
        '
        'tbNumeroFONACOTEmpresa
        '
        Me.tbNumeroFONACOTEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumeroFONACOTEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNumeroFONACOTEmpresa.Location = New System.Drawing.Point(303, 84)
        Me.tbNumeroFONACOTEmpresa.MaxLength = 20
        Me.tbNumeroFONACOTEmpresa.Name = "tbNumeroFONACOTEmpresa"
        Me.tbNumeroFONACOTEmpresa.Size = New System.Drawing.Size(212, 22)
        Me.tbNumeroFONACOTEmpresa.TabIndex = 3
        '
        'tbNumeroCreditoFONACOT
        '
        Me.tbNumeroCreditoFONACOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumeroCreditoFONACOT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNumeroCreditoFONACOT.Location = New System.Drawing.Point(303, 32)
        Me.tbNumeroCreditoFONACOT.MaxLength = 20
        Me.tbNumeroCreditoFONACOT.Name = "tbNumeroCreditoFONACOT"
        Me.tbNumeroCreditoFONACOT.Size = New System.Drawing.Size(212, 22)
        Me.tbNumeroCreditoFONACOT.TabIndex = 1
        '
        'tbNumeroFONACOTCliente
        '
        Me.tbNumeroFONACOTCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumeroFONACOTCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbNumeroFONACOTCliente.Location = New System.Drawing.Point(303, 58)
        Me.tbNumeroFONACOTCliente.MaxLength = 20
        Me.tbNumeroFONACOTCliente.Name = "tbNumeroFONACOTCliente"
        Me.tbNumeroFONACOTCliente.Size = New System.Drawing.Size(212, 22)
        Me.tbNumeroFONACOTCliente.TabIndex = 2
        '
        'tbFONACOT
        '
        Me.tbFONACOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbFONACOT.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbFONACOT.Location = New System.Drawing.Point(303, 6)
        Me.tbFONACOT.Name = "tbFONACOT"
        Me.tbFONACOT.Size = New System.Drawing.Size(100, 22)
        Me.tbFONACOT.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(10, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(251, 16)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "Número FONACOT de la Empresa :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(37, 62)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(224, 16)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Número FONACOT del Cliente :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(115, 36)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(146, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Número de Crédito :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(49, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(212, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Pago con Crédito FONACOT :"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel5.Controls.Add(Me.TextBox4)
        Me.Panel5.Controls.Add(Me.REDLabel7)
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Me.TextBox5)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Location = New System.Drawing.Point(10, 317)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(554, 57)
        Me.Panel5.TabIndex = 3
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(303, 29)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(105, 22)
        Me.TextBox4.TabIndex = 1
        '
        'REDLabel7
        '
        Me.REDLabel7.AutoSize = True
        Me.REDLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel7.Location = New System.Drawing.Point(338, 34)
        Me.REDLabel7.Name = "REDLabel7"
        Me.REDLabel7.Size = New System.Drawing.Size(45, 13)
        Me.REDLabel7.TabIndex = 9
        Me.REDLabel7.Text = "Label7"
        Me.REDLabel7.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(86, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(175, 16)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "No. de Nota de Crédito :"
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(303, 4)
        Me.TextBox5.MaxLength = 18
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(105, 22)
        Me.TextBox5.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(74, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(187, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Pago en Nota de Crédito :"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel4.Controls.Add(Me.CmbTipCuenta)
        Me.Panel4.Controls.Add(Me.LblTipCuenta)
        Me.Panel4.Controls.Add(Me.TarjetaTextBox)
        Me.Panel4.Controls.Add(Me.Label4)
        Me.Panel4.Controls.Add(Me.ComboBox2)
        Me.Panel4.Controls.Add(Me.TextBox3)
        Me.Panel4.Controls.Add(Me.Label9)
        Me.Panel4.Controls.Add(Me.TextBox2)
        Me.Panel4.Controls.Add(Me.Label11)
        Me.Panel4.Controls.Add(Me.Label10)
        Me.Panel4.Location = New System.Drawing.Point(10, 165)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(554, 150)
        Me.Panel4.TabIndex = 2
        '
        'CmbTipCuenta
        '
        Me.CmbTipCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbTipCuenta.FormattingEnabled = True
        Me.CmbTipCuenta.Location = New System.Drawing.Point(303, 64)
        Me.CmbTipCuenta.Name = "CmbTipCuenta"
        Me.CmbTipCuenta.Size = New System.Drawing.Size(239, 24)
        Me.CmbTipCuenta.TabIndex = 2
        '
        'LblTipCuenta
        '
        Me.LblTipCuenta.AutoSize = True
        Me.LblTipCuenta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTipCuenta.Location = New System.Drawing.Point(139, 72)
        Me.LblTipCuenta.Name = "LblTipCuenta"
        Me.LblTipCuenta.Size = New System.Drawing.Size(122, 16)
        Me.LblTipCuenta.TabIndex = 16
        Me.LblTipCuenta.Text = "Tipo de Cuenta :"
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.MUESTRABANCOS1BindingSource
        Me.ComboBox2.DisplayMember = "nombre"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(303, 34)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(239, 24)
        Me.ComboBox2.TabIndex = 1
        Me.ComboBox2.ValueMember = "Clave"
        '
        'MUESTRABANCOS1BindingSource
        '
        Me.MUESTRABANCOS1BindingSource.DataMember = "MUESTRABANCOS1"
        Me.MUESTRABANCOS1BindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(303, 94)
        Me.TextBox3.MaxLength = 16
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(212, 22)
        Me.TextBox3.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(115, 100)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(146, 16)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Número de Tarjeta :"
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(303, 122)
        Me.TextBox2.MaxLength = 10
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(105, 22)
        Me.TextBox2.TabIndex = 4
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(201, 42)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 16)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Banco :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(160, 128)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(101, 16)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Autorización :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.tbNumeroDeCuentaCheque)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.ChequeTextBox)
        Me.Panel3.Controls.Add(Me.ComboBox1)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.TextBox1)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Location = New System.Drawing.Point(10, 37)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(554, 122)
        Me.Panel3.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(117, 98)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(144, 16)
        Me.Label19.TabIndex = 19
        Me.Label19.Text = "Número de Cuenta :"
        '
        'tbNumeroDeCuentaCheque
        '
        Me.tbNumeroDeCuentaCheque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbNumeroDeCuentaCheque.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbNumeroDeCuentaCheque.Location = New System.Drawing.Point(303, 92)
        Me.tbNumeroDeCuentaCheque.MaxLength = 50
        Me.tbNumeroDeCuentaCheque.Name = "tbNumeroDeCuentaCheque"
        Me.tbNumeroDeCuentaCheque.Size = New System.Drawing.Size(212, 22)
        Me.tbNumeroDeCuentaCheque.TabIndex = 3
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.MUESTRABANCOSBindingSource
        Me.ComboBox1.DisplayMember = "nombre"
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(303, 34)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(239, 24)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "Clave"
        '
        'MUESTRABANCOSBindingSource
        '
        Me.MUESTRABANCOSBindingSource.DataMember = "MUESTRABANCOS"
        Me.MUESTRABANCOSBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(201, 42)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 16)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Banco :"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(303, 64)
        Me.TextBox1.MaxLength = 50
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(212, 22)
        Me.TextBox1.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(111, 70)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(149, 16)
        Me.Label12.TabIndex = 17
        Me.Label12.Text = "Número de Cheque :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel2.Controls.Add(Me.EfectivoTextBox)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(10, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(554, 32)
        Me.Panel2.TabIndex = 0
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(376, 711)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 1
        Me.Button8.Text = "&Aceptar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(518, 711)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "&Cancelar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'MUESTRABANCOSTableAdapter
        '
        Me.MUESTRABANCOSTableAdapter.ClearBeforeFill = True
        '
        'MUESTRABANCOS1TableAdapter
        '
        Me.MUESTRABANCOS1TableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Bonificacion_SupervisorBindingSource
        '
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataMember = "Inserta_Bonificacion_Supervisor"
        Me.Inserta_Bonificacion_SupervisorBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_Bonificacion_SupervisorTableAdapter
        '
        Me.Inserta_Bonificacion_SupervisorTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'FrmPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1024, 768)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBSaldoLabel)
        Me.Controls.Add(Me.CMBLabel8)
        Me.Controls.Add(Me.CMBAbonoLabel)
        Me.Controls.Add(Me.CMBLabel7)
        Me.Controls.Add(Me.CMBCambioLabel)
        Me.Controls.Add(Me.CMBLabel5)
        Me.Controls.Add(Me.CMBTotalaPagarLabel)
        Me.Controls.Add(Me.CMBLabel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmPago"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.pnFONACOT.ResumeLayout(False)
        Me.pnFONACOT.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.MUESTRABANCOS1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.MUESTRABANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Bonificacion_SupervisorBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents EfectivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents CMBTotalaPagarLabel As System.Windows.Forms.Label
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents CMBCambioLabel As System.Windows.Forms.Label
    Friend WithEvents ChequeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TarjetaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CMBAbonoLabel As System.Windows.Forms.Label
    Friend WithEvents CMBLabel7 As System.Windows.Forms.Label
    Friend WithEvents CMBSaldoLabel As System.Windows.Forms.Label
    Friend WithEvents CMBLabel8 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents MUESTRABANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRABANCOSTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRABANCOSTableAdapter
    Friend WithEvents MUESTRABANCOS1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRABANCOS1TableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRABANCOS1TableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Inserta_Bonificacion_SupervisorBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Bonificacion_SupervisorTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Bonificacion_SupervisorTableAdapter
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents REDLabel7 As System.Windows.Forms.Label
    Friend WithEvents CmbTipCuenta As System.Windows.Forms.ComboBox
    Friend WithEvents LblTipCuenta As System.Windows.Forms.Label
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents cbBancoTransferencia As System.Windows.Forms.ComboBox
    Friend WithEvents tbAutorizacionTransferencia As System.Windows.Forms.TextBox
    Friend WithEvents tbNumeroTransferencia As System.Windows.Forms.TextBox
    Friend WithEvents tbTransferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents pnFONACOT As System.Windows.Forms.Panel
    Friend WithEvents tbNumeroFONACOTEmpresa As System.Windows.Forms.TextBox
    Friend WithEvents tbNumeroCreditoFONACOT As System.Windows.Forms.TextBox
    Friend WithEvents tbNumeroFONACOTCliente As System.Windows.Forms.TextBox
    Friend WithEvents tbFONACOT As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents tbNumeroDeCuentaCheque As System.Windows.Forms.TextBox
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
