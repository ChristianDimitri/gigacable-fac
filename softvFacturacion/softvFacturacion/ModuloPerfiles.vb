﻿Imports System.Data.SqlClient
Module ModuloPerfiles
    Public BotonesDesactivar As New DataTable
    Public Sub UspGuardaBotonesFormularioSiste(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                UspGuardaBotonesFormulario(boton.Name, boton.Text, NomInterno)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfil(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                UspGuardaBotonesFormulario(BN.Name, BN.Text, NomInterno)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfil(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfil(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfil(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfil(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                UspGuardaBotonesFormulario(boton.Name, boton.Text, NomInterno)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfil(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                UspGuardaBotonesFormulario(BN.Name, BN.Text, NomInterno)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfil(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfil(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfil(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfil(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                UspGuardaBotonesFormulario(boton.Name, boton.Text, NomInterno)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfil(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                UspGuardaBotonesFormulario(BN.Name, BN.Text, NomInterno)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfil(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfil(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfil(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfil(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                UspGuardaBotonesFormulario(boton.Name, boton.Text, NomInterno)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfil(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                UspGuardaBotonesFormulario(BN.Name, BN.Text, NomInterno)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfil(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfil(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfil(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    Public Sub UspGuardaFormularios(ByVal NOMBREFORM As String, ByVal TEXTOFORM As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREFORM", SqlDbType.VarChar, NOMBREFORM, 250)
            BaseII.CreateMyParameter("@TEXTOFORM", SqlDbType.VarChar, TEXTOFORM, 250)
            BaseII.CreateMyParameter("@CLVMENU", SqlDbType.Int, GloClaveMenus)
            BaseII.Inserta("UspGuardaFormulariosFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Public Function UspValidaMenu(ByVal nombremenu As String) As Boolean
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREMENU", SqlDbType.VarChar, nombremenu, 250)
            BaseII.CreateMyParameter("@CLCTIPOUSU", SqlDbType.Int, GloTipoUsuario)
            DT = BaseII.ConsultaDT("UspValidaMenuFac")
            UspValidaMenu = CBool(DT.Rows(0)(0).ToString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspGuardaMenu(ByVal NombreMenu As String, ByVal NombreTexto As String, ByVal MenuProviene As String, ByVal secuencia As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREMENU", SqlDbType.VarChar, NombreMenu, 250)
            BaseII.CreateMyParameter("@NOMBRETEXTO", SqlDbType.VarChar, NombreTexto, 250)
            BaseII.CreateMyParameter("@MENUPROVIENE", SqlDbType.VarChar, MenuProviene, 250)
            BaseII.CreateMyParameter("@op", SqlDbType.Int, secuencia)
            BaseII.Inserta("UspGuardaMenuFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UspGuardaBotonesFormulario(ByVal Nombreboton As String, ByVal NombreTexto As String, ByVal NombreMenu As String)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, Nombreboton, 250)
            BaseII.CreateMyParameter("@TEXTOBOTON", SqlDbType.VarChar, NombreTexto, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, NombreMenu, 250)
            BaseII.Inserta("UspGuardaBotonesFormularioFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function UspDameClaveMenuFac(ByVal NombreMenu As String) As DataTable
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREMENU", SqlDbType.VarChar, NombreMenu, 500)
            UspDameClaveMenuFac = BaseII.ConsultaDT("UspDameClaveMenuFac")
        Catch ex As Exception
            Throw ex
        End Try
    End Function



End Module
