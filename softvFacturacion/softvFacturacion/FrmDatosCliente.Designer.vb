﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDatosCliente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBlblNombre = New System.Windows.Forms.Label()
        Me.CMBlblContrato = New System.Windows.Forms.Label()
        Me.CMBlblTitulo = New System.Windows.Forms.Label()
        Me.CMBlblCelular = New System.Windows.Forms.Label()
        Me.CMBlblTelefono = New System.Windows.Forms.Label()
        Me.CMBlblCORREO = New System.Windows.Forms.Label()
        Me.txtContrato = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtCelular = New System.Windows.Forms.TextBox()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.txtCorreo = New System.Windows.Forms.TextBox()
        Me.btnCorrecto = New System.Windows.Forms.Button()
        Me.btnIncorrecto = New System.Windows.Forms.Button()
        Me.CMBlblEditar = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'CMBlblNombre
        '
        Me.CMBlblNombre.AutoSize = True
        Me.CMBlblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblNombre.Location = New System.Drawing.Point(31, 121)
        Me.CMBlblNombre.Name = "CMBlblNombre"
        Me.CMBlblNombre.Size = New System.Drawing.Size(64, 13)
        Me.CMBlblNombre.TabIndex = 0
        Me.CMBlblNombre.Text = "NOMBRE:"
        '
        'CMBlblContrato
        '
        Me.CMBlblContrato.AutoSize = True
        Me.CMBlblContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblContrato.Location = New System.Drawing.Point(31, 67)
        Me.CMBlblContrato.Name = "CMBlblContrato"
        Me.CMBlblContrato.Size = New System.Drawing.Size(79, 13)
        Me.CMBlblContrato.TabIndex = 1
        Me.CMBlblContrato.Text = "CONTRATO:"
        '
        'CMBlblTitulo
        '
        Me.CMBlblTitulo.AutoSize = True
        Me.CMBlblTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTitulo.Location = New System.Drawing.Point(255, 26)
        Me.CMBlblTitulo.Name = "CMBlblTitulo"
        Me.CMBlblTitulo.Size = New System.Drawing.Size(175, 18)
        Me.CMBlblTitulo.TabIndex = 2
        Me.CMBlblTitulo.Text = "DATOS DEL CLIENTE"
        '
        'CMBlblCelular
        '
        Me.CMBlblCelular.AutoSize = True
        Me.CMBlblCelular.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblCelular.Location = New System.Drawing.Point(31, 236)
        Me.CMBlblCelular.Name = "CMBlblCelular"
        Me.CMBlblCelular.Size = New System.Drawing.Size(71, 13)
        Me.CMBlblCelular.TabIndex = 3
        Me.CMBlblCelular.Text = "CELULAR: "
        '
        'CMBlblTelefono
        '
        Me.CMBlblTelefono.AutoSize = True
        Me.CMBlblTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTelefono.Location = New System.Drawing.Point(222, 236)
        Me.CMBlblTelefono.Name = "CMBlblTelefono"
        Me.CMBlblTelefono.Size = New System.Drawing.Size(76, 13)
        Me.CMBlblTelefono.TabIndex = 4
        Me.CMBlblTelefono.Text = "TELEFONO:"
        '
        'CMBlblCORREO
        '
        Me.CMBlblCORREO.AutoSize = True
        Me.CMBlblCORREO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblCORREO.Location = New System.Drawing.Point(419, 236)
        Me.CMBlblCORREO.Name = "CMBlblCORREO"
        Me.CMBlblCORREO.Size = New System.Drawing.Size(63, 13)
        Me.CMBlblCORREO.TabIndex = 5
        Me.CMBlblCORREO.Text = "CORREO:"
        '
        'txtContrato
        '
        Me.txtContrato.Location = New System.Drawing.Point(34, 83)
        Me.txtContrato.Name = "txtContrato"
        Me.txtContrato.Size = New System.Drawing.Size(100, 20)
        Me.txtContrato.TabIndex = 6
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(34, 138)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(396, 20)
        Me.txtNombre.TabIndex = 7
        '
        'txtCelular
        '
        Me.txtCelular.Location = New System.Drawing.Point(34, 252)
        Me.txtCelular.Name = "txtCelular"
        Me.txtCelular.Size = New System.Drawing.Size(121, 20)
        Me.txtCelular.TabIndex = 8
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(225, 252)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(121, 20)
        Me.txtTelefono.TabIndex = 9
        '
        'txtCorreo
        '
        Me.txtCorreo.Location = New System.Drawing.Point(422, 252)
        Me.txtCorreo.Name = "txtCorreo"
        Me.txtCorreo.Size = New System.Drawing.Size(201, 20)
        Me.txtCorreo.TabIndex = 10
        '
        'btnCorrecto
        '
        Me.btnCorrecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCorrecto.Location = New System.Drawing.Point(34, 314)
        Me.btnCorrecto.Name = "btnCorrecto"
        Me.btnCorrecto.Size = New System.Drawing.Size(141, 32)
        Me.btnCorrecto.TabIndex = 11
        Me.btnCorrecto.Text = "CORRECTO"
        Me.btnCorrecto.UseVisualStyleBackColor = True
        '
        'btnIncorrecto
        '
        Me.btnIncorrecto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIncorrecto.Location = New System.Drawing.Point(482, 314)
        Me.btnIncorrecto.Name = "btnIncorrecto"
        Me.btnIncorrecto.Size = New System.Drawing.Size(141, 32)
        Me.btnIncorrecto.TabIndex = 12
        Me.btnIncorrecto.Text = "INCORRECTO"
        Me.btnIncorrecto.UseVisualStyleBackColor = True
        '
        'CMBlblEditar
        '
        Me.CMBlblEditar.AutoSize = True
        Me.CMBlblEditar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblEditar.Location = New System.Drawing.Point(246, 190)
        Me.CMBlblEditar.Name = "CMBlblEditar"
        Me.CMBlblEditar.Size = New System.Drawing.Size(197, 15)
        Me.CMBlblEditar.TabIndex = 13
        Me.CMBlblEditar.Text = "EDITAR DATOS DEL CLIENTE"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(269, 314)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(141, 32)
        Me.btnGuardar.TabIndex = 14
        Me.btnGuardar.Text = "GUARDAR"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'FrmDatosCliente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 372)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.CMBlblEditar)
        Me.Controls.Add(Me.btnIncorrecto)
        Me.Controls.Add(Me.btnCorrecto)
        Me.Controls.Add(Me.txtCorreo)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.txtCelular)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtContrato)
        Me.Controls.Add(Me.CMBlblCORREO)
        Me.Controls.Add(Me.CMBlblTelefono)
        Me.Controls.Add(Me.CMBlblCelular)
        Me.Controls.Add(Me.CMBlblTitulo)
        Me.Controls.Add(Me.CMBlblContrato)
        Me.Controls.Add(Me.CMBlblNombre)
        Me.Name = "FrmDatosCliente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Datos del Cliente"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBlblNombre As System.Windows.Forms.Label
    Friend WithEvents CMBlblContrato As System.Windows.Forms.Label
    Friend WithEvents CMBlblTitulo As System.Windows.Forms.Label
    Friend WithEvents CMBlblCelular As System.Windows.Forms.Label
    Friend WithEvents CMBlblTelefono As System.Windows.Forms.Label
    Friend WithEvents CMBlblCORREO As System.Windows.Forms.Label
    Friend WithEvents txtContrato As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtCelular As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents txtCorreo As System.Windows.Forms.TextBox
    Friend WithEvents btnCorrecto As System.Windows.Forms.Button
    Friend WithEvents btnIncorrecto As System.Windows.Forms.Button
    Friend WithEvents CMBlblEditar As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
End Class
