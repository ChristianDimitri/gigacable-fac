Imports System.Data.SqlClient

Public Class FrmServicios

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox1.SelectedValue = "CEXTV" Then
            GloClv_Txt = "CEXTV"
            FrmExtecionesTv.Show()
        ElseIf Me.ComboBox1.SelectedValue = "CAMDO" Or Me.ComboBox1.SelectedValue = "CADIG" Or Me.ComboBox1.SelectedValue = "CANET" Then
            If Me.ComboBox1.SelectedValue = "CADIG" Then
                GloClv_Txt = "CADIG"
            End If
            If Me.ComboBox1.SelectedValue = "CANET" Then
                GloClv_Txt = "CANET"
            End If
            If Me.ComboBox1.SelectedValue = "CAMDO" Then
                GloClv_Txt = "CAMDO"
            End If
            FormCAMDO.Show()

        ElseIf Me.ComboBox1.SelectedValue = "CEXTE" Then
            FrmCEXTETMP.Show()
        Else
            GloClv_Txt = Me.ComboBox1.SelectedValue
            GloBndExt = True
        End If
        Me.Close()
    End Sub

    Private Sub FrmServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameServiciosFacturacionTableAdapter.Connection = CON
        Me.DameServiciosFacturacionTableAdapter.Fill(Me.NewsoftvDataSet.DameServiciosFacturacion, GloContrato)
        CON.Close()
    End Sub
End Class