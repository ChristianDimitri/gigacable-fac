Imports System.Data.SqlClient
Public Class FrmPagosInternet2
    Dim validacion As Integer = 0
    Dim pagosbuenos As Integer = 0
    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Me.Close()
    End Sub

    Private Sub FrmPagosInternet2_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

    End Sub

    Private Sub FrmPagosInternet2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        If CLng(Me.TextBox1.Text) > 100 Then
            MsgBox("El Numero de Pagos Debe Ser Menor De 100", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim con As New SqlConnection(MiConexion)
        Dim cmdarn As New SqlClient.SqlCommand
        cmdarn = New SqlClient.SqlCommand
        con.Open()

        With cmdarn
            .Connection = con
            .CommandText = "ValidaNoPagos"
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0
            '(@pagos int,@bnd1 int output,@nopagos1 int output)
            Dim prm As New SqlParameter("@pagos", SqlDbType.Int)
            Dim prm1 As New SqlParameter("@bnd1", SqlDbType.Int)
            Dim prm2 As New SqlParameter("@nopagos1", SqlDbType.Int)

            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Output
            prm2.Direction = ParameterDirection.Output

            prm.Value = CLng(Me.TextBox1.Text)
            prm1.Value = 0
            prm2.Value = 0

            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)

            Dim i As Integer = cmdarn.ExecuteNonQuery()
            validacion = prm1.Value
            pagosbuenos = prm2.Value
        End With
        con.Close()

        If validacion = 0 Then

            Dim cmdarn1 As New SqlClient.SqlCommand
            cmdarn1 = New SqlClient.SqlCommand
            con.Open()
            With cmdarn1
                .Connection = con
                .CommandText = "deslosapagos_depositos"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0

                '(@clv_session bigint,@contrato bigint, @nopagos int)
                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm2 As New SqlParameter("@nopagos", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input

                prm.Value = locclvsessionpardep
                prm1.Value = Loccontratopardep
                prm2.Value = CLng(Me.TextBox1.Text)

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)

                Dim i As Integer = cmdarn1.ExecuteNonQuery()
            End With
            con.Close()
            bnd2pardep1 = True
            Me.Close()

        ElseIf validacion = 1 Then
            If pagosbuenos = 0 Then
                pagosbuenos = 1
            End If
            MsgBox("Los Pagos No Pueden Ser Mas De" + CStr(pagosbuenos), MsgBoxStyle.Information)
        End If

    End Sub
End Class