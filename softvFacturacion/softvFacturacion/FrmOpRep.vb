Imports System.Data.SqlClient

Public Class FrmOpRep
    Private Sub FrmOpRep_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If op = 0 Then
            Me.CMBLabel3.Visible = False
        ElseIf op = 1 Then
            If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
                Me.ComboBox1.Visible = True
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje Cajero(a):"
            ElseIf GloOpRepGral = "V" Then
                Me.ComboBox1.Visible = False
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                'Me.ComboBox4.Visible = True
                Me.ComboVendedores.Visible = True
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje el Vendedor:" ''ENTRA
            End If
        ElseIf op = 4 Then
            If GloOpRepGral = "V" Then
                Me.ComboBox1.Visible = True
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje Cajero(a):"
            Else
                'If GloOpRepGral = "V" Then
                Me.ComboBox1.Visible = False
                Me.ComboBox2.Visible = False
                Me.ComboBox3.Visible = False
                'Me.ComboBox4.Visible = True
                Me.ComboVendedores.Visible = True
                Me.CheckBox1.Visible = True
                Me.CMBLabel3.Visible = True
                Me.CMBLabel3.Text = "Escoje el Vendedor:"

            End If

        ElseIf op = 2 And (GloOpRepGral = "C" Or GloOpRepGral = "V") Then
            Me.ComboBox1.Visible = False
            Me.ComboBox3.Visible = False
            Me.ComboBox2.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje tu Caja:"
        ElseIf op = 2 And (GloOpRepGral = "T") Then
            Me.ComboBox1.Visible = False
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje el nombre de la Sucursal:"
        ElseIf op = 7 Then
            Me.ComboBox1.Visible = True
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = False
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje Cajero(a):"
        Else
            Me.ComboBox1.Visible = False
            Me.ComboBox2.Visible = False
            Me.ComboBox3.Visible = True
            Me.CheckBox1.Visible = True
            Me.CMBLabel3.Visible = True
            Me.CMBLabel3.Text = "Escoje el nombre de la Sucursal:"
        End If
        If GloTipoUsuario = 21 Then
            CheckBox1.Visible = False
        End If
    End Sub


    Private Sub FrmOpRep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DateTimePicker2.MaxDate = Today
        Me.DateTimePicker2.Value = Today
       
        Select Case op
            Case 1
                If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
                    MUESTRAUSUARIOSREPORTECORTES(0)
                    'Me.MUESTRAUSUARIOS1TableAdapter.Connection = CON
                    'Me.MUESTRAUSUARIOS1TableAdapter.Fill(Me.NewsoftvDataSet.MUESTRAUSUARIOS1, 0)
                ElseIf GloOpRepGral = "V" Then
                    Me.MUESTRAVENDORES_TodosTableAdapter.Connection = CON
                    Me.MUESTRAVENDORES_TodosTableAdapter.Fill(Me.Procedimientos_arnoldo.MUESTRAVENDORES_Todos, 0)
                    MUESTRAVENDEDORES(0)
                End If
            Case 2
                If GloOpRepGral = "C" Or GloOpRepGral = "V" Then
                    Me.MUESTRACAJASTableAdapter.Connection = CON
                    Me.MUESTRACAJASTableAdapter.Fill(Me.NewsoftvDataSet.MUESTRACAJAS, 1)
                ElseIf GloOpRepGral = "T" Then
                    Me.MUESTRASUCURSALESTableAdapter.Connection = CON
                    Me.MUESTRASUCURSALESTableAdapter.Fill(Me.NewsoftvDataSet.MUESTRASUCURSALES)
                End If
            Case 3
                Me.MUESTRASUCURSALESTableAdapter.Connection = CON
                Me.MUESTRASUCURSALESTableAdapter.Fill(Me.NewsoftvDataSet.MUESTRASUCURSALES)
            Case 4
                MUESTRAUSUARIOSREPORTECORTES(0)
                'Me.MUESTRAUSUARIOS1TableAdapter.Connection = CON
                'Me.MUESTRAUSUARIOS1TableAdapter.Fill(Me.NewsoftvDataSet.MUESTRAUSUARIOS1, 0)
            Case 7
                MUESTRAUSUARIOSREPORTECORTES(0)
                'Me.MUESTRAUSUARIOS1TableAdapter.Connection = CON
                'Me.MUESTRAUSUARIOS1TableAdapter.Fill(Me.NewsoftvDataSet.MUESTRAUSUARIOS1, 0)
        End Select

        If IdSistema = "VA" Then
            If (op = 1 And GloOpRepGral = "C") Or (op = 1 And GloOpRepGral = "V") Then
                Me.Button3.Visible = True
            Else
                Me.Button3.Visible = False
            End If
        ElseIf IdSistema <> "VA" Then
            Me.Button3.Visible = False
        End If
        If GloTipoUsuario = 21 Then
            CheckBox1.Visible = False
        End If
        CON.Close()
    End Sub

    Private Sub MUESTRAUSUARIOSREPORTECORTES(ByVal PRMOP As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, PRMOP)
            BaseII.CreateMyParameter("@CLVUSUARIO", SqlDbType.VarChar, GloUsuario, 10)
            ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRAUSUARIOSREPORTECORTES")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub MUESTRAVENDEDORES(ByVal PRMOP As Integer)
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, PRMOP)
            ComboVendedores.DataSource = BaseII.ConsultaDT("MUESTRAVENDORES_Todos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Me.CheckBox1.Checked = True Then
            Resumen = True
            Resumen1 = 1
            Resumen3 = True
            Resumen2 = 1
            FlagCaja = True
        Else
            Resumen1 = 0
            Resumen3 = False
            Resumen2 = 0
            Resumen = False
            FlagCaja = False
        End If
        If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
            If ComboBox1.Text.Length > 0 Then NomCajera = Me.ComboBox1.SelectedValue
        ElseIf GloOpRepGral = "V" And op = 4 Then
            If ComboBox1.Text.Length > 0 Then NomCajera = Me.ComboBox1.SelectedValue
        Else
            If ComboVendedores.Text.Length > 0 Then NomCajera = Me.ComboVendedores.SelectedValue
        End If

        SelCajaResumen = Me.ComboBox2.SelectedValue
        Fecha_ini = Me.DateTimePicker1.Text
        Fecha_Fin = Me.DateTimePicker2.Text
        NomCaja = CStr(Me.ComboBox2.SelectedValue)
        NomSucursal = CStr(Me.ComboBox3.SelectedValue)
        BanderaReporte = True
        If Me.ComboBox1.Visible = True Then
            ExtraT = Me.ComboBox1.Text
        ElseIf Me.ComboBox2.Visible = True Then
            ExtraT = Me.ComboBox2.Text
        ElseIf Me.ComboBox3.Visible = True Then
            ExtraT = Me.ComboBox3.Text
        ElseIf Me.ComboVendedores.Visible = True Then
            ExtraT = Me.ComboVendedores.Text
        End If
        Me.Close()
    End Sub
    Private Sub DateTimePicker1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker1.LostFocus
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker1_ValueChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateTimePicker2.TextChanged

        'If Fecha_ini > Fecha_Fin Then
        '    MsgBox("La Fecha inicial no puede ser menor que la final", MsgBoxStyle.Information, "Error Fecha")
        'End If
    End Sub

    Private Sub DateTimePicker2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker2.ValueChanged
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.CheckBox1.Checked = True Then
            Resumen = True
            Resumen1 = 1
            Resumen3 = True
            Resumen2 = 1
            FlagCaja = True
        Else
            Resumen1 = 0
            Resumen3 = False
            Resumen2 = 0
            Resumen = False
            FlagCaja = False
        End If
        If GloOpRepGral = "C" Or GloOpRepGral = "T" Then
            NomCajera = Me.ComboBox1.SelectedValue
        ElseIf GloOpRepGral = "V" And op = 4 Then
            NomCajera = Me.ComboBox1.SelectedValue
        Else
            NomCajera = Me.ComboVendedores.SelectedValue
        End If

        SelCajaResumen = Me.ComboBox2.SelectedValue
        Fecha_ini = Me.DateTimePicker1.Text
        Fecha_Fin = Me.DateTimePicker2.Text
        NomCaja = CStr(Me.ComboBox2.SelectedValue)
        NomSucursal = CStr(Me.ComboBox3.SelectedValue)
        BanderaReporte = True
        If Me.ComboBox1.Visible = True Then
            ExtraT = Me.ComboBox1.Text
        ElseIf Me.ComboBox2.Visible = True Then
            ExtraT = Me.ComboBox2.Text
        ElseIf Me.ComboBox3.Visible = True Then
            ExtraT = Me.ComboBox3.Text
        ElseIf Me.ComboVendedores.Visible = True Then
            ExtraT = Me.ComboVendedores.Text
        End If
        Locbndcortedet = True
        Me.Close()
    End Sub
End Class