﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAgregaPaqAdic
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.CmbPaqAdic = New System.Windows.Forms.ComboBox()
        Me.ButtonAceptar = New System.Windows.Forms.Button()
        Me.ButtonCancelar = New System.Windows.Forms.Button()
        Me.CmbTelefono = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(86, 47)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(242, 16)
        Me.CMBLabel1.TabIndex = 4
        Me.CMBLabel1.Text = "Seleccione el Paquete Adicional :"
        '
        'CmbPaqAdic
        '
        Me.CmbPaqAdic.DisplayMember = "DescripcionFac"
        Me.CmbPaqAdic.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbPaqAdic.FormattingEnabled = True
        Me.CmbPaqAdic.Location = New System.Drawing.Point(89, 66)
        Me.CmbPaqAdic.Name = "CmbPaqAdic"
        Me.CmbPaqAdic.Size = New System.Drawing.Size(408, 24)
        Me.CmbPaqAdic.TabIndex = 3
        Me.CmbPaqAdic.ValueMember = "Clv_Txt"
        '
        'ButtonAceptar
        '
        Me.ButtonAceptar.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonAceptar.ForeColor = System.Drawing.Color.Black
        Me.ButtonAceptar.Location = New System.Drawing.Point(166, 211)
        Me.ButtonAceptar.Name = "ButtonAceptar"
        Me.ButtonAceptar.Size = New System.Drawing.Size(124, 43)
        Me.ButtonAceptar.TabIndex = 5
        Me.ButtonAceptar.Text = "&ACEPTAR"
        Me.ButtonAceptar.UseVisualStyleBackColor = False
        '
        'ButtonCancelar
        '
        Me.ButtonCancelar.BackColor = System.Drawing.Color.DarkOrange
        Me.ButtonCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonCancelar.ForeColor = System.Drawing.Color.Black
        Me.ButtonCancelar.Location = New System.Drawing.Point(305, 211)
        Me.ButtonCancelar.Name = "ButtonCancelar"
        Me.ButtonCancelar.Size = New System.Drawing.Size(124, 43)
        Me.ButtonCancelar.TabIndex = 6
        Me.ButtonCancelar.Text = "&CANCELAR"
        Me.ButtonCancelar.UseVisualStyleBackColor = False
        '
        'CmbTelefono
        '
        Me.CmbTelefono.DisplayMember = "DescripcionFac"
        Me.CmbTelefono.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CmbTelefono.FormattingEnabled = True
        Me.CmbTelefono.Location = New System.Drawing.Point(89, 137)
        Me.CmbTelefono.Name = "CmbTelefono"
        Me.CmbTelefono.Size = New System.Drawing.Size(263, 24)
        Me.CmbTelefono.TabIndex = 7
        Me.CmbTelefono.ValueMember = "Clv_Txt"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(86, 118)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(177, 16)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Seleccione el Teléfono :"
        '
        'FrmAgregaPaqAdic
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 295)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CmbTelefono)
        Me.Controls.Add(Me.ButtonCancelar)
        Me.Controls.Add(Me.ButtonAceptar)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.CmbPaqAdic)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAgregaPaqAdic"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione el Paquete Adicional"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CmbPaqAdic As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonAceptar As System.Windows.Forms.Button
    Friend WithEvents ButtonCancelar As System.Windows.Forms.Button
    Friend WithEvents CmbTelefono As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
