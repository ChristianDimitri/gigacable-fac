<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmListadoPreliminar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_SessionBancosLabel As System.Windows.Forms.Label
        Dim Proceso_CanceladoLabel As System.Windows.Forms.Label
        Dim RealizadoLabel As System.Windows.Forms.Label
        Dim ClaverelLabel As System.Windows.Forms.Label
        Dim SucursalLabel As System.Windows.Forms.Label
        Dim EmisoraLabel As System.Windows.Forms.Label
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim FechaHoyLabel As System.Windows.Forms.Label
        Dim ANOLabel As System.Windows.Forms.Label
        Dim MESLabel As System.Windows.Forms.Label
        Dim DIALabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim ContadorLabel As System.Windows.Forms.Label
        Dim Clv_IdLabel As System.Windows.Forms.Label
        Dim Clv_SessionLabel As System.Windows.Forms.Label
        Dim Clv_FacturaLabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmListadoPreliminar))
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ConPreliminarBancosDataGridView = New System.Windows.Forms.DataGridView()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConPreliminarBancosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CONDETFACTURASBANCOSDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn4 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn5 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONDETFACTURASBANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.Dame_PreRecibo_oFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Clv_FacturaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_IdTextBox = New System.Windows.Forms.TextBox()
        Me.CONDETFACTURASBANCOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.RealizadoCheckBox = New System.Windows.Forms.CheckBox()
        Me.Clv_SessionBancosTextBox = New System.Windows.Forms.TextBox()
        Me.Proceso_CanceladoTextBox = New System.Windows.Forms.TextBox()
        Me.MITextBox = New System.Windows.Forms.TextBox()
        Me.DameGeneralesBancosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.HORATextBox = New System.Windows.Forms.TextBox()
        Me.DIATextBox = New System.Windows.Forms.TextBox()
        Me.MESTextBox = New System.Windows.Forms.TextBox()
        Me.ClaverelTextBox = New System.Windows.Forms.TextBox()
        Me.ANOTextBox = New System.Windows.Forms.TextBox()
        Me.SucursalTextBox = New System.Windows.Forms.TextBox()
        Me.FechaHoyTextBox = New System.Windows.Forms.TextBox()
        Me.EmisoraTextBox = New System.Windows.Forms.TextBox()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.FinText = New System.Windows.Forms.TextBox()
        Me.DameRangoFacturasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.INIText = New System.Windows.Forms.TextBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LblVersion = New System.Windows.Forms.Label()
        Me.LblFecha = New System.Windows.Forms.Label()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblUsuario = New System.Windows.Forms.Label()
        Me.DamedatosUsuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblSucursal = New System.Windows.Forms.Label()
        Me.DAMENOMBRESUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LblNomCaja = New System.Windows.Forms.Label()
        Me.LblSistema = New System.Windows.Forms.Label()
        Me.LblNomEmpresa = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Clv_SessionBancosTextBox1 = New System.Windows.Forms.TextBox()
        Me.ImporteTextBox = New System.Windows.Forms.TextBox()
        Me.DameGeneralesBancos_TotalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ContadorTextBox = New System.Windows.Forms.TextBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ConPreliminarBancosTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.ConPreliminarBancosTableAdapter()
        Me.GeneraBancosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GeneraBancosTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.GeneraBancosTableAdapter()
        Me.CANCELALISTADOPRELIMINARBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELALISTADOPRELIMINARTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CANCELALISTADOPRELIMINARTableAdapter()
        Me.DamedatosUsuarioTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter()
        Me.DAMENOMBRESUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter()
        Me.GUARDAFACTURASBANCOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GUARDAFACTURASBANCOSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.GUARDAFACTURASBANCOSTableAdapter()
        Me.ValidaAfectacionBancosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaAfectacionBancosTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.ValidaAfectacionBancosTableAdapter()
        Me.MUESTRAPERIODOSSeleccionarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAPERIODOS_SeleccionarTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAPERIODOS_SeleccionarTableAdapter()
        Me.DameGeneralesBancosTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancosTableAdapter()
        Me.DameGeneralesBancos_TotalTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancos_TotalTableAdapter()
        Me.DameGeneralesBancos_Total_DetalleBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralesBancos_Total_DetalleTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancos_Total_DetalleTableAdapter()
        Me.DameRangoFacturasTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameRangoFacturasTableAdapter()
        Me.Dame_PreRecibo_oFacturaTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.Dame_PreRecibo_oFacturaTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Inserta_Datos_HBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Datos_HTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Datos_HTableAdapter()
        Me.Inserta_Datos_Archivo_PBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_Datos_Archivo_PTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Datos_Archivo_PTableAdapter()
        Me.Valida_Proceso_CanceladoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Valida_Proceso_CanceladoTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Valida_Proceso_CanceladoTableAdapter()
        Me.Borra_Tablas_ArchivosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Tablas_ArchivosTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Borra_Tablas_ArchivosTableAdapter()
        Me.Checa_Archivo_ResProsaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Checa_Archivo_ResProsaTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Checa_Archivo_ResProsaTableAdapter()
        Me.Inserta_ProcesoArchivoDebitoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_ProcesoArchivoDebitoTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_ProcesoArchivoDebitoTableAdapter()
        Me.Procesa_Arhivo_santaderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procesa_Arhivo_santaderTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Procesa_Arhivo_santaderTableAdapter()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn20 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn21 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONDETFACTURASBANCOSTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CONDETFACTURASBANCOSTableAdapter()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter6 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter7 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter8 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter9 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Clv_SessionBancosLabel = New System.Windows.Forms.Label()
        Proceso_CanceladoLabel = New System.Windows.Forms.Label()
        RealizadoLabel = New System.Windows.Forms.Label()
        ClaverelLabel = New System.Windows.Forms.Label()
        SucursalLabel = New System.Windows.Forms.Label()
        EmisoraLabel = New System.Windows.Forms.Label()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        FechaHoyLabel = New System.Windows.Forms.Label()
        ANOLabel = New System.Windows.Forms.Label()
        MESLabel = New System.Windows.Forms.Label()
        DIALabel = New System.Windows.Forms.Label()
        ImporteLabel = New System.Windows.Forms.Label()
        ContadorLabel = New System.Windows.Forms.Label()
        Clv_IdLabel = New System.Windows.Forms.Label()
        Clv_SessionLabel = New System.Windows.Forms.Label()
        Clv_FacturaLabel = New System.Windows.Forms.Label()
        ContratoLabel = New System.Windows.Forms.Label()
        CType(Me.ConPreliminarBancosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConPreliminarBancosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONDETFACTURASBANCOSDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDETFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_PreRecibo_oFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDETFACTURASBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONDETFACTURASBANCOSBindingNavigator.SuspendLayout()
        CType(Me.DameGeneralesBancosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameRangoFacturasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DameGeneralesBancos_TotalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GeneraBancosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CANCELALISTADOPRELIMINARBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GUARDAFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaAfectacionBancosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAPERIODOSSeleccionarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralesBancos_Total_DetalleBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Datos_HBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_Datos_Archivo_PBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Valida_Proceso_CanceladoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Tablas_ArchivosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Checa_Archivo_ResProsaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_ProcesoArchivoDebitoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procesa_Arhivo_santaderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_SessionBancosLabel
        '
        Clv_SessionBancosLabel.AutoSize = True
        Clv_SessionBancosLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_SessionBancosLabel.Location = New System.Drawing.Point(15, 36)
        Clv_SessionBancosLabel.Name = "Clv_SessionBancosLabel"
        Clv_SessionBancosLabel.Size = New System.Drawing.Size(98, 16)
        Clv_SessionBancosLabel.TabIndex = 10
        Clv_SessionBancosLabel.Text = "No Proceso :"
        '
        'Proceso_CanceladoLabel
        '
        Proceso_CanceladoLabel.AutoSize = True
        Proceso_CanceladoLabel.Location = New System.Drawing.Point(344, 12)
        Proceso_CanceladoLabel.Name = "Proceso_CanceladoLabel"
        Proceso_CanceladoLabel.Size = New System.Drawing.Size(103, 13)
        Proceso_CanceladoLabel.TabIndex = 11
        Proceso_CanceladoLabel.Text = "Proceso Cancelado:"
        '
        'RealizadoLabel
        '
        RealizadoLabel.AutoSize = True
        RealizadoLabel.Location = New System.Drawing.Point(568, 8)
        RealizadoLabel.Name = "RealizadoLabel"
        RealizadoLabel.Size = New System.Drawing.Size(57, 13)
        RealizadoLabel.TabIndex = 12
        RealizadoLabel.Text = "Realizado:"
        '
        'ClaverelLabel
        '
        ClaverelLabel.AutoSize = True
        ClaverelLabel.Location = New System.Drawing.Point(688, 65)
        ClaverelLabel.Name = "ClaverelLabel"
        ClaverelLabel.Size = New System.Drawing.Size(47, 13)
        ClaverelLabel.TabIndex = 179
        ClaverelLabel.Text = "claverel:"
        '
        'SucursalLabel
        '
        SucursalLabel.AutoSize = True
        SucursalLabel.Location = New System.Drawing.Point(688, 84)
        SucursalLabel.Name = "SucursalLabel"
        SucursalLabel.Size = New System.Drawing.Size(51, 13)
        SucursalLabel.TabIndex = 180
        SucursalLabel.Text = "Sucursal:"
        '
        'EmisoraLabel
        '
        EmisoraLabel.AutoSize = True
        EmisoraLabel.Location = New System.Drawing.Point(688, 104)
        EmisoraLabel.Name = "EmisoraLabel"
        EmisoraLabel.Size = New System.Drawing.Size(47, 13)
        EmisoraLabel.TabIndex = 181
        EmisoraLabel.Text = "Emisora:"
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Location = New System.Drawing.Point(666, 126)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(69, 13)
        ConsecutivoLabel.TabIndex = 182
        ConsecutivoLabel.Text = "Consecutivo:"
        '
        'FechaHoyLabel
        '
        FechaHoyLabel.AutoSize = True
        FechaHoyLabel.Location = New System.Drawing.Point(666, 151)
        FechaHoyLabel.Name = "FechaHoyLabel"
        FechaHoyLabel.Size = New System.Drawing.Size(62, 13)
        FechaHoyLabel.TabIndex = 183
        FechaHoyLabel.Text = "Fecha Hoy:"
        '
        'ANOLabel
        '
        ANOLabel.AutoSize = True
        ANOLabel.Location = New System.Drawing.Point(688, 175)
        ANOLabel.Name = "ANOLabel"
        ANOLabel.Size = New System.Drawing.Size(33, 13)
        ANOLabel.TabIndex = 185
        ANOLabel.Text = "ANO:"
        '
        'MESLabel
        '
        MESLabel.AutoSize = True
        MESLabel.Location = New System.Drawing.Point(688, 200)
        MESLabel.Name = "MESLabel"
        MESLabel.Size = New System.Drawing.Size(33, 13)
        MESLabel.TabIndex = 186
        MESLabel.Text = "MES:"
        '
        'DIALabel
        '
        DIALabel.AutoSize = True
        DIALabel.Location = New System.Drawing.Point(707, 217)
        DIALabel.Name = "DIALabel"
        DIALabel.Size = New System.Drawing.Size(28, 13)
        DIALabel.TabIndex = 187
        DIALabel.Text = "DIA:"
        AddHandler DIALabel.Click, AddressOf Me.DIALabel_Click
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Location = New System.Drawing.Point(600, 158)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(45, 13)
        ImporteLabel.TabIndex = 192
        ImporteLabel.Text = "Importe:"
        '
        'ContadorLabel
        '
        ContadorLabel.AutoSize = True
        ContadorLabel.Location = New System.Drawing.Point(592, 184)
        ContadorLabel.Name = "ContadorLabel"
        ContadorLabel.Size = New System.Drawing.Size(53, 13)
        ContadorLabel.TabIndex = 193
        ContadorLabel.Text = "Contador:"
        '
        'Clv_IdLabel
        '
        Clv_IdLabel.AutoSize = True
        Clv_IdLabel.Location = New System.Drawing.Point(407, 100)
        Clv_IdLabel.Name = "Clv_IdLabel"
        Clv_IdLabel.Size = New System.Drawing.Size(37, 13)
        Clv_IdLabel.TabIndex = 190
        Clv_IdLabel.Text = "Clv Id:"
        '
        'Clv_SessionLabel
        '
        Clv_SessionLabel.AutoSize = True
        Clv_SessionLabel.Location = New System.Drawing.Point(639, 98)
        Clv_SessionLabel.Name = "Clv_SessionLabel"
        Clv_SessionLabel.Size = New System.Drawing.Size(65, 13)
        Clv_SessionLabel.TabIndex = 191
        Clv_SessionLabel.Text = "Clv Session:"
        '
        'Clv_FacturaLabel
        '
        Clv_FacturaLabel.AutoSize = True
        Clv_FacturaLabel.Location = New System.Drawing.Point(768, 100)
        Clv_FacturaLabel.Name = "Clv_FacturaLabel"
        Clv_FacturaLabel.Size = New System.Drawing.Size(64, 13)
        Clv_FacturaLabel.TabIndex = 192
        Clv_FacturaLabel.Text = "Clv Factura:"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Location = New System.Drawing.Point(887, 98)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(50, 13)
        ContratoLabel.TabIndex = 193
        ContratoLabel.Text = "Contrato:"
        '
        'ConPreliminarBancosDataGridView
        '
        Me.ConPreliminarBancosDataGridView.AllowUserToAddRows = False
        Me.ConPreliminarBancosDataGridView.AllowUserToDeleteRows = False
        Me.ConPreliminarBancosDataGridView.AutoGenerateColumns = False
        Me.ConPreliminarBancosDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConPreliminarBancosDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ConPreliminarBancosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Estado, Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn4, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn5})
        Me.ConPreliminarBancosDataGridView.DataSource = Me.ConPreliminarBancosBindingSource
        Me.ConPreliminarBancosDataGridView.Location = New System.Drawing.Point(12, 140)
        Me.ConPreliminarBancosDataGridView.Name = "ConPreliminarBancosDataGridView"
        Me.ConPreliminarBancosDataGridView.ReadOnly = True
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ConPreliminarBancosDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.ConPreliminarBancosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ConPreliminarBancosDataGridView.Size = New System.Drawing.Size(793, 219)
        Me.ConPreliminarBancosDataGridView.TabIndex = 2
        Me.ConPreliminarBancosDataGridView.TabStop = False
        '
        'Estado
        '
        Me.Estado.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.Estado.DataPropertyName = "Estado"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Estado.DefaultCellStyle = DataGridViewCellStyle2
        Me.Estado.HeaderText = "Status"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Width = 76
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_SessionBancos"
        Me.DataGridViewTextBoxColumn1.HeaderText = "No. Proceso"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "fecha"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "importe"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Importe Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 125
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.DataPropertyName = "Realizado"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.NullValue = False
        Me.DataGridViewCheckBoxColumn1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Afectado"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.ReadOnly = True
        Me.DataGridViewCheckBoxColumn1.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "NomSucursal"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn10.HeaderText = "Sucursal"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 93
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "NomCaja"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn11.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn11.HeaderText = "Caja"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 65
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "NomCajera"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn12.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn12.HeaderText = "Cajera"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 79
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "cajera"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridViewTextBoxColumn3.HeaderText = "cajera"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "sucursal"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle10
        Me.DataGridViewTextBoxColumn6.HeaderText = "sucursal"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Caja"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Caja"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Descripcion_Proceso"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Descripcion_Proceso"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Tipo_Cobro"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Tipo_Cobro"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Proceso_Cancelado"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Proceso_Cancelado"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'ConPreliminarBancosBindingSource
        '
        Me.ConPreliminarBancosBindingSource.DataMember = "ConPreliminarBancos"
        Me.ConPreliminarBancosBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LightGray
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(825, 49)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(179, 41)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Generar Listado Preliminar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.LightGray
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(825, 90)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(179, 41)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&Afectar Listado Preliminar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.LightGray
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(825, 131)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(179, 41)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&Cancelar Listado Preliminar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.LightGray
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(825, 336)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(179, 41)
        Me.Button4.TabIndex = 500
        Me.Button4.Text = "&SALIR"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel1.Location = New System.Drawing.Point(14, 116)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(259, 18)
        Me.CMBLabel1.TabIndex = 7
        Me.CMBLabel1.Text = "Listados Preliminares Generados"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.CONDETFACTURASBANCOSDataGridView)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.ContratoTextBox)
        Me.Panel1.Controls.Add(Clv_FacturaLabel)
        Me.Panel1.Controls.Add(Me.Clv_FacturaTextBox)
        Me.Panel1.Controls.Add(Clv_SessionLabel)
        Me.Panel1.Controls.Add(Me.Clv_SessionTextBox)
        Me.Panel1.Controls.Add(Clv_IdLabel)
        Me.Panel1.Controls.Add(Me.Clv_IdTextBox)
        Me.Panel1.Controls.Add(Me.CONDETFACTURASBANCOSBindingNavigator)
        Me.Panel1.Controls.Add(RealizadoLabel)
        Me.Panel1.Controls.Add(Me.RealizadoCheckBox)
        Me.Panel1.Controls.Add(Proceso_CanceladoLabel)
        Me.Panel1.Controls.Add(Clv_SessionBancosLabel)
        Me.Panel1.Controls.Add(Me.Clv_SessionBancosTextBox)
        Me.Panel1.Controls.Add(Me.Proceso_CanceladoTextBox)
        Me.Panel1.Controls.Add(Me.MITextBox)
        Me.Panel1.Controls.Add(Me.HORATextBox)
        Me.Panel1.Controls.Add(Me.DIATextBox)
        Me.Panel1.Controls.Add(DIALabel)
        Me.Panel1.Controls.Add(ClaverelLabel)
        Me.Panel1.Controls.Add(Me.MESTextBox)
        Me.Panel1.Controls.Add(MESLabel)
        Me.Panel1.Controls.Add(Me.ClaverelTextBox)
        Me.Panel1.Controls.Add(SucursalLabel)
        Me.Panel1.Controls.Add(Me.ANOTextBox)
        Me.Panel1.Controls.Add(ANOLabel)
        Me.Panel1.Controls.Add(Me.SucursalTextBox)
        Me.Panel1.Controls.Add(EmisoraLabel)
        Me.Panel1.Controls.Add(Me.FechaHoyTextBox)
        Me.Panel1.Controls.Add(FechaHoyLabel)
        Me.Panel1.Controls.Add(Me.EmisoraTextBox)
        Me.Panel1.Controls.Add(ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 381)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(992, 360)
        Me.Panel1.TabIndex = 10
        '
        'CONDETFACTURASBANCOSDataGridView
        '
        Me.CONDETFACTURASBANCOSDataGridView.AllowUserToAddRows = False
        Me.CONDETFACTURASBANCOSDataGridView.AllowUserToDeleteRows = False
        Me.CONDETFACTURASBANCOSDataGridView.AutoGenerateColumns = False
        Me.CONDETFACTURASBANCOSDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn38, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewCheckBoxColumn4, Me.DataGridViewCheckBoxColumn5, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn46, Me.DataGridViewTextBoxColumn47, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn51, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewTextBoxColumn34})
        Me.CONDETFACTURASBANCOSDataGridView.DataSource = Me.CONDETFACTURASBANCOSBindingSource
        Me.CONDETFACTURASBANCOSDataGridView.Location = New System.Drawing.Point(5, 31)
        Me.CONDETFACTURASBANCOSDataGridView.MultiSelect = False
        Me.CONDETFACTURASBANCOSDataGridView.Name = "CONDETFACTURASBANCOSDataGridView"
        Me.CONDETFACTURASBANCOSDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.CONDETFACTURASBANCOSDataGridView.Size = New System.Drawing.Size(978, 326)
        Me.CONDETFACTURASBANCOSDataGridView.TabIndex = 195
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "Clv_SessionBancos"
        Me.DataGridViewTextBoxColumn20.HeaderText = "No. "
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Width = 50
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "Clv_Id"
        Me.DataGridViewTextBoxColumn21.HeaderText = "Clv_Id"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Visible = False
        Me.DataGridViewTextBoxColumn21.Width = 5
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.DataPropertyName = "CONTRATO"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn41.DefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridViewTextBoxColumn41.HeaderText = "CONTRATO"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        Me.DataGridViewTextBoxColumn41.ReadOnly = True
        Me.DataGridViewTextBoxColumn41.Width = 60
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.DataPropertyName = "NOMBRE"
        Me.DataGridViewTextBoxColumn39.HeaderText = "NOMBRE"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        Me.DataGridViewTextBoxColumn39.Width = 200
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.DataPropertyName = "CUENTA_BANCO"
        Me.DataGridViewTextBoxColumn42.HeaderText = "CUENTA"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        Me.DataGridViewTextBoxColumn42.Width = 130
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.DataPropertyName = "IMPORTE"
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.Format = "C2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.DataGridViewTextBoxColumn38.DefaultCellStyle = DataGridViewCellStyle13
        Me.DataGridViewTextBoxColumn38.HeaderText = "IMPORTE"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.DataPropertyName = "REALIZADO"
        Me.DataGridViewCheckBoxColumn3.HeaderText = "AFECTADO"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        Me.DataGridViewCheckBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewCheckBoxColumn4
        '
        Me.DataGridViewCheckBoxColumn4.DataPropertyName = "CANCELADA"
        Me.DataGridViewCheckBoxColumn4.HeaderText = "CANCELADO"
        Me.DataGridViewCheckBoxColumn4.Name = "DataGridViewCheckBoxColumn4"
        Me.DataGridViewCheckBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewCheckBoxColumn5
        '
        Me.DataGridViewCheckBoxColumn5.DataPropertyName = "PAGADO"
        Me.DataGridViewCheckBoxColumn5.HeaderText = "AUTORIZADO"
        Me.DataGridViewCheckBoxColumn5.Name = "DataGridViewCheckBoxColumn5"
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.DataPropertyName = "TIPO_CUENTA"
        Me.DataGridViewTextBoxColumn43.HeaderText = "TIPO CUENTA"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.DataPropertyName = "TELEFONO"
        Me.DataGridViewTextBoxColumn40.HeaderText = "TELEFONO"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.DataPropertyName = "VENCIMIENTO"
        Me.DataGridViewTextBoxColumn44.HeaderText = "VENCIMIENTO"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.DataPropertyName = "CODIGOSEGURIDAD"
        Me.DataGridViewTextBoxColumn45.HeaderText = "CODIGOSEGURIDAD"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.DataPropertyName = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn35.HeaderText = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.Width = 5
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.DataPropertyName = "NOMTARJETA"
        Me.DataGridViewTextBoxColumn46.HeaderText = "NOMTARJETA"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.DataPropertyName = "BANCO"
        Me.DataGridViewTextBoxColumn47.HeaderText = "BANCO"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.DataPropertyName = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn36.HeaderText = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.Width = 5
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.DataPropertyName = "Error"
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Red
        Me.DataGridViewTextBoxColumn51.DefaultCellStyle = DataGridViewCellStyle14
        Me.DataGridViewTextBoxColumn51.HeaderText = "Mensaje Error"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.DataPropertyName = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn37.HeaderText = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.Width = 5
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.DataPropertyName = "SUCURSAL"
        Me.DataGridViewTextBoxColumn48.HeaderText = "SUCURSAL"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        Me.DataGridViewTextBoxColumn48.Visible = False
        Me.DataGridViewTextBoxColumn48.Width = 5
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.DataPropertyName = "CAJERA"
        Me.DataGridViewTextBoxColumn49.HeaderText = "CAJERA"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        Me.DataGridViewTextBoxColumn49.Width = 5
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.DataPropertyName = "CAJA"
        Me.DataGridViewTextBoxColumn50.HeaderText = "CAJA"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        Me.DataGridViewTextBoxColumn50.Width = 5
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.DataPropertyName = "FECHA"
        Me.DataGridViewTextBoxColumn34.HeaderText = "FECHA"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.Width = 5
        '
        'CONDETFACTURASBANCOSBindingSource
        '
        Me.CONDETFACTURASBANCOSBindingSource.DataMember = "CONDETFACTURASBANCOS"
        Me.CONDETFACTURASBANCOSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_PreRecibo_oFacturaBindingSource, "Contrato", True))
        Me.ContratoTextBox.Location = New System.Drawing.Point(890, 104)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(49, 20)
        Me.ContratoTextBox.TabIndex = 194
        '
        'Dame_PreRecibo_oFacturaBindingSource
        '
        Me.Dame_PreRecibo_oFacturaBindingSource.DataMember = "Dame_PreRecibo_oFactura"
        Me.Dame_PreRecibo_oFacturaBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_FacturaTextBox
        '
        Me.Clv_FacturaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_PreRecibo_oFacturaBindingSource, "Clv_Factura", True))
        Me.Clv_FacturaTextBox.Location = New System.Drawing.Point(838, 96)
        Me.Clv_FacturaTextBox.Name = "Clv_FacturaTextBox"
        Me.Clv_FacturaTextBox.Size = New System.Drawing.Size(43, 20)
        Me.Clv_FacturaTextBox.TabIndex = 193
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_PreRecibo_oFacturaBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(710, 95)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(353, 20)
        Me.Clv_SessionTextBox.TabIndex = 192
        '
        'Clv_IdTextBox
        '
        Me.Clv_IdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDETFACTURASBANCOSBindingSource, "Clv_Id", True))
        Me.Clv_IdTextBox.Location = New System.Drawing.Point(450, 97)
        Me.Clv_IdTextBox.Name = "Clv_IdTextBox"
        Me.Clv_IdTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_IdTextBox.TabIndex = 191
        '
        'CONDETFACTURASBANCOSBindingNavigator
        '
        Me.CONDETFACTURASBANCOSBindingNavigator.AddNewItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.CountItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.DeleteItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = False
        Me.CONDETFACTURASBANCOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton3, Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem, Me.ToolStripButton2, Me.ToolStripButton1, Me.ToolStripButton4, Me.ToolStripButton5})
        Me.CONDETFACTURASBANCOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveLastItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MoveNextItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.Name = "CONDETFACTURASBANCOSBindingNavigator"
        Me.CONDETFACTURASBANCOSBindingNavigator.PositionItem = Nothing
        Me.CONDETFACTURASBANCOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONDETFACTURASBANCOSBindingNavigator.Size = New System.Drawing.Size(992, 25)
        Me.CONDETFACTURASBANCOSBindingNavigator.TabIndex = 3
        Me.CONDETFACTURASBANCOSBindingNavigator.TabStop = True
        Me.CONDETFACTURASBANCOSBindingNavigator.Text = "BindingNavigator1"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton3.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(157, 22)
        Me.ToolStripButton3.Text = "&Deshacer Cambio"
        '
        'CONDETFACTURASBANCOSBindingNavigatorSaveItem
        '
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.ForeColor = System.Drawing.Color.Black
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONDETFACTURASBANCOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Name = "CONDETFACTURASBANCOSBindingNavigatorSaveItem"
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(147, 22)
        Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Text = "Guardar Detalle"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripButton2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(245, 22)
        Me.ToolStripButton2.Text = "Generar Archivo para PROSA"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripButton1.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(152, 22)
        Me.ToolStripButton1.Text = "&Imprimir Detalle"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton4.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(163, 22)
        Me.ToolStripButton4.Text = "Imprimir Facturas"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ToolStripButton5.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton5.ForeColor = System.Drawing.Color.Black
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(279, 22)
        Me.ToolStripButton5.Text = "Generar Archivo para BANCOMER"
        '
        'RealizadoCheckBox
        '
        Me.RealizadoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.ConPreliminarBancosBindingSource, "Realizado", True))
        Me.RealizadoCheckBox.Location = New System.Drawing.Point(631, 8)
        Me.RealizadoCheckBox.Name = "RealizadoCheckBox"
        Me.RealizadoCheckBox.Size = New System.Drawing.Size(104, 13)
        Me.RealizadoCheckBox.TabIndex = 13
        Me.RealizadoCheckBox.TabStop = False
        '
        'Clv_SessionBancosTextBox
        '
        Me.Clv_SessionBancosTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Clv_SessionBancosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.Clv_SessionBancosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPreliminarBancosBindingSource, "Clv_SessionBancos", True))
        Me.Clv_SessionBancosTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_SessionBancosTextBox.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Clv_SessionBancosTextBox.Location = New System.Drawing.Point(119, 37)
        Me.Clv_SessionBancosTextBox.Name = "Clv_SessionBancosTextBox"
        Me.Clv_SessionBancosTextBox.Size = New System.Drawing.Size(100, 15)
        Me.Clv_SessionBancosTextBox.TabIndex = 11
        Me.Clv_SessionBancosTextBox.TabStop = False
        '
        'Proceso_CanceladoTextBox
        '
        Me.Proceso_CanceladoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConPreliminarBancosBindingSource, "Proceso_Cancelado", True))
        Me.Proceso_CanceladoTextBox.Location = New System.Drawing.Point(450, 5)
        Me.Proceso_CanceladoTextBox.Name = "Proceso_CanceladoTextBox"
        Me.Proceso_CanceladoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Proceso_CanceladoTextBox.TabIndex = 12
        Me.Proceso_CanceladoTextBox.TabStop = False
        '
        'MITextBox
        '
        Me.MITextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "MI", True))
        Me.MITextBox.Location = New System.Drawing.Point(777, 243)
        Me.MITextBox.Name = "MITextBox"
        Me.MITextBox.Size = New System.Drawing.Size(23, 20)
        Me.MITextBox.TabIndex = 190
        Me.MITextBox.TabStop = False
        '
        'DameGeneralesBancosBindingSource
        '
        Me.DameGeneralesBancosBindingSource.DataMember = "DameGeneralesBancos"
        Me.DameGeneralesBancosBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'HORATextBox
        '
        Me.HORATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "HORA", True))
        Me.HORATextBox.Location = New System.Drawing.Point(741, 243)
        Me.HORATextBox.Name = "HORATextBox"
        Me.HORATextBox.Size = New System.Drawing.Size(30, 20)
        Me.HORATextBox.TabIndex = 189
        Me.HORATextBox.TabStop = False
        '
        'DIATextBox
        '
        Me.DIATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "DIA", True))
        Me.DIATextBox.Location = New System.Drawing.Point(741, 217)
        Me.DIATextBox.Name = "DIATextBox"
        Me.DIATextBox.Size = New System.Drawing.Size(100, 20)
        Me.DIATextBox.TabIndex = 188
        Me.DIATextBox.TabStop = False
        '
        'MESTextBox
        '
        Me.MESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "MES", True))
        Me.MESTextBox.Location = New System.Drawing.Point(741, 191)
        Me.MESTextBox.Name = "MESTextBox"
        Me.MESTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MESTextBox.TabIndex = 187
        Me.MESTextBox.TabStop = False
        '
        'ClaverelTextBox
        '
        Me.ClaverelTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "claverel", True))
        Me.ClaverelTextBox.Location = New System.Drawing.Point(741, 62)
        Me.ClaverelTextBox.Name = "ClaverelTextBox"
        Me.ClaverelTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ClaverelTextBox.TabIndex = 180
        Me.ClaverelTextBox.TabStop = False
        '
        'ANOTextBox
        '
        Me.ANOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "ANO", True))
        Me.ANOTextBox.Location = New System.Drawing.Point(741, 168)
        Me.ANOTextBox.Name = "ANOTextBox"
        Me.ANOTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ANOTextBox.TabIndex = 186
        Me.ANOTextBox.TabStop = False
        '
        'SucursalTextBox
        '
        Me.SucursalTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "Sucursal", True))
        Me.SucursalTextBox.Location = New System.Drawing.Point(741, 81)
        Me.SucursalTextBox.Name = "SucursalTextBox"
        Me.SucursalTextBox.Size = New System.Drawing.Size(100, 20)
        Me.SucursalTextBox.TabIndex = 181
        Me.SucursalTextBox.TabStop = False
        '
        'FechaHoyTextBox
        '
        Me.FechaHoyTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "FechaHoy", True))
        Me.FechaHoyTextBox.Location = New System.Drawing.Point(741, 144)
        Me.FechaHoyTextBox.Name = "FechaHoyTextBox"
        Me.FechaHoyTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FechaHoyTextBox.TabIndex = 184
        Me.FechaHoyTextBox.TabStop = False
        '
        'EmisoraTextBox
        '
        Me.EmisoraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "Emisora", True))
        Me.EmisoraTextBox.Location = New System.Drawing.Point(741, 101)
        Me.EmisoraTextBox.Name = "EmisoraTextBox"
        Me.EmisoraTextBox.Size = New System.Drawing.Size(100, 20)
        Me.EmisoraTextBox.TabIndex = 182
        Me.EmisoraTextBox.TabStop = False
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancosBindingSource, "Consecutivo", True))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(741, 123)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ConsecutivoTextBox.TabIndex = 183
        Me.ConsecutivoTextBox.TabStop = False
        '
        'FinText
        '
        Me.FinText.BackColor = System.Drawing.Color.WhiteSmoke
        Me.FinText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.FinText.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameRangoFacturasBindingSource, "Column2", True))
        Me.FinText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FinText.ForeColor = System.Drawing.SystemColors.Desktop
        Me.FinText.Location = New System.Drawing.Point(895, 332)
        Me.FinText.Name = "FinText"
        Me.FinText.Size = New System.Drawing.Size(100, 15)
        Me.FinText.TabIndex = 192
        Me.FinText.TabStop = False
        '
        'DameRangoFacturasBindingSource
        '
        Me.DameRangoFacturasBindingSource.DataMember = "DameRangoFacturas"
        Me.DameRangoFacturasBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'INIText
        '
        Me.INIText.BackColor = System.Drawing.Color.WhiteSmoke
        Me.INIText.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.INIText.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameRangoFacturasBindingSource, "Column1", True))
        Me.INIText.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INIText.ForeColor = System.Drawing.SystemColors.Desktop
        Me.INIText.Location = New System.Drawing.Point(832, 262)
        Me.INIText.Name = "INIText"
        Me.INIText.Size = New System.Drawing.Size(100, 15)
        Me.INIText.TabIndex = 191
        Me.INIText.TabStop = False
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel4.Location = New System.Drawing.Point(14, 360)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(228, 18)
        Me.CMBLabel4.TabIndex = 13
        Me.CMBLabel4.Text = "Detalle del Listado Preliminar"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Gainsboro
        Me.Panel2.Controls.Add(Me.LblVersion)
        Me.Panel2.Controls.Add(Me.LblFecha)
        Me.Panel2.Controls.Add(Me.LblUsuario)
        Me.Panel2.Controls.Add(Me.LblSucursal)
        Me.Panel2.Controls.Add(Me.LblNomCaja)
        Me.Panel2.Controls.Add(Me.LblSistema)
        Me.Panel2.Controls.Add(Me.LblNomEmpresa)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Location = New System.Drawing.Point(1, 2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1003, 46)
        Me.Panel2.TabIndex = 176
        '
        'LblVersion
        '
        Me.LblVersion.AutoSize = True
        Me.LblVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVersion.Location = New System.Drawing.Point(879, 28)
        Me.LblVersion.Name = "LblVersion"
        Me.LblVersion.Size = New System.Drawing.Size(52, 13)
        Me.LblVersion.TabIndex = 187
        Me.LblVersion.Text = "Label14"
        '
        'LblFecha
        '
        Me.LblFecha.AutoSize = True
        Me.LblFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Fecha", True))
        Me.LblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFecha.Location = New System.Drawing.Point(879, 7)
        Me.LblFecha.Name = "LblFecha"
        Me.LblFecha.Size = New System.Drawing.Size(52, 13)
        Me.LblFecha.TabIndex = 186
        Me.LblFecha.Text = "Label14"
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblUsuario
        '
        Me.LblUsuario.AutoSize = True
        Me.LblUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DamedatosUsuarioBindingSource, "Nombre", True))
        Me.LblUsuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUsuario.Location = New System.Drawing.Point(579, 7)
        Me.LblUsuario.Name = "LblUsuario"
        Me.LblUsuario.Size = New System.Drawing.Size(52, 13)
        Me.LblUsuario.TabIndex = 185
        Me.LblUsuario.Text = "Label14"
        '
        'DamedatosUsuarioBindingSource
        '
        Me.DamedatosUsuarioBindingSource.DataMember = "DamedatosUsuario"
        Me.DamedatosUsuarioBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblSucursal
        '
        Me.LblSucursal.AutoSize = True
        Me.LblSucursal.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMENOMBRESUCURSALBindingSource, "Nombre", True))
        Me.LblSucursal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSucursal.Location = New System.Drawing.Point(458, 28)
        Me.LblSucursal.Name = "LblSucursal"
        Me.LblSucursal.Size = New System.Drawing.Size(52, 13)
        Me.LblSucursal.TabIndex = 184
        Me.LblSucursal.Text = "Label14"
        '
        'DAMENOMBRESUCURSALBindingSource
        '
        Me.DAMENOMBRESUCURSALBindingSource.DataMember = "DAMENOMBRESUCURSAL"
        Me.DAMENOMBRESUCURSALBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'LblNomCaja
        '
        Me.LblNomCaja.AutoSize = True
        Me.LblNomCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomCaja.Location = New System.Drawing.Point(383, 7)
        Me.LblNomCaja.Name = "LblNomCaja"
        Me.LblNomCaja.Size = New System.Drawing.Size(52, 13)
        Me.LblNomCaja.TabIndex = 183
        Me.LblNomCaja.Text = "Label14"
        '
        'LblSistema
        '
        Me.LblSistema.AutoSize = True
        Me.LblSistema.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.LblSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSistema.Location = New System.Drawing.Point(78, 28)
        Me.LblSistema.Name = "LblSistema"
        Me.LblSistema.Size = New System.Drawing.Size(52, 13)
        Me.LblSistema.TabIndex = 182
        Me.LblSistema.Text = "Label14"
        '
        'LblNomEmpresa
        '
        Me.LblNomEmpresa.AutoSize = True
        Me.LblNomEmpresa.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Nombre", True))
        Me.LblNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNomEmpresa.Location = New System.Drawing.Point(78, 7)
        Me.LblNomEmpresa.Name = "LblNomEmpresa"
        Me.LblNomEmpresa.Size = New System.Drawing.Size(52, 13)
        Me.LblNomEmpresa.TabIndex = 181
        Me.LblNomEmpresa.Text = "Label14"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Gray
        Me.Label13.Location = New System.Drawing.Point(506, 5)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 15)
        Me.Label13.TabIndex = 180
        Me.Label13.Text = "Cajero(a) : "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Gray
        Me.Label12.Location = New System.Drawing.Point(806, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 15)
        Me.Label12.TabIndex = 179
        Me.Label12.Text = "Versión : "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Gray
        Me.Label7.Location = New System.Drawing.Point(815, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 15)
        Me.Label7.TabIndex = 178
        Me.Label7.Text = "Fecha : "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Gray
        Me.Label6.Location = New System.Drawing.Point(383, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 15)
        Me.Label6.TabIndex = 177
        Me.Label6.Text = "Sucursal : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Gray
        Me.Label5.Location = New System.Drawing.Point(336, 5)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 15)
        Me.Label5.TabIndex = 176
        Me.Label5.Text = "Caja : "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Gray
        Me.Label2.Location = New System.Drawing.Point(9, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 15)
        Me.Label2.TabIndex = 175
        Me.Label2.Text = "Sistema : "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Gray
        Me.Label3.Location = New System.Drawing.Point(4, 5)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 15)
        Me.Label3.TabIndex = 174
        Me.Label3.Text = "Empresa : "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.Location = New System.Drawing.Point(10, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(202, 18)
        Me.Label1.TabIndex = 178
        Me.Label1.Text = "Cargos Recurrentes del  :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(13, 54)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(792, 46)
        Me.Panel3.TabIndex = 179
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(218, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(122, 18)
        Me.Label4.TabIndex = 179
        Me.Label4.Text = "Primer Periodo"
        '
        'Clv_SessionBancosTextBox1
        '
        Me.Clv_SessionBancosTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDETFACTURASBANCOSBindingSource, "Clv_SessionBancos", True))
        Me.Clv_SessionBancosTextBox1.Location = New System.Drawing.Point(651, 155)
        Me.Clv_SessionBancosTextBox1.Name = "Clv_SessionBancosTextBox1"
        Me.Clv_SessionBancosTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.Clv_SessionBancosTextBox1.TabIndex = 191
        Me.Clv_SessionBancosTextBox1.TabStop = False
        '
        'ImporteTextBox
        '
        Me.ImporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancos_TotalBindingSource, "Importe", True))
        Me.ImporteTextBox.Location = New System.Drawing.Point(667, 207)
        Me.ImporteTextBox.Name = "ImporteTextBox"
        Me.ImporteTextBox.Size = New System.Drawing.Size(84, 20)
        Me.ImporteTextBox.TabIndex = 193
        Me.ImporteTextBox.TabStop = False
        '
        'DameGeneralesBancos_TotalBindingSource
        '
        Me.DameGeneralesBancos_TotalBindingSource.DataMember = "DameGeneralesBancos_Total"
        Me.DameGeneralesBancos_TotalBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'ContadorTextBox
        '
        Me.ContadorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameGeneralesBancos_TotalBindingSource, "Contador", True))
        Me.ContadorTextBox.Location = New System.Drawing.Point(651, 207)
        Me.ContadorTextBox.Name = "ContadorTextBox"
        Me.ContadorTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ContadorTextBox.TabIndex = 194
        Me.ContadorTextBox.TabStop = False
        '
        'FolderBrowserDialog1
        '
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.LightGray
        Me.Button5.Enabled = False
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(825, 172)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(179, 41)
        Me.Button5.TabIndex = 501
        Me.Button5.Text = "A&fectar Archivo Resultados PROSA"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.LightGray
        Me.Button6.Enabled = False
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(825, 213)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(179, 41)
        Me.Button6.TabIndex = 502
        Me.Button6.Text = "Af&ectar Archivo Resultados Santander"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'ConPreliminarBancosTableAdapter
        '
        Me.ConPreliminarBancosTableAdapter.ClearBeforeFill = True
        '
        'GeneraBancosBindingSource
        '
        Me.GeneraBancosBindingSource.DataMember = "GeneraBancos"
        Me.GeneraBancosBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'GeneraBancosTableAdapter
        '
        Me.GeneraBancosTableAdapter.ClearBeforeFill = True
        '
        'CANCELALISTADOPRELIMINARBindingSource
        '
        Me.CANCELALISTADOPRELIMINARBindingSource.DataMember = "CANCELALISTADOPRELIMINAR"
        Me.CANCELALISTADOPRELIMINARBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'CANCELALISTADOPRELIMINARTableAdapter
        '
        Me.CANCELALISTADOPRELIMINARTableAdapter.ClearBeforeFill = True
        '
        'DamedatosUsuarioTableAdapter
        '
        Me.DamedatosUsuarioTableAdapter.ClearBeforeFill = True
        '
        'DAMENOMBRESUCURSALTableAdapter
        '
        Me.DAMENOMBRESUCURSALTableAdapter.ClearBeforeFill = True
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'GUARDAFACTURASBANCOSBindingSource
        '
        Me.GUARDAFACTURASBANCOSBindingSource.DataMember = "GUARDAFACTURASBANCOS"
        Me.GUARDAFACTURASBANCOSBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'GUARDAFACTURASBANCOSTableAdapter
        '
        Me.GUARDAFACTURASBANCOSTableAdapter.ClearBeforeFill = True
        '
        'ValidaAfectacionBancosBindingSource
        '
        Me.ValidaAfectacionBancosBindingSource.DataMember = "ValidaAfectacionBancos"
        Me.ValidaAfectacionBancosBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'ValidaAfectacionBancosTableAdapter
        '
        Me.ValidaAfectacionBancosTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAPERIODOSSeleccionarBindingSource
        '
        Me.MUESTRAPERIODOSSeleccionarBindingSource.DataMember = "MUESTRAPERIODOS_Seleccionar"
        Me.MUESTRAPERIODOSSeleccionarBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'MUESTRAPERIODOS_SeleccionarTableAdapter
        '
        Me.MUESTRAPERIODOS_SeleccionarTableAdapter.ClearBeforeFill = True
        '
        'DameGeneralesBancosTableAdapter
        '
        Me.DameGeneralesBancosTableAdapter.ClearBeforeFill = True
        '
        'DameGeneralesBancos_TotalTableAdapter
        '
        Me.DameGeneralesBancos_TotalTableAdapter.ClearBeforeFill = True
        '
        'DameGeneralesBancos_Total_DetalleBindingSource
        '
        Me.DameGeneralesBancos_Total_DetalleBindingSource.DataMember = "DameGeneralesBancos_Total_Detalle"
        Me.DameGeneralesBancos_Total_DetalleBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameGeneralesBancos_Total_DetalleTableAdapter
        '
        Me.DameGeneralesBancos_Total_DetalleTableAdapter.ClearBeforeFill = True
        '
        'DameRangoFacturasTableAdapter
        '
        Me.DameRangoFacturasTableAdapter.ClearBeforeFill = True
        '
        'Dame_PreRecibo_oFacturaTableAdapter
        '
        Me.Dame_PreRecibo_oFacturaTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Inserta_Datos_HBindingSource
        '
        Me.Inserta_Datos_HBindingSource.DataMember = "Inserta_Datos_H"
        Me.Inserta_Datos_HBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_Datos_HTableAdapter
        '
        Me.Inserta_Datos_HTableAdapter.ClearBeforeFill = True
        '
        'Inserta_Datos_Archivo_PBindingSource
        '
        Me.Inserta_Datos_Archivo_PBindingSource.DataMember = "Inserta_Datos_Archivo_P"
        Me.Inserta_Datos_Archivo_PBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_Datos_Archivo_PTableAdapter
        '
        Me.Inserta_Datos_Archivo_PTableAdapter.ClearBeforeFill = True
        '
        'Valida_Proceso_CanceladoBindingSource
        '
        Me.Valida_Proceso_CanceladoBindingSource.DataMember = "Valida_Proceso_Cancelado"
        Me.Valida_Proceso_CanceladoBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Valida_Proceso_CanceladoTableAdapter
        '
        Me.Valida_Proceso_CanceladoTableAdapter.ClearBeforeFill = True
        '
        'Borra_Tablas_ArchivosBindingSource
        '
        Me.Borra_Tablas_ArchivosBindingSource.DataMember = "Borra_Tablas_Archivos"
        Me.Borra_Tablas_ArchivosBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Borra_Tablas_ArchivosTableAdapter
        '
        Me.Borra_Tablas_ArchivosTableAdapter.ClearBeforeFill = True
        '
        'Checa_Archivo_ResProsaBindingSource
        '
        Me.Checa_Archivo_ResProsaBindingSource.DataMember = "Checa_Archivo_ResProsa"
        Me.Checa_Archivo_ResProsaBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Checa_Archivo_ResProsaTableAdapter
        '
        Me.Checa_Archivo_ResProsaTableAdapter.ClearBeforeFill = True
        '
        'Inserta_ProcesoArchivoDebitoBindingSource
        '
        Me.Inserta_ProcesoArchivoDebitoBindingSource.DataMember = "Inserta_ProcesoArchivoDebito"
        Me.Inserta_ProcesoArchivoDebitoBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_ProcesoArchivoDebitoTableAdapter
        '
        Me.Inserta_ProcesoArchivoDebitoTableAdapter.ClearBeforeFill = True
        '
        'Procesa_Arhivo_santaderBindingSource
        '
        Me.Procesa_Arhivo_santaderBindingSource.DataMember = "Procesa_Arhivo_santader"
        Me.Procesa_Arhivo_santaderBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procesa_Arhivo_santaderTableAdapter
        '
        Me.Procesa_Arhivo_santaderTableAdapter.ClearBeforeFill = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Clv_SessionBancos"
        Me.DataGridViewTextBoxColumn13.HeaderText = "No. Proceso"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Clv_Id"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Clv_Id"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "CONTRATO"
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn24.DefaultCellStyle = DataGridViewCellStyle15
        Me.DataGridViewTextBoxColumn24.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "NOMBRE"
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn22.DefaultCellStyle = DataGridViewCellStyle16
        Me.DataGridViewTextBoxColumn22.HeaderText = "Nombre del Cliente"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "CUENTA_BANCO"
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn25.DefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridViewTextBoxColumn25.HeaderText = "Cuenta Bancaria"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "IMPORTE"
        Me.DataGridViewTextBoxColumn19.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        '
        'DataGridViewCheckBoxColumn20
        '
        Me.DataGridViewCheckBoxColumn20.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewCheckBoxColumn20.DataPropertyName = "REALIZADO"
        Me.DataGridViewCheckBoxColumn20.HeaderText = "Afectado"
        Me.DataGridViewCheckBoxColumn20.Name = "DataGridViewCheckBoxColumn20"
        Me.DataGridViewCheckBoxColumn20.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewCheckBoxColumn21
        '
        Me.DataGridViewCheckBoxColumn21.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewCheckBoxColumn21.DataPropertyName = "CANCELADA"
        Me.DataGridViewCheckBoxColumn21.HeaderText = "Cancelada"
        Me.DataGridViewCheckBoxColumn21.Name = "DataGridViewCheckBoxColumn21"
        Me.DataGridViewCheckBoxColumn21.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewCheckBoxColumn2.DataPropertyName = "PAGADO"
        Me.DataGridViewCheckBoxColumn2.HeaderText = "Autorizado"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "TIPO_CUENTA"
        Me.DataGridViewTextBoxColumn26.HeaderText = "Tipo de Cuenta"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "TELEFONO"
        Me.DataGridViewTextBoxColumn23.HeaderText = "Teléfono"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "VENCIMIENTO"
        Me.DataGridViewTextBoxColumn27.HeaderText = "Vencimiento"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "CODIGOSEGURIDAD"
        Me.DataGridViewTextBoxColumn28.HeaderText = "Codigo de Seguridad"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "NOMTARJETA"
        Me.DataGridViewTextBoxColumn29.HeaderText = "Titular de la Cuenta"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "BANCO"
        Me.DataGridViewTextBoxColumn30.HeaderText = "BANCO"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "SUCURSAL"
        Me.DataGridViewTextBoxColumn31.HeaderText = "SUCURSAL"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.DataPropertyName = "CAJERA"
        Me.DataGridViewTextBoxColumn32.HeaderText = "CAJERA"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.DataPropertyName = "CAJA"
        Me.DataGridViewTextBoxColumn33.HeaderText = "CAJA"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn18.HeaderText = "FECHA_GRAL"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "FECHA"
        Me.DataGridViewTextBoxColumn15.HeaderText = "FECHA"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn16.HeaderText = "IMPORTE_TOTAL"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn17.HeaderText = "PROCESO_CANCELADO"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'CONDETFACTURASBANCOSTableAdapter
        '
        Me.CONDETFACTURASBANCOSTableAdapter.ClearBeforeFill = True
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.LightGray
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(825, 254)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(179, 41)
        Me.Button7.TabIndex = 503
        Me.Button7.Text = "Afec&tar Archivo Resultados Bancomer Crédito"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.LightGray
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(825, 295)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(179, 41)
        Me.Button8.TabIndex = 504
        Me.Button8.Text = "Afec&tar Archivo Resultados Bancomer Débito"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'VerAcceso2TableAdapter6
        '
        Me.VerAcceso2TableAdapter6.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter7
        '
        Me.VerAcceso2TableAdapter7.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter8
        '
        Me.VerAcceso2TableAdapter8.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter9
        '
        Me.VerAcceso2TableAdapter9.ClearBeforeFill = True
        '
        'FrmListadoPreliminar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1020, 753)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ConPreliminarBancosDataGridView)
        Me.Controls.Add(ContadorLabel)
        Me.Controls.Add(Me.ContadorTextBox)
        Me.Controls.Add(ImporteLabel)
        Me.Controls.Add(Me.ImporteTextBox)
        Me.Controls.Add(Me.Clv_SessionBancosTextBox1)
        Me.Controls.Add(Me.INIText)
        Me.Controls.Add(Me.FinText)
        Me.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Name = "FrmListadoPreliminar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listados y Afectaciones de Clientes con Cargo Automático"
        CType(Me.ConPreliminarBancosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConPreliminarBancosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONDETFACTURASBANCOSDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDETFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_PreRecibo_oFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDETFACTURASBANCOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONDETFACTURASBANCOSBindingNavigator.ResumeLayout(False)
        Me.CONDETFACTURASBANCOSBindingNavigator.PerformLayout()
        CType(Me.DameGeneralesBancosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameRangoFacturasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DamedatosUsuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMENOMBRESUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DameGeneralesBancos_TotalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GeneraBancosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CANCELALISTADOPRELIMINARBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GUARDAFACTURASBANCOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaAfectacionBancosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAPERIODOSSeleccionarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralesBancos_Total_DetalleBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Datos_HBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_Datos_Archivo_PBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Valida_Proceso_CanceladoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Tablas_ArchivosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Checa_Archivo_ResProsaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_ProcesoArchivoDebitoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procesa_Arhivo_santaderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents ConPreliminarBancosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConPreliminarBancosTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.ConPreliminarBancosTableAdapter
    Friend WithEvents ConPreliminarBancosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CONDETFACTURASBANCOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONDETFACTURASBANCOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents Clv_SessionBancosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GeneraBancosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GeneraBancosTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.GeneraBancosTableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents LblVersion As System.Windows.Forms.Label
    Friend WithEvents LblFecha As System.Windows.Forms.Label
    Friend WithEvents LblUsuario As System.Windows.Forms.Label
    Friend WithEvents LblSucursal As System.Windows.Forms.Label
    Friend WithEvents LblNomCaja As System.Windows.Forms.Label
    Friend WithEvents LblSistema As System.Windows.Forms.Label
    Friend WithEvents LblNomEmpresa As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CANCELALISTADOPRELIMINARBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CANCELALISTADOPRELIMINARTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CANCELALISTADOPRELIMINARTableAdapter
    Friend WithEvents RealizadoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Proceso_CanceladoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DamedatosUsuarioTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DamedatosUsuarioTableAdapter
    Friend WithEvents DamedatosUsuarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMENOMBRESUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMENOMBRESUCURSALTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DAMENOMBRESUCURSALTableAdapter
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GUARDAFACTURASBANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GUARDAFACTURASBANCOSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.GUARDAFACTURASBANCOSTableAdapter
    Friend WithEvents ValidaAfectacionBancosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaAfectacionBancosTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.ValidaAfectacionBancosTableAdapter
    Friend WithEvents MUESTRAPERIODOSSeleccionarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents MUESTRAPERIODOS_SeleccionarTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.MUESTRAPERIODOS_SeleccionarTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DameGeneralesBancosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralesBancosTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancosTableAdapter
    Friend WithEvents ClaverelTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SucursalTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmisoraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaHoyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ANOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DIATextBox As System.Windows.Forms.TextBox
    Friend WithEvents HORATextBox As System.Windows.Forms.TextBox
    Friend WithEvents MITextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SessionBancosTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DameGeneralesBancos_TotalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralesBancos_TotalTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancos_TotalTableAdapter
    Friend WithEvents ImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContadorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DameGeneralesBancos_Total_DetalleBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralesBancos_Total_DetalleTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameGeneralesBancos_Total_DetalleTableAdapter
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents FinText As System.Windows.Forms.TextBox
    Friend WithEvents INIText As System.Windows.Forms.TextBox
    Friend WithEvents DameRangoFacturasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameRangoFacturasTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameRangoFacturasTableAdapter
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Inserta_Datos_HBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Datos_HTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Datos_HTableAdapter
    Friend WithEvents Inserta_Datos_Archivo_PBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_Datos_Archivo_PTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_Datos_Archivo_PTableAdapter
    Friend WithEvents Valida_Proceso_CanceladoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Valida_Proceso_CanceladoTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Valida_Proceso_CanceladoTableAdapter
    Friend WithEvents Borra_Tablas_ArchivosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Tablas_ArchivosTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Borra_Tablas_ArchivosTableAdapter
    Friend WithEvents Checa_Archivo_ResProsaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Checa_Archivo_ResProsaTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Checa_Archivo_ResProsaTableAdapter
    Friend WithEvents Clv_IdTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents Dame_PreRecibo_oFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_PreRecibo_oFacturaTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.Dame_PreRecibo_oFacturaTableAdapter
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_FacturaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Inserta_ProcesoArchivoDebitoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_ProcesoArchivoDebitoTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_ProcesoArchivoDebitoTableAdapter
    Friend WithEvents Procesa_Arhivo_santaderBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procesa_Arhivo_santaderTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Procesa_Arhivo_santaderTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn20 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn21 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONDETFACTURASBANCOSDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CONDETFACTURASBANCOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONDETFACTURASBANCOSTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CONDETFACTURASBANCOSTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn4 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn5 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter6 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter7 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter8 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter9 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
