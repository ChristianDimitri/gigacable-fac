
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic


Public Class FrmImprimir
    Private customersByCityReport As ReportDocument
    Dim bloqueado, identi As Integer
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False
    'Private Const PARAMETER_FIELD_NAME As String = "Op"    
    Private Sub ConfigureCrystalReports_NewXml(ByVal Clv_Factura As Long)
        Try
            Dim cnn As New SqlConnection(MiConexion)

            customersByCityReport = New ReportDocument
            'Dim connectionInfo As New ConnectionInfo



            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\FacturaFiscal.rpt"
            'reportPath = "C:\Users\TeamEdgar\Documents\Visual Studio 2008\Projects\Reportes\Reportes\CrystalReport1.rpt"



            Dim cmd As New SqlCommand("FactFiscales_New", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim parametro1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Clv_Factura
            cmd.Parameters.Add(parametro1)

            Dim da As New SqlDataAdapter(cmd)

            'Dim data1 As New DataTable()
            'Dim data2 As New DataTable()
            Dim ds As New DataSet()
            da.Fill(ds)



            'Dim cmd2 As New SqlCommand("DetFactFiscales_New ", cnn)
            'cmd2.CommandType = CommandType.StoredProcedure
            'Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            'parametro.Direction = ParameterDirection.Input
            'parametro.Value = Clv_Factura
            'cmd2.Parameters.Add(parametro)

            'Dim da2 As New SqlDataAdapter(cmd2)



            'da.Fill(data1)
            'da2.Fill(data2)

            ds.Tables(0).TableName = "FactFiscales_New"
            ds.Tables(1).TableName = "DetFactFiscales_New"

            'ds.Tables.Add(data1)
            'ds.Tables.Add(data2)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            'customersByCityReport.ExportToDisk(ExportFormatType.PortableDocFormat, "C:\Users\TeamEdgar\Documents\mipdf.pdf")
            'customersByCityReport.PrintToPrinter(1, True, 1, 1)

            CrystalReportViewer1.ReportSource = customersByCityReport




            customersByCityReport = Nothing
            System.GC.Collect()
            bnd = True
            'End Using
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable

        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()

        If IdSistema = "AG" And identi > 0 Then
            ConfigureCrystalReports_NewXml(Clv_Factura)
            ba = True
            Exit Sub
        Else
            ConfigureCrystalReports_tickets(Clv_Factura)
            Exit Sub

        End If


        'End If

        'customersByCityReport.Load(reportPath)
        
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        ''If GloImprimeTickets = True Then
        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If



        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        '    CrystalReportViewer1.ShowExportButton = False
        '    CrystalReportViewer1.ShowPrintButton = False
        '    CrystalReportViewer1.ShowRefreshButton = False
        'End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub

    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet

        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        Dim DameGralMsjTickets As New EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
        Dim DameGMT As New EricDataSet2.DameGeneralMsjTicketsDataTable


        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)
        CON.Close()

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        DameGralMsjTickets.Connection = CON2
        DameGralMsjTickets.Fill(DameGMT, eMsjTickets, eActTickets)
        CON2.Close()

        dSet = REPORTETicket(Clv_Factura, 0, 0, DateTime.Today, DateTime.Today, 0)
        rDocument.Load(RutaReportes + "\ReporteTicket.rpt")
        rDocument.SetDataSource(dSet)

        If ba = False Then
            rDocument.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            rDocument.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            If eActTickets = True Then
                rDocument.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

        CrystalReportViewer1.ReportSource = rDocument

        If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = False
            CrystalReportViewer1.ShowPrintButton = False
            CrystalReportViewer1.ShowRefreshButton = False
        End If

        customersByCityReport = Nothing
    End Sub

    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub

    Public Sub ReporteMesesAdelantados(ByVal FechaIni As DateTime, ByVal FechaFin As DateTime)
        Me.Text = "Reporte de Mensualidades Adelantadas"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + FechaIni.ToShortDateString + " al " + FechaFin.ToShortDateString

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReportMesesAdelantados.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport.SetParameterValue(0, FechaIni)
            customersByCityReport.SetParameterValue(1, FechaFin)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            'customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub

    Public Sub ReporteIngresosTarjetas(ByVal fechaI As DateTime, ByVal fechaF As DateTime, ByVal opc As Integer, ByVal tipo As String)
        Me.Text = "Reporte de Ingresos por Tarjetas"
        Try
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim reportPath As String
            Dim fechas As String = "Del " + fechaI.ToShortDateString + " al " + fechaF.ToShortDateString

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            reportPath = RutaReportes + "\ReporteTarjetas.rpt"
            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)
            customersByCityReport.SetParameterValue(0, fechaI)
            customersByCityReport.SetParameterValue(1, fechaF)
            customersByCityReport.SetParameterValue(2, Tipo)
            customersByCityReport.SetParameterValue(3, opc)

            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & Me.Text.ToString & "'"
            customersByCityReport.DataDefinition.FormulaFields("Fechas").Text = "'" & fechas & "'"
            customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

            CrystalReportViewer1.ReportSource = customersByCityReport
        Catch ex As CrystalDecisions.CrystalReports.Engine.ParameterFieldCurrentValueException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try
    End Sub


    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub


    Private Sub FrmImprimir_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LiTipo = 2 Then
            If GloClv_Factura = 0 Then Me.Opacity = 0 Else Me.Opacity = 1
            ConfigureCrystalReports(GloClv_Factura)
        ElseIf LiTipo = 3 Then
            ConfigureCrystalReportsNota(GLONOTA)
        ElseIf LiTipo = 4 Then
            ConfigureCrystalReportsOrden(0, "")
        ElseIf LiTipo = 5 Then
            ConfigureCrystalReportsQueja(0, "")
        ElseIf LiTipo = 6 Then
            ConfigureCrystalReports_tickets2(GloClv_Factura)
        ElseIf LiTipo = 7 Then
            ConfigureCrystalReports_NewXml(GloClv_Factura)
        ElseIf LiTipo = 8 Then
            LiTipo = 0
            REPORTEPagosProporciolaes(eFechaIni, eFechaFin)
        ElseIf LiTipo = 9 Then
            LiTipo = 0
            REPORTEBonificaciones(eFechaIni, eFechaFin)
        ElseIf LiTipo = 10 Then
            LiTipo = 0
            REPORTEDescuentos(eFechaIni, eFechaFin)
        End If
    End Sub

    Private Sub ConfigureCrystalReports_tickets2(ByVal Clv_Factura As Long)
        Try


            Dim ba As Boolean = False
            Select Case IdSistema
                Case "LO"
                    customersByCityReport = New ReporteCajasTickets_2Log

            End Select


            Dim connectionInfo As New ConnectionInfo
            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing

            '        If GloImprimeTickets = False Then
            'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
            'Else
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.BusFacFiscalTableAdapter.Connection = CON
            'Me.BusFacFiscalTableAdapter.Fill(Me.NewsoftvDataSet2.BusFacFiscal, Clv_Factura, identi)
            'CON.Close()
            'CON.Dispose()

            'eActTickets = False
            'Dim CON2 As New SqlConnection(MiConexion)
            'CON2.Open()
            'Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
            'Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
            'CON2.Close()
            'CON2.Dispose()

            'If IdSistema = "VA" Then
            '    'reportPath = RutaReportes + "\ReporteCajasTicketsCosmo.rpt"
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'Else
            '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
            'End If

            'End If

            'customersByCityReport.Load(reportPath)
            'If GloImprimeTickets = False Then
            '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
            'End If
            SetDBLogonForReport(connectionInfo, customersByCityReport)


            '@Clv_Factura 
            customersByCityReport.SetParameterValue(0, Clv_Factura)
            ''@Clv_Factura_Ini
            'customersByCityReport.SetParameterValue(1, "0")
            ''@Clv_Factura_Fin
            'customersByCityReport.SetParameterValue(2, "0")
            ''@Fecha_Ini
            'customersByCityReport.SetParameterValue(3, "01/01/1900")
            ''@Fecha_Fin
            'customersByCityReport.SetParameterValue(4, "01/01/1900")
            ''@op
            'customersByCityReport.SetParameterValue(5, "0")

            'If ba = False Then
            '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            '    If eActTickets = True Then
            '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            '    End If
            'End If

            'If facticket = 1 Then
            '    customersByCityReport.PrintOptions.PrinterName = "EPSON TM-U220 Receipt"

            'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets


            'customersByCityReport.PrintToPrinter(1, True, 1, 1)





            CrystalReportViewer1.ReportSource = customersByCityReport

            'If GloOpFacturas = 3 Then
            CrystalReportViewer1.ShowExportButton = True
            CrystalReportViewer1.ShowPrintButton = True
            CrystalReportViewer1.ShowRefreshButton = True
            'End If
            'SetDBLogonForReport2(connectionInfo)
            'customersByCityReport.Dispose()
        Catch ex As Exception
            MsgBox(Err.Description)
            CrystalReportViewer1.Dispose()
            customersByCityReport.Dispose()
            Me.Close()
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsNota(ByVal nota As Long)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport1(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, nota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        CrystalReportViewer1.ReportSource = customersByCityReport
        'customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport1(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReportsOrden(ByVal op As String, ByVal Titulo As String)
        Try
            Dim CON As New SqlConnection(MiConexion)


            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0"
            Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
            Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
            Dim Num1 As String = 0, Num2 As String = 0
            Dim nclv_trabajo As String = "0"
            Dim nClv_colonia As String = "0"
            Dim Impresora As String = Nothing
            Dim a As Integer = 0



            '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
            '    "=True;User ID=DeSistema;Password=1975huli")

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim mySelectFormula As String = Titulo
            Dim OpOrdenar As String = "0"


            Dim reportPath As String = Nothing

            If IdSistema = "AG" Then

                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBueno.rpt"
            ElseIf IdSistema = "TO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCabStar.rpt"
            ElseIf IdSistema = "SA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoTvRey.rpt"
            ElseIf IdSistema = "VA" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoCosmo.rpt"
            ElseIf IdSistema = "LO" Then
                reportPath = RutaReportes + "\ReporteFormatoOrdenesServBuenoLogitel.rpt"
            End If


            customersByCityReport.Load(reportPath)
            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Clv_TipSer int
            customersByCityReport.SetParameterValue(0, 0)
            ',@op1 smallint
            customersByCityReport.SetParameterValue(1, 1)
            ',@op2 smallint
            customersByCityReport.SetParameterValue(2, 0)
            ',@op3 smallint
            customersByCityReport.SetParameterValue(3, 0)
            ',@op4 smallint,
            customersByCityReport.SetParameterValue(4, 0)
            '@op5 smallint
            customersByCityReport.SetParameterValue(5, 0)
            ',@StatusPen bit
            customersByCityReport.SetParameterValue(6, 0)
            ',@StatusEje bit
            customersByCityReport.SetParameterValue(7, 0)
            ',@StatusVis bit,
            customersByCityReport.SetParameterValue(8, 0)
            '@Clv_OrdenIni bigint
            customersByCityReport.SetParameterValue(9, CLng(gloClave))
            ',@Clv_OrdenFin bigint
            customersByCityReport.SetParameterValue(10, CLng(gloClave))
            ',@Fec1Ini Datetime
            customersByCityReport.SetParameterValue(11, "01/01/1900")
            ',@Fec1Fin Datetime,
            customersByCityReport.SetParameterValue(12, "01/01/1900")
            '@Fec2Ini Datetime
            customersByCityReport.SetParameterValue(13, "01/01/1900")
            ',@Fec2Fin Datetime
            customersByCityReport.SetParameterValue(14, "01/01/1900")
            ',@Clv_Trabajo int
            customersByCityReport.SetParameterValue(15, 0)
            ',@Clv_Colonia int
            customersByCityReport.SetParameterValue(16, 0)
            ',@OpOrden int
            customersByCityReport.SetParameterValue(17, OpOrdenar)





            'mySelectFormula = "Orden " & GloNom_TipSer
            customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
            'CON.Open()
            'Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
            'Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora, a)
            'CON.Close()
            'If a = 1 Then
            '    MsgBox("No se tiene asignada una Impresora de Ordenes de Servicio", MsgBoxStyle.Information)
            '    Exit Sub
            'Else
            '    customersByCityReport.PrintOptions.PrinterName = Impresora
            '    customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'End If
            CrystalReportViewer1.ReportSource = customersByCityReport
            '--SetDBLogonForReport(connectionInfo)

            customersByCityReport = Nothing
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub ConfigureCrystalReportsQueja(ByVal op As Integer, ByVal Titulo As String)

        Dim mySelectFormula As String = ""
        Dim rDocument As New ReportDocument
        Dim dSet As New DataSet

        dSet = ReporteAreaTecnicaQuejas(Glo_tipSer, 1, 0, 0, 0, 0, 0, False, False, False, gloClave, gloClave, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 0, 0, 0, 0, 0, 0)

        rDocument.Load(RutaReportes + "\ReporteFormatoQuejasBueno.rpt")
        rDocument.SetDataSource(dSet)

        mySelectFormula = "Quejas "
        rDocument.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"


        CrystalReportViewer1.ReportSource = rDocument
        rDocument = Nothing


        ''Try
        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        'Dim Op1 As String = "0", Op2 As String = "0", Op3 As String = "0", Op4 As String = "0", Op5 As String = "0", Op6 As String = "0"
        'Dim StatusPen As String = "0", StatusEje As String = "0", StatusVis As String = "0"
        'Dim Fec1Ini As String = "01/01/1900", Fec1Fin As String = "01/01/1900", Fec2Ini As String = "01/01/1900", Fec2Fin As String = "01/01/1900"
        'Dim Num1 As String = 0, Num2 As String = 0
        'Dim nclv_trabajo As String = "0"
        'Dim nClv_colonia As String = "0"
        'Dim a As Integer = 0



        ''"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        ''    "=True;User ID=DeSistema;Password=1975huli")

        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword
        'Dim Impresora_Ordenes As String = Nothing
        'Dim mySelectFormula As String = Titulo
        'Dim OpOrdenar As String = "0"


        'Dim reportPath As String = Nothing

        'If IdSistema = "TO" Then
        '    reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCabStar.rpt"
        'ElseIf IdSistema = "AG" Then
        '    reportPath = RutaReportes + "\ReporteFormatoQuejasBueno.rpt"
        'ElseIf IdSistema = "SA" Then
        '    reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoTvRey.rpt"
        'ElseIf IdSistema = "VA" Then
        '    reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoCosmo.rpt"
        'ElseIf IdSistema = "LO" Then
        '    reportPath = RutaReportes + "\ReporteFormatoQuejasBuenoLogitel.rpt"
        'End If


        ''MsgBox(reportPath)
        'customersByCityReport.Load(reportPath)
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_TipSer int
        'customersByCityReport.SetParameterValue(0, Glo_tipSer)
        '',@op1 smallint
        'customersByCityReport.SetParameterValue(1, 1)
        '',@op2 smallint
        'customersByCityReport.SetParameterValue(2, 0)
        '',@op3 smallint
        'customersByCityReport.SetParameterValue(3, 0)
        '',@op4 smallint,
        'customersByCityReport.SetParameterValue(4, 0)
        ''@op5 smallint
        'customersByCityReport.SetParameterValue(5, 0)
        ''@op6 smallint
        'customersByCityReport.SetParameterValue(6, 0)
        '',@StatusPen bit
        'customersByCityReport.SetParameterValue(7, 0)
        '',@StatusEje bit
        'customersByCityReport.SetParameterValue(8, 0)
        '',@StatusVis bit,
        'customersByCityReport.SetParameterValue(9, 0)
        ''@Clv_OrdenIni bigint
        'customersByCityReport.SetParameterValue(10, CInt(gloClave))
        '',@Clv_OrdenFin bigint
        'customersByCityReport.SetParameterValue(11, CInt(gloClave))
        '',@Fec1Ini Datetime
        'customersByCityReport.SetParameterValue(12, "01/01/1900")
        '',@Fec1Fin Datetime,
        'customersByCityReport.SetParameterValue(13, "01/01/1900")
        ''@Fec2Ini Datetime
        'customersByCityReport.SetParameterValue(14, "01/01/1900")
        '',@Fec2Fin Datetime
        'customersByCityReport.SetParameterValue(15, "01/01/1900")
        '',@Clv_Trabajo int
        'customersByCityReport.SetParameterValue(16, 0)
        '',@Clv_Colonia int
        'customersByCityReport.SetParameterValue(17, 0)
        '',@OpOrden int
        'customersByCityReport.SetParameterValue(18, OpOrdenar)
        ''@Clv_Departamento
        'customersByCityReport.SetParameterValue(19, 0)

        'customersByCityReport.SetParameterValue(20, 0)
        ''@Clv_Departamento
        'customersByCityReport.SetParameterValue(21, 0)

        ''Titulos de Reporte
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()

        'mySelectFormula = "Quejas " ' & Me.TextBox2.Text
        'customersByCityReport.DataDefinition.FormulaFields("Queja").Text = "'" & mySelectFormula & "'"
        ''Me.Dame_Impresora_OrdenesTableAdapter.Connection = CON
        ''Me.Dame_Impresora_OrdenesTableAdapter.Fill(Me.DataSetarnoldo.Dame_Impresora_Ordenes, Impresora_Ordenes, a)
        ''If a = 1 Then
        ''    MsgBox("No se ha asignado una Impresora de Quejas")
        ''    Exit Sub
        ''Else
        ''    customersByCityReport.PrintOptions.PrinterName = Impresora_Ordenes
        ''    customersByCityReport.PrintToPrinter(1, True, 1, 1)
        ''End If

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'CON.Close()
        'customersByCityReport = Nothing
        ''Catch ex As System.Exception
        ''System.Windows.Forms.MessageBox.Show(ex.Message)
        '' End Try
    End Sub

    Private Function REPORTETicket(ByVal CLV_FACTURA As Integer, ByVal CLV_FACTURA_INI As Integer, ByVal CLV_FACTURA_FIN As Integer, ByVal FECHA_INI As DateTime, ByVal FECHA_FIN As DateTime, ByVal OP As Integer) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CATALOGOCAJAS")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("DATOSFISCALES")
        tableNameList.Add("DETFACTURAS")
        tableNameList.Add("DETFACTURASIMPUESTOS")
        tableNameList.Add("FACTURAS")
        tableNameList.Add("GENERALDESCONEXION")
        tableNameList.Add("ReportesFacturas;1")
        tableNameList.Add("SUCURSALES")
        tableNameList.Add("USUARIOS")
        tableNameList.Add("PROMO")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_FACTURA", SqlDbType.Int, CLV_FACTURA)
        BaseII.CreateMyParameter("@CLV_FACTURA_INI", SqlDbType.Int, CLV_FACTURA_INI)
        BaseII.CreateMyParameter("@CLV_FACTURA_FIN", SqlDbType.Int, CLV_FACTURA_FIN)
        BaseII.CreateMyParameter("@FECHA_INI", SqlDbType.DateTime, FECHA_INI)
        BaseII.CreateMyParameter("@FECHA_FIN", SqlDbType.DateTime, FECHA_FIN)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        Return BaseII.ConsultaDS("REPORTETicket", tableNameList)
    End Function

    Private Function ReporteAreaTecnicaQuejas(ByVal Clv_TipSer As Integer, ByVal op1 As Integer, ByVal op2 As Integer, ByVal op3 As Integer, ByVal op4 As Integer, ByVal op5 As Integer, ByVal op6 As Integer,
                                      ByVal StatusPen As Boolean, ByVal StatusEje As Boolean, ByVal StatusVis As Boolean, ByVal Clv_OrdenIni As Integer, ByVal Clv_OrdenFin As Integer,
                                      ByVal Fec1Ini As DateTime, ByVal Fec1Fin As DateTime, ByVal Fec2Ini As DateTime, ByVal Fec2Fin As DateTime, ByVal Clv_Trabajo As Integer, ByVal Clv_Colonia As Integer,
                                      ByVal OpOrden As Integer, ByVal clv_Depto As Integer, ByVal Op7 As Integer, ByVal Contrato As Integer) As DataSet

        Dim tableNameList As New List(Of String)
        tableNameList.Add("CALLES")
        tableNameList.Add("CIUDADES")
        tableNameList.Add("CLIENTES")
        tableNameList.Add("COLONIAS")
        tableNameList.Add("Quejas")
        tableNameList.Add("Rel_Contrato_Macs")
        tableNameList.Add("Rel_Contrato_NoInt")
        tableNameList.Add("ReporteAreaTecnicaQuejas")
        tableNameList.Add("Servicio_Clientes")
        tableNameList.Add("Trabajos")
        tableNameList.Add("XPARAMETROS")
        tableNameList.Add("XSERVICIOS")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@op1", SqlDbType.Int, op1)
        BaseII.CreateMyParameter("@op2", SqlDbType.Int, op2)
        BaseII.CreateMyParameter("@op3", SqlDbType.Int, op3)
        BaseII.CreateMyParameter("@op4", SqlDbType.Int, op4)
        BaseII.CreateMyParameter("@op5", SqlDbType.Int, op5)
        BaseII.CreateMyParameter("@op6", SqlDbType.Int, op6)
        BaseII.CreateMyParameter("@StatusPen", SqlDbType.Bit, StatusPen)
        BaseII.CreateMyParameter("@StatusEje", SqlDbType.Bit, StatusEje)
        BaseII.CreateMyParameter("@StatusVis", SqlDbType.Bit, StatusVis)
        BaseII.CreateMyParameter("@Clv_OrdenIni", SqlDbType.Int, Clv_OrdenIni)
        BaseII.CreateMyParameter("@Clv_OrdenFin", SqlDbType.Int, Clv_OrdenFin)
        BaseII.CreateMyParameter("@Fec1Ini", SqlDbType.DateTime, Fec1Ini)
        BaseII.CreateMyParameter("@Fec1Fin", SqlDbType.DateTime, Fec1Fin)
        BaseII.CreateMyParameter("@Fec2Ini", SqlDbType.DateTime, Fec2Ini)
        BaseII.CreateMyParameter("@Fec2Fin", SqlDbType.DateTime, Fec2Fin)
        BaseII.CreateMyParameter("@Clv_Trabajo", SqlDbType.Int, Clv_Trabajo)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@OpOrden", SqlDbType.Int, OpOrden)
        BaseII.CreateMyParameter("@clv_Depto", SqlDbType.Int, clv_Depto)
        BaseII.CreateMyParameter("@Op7", SqlDbType.Int, Op7)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        Return BaseII.ConsultaDS("ReporteAreaTecnicaQuejas", tableNameList)

    End Function

    Private Sub REPORTEPagosProporciolaes(ByVal FECHAINI As DateTime, ByVal FECHAFIN As DateTime)

        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        Dim rDocument As New ReportDocument

        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEPagosProporciolaes")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FECHAINI)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FECHAFIN)
        dSet = BaseII.ConsultaDS("REPORTEPagosProporciolaes", tableNameList)

        rDocument.Load(RutaReportes + "\REPORTEPagosProporciolaes.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument

    End Sub

    Private Sub REPORTEBonificaciones(ByVal FECHAINI As DateTime, ByVal FECHAFIN As DateTime)

        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        Dim rDocument As New ReportDocument

        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEBonificaciones")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FECHAINI)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FECHAFIN)
        dSet = BaseII.ConsultaDS("REPORTEBonificaciones", tableNameList)

        rDocument.Load(RutaReportes + "\REPORTEBonificaciones.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument

    End Sub

    Private Sub REPORTEDescuentos(ByVal FECHAINI As DateTime, ByVal FECHAFIN As DateTime)

        Dim dSet As New DataSet
        Dim tableNameList As New List(Of String)
        Dim rDocument As New ReportDocument

        tableNameList.Add("Encabezados")
        tableNameList.Add("REPORTEDescuentos")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@FECHAINI", SqlDbType.DateTime, FECHAINI)
        BaseII.CreateMyParameter("@FECHAFIN", SqlDbType.DateTime, FECHAFIN)
        dSet = BaseII.ConsultaDS("REPORTEDescuentos", tableNameList)

        rDocument.Load(RutaReportes + "\REPORTEDescuentos.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument

    End Sub

End Class