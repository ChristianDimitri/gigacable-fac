Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class BrwFacturas_Cancelar

    Private customersByCityReport As ReportDocument
    Dim bloqueado, identi As Integer
    Private op As String = Nothing
    Private Titulo As String = Nothing
    Private eMsjTickets As String = Nothing
    Private eActTickets As Boolean = False

    Private Sub ConfigureCrystalReports_NewXml(ByVal Clv_Factura As Long)
        Try
            Dim cnn As New SqlConnection(MiConexion)

            customersByCityReport = New ReportDocument
            'Dim connectionInfo As New ConnectionInfo



            'connectionInfo.ServerName = GloServerName
            'connectionInfo.DatabaseName = GloDatabaseName
            'connectionInfo.UserID = GloUserID
            'connectionInfo.Password = GloPassword


            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\FacturaFiscal.rpt"
            'reportPath = "\FacturaFiscal.rpt"



            Dim cmd As New SqlCommand("FactFiscales_New", cnn)
            cmd.CommandType = CommandType.StoredProcedure
            Dim parametro1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            parametro1.Direction = ParameterDirection.Input
            parametro1.Value = Clv_Factura
            cmd.Parameters.Add(parametro1)

            Dim da As New SqlDataAdapter(cmd)

            'Dim data1 As New DataTable()
            'Dim data2 As New DataTable()





            'Dim cmd2 As New SqlCommand("DetFactFiscales_New ", cnn)
            'cmd2.CommandType = CommandType.StoredProcedure
            'Dim parametro As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
            'parametro.Direction = ParameterDirection.Input
            'parametro.Value = Clv_Factura
            'cmd2.Parameters.Add(parametro1)

            'Dim da2 As New SqlDataAdapter(cmd2)


            Dim ds As New DataSet()
            da.Fill(ds)
            'da.Fill(data1)
            'da2.Fill(data2)

            ds.Tables(0).TableName = "FactFiscales_New"
            ds.Tables(1).TableName = "DetFactFiscales_New"

            'ds.Tables.Add(data1)
            'ds.Tables.Add(data2)

            customersByCityReport.Load(reportPath)
            customersByCityReport.SetDataSource(ds)

            'customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            'customersByCityReport.ExportToDisk(ExportFormatType.PortableDocFormat, "C:\Users\TeamEdgar\Documents\mipdf.pdf")
            customersByCityReport.PrintToPrinter(1, True, 1, 1)

            'CrystalReportViewer1.ReportSource = customersByCityReport

            customersByCityReport = Nothing
            Bnd = True

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        'customersByCityReport = New ReportDocument
        'Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        'connectionInfo.ServerName = GloServerName
        'connectionInfo.DatabaseName = GloDatabaseName
        'connectionInfo.UserID = GloUserID
        'connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)


        If IdSistema = "AG" And facnormal = True And identi > 0 Then
            ConfigureCrystalReports_NewXml(Clv_Factura)
            ba = True
            Exit Sub

        Else

            ConfigureCrystalReports_tickets(Clv_Factura)
            Exit Sub

        End If

        'End If

        'customersByCityReport.Load(reportPath)
        ''If GloImprimeTickets = False Then
        ''SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        '' End If
        'SetDBLogonForReport(connectionInfo, customersByCityReport)

        ''@Clv_Factura 
        'customersByCityReport.SetParameterValue(0, GloClv_Factura)
        ''@Clv_Factura_Ini
        'customersByCityReport.SetParameterValue(1, "0")
        ''@Clv_Factura_Fin
        'customersByCityReport.SetParameterValue(2, "0")
        ''@Fecha_Ini
        'customersByCityReport.SetParameterValue(3, "01/01/1900")
        ''@Fecha_Fin
        'customersByCityReport.SetParameterValue(4, "01/01/1900")
        ''@op
        'customersByCityReport.SetParameterValue(5, "0")
        ''If GloImprimeTickets = True Then
        'If ba = False Then
        '    customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        '    customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        '    If eActTickets = True Then
        '        customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
        '    End If
        'End If

        'If (IdSistema = "TO" Or IdSistema = "SA" Or IdSistema = "AG" Or IdSistema = "VA") And facnormal = True And identi > 0 Then
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else
        '    customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        'End If





        'customersByCityReport.PrintToPrinter(1, True, 1, 1)
        'CON.Close()
        ''If GloOpFacturas = 3 Then
        ''CrystalReportViewer1.ShowExportButton = False
        ''CrystalReportViewer1.ShowPrintButton = False
        ''CrystalReportViewer1.ShowRefreshButton = False
        ''End If
        ''SetDBLogonForReport2(connectionInfo)
        'customersByCityReport = Nothing
    End Sub
    Private Sub ConfigureCrystalReports_tickets(ByVal Clv_Factura As Long)
        Dim ba As Boolean = False
        Select Case IdSistema
            Case "VA"
                customersByCityReport = New ReporteCajasTickets_2VA
            Case "LO"
                customersByCityReport = New ReporteCajasTickets_2Log
            Case "AG"
                customersByCityReport = New ReporteCajasTickets_2AG
            Case "SA"
                customersByCityReport = New ReporteCajasTickets_2SA
            Case "TO"
                customersByCityReport = New ReporteCajasTickets_2TOM
            Case Else
                customersByCityReport = New ReporteCajasTickets_2OLD
        End Select

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable

        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        'Dim reportPath As String = Nothing

        eActTickets = False
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DameGeneralMsjTicketsTableAdapter.Connection = CON2
        Me.DameGeneralMsjTicketsTableAdapter.Fill(Me.EricDataSet2.DameGeneralMsjTickets, eMsjTickets, eActTickets)
        CON2.Close()

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        'reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        busfac.Connection = CON
        busfac.Fill(bfac, Clv_Factura, identi)


        'customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, GloClv_Factura)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
            If eActTickets = True Then
                customersByCityReport.DataDefinition.FormulaFields("Mensaje").Text = "'" & eMsjTickets & "'"
            End If
        End If

            customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub


    Private Sub OpenSubreport(ByVal reportObjectName As String)

        ' Preview the subreport.

    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub SetDBLogonForSubReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.Subreports(0).SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        'customersByCityReport.Subreports(0).DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)

        Dim I As Integer = myReportDocument.Subreports.Count
        Dim X As Integer = 0
        For X = 0 To I - 1
            Dim myTables As Tables = myReportDocument.Subreports(X).Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Next X
    End Sub

    'Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
    '    Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
    '    For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
    '        myTableLogOnInfo.ConnectionInfo = myConnectionInfo
    '    Next
    'End Sub

    Private Sub BrwFacturas_Cancelar_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'entra cuando se ha cancelado una factura
        Try

            If eEntra = True Then
                eEntra = False
                Dim MSG As String = Nothing
                Dim BNDERROR As Integer = 0
                Dim CON As New SqlConnection(MiConexion)
                If IdSistema = "LO" Then

                    CON.Open()
                    Me.CANCELACIONFACTURASTableAdapter.Connection = CON
                    Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
                    CON.Close()
                Else

                    CON.Open()
                    Me.CANCELACIONFACTURASTableAdapter.Connection = CON
                    Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
                    Me.CancelaCNRPPETableAdapter.Connection = CON
                    Me.CancelaCNRPPETableAdapter.Fill(Me.EricDataSet.CancelaCNRPPE, Me.Clv_FacturaLabel1.Text)
                    Me.CancelaCambioServClienteTableAdapter.Connection = CON
                    Me.CancelaCambioServClienteTableAdapter.Fill(Me.DataSetEdgar.CancelaCambioServCliente, Me.Clv_FacturaLabel1.Text)
                    Dim comando As New SqlClient.SqlCommand
                    With comando
                        .CommandText = "Cancela_NotasDeFactura"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = CON
                        Dim prm As New SqlParameter("@Clv_factura", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = Me.Clv_FacturaLabel1.Text

                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery

                    End With
                    CON.Close()

                    'FacturaFiscalCFD---------------------------------------------------------------------
                    'Generaci�n de FF
                    facturaFiscalCFD = False
                    facturaFiscalCFD = ChecaSiEsFacturaFiscal("N", Clv_FacturaLabel1.Text)
                    If facturaFiscalCFD = True Then
                        DameSerieFolio(0, Clv_FacturaLabel1.Text)
                        CancelaFacturaCFD("N", Clv_FacturaLabel1.Text, eSerie, eFolio, ClienteLabel1.Text, "")
                    End If
                    '--------------------------------------------------------------------------------------

                End If

                
                bitsist(GloUsuario, Me.Clv_FacturaLabel1.Text, GloSistema, Me.Name, "", "Se Cancelo la Factura", "El DIa: " + Me.FECHADateTimePicker.Text, LocClv_Ciudad)
                FrmMotivoCancelacionFactura.Show()
                'MsgBox(MSG)
                If BNDERROR = 0 Then
                    Busca(0)
                End If

            End If

            'entra cuando se ha reimpreso una factura
            If eEntraReImprimir = True Then
                eEntraReImprimir = False
                GloClv_Factura = Me.Clv_FacturaLabel1.Text
                If IdSistema = "AG" Or IdSistema = "VA" Then
                    LiTipo = 2
                    FrmImprimir.Show()
                Else
                    If IdSistema = "LO" Then
                        LiTipo = 6
                        FrmImprimir.Show()
                    Else
                        If LocImpresoraTickets = "" Then
                            MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
                        Else
                            ConfigureCrystalReports(GloClv_Factura)
                        End If
                    End If
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Private Sub BrwFacturas_Cancelar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        Dim op As Integer
        CON.Open()

        eEntra = False
        eEntraReImprimir = False
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet1.MUESTRATIPOFACTURA' Puede moverla o quitarla seg�n sea necesario.
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.NewsoftvDataSet1.DAMEFECHADELSERVIDOR, Me.FECHADateTimePicker.Text)

        If IdSistema = "LO" Then
            CMBLabel5.Text = "Tipo de Pago"
            CMBLabel1.Text = "Buscar Pago por :"
            Button2.Text = "&Cancelar Pago"
            Button6.Text = "&Reimprimir Pago"
            Button8.Text = "&Ver Pago"
            Label8.Text = "Datos del Pago"
        ElseIf IdSistema = "AG" Then
            op = 2
        Else
            op = 1
        End If
        CON.Close()
        'Tipo_Factura()
        If GloOpFacturas = 0 Then
            If IdSistema = "LO" Then
                Me.Text = "Cancelaci�n de Pagos"
            Else
                Me.Text = "Cancelaci�n de Facturas"
            End If
            Me.CMBPanel2.Visible = True
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            Me.Button6.TabStop = False
            Me.Button8.TabStop = False
            Dim CON1 As New SqlConnection(MiConexion)
            CON1.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, 1)
            Me.BUSCAFACTURASTableAdapter.Connection = CON1
            Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", GloTipo)
            CON1.Close()
        ElseIf GloOpFacturas = 1 Then
            If IdSistema = "LO" Then
                Me.Text = "Reimpresi�n de Pagos"
            Else
                Me.Text = "Reimpresi�n de Facturas"
            End If
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = True
            Me.CMBPanel4.Visible = False
            Me.Panel5.Visible = True
            GloTipo = CStr(Me.ComboBox4.SelectedValue)
            CON.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, 1)
            CON.Close()
            Busca(0)
            Me.Button8.TabStop = False
            Me.Button2.TabStop = False
        ElseIf GloOpFacturas = 3 Then
            Me.Text = "Ver Historial de Pagos"
            Me.ComboBox4.SelectedValue = GloTipo
            Me.CMBPanel2.Visible = False
            Me.CMBPanel3.Visible = False
            Me.CMBPanel4.Visible = True
            Me.Panel5.Visible = False
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.MUESTRATIPOFACTURATableAdapter.Connection = CON
            Me.MUESTRATIPOFACTURATableAdapter.Fill(Me.DataSetLydia.MUESTRATIPOFACTURA, op)
            Me.BUSCAFACTURASTableAdapter.Connection = CON2
            Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 13, "", 0, "01/01/1900", GloContrato, "", GloTipo)
            CON2.Close()
            Me.Button6.TabStop = False
            Me.Button2.TabStop = False
        End If

        If GloTipoUsuario = 21 Then
            Me.Button2.Enabled = False
        End If

        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub
    Private Sub buscaNotas(ByVal opcion As Integer)
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        Try
            conlidia.Open()
            Me.BUSCANOTASDECREDITOTableAdapter.Connection = conlidia
            If opcion = 5 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, GloContrato, 0, "")
            ElseIf opcion = 6 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, 0, "")
            ElseIf opcion = 2 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, Me.FECHATextBox.Text, 0, 0, "")
            ElseIf opcion = 3 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, opcion, 0, "01/01/1900", Me.CONTRATOTextBox.Text, 0, "")
            ElseIf opcion = 4 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, 3, 0, "01/01/1900", GloContrato, 0, "")
            End If

            conlidia.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Busca(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloOpFacturas = 3 Then
                If OP = 0 Then
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 10, "", 0, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 11, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", GloContrato, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 12, "", 0, Me.FECHATextBox.Text, GloContrato, "", Me.ComboBox4.SelectedValue)
                End If
            Else
                If OP = 0 Then
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 0, "", 0, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 1 Then
                    If IsNumeric(Me.FOLIOTextBox.Text) = False Then Me.FOLIOTextBox.Text = 0
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 1, Me.SERIETextBox.Text, Me.FOLIOTextBox.Text, "01/01/1900", 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 2 Then
                    If IsDate(Me.FECHATextBox.Text) = False Then Me.FECHATextBox.Text = "01/01/1900"
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 2, "", 0, Me.FECHATextBox.Text, 0, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 3 Then
                    If IsNumeric(Me.CONTRATOTextBox.Text) = False Then Me.CONTRATOTextBox.Text = 0
                    Me.BUSCAFACTURASTableAdapter.Connection = CON
                    Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 3, "", 0, "01/01/1900", Me.CONTRATOTextBox.Text, "", Me.ComboBox4.SelectedValue)
                ElseIf OP = 4 Then
                    If Len(Trim(Me.NOMBRETextBox.Text)) > 0 Then
                        Me.BUSCAFACTURASTableAdapter.Connection = CON
                        Me.BUSCAFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.BUSCAFACTURAS, 4, "", 0, "01/01/1900", 0, Me.NOMBRETextBox.Text, Me.ComboBox4.SelectedValue)
                    Else
                        MsgBox("La B�squeda no se puede realizar sin Datos", MsgBoxStyle.Information)
                    End If
                End If
            End If
            CON.Close()
            Me.SERIETextBox.Clear()
            Me.FOLIOTextBox.Clear()
            Me.FECHATextBox.Clear()
            Me.CONTRATOTextBox.Clear()
            Me.NOMBRETextBox.Clear()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ComboBox4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox4.SelectedIndexChanged
        If Me.ComboBox4.SelectedValue = "N" Then
            Me.BUSCANOTASDECREDITODataGridView.Visible = True
            Me.CMBLabel1.Text = "Buscar Nota por:"
            Me.Label3.Visible = False
            Me.Panel2.Visible = True
            Me.SERIETextBox.Visible = False
            Me.Label4.Text = "Nota :"
            Me.Button8.Text = "&Ver Nota"
            buscaNotas(4)
        Else
            Me.Button8.Text = "&Ver Factura"
            Me.Label3.Visible = True
            Me.SERIETextBox.Visible = True
            Me.Panel2.Visible = False
            Me.Label4.Text = "Serie :"
            Me.CMBLabel1.Text = "Buscar Factura por:"
            If IdSistema = "LO" Then
                Me.Button8.Text = "&Ver Pago"
                Me.CMBLabel1.Text = "Buscar Pago por:"
            End If
            Me.BUSCANOTASDECREDITODataGridView.Visible = False
            Busca(0)
        End If

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(6)
        Else
            Busca(1)
        End If

    End Sub

    Private Sub FECHATextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FECHATextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(5)
            Else
                Busca(2)
            End If

        End If
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            buscaNotas(5)
        Else
            Busca(2)
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Busca(3)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Busca(4)
    End Sub

    Private Sub SERIETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SERIETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub


    Private Sub FOLIOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FOLIOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.ComboBox4.SelectedValue = "N" Then
                buscaNotas(6)
            Else
                Busca(1)
            End If
        End If
    End Sub


    Private Sub CONTRATOTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles CONTRATOTextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub


    Private Sub NOMBRETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NOMBRETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(4)
        End If
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim Cancelada As Boolean = False

        factura = Clv_FacturaLabel1.Text

        Cancelada = Valida_Factura_Cancelada()

        If Cancelada = False Then
            Dim resp As MsgBoxResult
            Dim eRes As Integer = 0
            Dim eMsg As String = Nothing

            If IsNumeric(Me.Clv_FacturaLabel1.Text) = False Then
                If IdSistema = "LO" Then
                    MsgBox("Seleccione el Pago ", MsgBoxStyle.Information)
                Else
                    MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
                End If
                Exit Sub
            End If
            'Eric Cambio hecho el 6Nov2008
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ValidaCancelacionFacturaTableAdapter.Connection = CON
            Me.ValidaCancelacionFacturaTableAdapter.Fill(Me.EricDataSet2.ValidaCancelacionFactura, CLng(Me.Clv_FacturaLabel1.Text), eRes, eMsg)
            CON.Close()
            If eRes = 1 Then
                MsgBox(eMsg, MsgBoxStyle.Information)
                Exit Sub
            End If
            resp = MsgBox("Deseas Cancelar la " & Me.ComboBox4.Text & " : " & Me.SerieLabel1.Text & "-" & Me.FacturaLabel1.Text, MsgBoxStyle.OkCancel, "Cancelaci�n de Facturas")
            If resp = MsgBoxResult.Ok Then
                eCveFactura = Me.Clv_FacturaLabel1.Text
                'Variable que me permite saber si se cancela o se reimprime factura
                eReImprimirF = 0
                FrmMotivoCancelacionFactura.Show()
            End If
        Else

            MsgBox("La factura ya esta cancelada", MsgBoxStyle.Information)

        End If

    End Sub


    Private Sub CANCELAFACTURA()
        'Try
        '    Dim MSG As String = nothing
        '    Dim BNDERROR As Integer = 0
        '    Me.CANCELACIONFACTURASTableAdapter.Fill(Me.NewsoftvDataSet1.CANCELACIONFACTURAS, Me.Clv_FacturaLabel1.Text, 0, MSG, BNDERROR)
        '    FrmMotivoCancelacionFactura.Show()
        '    'MsgBox(MSG)
        '    If BNDERROR = 0 Then
        '        Busca(0)
        '    End If
        'Catch ex As System.Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then

            eReImprimirF = 1
            eCveFactura = Me.Clv_FacturaLabel1.Text
            FrmMotivoCancelacionFactura.Show()
            'GloClv_Factura = Me.Clv_FacturaLabel1.Text
            'FrmImprimir.Show()
        Else
            If IdSistema = "LO" Then
                MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
            Else
                MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If Me.ComboBox4.SelectedValue = "N" Then
            If IsNumeric(Me.Label20.Text) = True Then
                GLONOTA = Me.Label20.Text
                LiTipo = 3
                FrmImprimir.Show()
            Else
                MsgBox("Seleccione la Nota ", MsgBoxStyle.Information)

            End If
        Else
            If IsNumeric(Me.Clv_FacturaLabel1.Text) = True Then

                GloClv_Factura = Me.Clv_FacturaLabel1.Text
                If IdSistema = "LO" Then
                    LiTipo = 6
                Else
                    LiTipo = 2
                End If
                FrmImprimir.Show()
            Else
                If IdSistema = "LO" Then
                    MsgBox("Seleccione la Pago ", MsgBoxStyle.Information)
                Else
                    MsgBox("Seleccione la Factura ", MsgBoxStyle.Information)
                End If
            End If
        End If
        
    End Sub

    Private Sub FacturaLabel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub BUSCANOTASDECREDITODataGridView_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BUSCANOTASDECREDITODataGridView.SelectionChanged
        Dim conelidia As New SqlConnection(MiConexion)
        'If Me.Label20.Text = "" Then
        '    Me.Label20.Text = 0
        'End If
        If Me.Label20.Text <> "" Then
            conelidia.Open()
            Me.DetalleNOTASDECREDITOTableAdapter.Connection = conelidia
            Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, Me.Label20.Text)
            conelidia.Close()
        ElseIf IsNumeric(Me.FOLIOTextBox.Text) = True Then
            'conelidia.Open()
            'Me.DetalleNOTASDECREDITOTableAdapter.Connection = conelidia
            'Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, Me.FOLIOTextBox.Text)
            'conelidia.Close()

        End If
    End Sub
    
    Private Sub FillToolStripButton_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Public Function Valida_Factura_Cancelada() As Boolean

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Factura", SqlDbType.Int, Clv_FacturaLabel1.Text)
        BaseII.CreateMyParameter("@Cancelada", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("Valida_Factura_Cancelada")
        Return BaseII.dicoPar("@Cancelada")


    End Function


#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region

End Class