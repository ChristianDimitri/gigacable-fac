<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDESGLOSEMONEDA
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim CajeraLabel As System.Windows.Forms.Label
        Dim TarjetaLabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim ChequesLabel As System.Windows.Forms.Label
        Dim M005Label As System.Windows.Forms.Label
        Dim M010Label As System.Windows.Forms.Label
        Dim M020Label As System.Windows.Forms.Label
        Dim B1000Label As System.Windows.Forms.Label
        Dim M050Label As System.Windows.Forms.Label
        Dim B500Label As System.Windows.Forms.Label
        Dim M1Label As System.Windows.Forms.Label
        Dim B200Label As System.Windows.Forms.Label
        Dim M2Label As System.Windows.Forms.Label
        Dim B100Label As System.Windows.Forms.Label
        Dim M5Label As System.Windows.Forms.Label
        Dim B50Label As System.Windows.Forms.Label
        Dim M10Label As System.Windows.Forms.Label
        Dim B20Label As System.Windows.Forms.Label
        Dim M20Label As System.Windows.Forms.Label
        Dim M100Label As System.Windows.Forms.Label
        Dim M50Label As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDESGLOSEMONEDA))
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextBoxTotalBilletes = New System.Windows.Forms.TextBox()
        Me.TextBoxTotalMonedas = New System.Windows.Forms.TextBox()
        Me.TextBoxM005 = New System.Windows.Forms.TextBox()
        Me.TextBoxM010 = New System.Windows.Forms.TextBox()
        Me.TextBoxM020 = New System.Windows.Forms.TextBox()
        Me.TextBoxM050 = New System.Windows.Forms.TextBox()
        Me.TextBoxM1 = New System.Windows.Forms.TextBox()
        Me.TextBoxM2 = New System.Windows.Forms.TextBox()
        Me.TextBoxM5 = New System.Windows.Forms.TextBox()
        Me.TextBoxM10 = New System.Windows.Forms.TextBox()
        Me.TextBoxM20 = New System.Windows.Forms.TextBox()
        Me.TextBoxM50 = New System.Windows.Forms.TextBox()
        Me.TextBoxM100 = New System.Windows.Forms.TextBox()
        Me.TextBoxB1000 = New System.Windows.Forms.TextBox()
        Me.TextBoxB500 = New System.Windows.Forms.TextBox()
        Me.TextBoxB200 = New System.Windows.Forms.TextBox()
        Me.TextBoxB100 = New System.Windows.Forms.TextBox()
        Me.TextBoxB50 = New System.Windows.Forms.TextBox()
        Me.TextBoxB20 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.LabelTipo = New System.Windows.Forms.Label()
        Me.TextBoxTipo = New System.Windows.Forms.TextBox()
        Me.ComboBoxTipo = New System.Windows.Forms.ComboBox()
        Me.CONDESGLOSEMONEDABindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.ConsultaDesgloseDolarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.CONDESGLOSEMONEDABindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ConsecutivoTextBox = New System.Windows.Forms.TextBox()
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.TarjetaTextBox = New System.Windows.Forms.TextBox()
        Me.ChequesTextBox = New System.Windows.Forms.TextBox()
        Me.ImporteTextBox = New System.Windows.Forms.TextBox()
        Me.M005TextBox = New System.Windows.Forms.TextBox()
        Me.M010TextBox = New System.Windows.Forms.TextBox()
        Me.M020TextBox = New System.Windows.Forms.TextBox()
        Me.M050TextBox = New System.Windows.Forms.TextBox()
        Me.B1000TextBox = New System.Windows.Forms.TextBox()
        Me.M1TextBox = New System.Windows.Forms.TextBox()
        Me.B500TextBox = New System.Windows.Forms.TextBox()
        Me.M2TextBox = New System.Windows.Forms.TextBox()
        Me.B200TextBox = New System.Windows.Forms.TextBox()
        Me.M5TextBox = New System.Windows.Forms.TextBox()
        Me.B100TextBox = New System.Windows.Forms.TextBox()
        Me.M10TextBox = New System.Windows.Forms.TextBox()
        Me.B50TextBox = New System.Windows.Forms.TextBox()
        Me.M20TextBox = New System.Windows.Forms.TextBox()
        Me.B20TextBox = New System.Windows.Forms.TextBox()
        Me.M50TextBox = New System.Windows.Forms.TextBox()
        Me.M100TextBox = New System.Windows.Forms.TextBox()
        Me.ReferenciaTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_Desglose_DolarTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Consulta_Desglose_DolarTableAdapter()
        Me.Inserta_REl_dolarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Inserta_REl_dolarTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_REl_dolarTableAdapter()
        Me.MUESTRAUSUARIOS2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONDESGLOSEMONEDATableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.CONDESGLOSEMONEDATableAdapter()
        Me.MUESTRAUSUARIOS2TableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRAUSUARIOS2TableAdapter()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.tbTransferencia = New System.Windows.Forms.TextBox()
        Me.tbFONACOT = New System.Windows.Forms.TextBox()
        ConsecutivoLabel = New System.Windows.Forms.Label()
        FechaLabel = New System.Windows.Forms.Label()
        CajeraLabel = New System.Windows.Forms.Label()
        TarjetaLabel = New System.Windows.Forms.Label()
        ImporteLabel = New System.Windows.Forms.Label()
        ChequesLabel = New System.Windows.Forms.Label()
        M005Label = New System.Windows.Forms.Label()
        M010Label = New System.Windows.Forms.Label()
        M020Label = New System.Windows.Forms.Label()
        B1000Label = New System.Windows.Forms.Label()
        M050Label = New System.Windows.Forms.Label()
        B500Label = New System.Windows.Forms.Label()
        M1Label = New System.Windows.Forms.Label()
        B200Label = New System.Windows.Forms.Label()
        M2Label = New System.Windows.Forms.Label()
        B100Label = New System.Windows.Forms.Label()
        M5Label = New System.Windows.Forms.Label()
        B50Label = New System.Windows.Forms.Label()
        M10Label = New System.Windows.Forms.Label()
        B20Label = New System.Windows.Forms.Label()
        M20Label = New System.Windows.Forms.Label()
        M100Label = New System.Windows.Forms.Label()
        M50Label = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.CONDESGLOSEMONEDABindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONDESGLOSEMONEDABindingNavigator.SuspendLayout()
        CType(Me.ConsultaDesgloseDolarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONDESGLOSEMONEDABindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Inserta_REl_dolarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOS2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ConsecutivoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ConsecutivoLabel.Location = New System.Drawing.Point(219, 46)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(100, 15)
        ConsecutivoLabel.TabIndex = 2
        ConsecutivoLabel.Text = "Clave Entrega:"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FechaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FechaLabel.Location = New System.Drawing.Point(269, 77)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(50, 15)
        FechaLabel.TabIndex = 4
        FechaLabel.Text = "Fecha:"
        '
        'CajeraLabel
        '
        CajeraLabel.AutoSize = True
        CajeraLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CajeraLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CajeraLabel.Location = New System.Drawing.Point(248, 111)
        CajeraLabel.Name = "CajeraLabel"
        CajeraLabel.Size = New System.Drawing.Size(71, 15)
        CajeraLabel.TabIndex = 6
        CajeraLabel.Text = "Cajero(a):"
        '
        'TarjetaLabel
        '
        TarjetaLabel.AutoSize = True
        TarjetaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TarjetaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TarjetaLabel.Location = New System.Drawing.Point(94, 480)
        TarjetaLabel.Name = "TarjetaLabel"
        TarjetaLabel.Size = New System.Drawing.Size(56, 15)
        TarjetaLabel.TabIndex = 52
        TarjetaLabel.Text = "Tarjeta:"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ImporteLabel.Location = New System.Drawing.Point(141, 583)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(106, 15)
        ImporteLabel.TabIndex = 8
        ImporteLabel.Text = "Total en Pesos:"
        '
        'ChequesLabel
        '
        ChequesLabel.AutoSize = True
        ChequesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChequesLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ChequesLabel.Location = New System.Drawing.Point(83, 454)
        ChequesLabel.Name = "ChequesLabel"
        ChequesLabel.Size = New System.Drawing.Size(67, 15)
        ChequesLabel.TabIndex = 50
        ChequesLabel.Text = "Cheques:"
        '
        'M005Label
        '
        M005Label.AutoSize = True
        M005Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M005Label.ForeColor = System.Drawing.Color.LightSlateGray
        M005Label.Location = New System.Drawing.Point(366, 515)
        M005Label.Name = "M005Label"
        M005Label.Size = New System.Drawing.Size(47, 15)
        M005Label.TabIndex = 48
        M005Label.Text = "M005:"
        '
        'M010Label
        '
        M010Label.AutoSize = True
        M010Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M010Label.ForeColor = System.Drawing.Color.LightSlateGray
        M010Label.Location = New System.Drawing.Point(366, 489)
        M010Label.Name = "M010Label"
        M010Label.Size = New System.Drawing.Size(47, 15)
        M010Label.TabIndex = 46
        M010Label.Text = "M010:"
        '
        'M020Label
        '
        M020Label.AutoSize = True
        M020Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M020Label.ForeColor = System.Drawing.Color.LightSlateGray
        M020Label.Location = New System.Drawing.Point(366, 463)
        M020Label.Name = "M020Label"
        M020Label.Size = New System.Drawing.Size(47, 15)
        M020Label.TabIndex = 44
        M020Label.Text = "M020:"
        '
        'B1000Label
        '
        B1000Label.AutoSize = True
        B1000Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B1000Label.ForeColor = System.Drawing.Color.LightSlateGray
        B1000Label.Location = New System.Drawing.Point(83, 255)
        B1000Label.Name = "B1000Label"
        B1000Label.Size = New System.Drawing.Size(52, 15)
        B1000Label.TabIndex = 16
        B1000Label.Text = "B1000:"
        '
        'M050Label
        '
        M050Label.AutoSize = True
        M050Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M050Label.ForeColor = System.Drawing.Color.LightSlateGray
        M050Label.Location = New System.Drawing.Point(366, 437)
        M050Label.Name = "M050Label"
        M050Label.Size = New System.Drawing.Size(47, 15)
        M050Label.TabIndex = 42
        M050Label.Text = "M050:"
        '
        'B500Label
        '
        B500Label.AutoSize = True
        B500Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B500Label.ForeColor = System.Drawing.Color.LightSlateGray
        B500Label.Location = New System.Drawing.Point(83, 281)
        B500Label.Name = "B500Label"
        B500Label.Size = New System.Drawing.Size(44, 15)
        B500Label.TabIndex = 18
        B500Label.Text = "B500:"
        '
        'M1Label
        '
        M1Label.AutoSize = True
        M1Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M1Label.ForeColor = System.Drawing.Color.LightSlateGray
        M1Label.Location = New System.Drawing.Point(366, 411)
        M1Label.Name = "M1Label"
        M1Label.Size = New System.Drawing.Size(31, 15)
        M1Label.TabIndex = 40
        M1Label.Text = "M1:"
        '
        'B200Label
        '
        B200Label.AutoSize = True
        B200Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B200Label.ForeColor = System.Drawing.Color.LightSlateGray
        B200Label.Location = New System.Drawing.Point(83, 307)
        B200Label.Name = "B200Label"
        B200Label.Size = New System.Drawing.Size(44, 15)
        B200Label.TabIndex = 20
        B200Label.Text = "B200:"
        '
        'M2Label
        '
        M2Label.AutoSize = True
        M2Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M2Label.ForeColor = System.Drawing.Color.LightSlateGray
        M2Label.Location = New System.Drawing.Point(366, 385)
        M2Label.Name = "M2Label"
        M2Label.Size = New System.Drawing.Size(31, 15)
        M2Label.TabIndex = 38
        M2Label.Text = "M2:"
        '
        'B100Label
        '
        B100Label.AutoSize = True
        B100Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B100Label.ForeColor = System.Drawing.Color.LightSlateGray
        B100Label.Location = New System.Drawing.Point(83, 333)
        B100Label.Name = "B100Label"
        B100Label.Size = New System.Drawing.Size(44, 15)
        B100Label.TabIndex = 22
        B100Label.Text = "B100:"
        '
        'M5Label
        '
        M5Label.AutoSize = True
        M5Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M5Label.ForeColor = System.Drawing.Color.LightSlateGray
        M5Label.Location = New System.Drawing.Point(366, 359)
        M5Label.Name = "M5Label"
        M5Label.Size = New System.Drawing.Size(31, 15)
        M5Label.TabIndex = 36
        M5Label.Text = "M5:"
        '
        'B50Label
        '
        B50Label.AutoSize = True
        B50Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B50Label.ForeColor = System.Drawing.Color.LightSlateGray
        B50Label.Location = New System.Drawing.Point(83, 359)
        B50Label.Name = "B50Label"
        B50Label.Size = New System.Drawing.Size(36, 15)
        B50Label.TabIndex = 24
        B50Label.Text = "B50:"
        '
        'M10Label
        '
        M10Label.AutoSize = True
        M10Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M10Label.ForeColor = System.Drawing.Color.LightSlateGray
        M10Label.Location = New System.Drawing.Point(366, 333)
        M10Label.Name = "M10Label"
        M10Label.Size = New System.Drawing.Size(39, 15)
        M10Label.TabIndex = 34
        M10Label.Text = "M10:"
        '
        'B20Label
        '
        B20Label.AutoSize = True
        B20Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        B20Label.ForeColor = System.Drawing.Color.LightSlateGray
        B20Label.Location = New System.Drawing.Point(83, 385)
        B20Label.Name = "B20Label"
        B20Label.Size = New System.Drawing.Size(36, 15)
        B20Label.TabIndex = 26
        B20Label.Text = "B20:"
        '
        'M20Label
        '
        M20Label.AutoSize = True
        M20Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M20Label.ForeColor = System.Drawing.Color.LightSlateGray
        M20Label.Location = New System.Drawing.Point(366, 307)
        M20Label.Name = "M20Label"
        M20Label.Size = New System.Drawing.Size(39, 15)
        M20Label.TabIndex = 32
        M20Label.Text = "M20:"
        '
        'M100Label
        '
        M100Label.AutoSize = True
        M100Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M100Label.ForeColor = System.Drawing.Color.LightSlateGray
        M100Label.Location = New System.Drawing.Point(366, 255)
        M100Label.Name = "M100Label"
        M100Label.Size = New System.Drawing.Size(47, 15)
        M100Label.TabIndex = 28
        M100Label.Text = "M100:"
        '
        'M50Label
        '
        M50Label.AutoSize = True
        M50Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        M50Label.ForeColor = System.Drawing.Color.LightSlateGray
        M50Label.Location = New System.Drawing.Point(366, 281)
        M50Label.Name = "M50Label"
        M50Label.Size = New System.Drawing.Size(39, 15)
        M50Label.TabIndex = 30
        M50Label.Text = "M50:"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(856, 680)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(148, 40)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Label10)
        Me.Panel1.Controls.Add(Label9)
        Me.Panel1.Controls.Add(Me.tbFONACOT)
        Me.Panel1.Controls.Add(Me.tbTransferencia)
        Me.Panel1.Controls.Add(Me.TextBoxTotalBilletes)
        Me.Panel1.Controls.Add(Me.TextBoxTotalMonedas)
        Me.Panel1.Controls.Add(Me.TextBoxM005)
        Me.Panel1.Controls.Add(Me.TextBoxM010)
        Me.Panel1.Controls.Add(Me.TextBoxM020)
        Me.Panel1.Controls.Add(Me.TextBoxM050)
        Me.Panel1.Controls.Add(Me.TextBoxM1)
        Me.Panel1.Controls.Add(Me.TextBoxM2)
        Me.Panel1.Controls.Add(Me.TextBoxM5)
        Me.Panel1.Controls.Add(Me.TextBoxM10)
        Me.Panel1.Controls.Add(Me.TextBoxM20)
        Me.Panel1.Controls.Add(Me.TextBoxM50)
        Me.Panel1.Controls.Add(Me.TextBoxM100)
        Me.Panel1.Controls.Add(Me.TextBoxB1000)
        Me.Panel1.Controls.Add(Me.TextBoxB500)
        Me.Panel1.Controls.Add(Me.TextBoxB200)
        Me.Panel1.Controls.Add(Me.TextBoxB100)
        Me.Panel1.Controls.Add(Me.TextBoxB50)
        Me.Panel1.Controls.Add(Me.TextBoxB20)
        Me.Panel1.Controls.Add(Me.TextBox5)
        Me.Panel1.Controls.Add(Me.LabelTipo)
        Me.Panel1.Controls.Add(Me.TextBoxTipo)
        Me.Panel1.Controls.Add(Me.ComboBoxTipo)
        Me.Panel1.Controls.Add(Me.CONDESGLOSEMONEDABindingNavigator)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.TextBox3)
        Me.Panel1.Controls.Add(Me.TextBox2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.TextBox4)
        Me.Panel1.Controls.Add(Me.TextBox1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(ConsecutivoLabel)
        Me.Panel1.Controls.Add(Me.ConsecutivoTextBox)
        Me.Panel1.Controls.Add(FechaLabel)
        Me.Panel1.Controls.Add(Me.FechaDateTimePicker)
        Me.Panel1.Controls.Add(Me.TarjetaTextBox)
        Me.Panel1.Controls.Add(CajeraLabel)
        Me.Panel1.Controls.Add(TarjetaLabel)
        Me.Panel1.Controls.Add(Me.ChequesTextBox)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(ChequesLabel)
        Me.Panel1.Controls.Add(Me.ImporteTextBox)
        Me.Panel1.Controls.Add(Me.M005TextBox)
        Me.Panel1.Controls.Add(M005Label)
        Me.Panel1.Controls.Add(Me.M010TextBox)
        Me.Panel1.Controls.Add(M010Label)
        Me.Panel1.Controls.Add(Me.M020TextBox)
        Me.Panel1.Controls.Add(M020Label)
        Me.Panel1.Controls.Add(Me.M050TextBox)
        Me.Panel1.Controls.Add(B1000Label)
        Me.Panel1.Controls.Add(M050Label)
        Me.Panel1.Controls.Add(Me.B1000TextBox)
        Me.Panel1.Controls.Add(Me.M1TextBox)
        Me.Panel1.Controls.Add(B500Label)
        Me.Panel1.Controls.Add(M1Label)
        Me.Panel1.Controls.Add(Me.B500TextBox)
        Me.Panel1.Controls.Add(Me.M2TextBox)
        Me.Panel1.Controls.Add(B200Label)
        Me.Panel1.Controls.Add(M2Label)
        Me.Panel1.Controls.Add(Me.B200TextBox)
        Me.Panel1.Controls.Add(Me.M5TextBox)
        Me.Panel1.Controls.Add(B100Label)
        Me.Panel1.Controls.Add(M5Label)
        Me.Panel1.Controls.Add(Me.B100TextBox)
        Me.Panel1.Controls.Add(Me.M10TextBox)
        Me.Panel1.Controls.Add(B50Label)
        Me.Panel1.Controls.Add(M10Label)
        Me.Panel1.Controls.Add(Me.B50TextBox)
        Me.Panel1.Controls.Add(Me.M20TextBox)
        Me.Panel1.Controls.Add(B20Label)
        Me.Panel1.Controls.Add(M20Label)
        Me.Panel1.Controls.Add(Me.B20TextBox)
        Me.Panel1.Controls.Add(Me.M50TextBox)
        Me.Panel1.Controls.Add(M100Label)
        Me.Panel1.Controls.Add(M50Label)
        Me.Panel1.Controls.Add(Me.M100TextBox)
        Me.Panel1.Controls.Add(Me.ReferenciaTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(992, 662)
        Me.Panel1.TabIndex = 0
        '
        'TextBoxTotalBilletes
        '
        Me.TextBoxTotalBilletes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxTotalBilletes.CausesValidation = False
        Me.TextBoxTotalBilletes.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTotalBilletes.Location = New System.Drawing.Point(253, 403)
        Me.TextBoxTotalBilletes.Name = "TextBoxTotalBilletes"
        Me.TextBoxTotalBilletes.ReadOnly = True
        Me.TextBoxTotalBilletes.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxTotalBilletes.TabIndex = 99
        Me.TextBoxTotalBilletes.TabStop = False
        '
        'TextBoxTotalMonedas
        '
        Me.TextBoxTotalMonedas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxTotalMonedas.CausesValidation = False
        Me.TextBoxTotalMonedas.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTotalMonedas.Location = New System.Drawing.Point(538, 534)
        Me.TextBoxTotalMonedas.Name = "TextBoxTotalMonedas"
        Me.TextBoxTotalMonedas.ReadOnly = True
        Me.TextBoxTotalMonedas.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxTotalMonedas.TabIndex = 98
        Me.TextBoxTotalMonedas.TabStop = False
        '
        'TextBoxM005
        '
        Me.TextBoxM005.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM005.CausesValidation = False
        Me.TextBoxM005.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM005.Location = New System.Drawing.Point(538, 507)
        Me.TextBoxM005.Name = "TextBoxM005"
        Me.TextBoxM005.ReadOnly = True
        Me.TextBoxM005.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM005.TabIndex = 97
        Me.TextBoxM005.TabStop = False
        '
        'TextBoxM010
        '
        Me.TextBoxM010.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM010.CausesValidation = False
        Me.TextBoxM010.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM010.Location = New System.Drawing.Point(538, 481)
        Me.TextBoxM010.Name = "TextBoxM010"
        Me.TextBoxM010.ReadOnly = True
        Me.TextBoxM010.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM010.TabIndex = 96
        Me.TextBoxM010.TabStop = False
        '
        'TextBoxM020
        '
        Me.TextBoxM020.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM020.CausesValidation = False
        Me.TextBoxM020.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM020.Location = New System.Drawing.Point(538, 455)
        Me.TextBoxM020.Name = "TextBoxM020"
        Me.TextBoxM020.ReadOnly = True
        Me.TextBoxM020.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM020.TabIndex = 95
        Me.TextBoxM020.TabStop = False
        '
        'TextBoxM050
        '
        Me.TextBoxM050.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM050.CausesValidation = False
        Me.TextBoxM050.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM050.Location = New System.Drawing.Point(538, 429)
        Me.TextBoxM050.Name = "TextBoxM050"
        Me.TextBoxM050.ReadOnly = True
        Me.TextBoxM050.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM050.TabIndex = 94
        Me.TextBoxM050.TabStop = False
        '
        'TextBoxM1
        '
        Me.TextBoxM1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM1.CausesValidation = False
        Me.TextBoxM1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM1.Location = New System.Drawing.Point(538, 403)
        Me.TextBoxM1.Name = "TextBoxM1"
        Me.TextBoxM1.ReadOnly = True
        Me.TextBoxM1.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM1.TabIndex = 93
        Me.TextBoxM1.TabStop = False
        '
        'TextBoxM2
        '
        Me.TextBoxM2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM2.CausesValidation = False
        Me.TextBoxM2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM2.Location = New System.Drawing.Point(538, 377)
        Me.TextBoxM2.Name = "TextBoxM2"
        Me.TextBoxM2.ReadOnly = True
        Me.TextBoxM2.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM2.TabIndex = 92
        Me.TextBoxM2.TabStop = False
        '
        'TextBoxM5
        '
        Me.TextBoxM5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM5.CausesValidation = False
        Me.TextBoxM5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM5.Location = New System.Drawing.Point(538, 351)
        Me.TextBoxM5.Name = "TextBoxM5"
        Me.TextBoxM5.ReadOnly = True
        Me.TextBoxM5.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM5.TabIndex = 91
        Me.TextBoxM5.TabStop = False
        '
        'TextBoxM10
        '
        Me.TextBoxM10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM10.CausesValidation = False
        Me.TextBoxM10.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM10.Location = New System.Drawing.Point(538, 325)
        Me.TextBoxM10.Name = "TextBoxM10"
        Me.TextBoxM10.ReadOnly = True
        Me.TextBoxM10.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM10.TabIndex = 90
        Me.TextBoxM10.TabStop = False
        '
        'TextBoxM20
        '
        Me.TextBoxM20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM20.CausesValidation = False
        Me.TextBoxM20.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM20.Location = New System.Drawing.Point(538, 299)
        Me.TextBoxM20.Name = "TextBoxM20"
        Me.TextBoxM20.ReadOnly = True
        Me.TextBoxM20.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM20.TabIndex = 89
        Me.TextBoxM20.TabStop = False
        '
        'TextBoxM50
        '
        Me.TextBoxM50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM50.CausesValidation = False
        Me.TextBoxM50.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM50.Location = New System.Drawing.Point(538, 273)
        Me.TextBoxM50.Name = "TextBoxM50"
        Me.TextBoxM50.ReadOnly = True
        Me.TextBoxM50.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM50.TabIndex = 88
        Me.TextBoxM50.TabStop = False
        '
        'TextBoxM100
        '
        Me.TextBoxM100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxM100.CausesValidation = False
        Me.TextBoxM100.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxM100.Location = New System.Drawing.Point(538, 247)
        Me.TextBoxM100.Name = "TextBoxM100"
        Me.TextBoxM100.ReadOnly = True
        Me.TextBoxM100.Size = New System.Drawing.Size(91, 21)
        Me.TextBoxM100.TabIndex = 87
        Me.TextBoxM100.TabStop = False
        '
        'TextBoxB1000
        '
        Me.TextBoxB1000.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB1000.CausesValidation = False
        Me.TextBoxB1000.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB1000.Location = New System.Drawing.Point(253, 247)
        Me.TextBoxB1000.Name = "TextBoxB1000"
        Me.TextBoxB1000.ReadOnly = True
        Me.TextBoxB1000.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB1000.TabIndex = 81
        Me.TextBoxB1000.TabStop = False
        '
        'TextBoxB500
        '
        Me.TextBoxB500.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB500.CausesValidation = False
        Me.TextBoxB500.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB500.Location = New System.Drawing.Point(253, 273)
        Me.TextBoxB500.Name = "TextBoxB500"
        Me.TextBoxB500.ReadOnly = True
        Me.TextBoxB500.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB500.TabIndex = 82
        Me.TextBoxB500.TabStop = False
        '
        'TextBoxB200
        '
        Me.TextBoxB200.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB200.CausesValidation = False
        Me.TextBoxB200.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB200.Location = New System.Drawing.Point(253, 299)
        Me.TextBoxB200.Name = "TextBoxB200"
        Me.TextBoxB200.ReadOnly = True
        Me.TextBoxB200.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB200.TabIndex = 83
        Me.TextBoxB200.TabStop = False
        '
        'TextBoxB100
        '
        Me.TextBoxB100.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB100.CausesValidation = False
        Me.TextBoxB100.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB100.Location = New System.Drawing.Point(253, 325)
        Me.TextBoxB100.Name = "TextBoxB100"
        Me.TextBoxB100.ReadOnly = True
        Me.TextBoxB100.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB100.TabIndex = 84
        Me.TextBoxB100.TabStop = False
        '
        'TextBoxB50
        '
        Me.TextBoxB50.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB50.CausesValidation = False
        Me.TextBoxB50.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB50.Location = New System.Drawing.Point(253, 351)
        Me.TextBoxB50.Name = "TextBoxB50"
        Me.TextBoxB50.ReadOnly = True
        Me.TextBoxB50.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB50.TabIndex = 85
        Me.TextBoxB50.TabStop = False
        '
        'TextBoxB20
        '
        Me.TextBoxB20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxB20.CausesValidation = False
        Me.TextBoxB20.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxB20.Location = New System.Drawing.Point(253, 377)
        Me.TextBoxB20.Name = "TextBoxB20"
        Me.TextBoxB20.ReadOnly = True
        Me.TextBoxB20.Size = New System.Drawing.Size(89, 21)
        Me.TextBoxB20.TabIndex = 86
        Me.TextBoxB20.TabStop = False
        '
        'TextBox5
        '
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Enabled = False
        Me.TextBox5.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(253, 582)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(200, 21)
        Me.TextBox5.TabIndex = 22
        Me.TextBox5.TabStop = False
        '
        'LabelTipo
        '
        Me.LabelTipo.AutoSize = True
        Me.LabelTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTipo.ForeColor = System.Drawing.Color.LightSlateGray
        Me.LabelTipo.Location = New System.Drawing.Point(196, 149)
        Me.LabelTipo.Name = "LabelTipo"
        Me.LabelTipo.Size = New System.Drawing.Size(123, 15)
        Me.LabelTipo.TabIndex = 80
        Me.LabelTipo.Text = "Tipo de Desglose:"
        Me.LabelTipo.Visible = False
        '
        'TextBoxTipo
        '
        Me.TextBoxTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxTipo.Location = New System.Drawing.Point(325, 141)
        Me.TextBoxTipo.Name = "TextBoxTipo"
        Me.TextBoxTipo.ReadOnly = True
        Me.TextBoxTipo.Size = New System.Drawing.Size(200, 21)
        Me.TextBoxTipo.TabIndex = 3
        Me.TextBoxTipo.TabStop = False
        Me.TextBoxTipo.Visible = False
        '
        'ComboBoxTipo
        '
        Me.ComboBoxTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxTipo.FormattingEnabled = True
        Me.ComboBoxTipo.Location = New System.Drawing.Point(325, 141)
        Me.ComboBoxTipo.Name = "ComboBoxTipo"
        Me.ComboBoxTipo.Size = New System.Drawing.Size(200, 23)
        Me.ComboBoxTipo.TabIndex = 76
        Me.ComboBoxTipo.Visible = False
        '
        'CONDESGLOSEMONEDABindingNavigator
        '
        Me.CONDESGLOSEMONEDABindingNavigator.AddNewItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.CountItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONDESGLOSEMONEDABindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.CONDESGLOSEMONEDABindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CONDESGLOSEMONEDABindingNavigatorSaveItem, Me.BindingNavigatorDeleteItem})
        Me.CONDESGLOSEMONEDABindingNavigator.Location = New System.Drawing.Point(727, 0)
        Me.CONDESGLOSEMONEDABindingNavigator.MoveFirstItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.MoveLastItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.MoveNextItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.MovePreviousItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.Name = "CONDESGLOSEMONEDABindingNavigator"
        Me.CONDESGLOSEMONEDABindingNavigator.PositionItem = Nothing
        Me.CONDESGLOSEMONEDABindingNavigator.Size = New System.Drawing.Size(236, 25)
        Me.CONDESGLOSEMONEDABindingNavigator.TabIndex = 27
        Me.CONDESGLOSEMONEDABindingNavigator.TabStop = True
        Me.CONDESGLOSEMONEDABindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&Eliminar"
        '
        'CONDESGLOSEMONEDABindingNavigatorSaveItem
        '
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.Image = CType(resources.GetObject("CONDESGLOSEMONEDABindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.Name = "CONDESGLOSEMONEDABindingNavigatorSaveItem"
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.Size = New System.Drawing.Size(134, 22)
        Me.CONDESGLOSEMONEDABindingNavigatorSaveItem.Text = "&Guardar datos"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label8.Location = New System.Drawing.Point(784, 401)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 15)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Gran Total:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label7.Location = New System.Drawing.Point(698, 302)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 15)
        Me.Label7.TabIndex = 72
        Me.Label7.Text = "Conversion a Pesos:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label6.Location = New System.Drawing.Point(723, 270)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 15)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Tipo de Cambio:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(701, 238)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(134, 15)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Importe en Dólares:"
        '
        'TextBox3
        '
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaDesgloseDolarBindingSource, "conversion_pesos", True))
        Me.TextBox3.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox3.Location = New System.Drawing.Point(841, 297)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(100, 21)
        Me.TextBox3.TabIndex = 21
        '
        'ConsultaDesgloseDolarBindingSource
        '
        Me.ConsultaDesgloseDolarBindingSource.DataMember = "Consulta_Desglose_Dolar"
        Me.ConsultaDesgloseDolarBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TextBox2
        '
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaDesgloseDolarBindingSource, "tipo_cambio", True))
        Me.TextBox2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox2.Location = New System.Drawing.Point(841, 267)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 21)
        Me.TextBox2.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(803, 201)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 24)
        Me.Label4.TabIndex = 67
        Me.Label4.Text = "Dólares:"
        '
        'TextBox4
        '
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConsultaDesgloseDolarBindingSource, "Importe_Dolares", True))
        Me.TextBox4.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TextBox4.Location = New System.Drawing.Point(841, 238)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 21)
        Me.TextBox4.TabIndex = 25
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Cajera", True))
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(325, 106)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(514, 20)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.TabStop = False
        '
        'CONDESGLOSEMONEDABindingSource1
        '
        Me.CONDESGLOSEMONEDABindingSource1.DataMember = "CONDESGLOSEMONEDA"
        Me.CONDESGLOSEMONEDABindingSource1.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(380, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 24)
        Me.Label3.TabIndex = 58
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(408, 211)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(102, 24)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Monedas:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(93, 211)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 24)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Billetes:"
        '
        'ConsecutivoTextBox
        '
        Me.ConsecutivoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Consecutivo", True))
        Me.ConsecutivoTextBox.Enabled = False
        Me.ConsecutivoTextBox.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoTextBox.Location = New System.Drawing.Point(325, 45)
        Me.ConsecutivoTextBox.Name = "ConsecutivoTextBox"
        Me.ConsecutivoTextBox.Size = New System.Drawing.Size(89, 20)
        Me.ConsecutivoTextBox.TabIndex = 0
        Me.ConsecutivoTextBox.TabStop = False
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.CausesValidation = False
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Fecha", True))
        Me.FechaDateTimePicker.Enabled = False
        Me.FechaDateTimePicker.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(325, 72)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FechaDateTimePicker.TabIndex = 1
        Me.FechaDateTimePicker.TabStop = False
        '
        'TarjetaTextBox
        '
        Me.TarjetaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TarjetaTextBox.CausesValidation = False
        Me.TarjetaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Tarjeta", True))
        Me.TarjetaTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TarjetaTextBox.Location = New System.Drawing.Point(158, 474)
        Me.TarjetaTextBox.Name = "TarjetaTextBox"
        Me.TarjetaTextBox.Size = New System.Drawing.Size(89, 21)
        Me.TarjetaTextBox.TabIndex = 11
        '
        'ChequesTextBox
        '
        Me.ChequesTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ChequesTextBox.CausesValidation = False
        Me.ChequesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Cheques", True))
        Me.ChequesTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChequesTextBox.Location = New System.Drawing.Point(158, 448)
        Me.ChequesTextBox.Name = "ChequesTextBox"
        Me.ChequesTextBox.Size = New System.Drawing.Size(89, 21)
        Me.ChequesTextBox.TabIndex = 10
        '
        'ImporteTextBox
        '
        Me.ImporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ImporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Total", True))
        Me.ImporteTextBox.Enabled = False
        Me.ImporteTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteTextBox.Location = New System.Drawing.Point(726, 421)
        Me.ImporteTextBox.Name = "ImporteTextBox"
        Me.ImporteTextBox.ReadOnly = True
        Me.ImporteTextBox.Size = New System.Drawing.Size(200, 21)
        Me.ImporteTextBox.TabIndex = 9
        Me.ImporteTextBox.TabStop = False
        '
        'M005TextBox
        '
        Me.M005TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M005TextBox.CausesValidation = False
        Me.M005TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M005", True))
        Me.M005TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M005TextBox.Location = New System.Drawing.Point(441, 507)
        Me.M005TextBox.Name = "M005TextBox"
        Me.M005TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M005TextBox.TabIndex = 24
        '
        'M010TextBox
        '
        Me.M010TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M010TextBox.CausesValidation = False
        Me.M010TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M010", True))
        Me.M010TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M010TextBox.Location = New System.Drawing.Point(441, 481)
        Me.M010TextBox.Name = "M010TextBox"
        Me.M010TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M010TextBox.TabIndex = 23
        '
        'M020TextBox
        '
        Me.M020TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M020TextBox.CausesValidation = False
        Me.M020TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M020", True))
        Me.M020TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M020TextBox.Location = New System.Drawing.Point(441, 455)
        Me.M020TextBox.Name = "M020TextBox"
        Me.M020TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M020TextBox.TabIndex = 22
        '
        'M050TextBox
        '
        Me.M050TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M050TextBox.CausesValidation = False
        Me.M050TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M050", True))
        Me.M050TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M050TextBox.Location = New System.Drawing.Point(441, 429)
        Me.M050TextBox.Name = "M050TextBox"
        Me.M050TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M050TextBox.TabIndex = 21
        '
        'B1000TextBox
        '
        Me.B1000TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B1000TextBox.CausesValidation = False
        Me.B1000TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B1000", True))
        Me.B1000TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B1000TextBox.Location = New System.Drawing.Point(158, 247)
        Me.B1000TextBox.Name = "B1000TextBox"
        Me.B1000TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B1000TextBox.TabIndex = 4
        '
        'M1TextBox
        '
        Me.M1TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M1TextBox.CausesValidation = False
        Me.M1TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M1", True))
        Me.M1TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M1TextBox.Location = New System.Drawing.Point(441, 403)
        Me.M1TextBox.Name = "M1TextBox"
        Me.M1TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M1TextBox.TabIndex = 20
        '
        'B500TextBox
        '
        Me.B500TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B500TextBox.CausesValidation = False
        Me.B500TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B500", True))
        Me.B500TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B500TextBox.Location = New System.Drawing.Point(158, 273)
        Me.B500TextBox.Name = "B500TextBox"
        Me.B500TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B500TextBox.TabIndex = 5
        '
        'M2TextBox
        '
        Me.M2TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M2TextBox.CausesValidation = False
        Me.M2TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M2", True))
        Me.M2TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M2TextBox.Location = New System.Drawing.Point(441, 377)
        Me.M2TextBox.Name = "M2TextBox"
        Me.M2TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M2TextBox.TabIndex = 19
        '
        'B200TextBox
        '
        Me.B200TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B200TextBox.CausesValidation = False
        Me.B200TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B200", True))
        Me.B200TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B200TextBox.Location = New System.Drawing.Point(158, 299)
        Me.B200TextBox.Name = "B200TextBox"
        Me.B200TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B200TextBox.TabIndex = 6
        '
        'M5TextBox
        '
        Me.M5TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M5TextBox.CausesValidation = False
        Me.M5TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M5", True))
        Me.M5TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M5TextBox.Location = New System.Drawing.Point(441, 351)
        Me.M5TextBox.Name = "M5TextBox"
        Me.M5TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M5TextBox.TabIndex = 18
        '
        'B100TextBox
        '
        Me.B100TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B100TextBox.CausesValidation = False
        Me.B100TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B100", True))
        Me.B100TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B100TextBox.Location = New System.Drawing.Point(158, 325)
        Me.B100TextBox.Name = "B100TextBox"
        Me.B100TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B100TextBox.TabIndex = 7
        '
        'M10TextBox
        '
        Me.M10TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M10TextBox.CausesValidation = False
        Me.M10TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M10", True))
        Me.M10TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M10TextBox.Location = New System.Drawing.Point(441, 325)
        Me.M10TextBox.Name = "M10TextBox"
        Me.M10TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M10TextBox.TabIndex = 17
        '
        'B50TextBox
        '
        Me.B50TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B50TextBox.CausesValidation = False
        Me.B50TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B50", True))
        Me.B50TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B50TextBox.Location = New System.Drawing.Point(158, 351)
        Me.B50TextBox.Name = "B50TextBox"
        Me.B50TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B50TextBox.TabIndex = 8
        '
        'M20TextBox
        '
        Me.M20TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M20TextBox.CausesValidation = False
        Me.M20TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M20", True))
        Me.M20TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M20TextBox.Location = New System.Drawing.Point(441, 299)
        Me.M20TextBox.Name = "M20TextBox"
        Me.M20TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M20TextBox.TabIndex = 16
        '
        'B20TextBox
        '
        Me.B20TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B20TextBox.CausesValidation = False
        Me.B20TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "B20", True))
        Me.B20TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.B20TextBox.Location = New System.Drawing.Point(158, 377)
        Me.B20TextBox.Name = "B20TextBox"
        Me.B20TextBox.Size = New System.Drawing.Size(89, 21)
        Me.B20TextBox.TabIndex = 9
        '
        'M50TextBox
        '
        Me.M50TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M50TextBox.CausesValidation = False
        Me.M50TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M50", True))
        Me.M50TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M50TextBox.Location = New System.Drawing.Point(441, 273)
        Me.M50TextBox.Name = "M50TextBox"
        Me.M50TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M50TextBox.TabIndex = 15
        '
        'M100TextBox
        '
        Me.M100TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M100TextBox.CausesValidation = False
        Me.M100TextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "M100", True))
        Me.M100TextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.M100TextBox.Location = New System.Drawing.Point(441, 247)
        Me.M100TextBox.Name = "M100TextBox"
        Me.M100TextBox.Size = New System.Drawing.Size(91, 21)
        Me.M100TextBox.TabIndex = 14
        '
        'ReferenciaTextBox
        '
        Me.ReferenciaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONDESGLOSEMONEDABindingSource1, "Referencia", True))
        Me.ReferenciaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReferenciaTextBox.Location = New System.Drawing.Point(430, 581)
        Me.ReferenciaTextBox.Name = "ReferenciaTextBox"
        Me.ReferenciaTextBox.Size = New System.Drawing.Size(10, 21)
        Me.ReferenciaTextBox.TabIndex = 78
        Me.ReferenciaTextBox.TabStop = False
        '
        'Consulta_Desglose_DolarTableAdapter
        '
        Me.Consulta_Desglose_DolarTableAdapter.ClearBeforeFill = True
        '
        'Inserta_REl_dolarBindingSource
        '
        Me.Inserta_REl_dolarBindingSource.DataMember = "Inserta_REl_dolar"
        Me.Inserta_REl_dolarBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Inserta_REl_dolarTableAdapter
        '
        Me.Inserta_REl_dolarTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAUSUARIOS2BindingSource
        '
        Me.MUESTRAUSUARIOS2BindingSource.DataMember = "MUESTRAUSUARIOS2"
        Me.MUESTRAUSUARIOS2BindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'CONDESGLOSEMONEDATableAdapter
        '
        Me.CONDESGLOSEMONEDATableAdapter.ClearBeforeFill = True
        '
        'MUESTRAUSUARIOS2TableAdapter
        '
        Me.MUESTRAUSUARIOS2TableAdapter.ClearBeforeFill = True
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(799, 0)
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'tbTransferencia
        '
        Me.tbTransferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbTransferencia.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbTransferencia.Location = New System.Drawing.Point(158, 501)
        Me.tbTransferencia.Name = "tbTransferencia"
        Me.tbTransferencia.Size = New System.Drawing.Size(89, 21)
        Me.tbTransferencia.TabIndex = 12
        '
        'tbFONACOT
        '
        Me.tbFONACOT.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.tbFONACOT.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold)
        Me.tbFONACOT.Location = New System.Drawing.Point(158, 527)
        Me.tbFONACOT.Name = "tbFONACOT"
        Me.tbFONACOT.Size = New System.Drawing.Size(89, 21)
        Me.tbFONACOT.TabIndex = 13
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label9.ForeColor = System.Drawing.Color.LightSlateGray
        Label9.Location = New System.Drawing.Point(51, 506)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(99, 15)
        Label9.TabIndex = 102
        Label9.Text = "Transferencia:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.LightSlateGray
        Label10.Location = New System.Drawing.Point(76, 532)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(74, 15)
        Label10.TabIndex = 103
        Label10.Text = "FONACOT:"
        '
        'FrmDESGLOSEMONEDA
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmDESGLOSEMONEDA"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Desglose De Moneda"
        Me.TopMost = True
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONDESGLOSEMONEDABindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONDESGLOSEMONEDABindingNavigator.ResumeLayout(False)
        Me.CONDESGLOSEMONEDABindingNavigator.PerformLayout()
        CType(Me.ConsultaDesgloseDolarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONDESGLOSEMONEDABindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Inserta_REl_dolarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOS2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents TarjetaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ChequesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents M005TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M010TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M020TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M050TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B1000TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M1TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B500TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M2TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B200TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M5TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B100TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M10TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B50TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M20TextBox As System.Windows.Forms.TextBox
    Friend WithEvents B20TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M50TextBox As System.Windows.Forms.TextBox
    Friend WithEvents M100TextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents CONDESGLOSEMONEDATableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.CONDESGLOSEMONEDATableAdapter
    Friend WithEvents MUESTRAUSUARIOS2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOS2TableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRAUSUARIOS2TableAdapter
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents ConsultaDesgloseDolarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Consulta_Desglose_DolarTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Consulta_Desglose_DolarTableAdapter
    Friend WithEvents Inserta_REl_dolarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Inserta_REl_dolarTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Inserta_REl_dolarTableAdapter
    Friend WithEvents CONDESGLOSEMONEDABindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents CONDESGLOSEMONEDABindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONDESGLOSEMONEDABindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents ComboBoxTipo As System.Windows.Forms.ComboBox
    Friend WithEvents ReferenciaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxTipo As System.Windows.Forms.TextBox
    Friend WithEvents LabelTipo As System.Windows.Forms.Label
    Friend WithEvents TextBoxTotalBilletes As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxTotalMonedas As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM005 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM010 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM020 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM050 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM5 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM20 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM50 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxM100 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB1000 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB500 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB200 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB100 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB50 As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxB20 As System.Windows.Forms.TextBox
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents tbFONACOT As System.Windows.Forms.TextBox
    Friend WithEvents tbTransferencia As System.Windows.Forms.TextBox
End Class
