Imports System.Data.SqlClient

Public Class FrmSupervisor

    Private Sub Acceso()
        Try
            Dim bnd As Integer = 0



            If eAccesoAdmin = True Then
                bnd = 1
            ElseIf IdSistema = "SA" And locband_pant = 3 Then
                GloClv_Txt = Me.Clv_UsuarioTextBox.Text
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.VerAcceso2TableAdapter.Connection = CON
                Me.VerAcceso2TableAdapter.Fill(Me.ProcedimientosArnoldo3.VerAcceso2, GloClv_Txt, Me.PasaporteTextBox.Text, bnd, locnomsupervisor)
                CON.Close()
                LocSupBon = GloClv_Txt
            Else
                GloClv_Txt = Me.Clv_UsuarioTextBox.Text
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.VerAcceso1TableAdapter.Connection = CON
                Me.VerAcceso1TableAdapter.Fill(Me.NewsoftvDataSet.VerAcceso1, GloClv_Txt, Me.PasaporteTextBox.Text, bnd, locnomsupervisor)
                LocSupBon = GloClv_Txt
                CON.Close()
            End If

            bnd = 1

            If bnd = 1 Then
                If locband_pant = 1 Then
                    My.Forms.BwrEntregasParciales.Show()
                    Me.Close()
                ElseIf locband_pant = 2 Then
                    My.Forms.FrmArqueo.Show()
                    Me.Close()
                ElseIf locband_pant = 3 Then

                    'Bonificación para usuario "Cajero"
                    If GloTipoUsuario = 21 And locband_pant = 3 Then
                        eAccesoAdmin = False
                    End If

                    FrmBonificacion.Show()
                    Me.Close()
                ElseIf locband_pant = 4 Then
                    My.Forms.FrmCortesdeFacturas.Show()
                    Me.Close()
                ElseIf locband_pant = 5 Then
                    BrwFacturas_Cancelar.Show()
                    Me.Close() 'cancelar,
                ElseIf locband_pant = 6 Then
                    'reimprimir 
                    BrwFacturas_Cancelar.Show()
                    Me.Close()
                ElseIf locband_pant = 7 Then

                ElseIf locband_pant = 8 Then
                    BndSupervisorFac = True
                    GloClvSupAuto = GloClv_Txt
                    Me.Close()
                ElseIf locband_pant = 10 Then
                    BrwFacturasCancelarRE.Show()
                    Me.Close()
                End If
            Else
                MsgBox(" Acceso Denegado ", MsgBoxStyle.Information)
                'End
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_UsuarioLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmSupervisor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Bonificación para usuario "Cajero"
        If GloTipoUsuario = 21 And locband_pant = 3 Then
            eAccesoAdmin = True
        End If
        If GloTipoUsuario = 21 And locband_pant = 4 Then
            eAccesoAdmin = True
        End If

        eAccesoAdmin = True

        If eAccesoAdmin = True Then
            Acceso()
        Else
            If locband_pant = 8 Then
                MsgBox("El Cliente Actual Es De Cargo Recurrente, Se Requiere Una Autorización Del Supervisor Para Ser Cobrado", MsgBoxStyle.Information)
            End If
        End If
        colorea(Me)
        Me.Text = loctitulo
    End Sub

    Private Sub Clv_UsuarioTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_UsuarioTextBox.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Acceso()
    End Sub

    Private Sub PasaporteTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles PasaporteTextBox.KeyPress
        If Asc(e.KeyChar.ToString) = 13 Then
            Acceso()
        End If
    End Sub



    Private Sub PasaporteTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasaporteTextBox.TextChanged

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Acceso()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()

    End Sub

    
End Class