﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;


namespace GeneraCFDFacturaNet
{
    public class Generador
    {
        public StringBuilder GeneraCFD(DataSet ds)
        {

            Int32 i = 0;
            String Caracter = "";
            StringBuilder stringBuilder = new StringBuilder();

            //stringBuilder.AppendLine("Outputmode=");
            //stringBuilder.AppendLine("<Factura>");
            stringBuilder.AppendLine("<Documento>");

            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Comprobante>");
            //stringBuilder.AppendLine("version=" + ds.Tables["Comprobante"].Rows[0]["version"].ToString());
            stringBuilder.AppendLine("serie=" + ds.Tables["Comprobante"].Rows[0]["serie"].ToString());
            stringBuilder.AppendLine("folio=" + ds.Tables["Comprobante"].Rows[0]["folio"].ToString());
            stringBuilder.AppendLine("fecha=" + ds.Tables["Comprobante"].Rows[0]["fecha"].ToString());
            //stringBuilder.AppendLine("noAprobacion=" + ds.Tables["Comprobante"].Rows[0]["noAprobacion"].ToString());
            //stringBuilder.AppendLine("anoAprobacion=" + ds.Tables["Comprobante"].Rows[0]["anoAprobacion"].ToString());
            //stringBuilder.AppendLine("certificado=" + ds.Tables["Comprobante"].Rows[0]["certificado"].ToString());
            stringBuilder.AppendLine("tipoDeComprobante=" + ds.Tables["Comprobante"].Rows[0]["tipoDeComprobante"].ToString());
            stringBuilder.AppendLine("TipoDeComprobanteLeyenda=" + ds.Tables["Comprobante"].Rows[0]["TipoDeComprobanteLeyenda"].ToString());
            stringBuilder.AppendLine("tituloDocumento=" + ds.Tables["Comprobante"].Rows[0]["tituloDocumento"].ToString());
            stringBuilder.AppendLine("FormaPago=" + ds.Tables["Comprobante"].Rows[0]["formaDePago"].ToString());
            stringBuilder.AppendLine("FormaPagoLeyenda=" + ds.Tables["Comprobante"].Rows[0]["FormaPagoLeyenda"].ToString());
            //stringBuilder.AppendLine("metodoDePago=" + ds.Tables["Comprobante"].Rows[0]["metodoDePago"].ToString());
            //stringBuilder.AppendLine("numCtaPago=" + ds.Tables["Comprobante"].Rows[0]["numCtaPago"].ToString());
            stringBuilder.AppendLine("condicionesDePago=" + ds.Tables["Comprobante"].Rows[0]["condicionesDePago"].ToString());
            //stringBuilder.AppendLine("metodoDePago=" + ds.Tables["Comprobante"].Rows[0]["metodoDePago"].ToString());
            stringBuilder.AppendLine("MetodoPago=" + ds.Tables["Comprobante"].Rows[0]["MetodoPago"].ToString());
            stringBuilder.AppendLine("MetodoPagoLeyenda=" + ds.Tables["Comprobante"].Rows[0]["MetodoPagoLeyenda"].ToString());
            stringBuilder.AppendLine("numCtaPago=" + ds.Tables["Comprobante"].Rows[0]["numCtaPago"].ToString());
            stringBuilder.AppendLine("subtotal=" + ds.Tables["Comprobante"].Rows[0]["subtotal"].ToString());
            stringBuilder.AppendLine("descuento=" + ds.Tables["Comprobante"].Rows[0]["descuento"].ToString());
            stringBuilder.AppendLine("iva=" + ds.Tables["Comprobante"].Rows[0]["iva"].ToString());
            //stringBuilder.AppendLine("ieps=" + ds.Tables["Comprobante"].Rows[0]["ieps"].ToString());

            //if (Double.Parse(ds.Tables["Comprobante"].Rows[0]["ieps"].ToString()) > 0)
            //{
            //    stringBuilder.AppendLine("ieps=" + ds.Tables["Comprobante"].Rows[0]["ieps"].ToString());
            //}

            stringBuilder.AppendLine("total=" + ds.Tables["Comprobante"].Rows[0]["total"].ToString());
            stringBuilder.AppendLine("Moneda=" + ds.Tables["Comprobante"].Rows[0]["Moneda"].ToString());
            //stringBuilder.AppendLine("sretencion=" + ds.Tables["Comprobante"].Rows[0]["retencion"].ToString() );
            //stringBuilder.AppendLine("factorRetencionIVA=" + ds.Tables["Comprobante"].Rows[0]["factorRetencionIVA"].ToString() );
            //stringBuilder.AppendLine("retencionISR=" + ds.Tables["Comprobante"].Rows[0]["retencionISR"].ToString() );
            //stringBuilder.AppendLine("factorRetencionISR=" + ds.Tables["Comprobante"].Rows[0]["factorRetencionISR"].ToString() );
            stringBuilder.AppendLine("LugarExpedicion=" + ds.Tables["Comprobante"].Rows[0]["LugarExpedicion"].ToString());
            stringBuilder.AppendLine("</Comprobante>");

            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Emisor>");
            stringBuilder.AppendLine("erfc=" + ds.Tables["Emisor"].Rows[0]["erfc"].ToString());
            stringBuilder.AppendLine("enombre=" + ds.Tables["Emisor"].Rows[0]["enombre"].ToString());
            stringBuilder.AppendLine("regimenFiscal=" + ds.Tables["Emisor"].Rows[0]["regimenFiscal"].ToString());
            stringBuilder.AppendLine("RegimenFiscalLeyenda=" + ds.Tables["Emisor"].Rows[0]["RegimenFiscalLeyenda"].ToString());
            //stringBuilder.AppendLine("</Emisor>");

            //stringBuilder.AppendLine("");
            //stringBuilder.AppendLine("<DomicilioFiscal>");
            stringBuilder.AppendLine("ecalle=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecalle"].ToString());
            stringBuilder.AppendLine("enoExterior=" + ds.Tables["DomicilioFiscal"].Rows[0]["enoExterior"].ToString());
            stringBuilder.AppendLine("enoInterior=" + ds.Tables["DomicilioFiscal"].Rows[0]["enoInterior"].ToString());
            stringBuilder.AppendLine("ecolonia=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecolonia"].ToString());
            stringBuilder.AppendLine("elocalidad=" + ds.Tables["DomicilioFiscal"].Rows[0]["elocalidad"].ToString());
            stringBuilder.AppendLine("ereferencia=" + ds.Tables["DomicilioFiscal"].Rows[0]["ereferencia"].ToString());
            stringBuilder.AppendLine("emunicipio=" + ds.Tables["DomicilioFiscal"].Rows[0]["emunicipio"].ToString());
            stringBuilder.AppendLine("eestado=" + ds.Tables["DomicilioFiscal"].Rows[0]["eestado"].ToString());
            stringBuilder.AppendLine("epais=" + ds.Tables["DomicilioFiscal"].Rows[0]["epais"].ToString());
            stringBuilder.AppendLine("ecodigoPostal=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecodigoPostal"].ToString());
            stringBuilder.AppendLine("etel=" + ds.Tables["DomicilioFiscal"].Rows[0]["etel"].ToString());
            stringBuilder.AppendLine("eemail=" + ds.Tables["DomicilioFiscal"].Rows[0]["eemail"].ToString());
            //stringBuilder.AppendLine("</DomicilioFiscal>");
            stringBuilder.AppendLine("</Emisor>");

            //if (ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString().Length > 0)
            //{
            //    stringBuilder.AppendLine("");
            //    stringBuilder.AppendLine("<ExpedidoEn>");
            //    stringBuilder.AppendLine("ex_calle=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString());
            //    stringBuilder.AppendLine("ex_noExterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noExterior"].ToString());
            //    stringBuilder.AppendLine("ex_noInterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noInterior"].ToString());
            //    stringBuilder.AppendLine("ex_colonia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_colonia"].ToString());
            //    stringBuilder.AppendLine("ex_localidad=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_localidad"].ToString());
            //    stringBuilder.AppendLine("ex_referencia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_referencia"].ToString());
            //    stringBuilder.AppendLine("ex_municipio=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_municipio"].ToString());
            //    stringBuilder.AppendLine("ex_estado=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_estado"].ToString());
            //    stringBuilder.AppendLine("ex_pais=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_pais"].ToString());
            //    stringBuilder.AppendLine("ex_codigoPostal=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_codigoPostal"].ToString());
            //    stringBuilder.AppendLine("</ExpedidoEn>");
            //}

            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Receptor>");
            stringBuilder.AppendLine("rfc=" + ds.Tables["Receptor"].Rows[0]["rfc"].ToString());
            stringBuilder.AppendLine("nombre=" + ds.Tables["Receptor"].Rows[0]["nombre"].ToString());
            stringBuilder.AppendLine("noCliente=" + ds.Tables["Receptor"].Rows[0]["noCliente"].ToString());
            stringBuilder.AppendLine("UsoCFDI=" + ds.Tables["Receptor"].Rows[0]["UsoCFDI"].ToString());
            stringBuilder.AppendLine("UsoCFDILeyenda=" + ds.Tables["Receptor"].Rows[0]["UsoCFDILeyenda"].ToString());
            //stringBuilder.AppendLine("</Receptor>");

            //stringBuilder.AppendLine("");
            //stringBuilder.AppendLine("<Cliente>");
            stringBuilder.AppendLine("calle=" + ds.Tables["Cliente"].Rows[0]["calle"].ToString());
            stringBuilder.AppendLine("noExterior=" + ds.Tables["Cliente"].Rows[0]["noExterior"].ToString());
            stringBuilder.AppendLine("noInterior=" + ds.Tables["Cliente"].Rows[0]["noInterior"].ToString());
            stringBuilder.AppendLine("colonia=" + ds.Tables["Cliente"].Rows[0]["colonia"].ToString());
            stringBuilder.AppendLine("localidad=" + ds.Tables["Cliente"].Rows[0]["localidad"].ToString());
            stringBuilder.AppendLine("referencia=" + ds.Tables["Cliente"].Rows[0]["referencia"].ToString());
            stringBuilder.AppendLine("municipio=" + ds.Tables["Cliente"].Rows[0]["municipio"].ToString());
            stringBuilder.AppendLine("estado=" + ds.Tables["Cliente"].Rows[0]["estado"].ToString());
            stringBuilder.AppendLine("pais=" + ds.Tables["Cliente"].Rows[0]["pais"].ToString());
            stringBuilder.AppendLine("codigoPostal=" + ds.Tables["Cliente"].Rows[0]["codigoPostal"].ToString());
            stringBuilder.AppendLine("tel=" + ds.Tables["Cliente"].Rows[0]["tel"].ToString());
            stringBuilder.AppendLine("email=" + ds.Tables["Cliente"].Rows[0]["email"].ToString());
            //stringBuilder.AppendLine("</Cliente>");
            stringBuilder.AppendLine("</Receptor>");

            if (ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString().Length > 0)
            {
                //stringBuilder.AppendLine("");
                stringBuilder.AppendLine("<ExpedidoEn>");
                stringBuilder.AppendLine("ex_calle=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString());
                stringBuilder.AppendLine("ex_noExterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noExterior"].ToString());
                stringBuilder.AppendLine("ex_noInterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noInterior"].ToString());
                stringBuilder.AppendLine("ex_colonia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_colonia"].ToString());
                stringBuilder.AppendLine("ex_localidad=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_localidad"].ToString());
                //stringBuilder.AppendLine("ex_referencia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_referencia"].ToString());
                stringBuilder.AppendLine("ex_municipio=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_municipio"].ToString());
                stringBuilder.AppendLine("ex_estado=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_estado"].ToString());
                stringBuilder.AppendLine("ex_pais=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_pais"].ToString());
                stringBuilder.AppendLine("ex_codigoPostal=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_codigoPostal"].ToString());
                stringBuilder.AppendLine("</ExpedidoEn>");
            }

            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Concepto>");



            foreach (DataRow e in ds.Tables["Concepto"].Rows)
            {
                if (i < 25)
                    Caracter = "0";
                else
                    Caracter = "";

                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ClaveProdServ=" + ds.Tables["Concepto"].Rows[i]["ClvProdServ"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_NoIdentificacion=" + ds.Tables["Concepto"].Rows[i]["NoIdentificacion"].ToString());                
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_cantidad=" + ds.Tables["Concepto"].Rows[i]["cantidad"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ClaveUnidad=" + ds.Tables["Concepto"].Rows[i]["ClaveUnidad"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_unidad=" + ds.Tables["Concepto"].Rows[i]["unidad"].ToString());                
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_descripcion=" + ds.Tables["Concepto"].Rows[i]["descripcion"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_valorUnitario=" + ds.Tables["Concepto"].Rows[i]["valorUnitario"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_importe=" + ds.Tables["Concepto"].Rows[i]["importe"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_Descuento=" + ds.Tables["Concepto"].Rows[i]["Descuento"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_factorIVA=" + ds.Tables["Concepto"].Rows[i]["factorIVA"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_cuentaPredial=" + ds.Tables["Concepto"].Rows[i]["cuentaPredial"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_Pedimento=" + ds.Tables["Concepto"].Rows[i]["Pedimento"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_fechaPedimento=" + ds.Tables["Concepto"].Rows[i]["fechaPedimento"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_aduana=" + ds.Tables["Concepto"].Rows[i]["aduana"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITBase=" + ds.Tables["Concepto"].Rows[i]["ITBaseIVA"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITImpuesto=" + ds.Tables["Concepto"].Rows[i]["ITImpuestoIVA"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITTipoFactor=" + ds.Tables["Concepto"].Rows[i]["ITTipoFactorIVA"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITTasaoCuota=" + ds.Tables["Concepto"].Rows[i]["ITTasaoCuotaIVA"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITImporte=" + ds.Tables["Concepto"].Rows[i]["ITImporteIVA"].ToString());


                String x = "";
                x= (ds.Tables["Concepto"].Rows[i]["ITImporteIEPS"].ToString());
                if ( x != "0.00" & x != "0.0000")
                {
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITBase2=" + ds.Tables["Concepto"].Rows[i]["ITBaseIEPS"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITImpuesto2=" + ds.Tables["Concepto"].Rows[i]["ITImpuestoIEPS"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITTipoFactor2=" + ds.Tables["Concepto"].Rows[i]["ITTipoFactorIEPS"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITTasaoCuota2=" + ds.Tables["Concepto"].Rows[i]["ITTasaoCuotaIEPS"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_ITImporte2=" + ds.Tables["Concepto"].Rows[i]["ITImporteIEPS"].ToString());                
                } 

                i += 1;
            }
            stringBuilder.AppendLine("</Concepto>");



            stringBuilder.AppendLine("<Impuestos>");
            stringBuilder.AppendLine("TotalImpuestosRetenidos=" + ds.Tables["Impuestos"].Rows[0]["TotalImpuestosRetenidos"].ToString());
            stringBuilder.AppendLine("TotalImpuestosTrasladados=" + ds.Tables["Impuestos"].Rows[0]["TotalImpuestosTrasladados"].ToString());

            stringBuilder.AppendLine("<trasladados>");

            String a = "";
            a = (ds.Tables["TrasladosIVA"].Rows[0]["ImpTrasladado1_ImporteIVA"].ToString());
            if (a != "0.00")
                {
                    stringBuilder.AppendLine("ImpTrasladado1_Impuesto=" + ds.Tables["TrasladosIVA"].Rows[0]["ImpTrasladado1_ImpuestoIVA"].ToString());
                    stringBuilder.AppendLine("ImpTrasladado1_TipoFactor=" + ds.Tables["TrasladosIVA"].Rows[0]["ImpTrasladado1_TipoFactorIVA"].ToString());
                    stringBuilder.AppendLine("ImpTrasladado1_TasaOCuota=" + ds.Tables["TrasladosIVA"].Rows[0]["ImpTrasladado1_TasaOCuotaIVA"].ToString());
                    stringBuilder.AppendLine("ImpTrasladado1_Importe=" + ds.Tables["TrasladosIVA"].Rows[0]["ImpTrasladado1_ImporteIVA"].ToString());
                }


            String b = "";
            b = (ds.Tables["TrasladosIEPS"].Rows[0]["ImpTrasladado1_ImporteIEPS"].ToString());
            if (b != "0.00")
            {
                stringBuilder.AppendLine("ImpTrasladado2_Impuesto=" + ds.Tables["TrasladosIEPS"].Rows[0]["ImpTrasladado1_ImpuestoIEPS"].ToString());
                stringBuilder.AppendLine("ImpTrasladado2_TipoFactor=" + ds.Tables["TrasladosIEPS"].Rows[0]["ImpTrasladado1_TipoFactorIEPS"].ToString());
                stringBuilder.AppendLine("ImpTrasladado2_TasaOCuota=" + ds.Tables["TrasladosIEPS"].Rows[0]["ImpTrasladado1_TasaOCuotaIEPS"].ToString());
                stringBuilder.AppendLine("ImpTrasladado2_Importe=" + ds.Tables["TrasladosIEPS"].Rows[0]["ImpTrasladado1_ImporteIEPS"].ToString());
            }
            stringBuilder.AppendLine("</trasladados>");
            stringBuilder.AppendLine("</Impuestos>");



            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Otros>");
            
            stringBuilder.AppendLine("cant_letra=" + ds.Tables["Otros"].Rows[0]["cant_letra"].ToString());
            stringBuilder.AppendLine("factoriva=" + ds.Tables["Otros"].Rows[0]["factoriva"].ToString());
            //stringBuilder.AppendLine("moneda=" + ds.Tables["Otros"].Rows[0]["moneda"].ToString());
            stringBuilder.AppendLine("tipoCambio=" + ds.Tables["Otros"].Rows[0]["tipoCambio"].ToString());
            stringBuilder.AppendLine("observaciones=" + ds.Tables["Otros"].Rows[0]["observaciones"].ToString());
            stringBuilder.AppendLine("tipoimpresion=" + ds.Tables["Otros"].Rows[0]["tipoimpresion"].ToString());
            stringBuilder.AppendLine("formato=" + ds.Tables["Otros"].Rows[0]["formato"].ToString());
            stringBuilder.AppendLine("expedicion=" + ds.Tables["Otros"].Rows[0]["expedicion"].ToString());
            stringBuilder.AppendLine("</Otros>");

            //stringBuilder.AppendLine("");
            //stringBuilder.AppendLine("<addenda>");
            //stringBuilder.AppendLine("addenda=" + ds.Tables["addenda"].Rows[0]["Extra"].ToString());
            //stringBuilder.AppendLine("</addenda>");

            //stringBuilder.AppendLine("");
            stringBuilder.AppendLine("</Documento>");
            //stringBuilder.AppendLine("</Factura>");
            return stringBuilder;

        }

        public StringBuilder CancelaCFD(DataSet ds)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("Outputmode=");
            stringBuilder.AppendLine("<Factura>");
            stringBuilder.AppendLine("serie=" + ds.Tables["Factura"].Rows[0]["serie"].ToString());
            stringBuilder.AppendLine("folio=" + ds.Tables["Factura"].Rows[0]["folio"].ToString());
            stringBuilder.AppendLine("email=" + ds.Tables["Factura"].Rows[0]["email"].ToString());
            stringBuilder.AppendLine("</Factura>");

            return stringBuilder;
        }

    }
}
